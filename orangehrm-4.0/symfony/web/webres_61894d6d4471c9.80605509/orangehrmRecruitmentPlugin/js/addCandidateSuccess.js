var nextId = 0;
var toDisable = new Array();
var item = 0;
var vacancyId;
$(document).ready(function() {
    
    vacancyId = $('#addCandidate_vacancy').val();
    if(candidateStatus != activeStatus) {
        $("#btnSave").attr('disabled', 'disabled');
    }

    $(".addText").live('click', function(){
       
        if($("#btnSave").attr('value') == lang_edit){
        
        }else{
            if((allowedVacancylist.length -1) > nextId){
                buildVacancyDrpDwn("", "show allowed vacancies", true);
                newId = /\d+(?:\.\d+)?/.exec(this.id);
                $("#removeButton"+newId).css("padding-left", "195px");
                $("#addButton"+newId).hide();
                if((allowedVacancylist.length -1) <= nextId){
                    $("#addButton"+(nextId-1)).hide();
                    $("#removeButton"+(nextId-1)).css("padding-left", "195px");
                }
            }
        }
    });

    $('.removeText').live('click', function(){

        result = /\d+(?:\.\d+)?/.exec(this.id);
        if(vacancyString.trim() != "" && result < vacancyList.length){
            if($("#btnSave").attr('value') == lang_edit){
            } else{
                $('#deleteConfirmation').modal();
            }
        }
        else{
            $('#jobDropDown'+result).remove();
            validate();
            $("#addButton"+($('.vacancyDrop').length-1)).show();
            $("#removeButton"+($('.vacancyDrop').length-1)).css("padding-left", "128px");
            if(result == $('.vacancyDrop').length-1){
                $("#addButton"+(nextId-1)).show();
                $("#removeButton"+(nextId-1)).css("padding-left", "128px");
            }
            nextId--;
        }
    });

    $('#btnSave').click(function() {
        var isExistedVacancyGoingToBeDeleted = 0;
        if($("#btnSave").attr('value') == lang_edit) {
            $('#addCandidateHeading').text(lang_editCandidateTitle);
            for(i=0; i < widgetList.length; i++) {
                $(widgetList[i]).removeAttr("disabled");
            }

            if(candidateVacancyStatus == "SHORTLISTED" && psiFileId != "")
                $('.actionDrpDown').removeAttr("disabled");

            $(".vacancyDrop").each(function(){
                if($.inArray($(this).attr('id'), toDisable) > -1){
                    $(this).attr('disabled', 'disabled');
                }
            });
            $('#radio').show();
            //put checked here
            $('#addCandidate_resumeUpdate_1').attr('checked', 'checked');
            $('#addCandidate_photoUpdate_1').attr('checked', 'checked');
            $('#addCandidate_graduatecertificateUpdate_1').attr('checked', 'checked');
            $('#addCandidate_transcriptUpdate_1').attr('checked', 'checked');
            $('#addCandidate_refletterUpdate_1').attr('checked', 'checked');
            $('#addCandidate_dstrfileUpdate_1').attr('checked', 'checked');
            $('#addCandidate_idicardfileUpdate_1').attr('checked', 'checked');
            $('#addCandidate_dhiperkesfileUpdate_1').attr('checked', 'checked');
            $('#addCandidate_aclsfileUpdate_1').attr('checked', 'checked');
            $('#addCandidate_atlsfileUpdate_1').attr('checked', 'checked');
            $('#addCandidate_nstrfileUpdate_1').attr('checked', 'checked');
            $('#addCandidate_ppnifileUpdate_1').attr('checked', 'checked');
            $('#addCandidate_nhiperkesfileUpdate_1').attr('checked', 'checked');
            $('#addCandidate_ppgdfileUpdate_1').attr('checked', 'checked');
            $('#addCandidate_psikotestUpdate_1').attr('checked', 'checked');
            $('#addCandidate_kontrakUpdate_1').attr('checked', 'checked');
            $("#btnSave").attr('value', lang_save);
            $("#btnBack").attr('value', lang_cancel);
            $('#fileUploadSection').show();
            if(candidateVacancyStatus == "EXTACTION")
                $('#addCandidate_psikotest').removeAttr("disabled");
        } else {
            
            if(isValidForm()) {
                
                $('#addCandidate_keyWords.inputFormatHint').val('');
                getVacancy();
                if(candidateId != "") {
                    if($('#addCandidate_vacancy').val() != vacancyId && vacancyId != "") {
                        $('#deleteConfirmationForSave').modal();
                    } else {
                        $('form#frmAddCandidate').submit();
                    }
                
                } else {
                    $('form#frmAddCandidate').submit();
                }
            }
        }

    });
   
    $("input.fileEditOptions").click(function () {

        if(attachment != "" && !$('#addCandidate_resumeUpdate_3').attr("checked")){
            $('#addCandidate_resume').val("");
        }
        if ($('#addCandidate_resumeUpdate_3').attr("checked")) {
            $('#addCandidate_resume').show();
            $('#fieldHelpBottomSpec_resumeUpdate').show();
        } else {
            $('#addCandidate_resume').hide();
            $('#fieldHelpBottomSpec_resumeUpdate').hide();
        }

        if(attachment != "" && !$('#addCandidate_photoUpdate_3').attr("checked")){
            $('#addCandidate_photo').val("");
        }
        if ($('#addCandidate_photoUpdate_3').attr("checked")) {
            $('#addCandidate_photo').show();
            $('#fieldHelpBottomSpec_photoUpdate').show();
        } else {
            $('#addCandidate_photo').hide();
            $('#fieldHelpBottomSpec_photoUpdate').hide();
        }

        if(attachment != "" && !$('#addCandidate_graduatecertificateUpdate_3').attr("checked")){
            $('#addCandidate_graduatecertificate').val("");
        }
        if ($('#addCandidate_graduatecertificateUpdate_3').attr("checked")) {
            $('#addCandidate_graduatecertificate').show();
            $('#fieldHelpBottomSpec_graduatecertificateUpdate').show();
        } else {
            $('#addCandidate_graduatecertificate').hide();
            $('#fieldHelpBottomSpec_graduatecertificateUpdate').hide();
        }

        if(attachment != "" && !$('#addCandidate_transcriptUpdate_3').attr("checked")){
            $('#addCandidate_transcript').val("");
        }
        if ($('#addCandidate_transcriptUpdate_3').attr("checked")) {
            $('#addCandidate_transcript').show();
            $('#fieldHelpBottomSpec_transcriptUpdate').show();
        } else {
            $('#addCandidate_transcript').hide();
            $('#fieldHelpBottomSpec_transcriptUpdate').hide();
        }

        if(attachment != "" && !$('#addCandidate_refletterUpdate_3').attr("checked")){
            $('#addCandidate_refletter').val("");
        }
        if ($('#addCandidate_refletterUpdate_3').attr("checked")) {
            $('#addCandidate_refletter').show();
            $('#fieldHelpBottomSpec_refletterUpdate').show();
        } else {
            $('#addCandidate_refletter').hide();
            $('#fieldHelpBottomSpec_refletterUpdate').hide();
        }

        if(attachment != "" && !$('#addCandidate_dstrfileUpdate_3').attr("checked")){
            $('#addCandidate_dstrfile').val("");
        }
        if ($('#addCandidate_dstrfileUpdate_3').attr("checked")) {
            $('#addCandidate_dstrfile').show();
            $('#fieldHelpBottomSpec_dstrfileUpdate').show();
        } else {
            $('#addCandidate_dstrfile').hide();
            $('#fieldHelpBottomSpec_dstrfileUpdate').hide();
        }

        if(attachment != "" && !$('#addCandidate_idicardfileUpdate_3').attr("checked")){
            $('#addCandidate_idicardfile').val("");
        }
        if ($('#addCandidate_idicardfileUpdate_3').attr("checked")) {
            $('#addCandidate_idicardfile').show();
            $('#fieldHelpBottomSpec_idicardfileUpdate').show();
        } else {
            $('#addCandidate_idicardfile').hide();
            $('#fieldHelpBottomSpec_idicardfileUpdate').hide();
        }

        if(attachment != "" && !$('#addCandidate_dhiperkesfileUpdate_3').attr("checked")){
            $('#addCandidate_dhiperkesfile').val("");
        }
        if ($('#addCandidate_dhiperkesfileUpdate_3').attr("checked")) {
            $('#addCandidate_dhiperkesfile').show();
            $('#fieldHelpBottomSpec_dhiperkesfileUpdate').show();
        } else {
            $('#addCandidate_dhiperkesfile').hide();
            $('#fieldHelpBottomSpec_dhiperkesfileUpdate').hide();
        }

        if(attachment != "" && !$('#addCandidate_aclsfileUpdate_3').attr("checked")){
            $('#addCandidate_aclsfile').val("");
        }
        if ($('#addCandidate_aclsfileUpdate_3').attr("checked")) {
            $('#addCandidate_aclsfile').show();
            $('#fieldHelpBottomSpec_aclsfileUpdate').show();
        } else {
            $('#addCandidate_aclsfile').hide();
            $('#fieldHelpBottomSpec_aclsfileUpdate').hide();
        }

        if(attachment != "" && !$('#addCandidate_atlsfileUpdate_3').attr("checked")){
            $('#addCandidate_atlsfile').val("");
        }
        if ($('#addCandidate_atlsfileUpdate_3').attr("checked")) {
            $('#addCandidate_atlsfile').show();
            $('#fieldHelpBottomSpec_atlsfileUpdate').show();
        } else {
            $('#addCandidate_atlsfile').hide();
            $('#fieldHelpBottomSpec_atlsfileUpdate').hide();
        }

        if(attachment != "" && !$('#addCandidate_nstrfileUpdate_3').attr("checked")){
            $('#addCandidate_nstrfile').val("");
        }
        if ($('#addCandidate_nstrfileUpdate_3').attr("checked")) {
            $('#addCandidate_nstrfile').show();
            $('#fieldHelpBottomSpec_nstrfileUpdate').show();
        } else {
            $('#addCandidate_nstrfile').hide();
            $('#fieldHelpBottomSpec_nstrfileUpdate').hide();
        }

        if(attachment != "" && !$('#addCandidate_ppnifileUpdate_3').attr("checked")){
            $('#addCandidate_ppnifile').val("");
        }
        if ($('#addCandidate_ppnifileUpdate_3').attr("checked")) {
            $('#addCandidate_ppnifile').show();
            $('#fieldHelpBottomSpec_ppnifileUpdate').show();
        } else {
            $('#addCandidate_ppnifile').hide();
            $('#fieldHelpBottomSpec_ppnifileUpdate').hide();
        }

        if(attachment != "" && !$('#addCandidate_nhiperkesfileUpdate_3').attr("checked")){
            $('#addCandidate_nhiperkesfile').val("");
        }
        if ($('#addCandidate_nhiperkesfileUpdate_3').attr("checked")) {
            $('#addCandidate_nhiperkesfile').show();
            $('#fieldHelpBottomSpec_nhiperkesfileUpdate').show();
        } else {
            $('#addCandidate_nhiperkesfile').hide();
            $('#fieldHelpBottomSpec_nhiperkesfileUpdate').hide();
        }

        if(attachment != "" && !$('#addCandidate_ppgdfileUpdate_3').attr("checked")){
            $('#addCandidate_ppgdfile').val("");
        }
        if ($('#addCandidate_ppgdfileUpdate_3').attr("checked")) {
            $('#addCandidate_ppgdfile').show();
            $('#fieldHelpBottomSpec_ppgdfileUpdate').show();
        } else {
            $('#addCandidate_ppgdfile').hide();
            $('#fieldHelpBottomSpec_ppgdfileUpdate').hide();
        }

        if(attachment != "" && !$('#addCandidate_psikotestUpdate_3').attr("checked")){
            $('#addCandidate_psikotest').val("");
        }
        if ($('#addCandidate_psikotestUpdate_3').attr("checked")) {
            $('#addCandidate_psikotest').show();
            $('#fieldHelpBottomSpec_psikotestUpdate').show();
        } else {
            $('#addCandidate_psikotest').hide();
            $('#fieldHelpBottomSpec_psikotestUpdate').hide();
        }

        if(attachment != "" && !$('#addCandidate_kontrakUpdate_3').attr("checked")){
            $('#addCandidate_kontrak').val("");
        }
        if ($('#addCandidate_kontrakUpdate_3').attr("checked")) {
            $('#addCandidate_kontrak').show();
            $('#fieldHelpBottomSpec_kontrakUpdate').show();
        } else {
            $('#addCandidate_kontrak').hide();
            $('#fieldHelpBottomSpec_kontrakUpdate').hide();
        }
    });

    if ($("#addCandidate_keyWords").val() == '') {
        $("#addCandidate_keyWords").val(lang_commaSeparated)
        .addClass("inputFormatHint");
    }

    $("#addCandidate_keyWords").one('focus', function() {

        if ($(this).hasClass("inputFormatHint")) {
            $(this).val("");
            $(this).removeClass("inputFormatHint");
        }
    });
    
    if(candidateId != ""){
        var widgetList = new Array(
            '.formInputText',
            '.contactNo',
            '.calendar',
            '.editable',
            '.editabletextarea',
            '.duplexBox',
            '#addCandidate_keyWords',
            '#addCandidate_resume',
            '.vacancyDrop',
            '#addCandidate_vacancy',
            '#addCandidate_appliedDate',
            '#frmDateBtn',
            '#addCandidate_comment',
            '#addCandidate_resumeUpdate_1',
            '#addCandidate_resumeUpdate_2',
            '#addCandidate_resumeUpdate_3',
            '#addCandidate_appliedDate_Button',
            '#addCandidate_photo',
            '#addCandidate_photoUpdate_1',
            '#addCandidate_photoUpdate_3',
            '#addCandidate_graduatecertificateUpdate_1',
            '#addCandidate_graduatecertificateUpdate_3',
            '#addCandidate_transcriptUpdate_1',
            '#addCandidate_transcriptUpdate_3',
            '#addCandidate_refletterUpdate_1',
            '#addCandidate_refletterUpdate_3',
            '#addCandidate_dstrfileUpdate_1',
            '#addCandidate_dstrfileUpdate_3',
            '#addCandidate_idicardfileUpdate_1',
            '#addCandidate_idicardfileUpdate_3',
            '#addCandidate_dhiperkesfileUpdate_1',
            '#addCandidate_dhiperkesfileUpdate_3',
            '#addCandidate_aclsfileUpdate_1',
            '#addCandidate_aclsfileUpdate_3',
            '#addCandidate_atlsfileUpdate_1',
            '#addCandidate_atlsfileUpdate_3',
            '#addCandidate_nstrfileUpdate_1',
            '#addCandidate_nstrfileUpdate_3',
            '#addCandidate_ppnifileUpdate_1',
            '#addCandidate_ppnifileUpdate_3',
            '#addCandidate_nhiperkesfileUpdate_1',
            '#addCandidate_nhiperkesfileUpdate_3',
            '#addCandidate_ppgdfileUpdate_1',
            '#addCandidate_ppgdfileUpdate_3',
            '#addCandidate_psikotestUpdate_1',
            '#addCandidate_psikotestUpdate_3',
            '#addCandidate_kontrakUpdate_1',
            '#addCandidate_kontrakUpdate_3'
        );
        for(i=0; i < widgetList.length; i++) {
            $(widgetList[i]).attr("disabled", "disabled");
        }

        if (candidateStatus != activeStatus) {
            $('.actionDrpDown').attr('disabled', 'disabled');
        }

        if(candidateVacancyStatus == "SHORTLISTED" && psiFileId == ""){
            $('.actionDrpDown').attr('disabled', 'disabled');
        }

        $('#fileUploadSection').hide();
        //must play with this!!!
        //$('#radio').hide();
        $("#btnSave").attr('value', lang_edit);
    } else {
    }

    $('.actionDrpDown').change(function(){
        var id = $(this).attr('id');
        var idList = id.split("_")
        var candidateVacancyId = idList[1];
        var selectedAction = $(this).val();
        var url = changeStatusUrl;
        if(selectedAction == interviewAction || selectedAction == interviewAction2){
            url = interviewUrl;
        }
        if(selectedAction == removeAction){
            url = interviewUrl;
        }
        window.location.replace(url+'?candidateVacancyId='+candidateVacancyId+'&selectedAction='+selectedAction);
    });

    $('#btnBack').click(function(){
        if($("#btnBack").attr('value') == lang_cancel){
            window.location.replace(cancelBtnUrl+'?id='+candidateId);
        }else{
            window.location.replace(backBtnUrl+'?candidateId='+candidateId);
        }
    });

    $('#addCandidate_vacancy').change(function(){
        $('#actionPane').hide();
        if( $('#addCandidate_vacancy').val() == vacancyId){
            $('#actionPane').show();
        }
    });

    $('#dialogSaveButton').click(function() {
        $('form#frmAddCandidate').submit();
    });
    
    $('#dialogCancelButton').click(function() {
        $('#addCandidate_vacancy').val(vacancyId);
        $('#actionPane').show();
    });

    $('.vacancyDrop').change(function(){
        toRemove = /\d+(?:\.\d+)?/.exec(this.id)
        $("#"+toRemove).hide();
    });
});

function buildVacancyDrpDwn(vacancyId, mode, removeBtn) {
    if(nextId < 5){
        var newjobDropDown = $(document.createElement('div')).attr("id", 'jobDropDown' + nextId);
        $('#jobDropDown' + nextId).addClass('jobDropDown');
        htmlTxt =  '<label><?php echo __(Job Vacancy); ?></label>' +
            '<select  id="jobDropDown' + nextId +'"'+' onchange="validate()"'+' class="vacancyDrop"'+'>'+buildVacancyList(vacancyId, mode)+'</select>'+
            '<span '+'class="addText"'+ 'id="addButton'+nextId+'">'+'Add another'+'</span>'
        if(removeBtn){
            htmlTxt += '<span '+'class="removeText"'+ 'id="removeButton'+nextId+'">'+lang_remove+'</span>'
        }else{
            toDisable[item] = "jobDropDown"+nextId;
        }
        newjobDropDown.after().html(htmlTxt);
        nextId++;
        newjobDropDown.appendTo("#textBoxesGroup");
    }

}

function buildVacancyList(vacancyId, mode){

    var listArray = new Array();
    if(mode == "show all vacancies"){
        listArray = list;
    }
    if(mode == "show allowed vacancies"){
        listArray = allowedVacancylist;
    }
    if(mode == "show with closed vacancies"){
        listArray = allowedVacancylistWithClosedVacancies;
    }

    var numOptions = listArray.length;
    var optionHtml = "";
    for (var i = 0; i < numOptions; i++) {

        if(listArray[i].id == vacancyId){
            optionHtml += '<option selected="selected" value="' + listArray[i].id + '">' + listArray[i].name + '</option>';
        }else{
            optionHtml += '<option value="' + listArray[i].id + '">' + listArray[i].name + '</option>';
        }
    }
    return optionHtml;
}

function validate(){
    var flag = validateVacancy();
    if(!flag) {
        $('#btnSave').attr('disabled', 'disabled');
        $('#vacancyError').attr('class', "vacancyErr");
    }
    else{
        $('#btnSave').removeAttr('disabled');
    }

}

function getVacancy() {

    var strID = "";
    
    $('.vacancyDrop').each(function() {
        if(!isEmpty($(this).val())) {
            strID = strID + $(this).val() + "_";
        }
    });
    
    $('#addCandidate_vacancyList').val(strID);

}

function validateVacancy(){

    var flag = true;
    $(".messageBalloon_success").remove();
    $(".messageBalloon_failure").remove()
    $('#vacancyError').removeAttr('class');
    $('#vacancyError').html("");
    
    var errorStyle = "background-color:#FFDFDF;";
    var normalStyle = "background-color:#FFFFFF;";
    var vacancyArray = new Array();
    var errorElements = new Array();
    var index = 0;
    var num = 0;

    $('.vacancyDrop').each(function(){
        element = $(this);
        $(element).attr('style', normalStyle);
        vacancyArray[index] = $(element);
        index++;
    });

    for(var i=0; i<vacancyArray.length; i++){
        var currentElement = vacancyArray[i];
        for(var j=1+i; j<vacancyArray.length; j++){
            if(currentElement.val()!=""){
                if(currentElement.val() == vacancyArray[j].val() ){
                    errorElements[num] = currentElement;
                    errorElements[++num] = vacancyArray[j];
                    num++;
                    $('#vacancyError').html(lang_identical_rows);
                    flag = false;
                }
            }
        }
        for(var k=0; k<errorElements.length; k++){
            errorElements[k].attr('style', errorStyle);
        }
    }
    return flag;
}

function stringToDate(_date,_format)
{
    var _delimiter = _format.match(/\W/g)[0];
    var formatLowerCase=_format.toLowerCase();
    var formatItems=formatLowerCase.split(_delimiter);
    var dateItems=_date.split(_delimiter);
    var monthIndex=formatItems.indexOf("mm");
    var dayIndex=formatItems.indexOf("dd");
    var yearIndex=formatItems.indexOf("yy");
    var month=parseInt(dateItems[monthIndex]);
    month-=1;
    //alert(dateItems + formatItems + yearIndex + dateItems[yearIndex]+','+dateItems[monthIndex]+','+dateItems[dayIndex]);

    var formatedDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
    return formatedDate;
}

function isValidForm(){

    $.validator.addMethod('date_range_comp', function(value, element, params) {

    var valid = false;
    var fromDate = $.trim(value);
    var toDate = $.trim(currentDate);
    var format = datepickerDateFormat;

    if(fromDate == format || toDate == format || fromDate == "" || toDate =="") {
        valid = true;
    }else{
        var parsedFromDate = $.datepicker.parseDate(format, fromDate);
        var parsedToDate = $.datepicker.parseDate(format, toDate);
        if(parsedFromDate <= parsedToDate){
            valid = true;
        }
    }
    return valid;
    });

    jQuery.validator.addMethod("greaterThan",
        function(value, element, param){
            var dateValue = stringToDate(value, dateFormat);
            //alert(dateValue + value + format);

            if(!value) {
                return true;
            }

            if (!/Invalid|NaN/.test(dateValue)) {
                return dateValue > param;
            }
            //return isNaN(value) && isNaN($(params).val())
            //    || (Number(value) > Number($(params).val()));
        }, ""
    );

    var todayDate = new Date();

    var validator = $("#frmAddCandidate").validate({

        rules: {
            'addCandidate[doctor_strexpire]' : {
                greaterThan:todayDate
            },

            'addCandidate[nurse_strexpire]' : {
                greaterThan:todayDate
            },

            'addCandidate[firstName]' : {
                maxlength:30
            },

            'addCandidate[middleName]' : {
                required:true,
                maxlength:30
            },

            'addCandidate[lastName]' : {
                required:true,
                maxlength:30
            },

            'addCandidate[gender]' : {
                required:true
            },

            'addCandidate[birthplace]' : {
                required:true,
                maxlength:255
            },

            'addCandidate[dob]' : {
                required:true
            },

            'addCandidate[currentaddr]' : {
                required:true,
                maxlength:255
            },

            'addCandidate[currentcity]' : {
                required:true
            },

            'addCandidate[currentkecamatan]' : {
                required:true
            },

            'addCandidate[currentkelurahan]' : {
                required:true
            },

            'addCandidate[currentprovince]' : {
                required:true
            },

            'addCandidate[currentzip]' : {
                required:true,
                maxlength:10
            },

            'addCandidate[permanentaddr]' : {
                required:true,
                maxlength:255
            },

            'addCandidate[permanentcity]' : {
                required:true
            },

            'addCandidate[permanentkecamatan]' : {
                required:true
            },

            'addCandidate[permanentkelurahan]' : {
                required:true
            },

            'addCandidate[permanentprovince]' : {
                required:true
            },

            'addCandidate[permanentzip]' : {
                required:true,
                maxlength:10
            },

            'addCandidate[religion]' : {
                required:true
            },

            'addCandidate[marital]' : {
                required:true
            },

            'addCandidate[phone]' : {
                required:true,
                maxlength:45
            },

            'addCandidate[mobile]' : {
                required:true,
                maxlength:45
            },

            'addCandidate[ektp]' : {
                required:true,
                maxlength:45
            },

            'addCandidate[email]' : {
                required:true,
                email:true,
                maxlength:30
            },

            'addCandidate[contactNo]': {
                phone: true,
                maxlength:30
            },

            'addCandidate[photo]' : {
                required:true
            },

            'addCandidate[resume]' : {
                required:true
            },

            'addCandidate[keyWords]': {
                maxlength:250
            },

            'addCandidate[comment]': {
                maxlength:250
            },

            'addCandidate[wantsalary]': {
                required:true,
                number:true
            },

            'addCandidate[vacancy]': {
                required:true
            },

            'addCandidate[bloodtype]': {
                required: true
            },
            'addCandidate[heght]': {
                required: true,
                maxlength:5
            },
            'addCandidate[weight]': {
                required: true,
                maxlength:5
            },
            'addCandidate[npwp]': {
                required: true,
                maxlength:45
            },
            'addCandidate[bpjsnaker]': {
                required: true,
                maxlength:45
            },
            'addCandidate[bpjskes]': {
                required: true,
                maxlength:45
            },
            'addCandidate[faskes]': {
                required: true,
                maxlength:41000
            },
            'addCandidate[kk]': {
                required: true,
                maxlength:45
            },
            'addCandidate[familyhead]': {
                required: true
            },

            'addCandidate[emergencyname]': {
                required: true
            },

            'addCandidate[emergencyaddr]': {
                required: true
            },

            'addCandidate[emergencyprovince]': {
                required: true
            },

            'addCandidate[emergencycity]': {
                required: true
            },

            'addCandidate[emergencykecamatan]': {
                required: true
            },

            'addCandidate[emergencykelurahan]': {
                required: true
            },

            'addCandidate[emergencyzip]': {
                required: true
            },

            'addCandidate[emergencyrelation]': {
                required: true
            },

            'addCandidate[emergencyphone]': {
                required: true
            },

            'addCandidate[appliedDate]' : {
                valid_date: function() {
                    return {
                        format:datepickerDateFormat,
                        required:false,
                        displayFormat:displayDateFormat

                    }
                },
                    date_range_comp: true
                }
            },
        messages: {
            'addCandidate[doctor_strexpire]' : {
                greaterThan: lang_Strexpire
            },

            'addCandidate[nurse_strexpire]' : {
                greaterThan: lang_Strexpire
            },

            'addCandidate[firstName]' : {
                required: lang_firstNameRequired,
                maxlength: lang_tooLargeInput
            },

            'addCandidate[middleName]' : {
                maxlength: lang_tooLargeInput
            },
            'addCandidate[lastName]' : {
                required: lang_lastNameRequired,
                maxlength: lang_tooLargeInput
            },

            'addCandidate[gender]' : {
                required: lang_genderRequired
            },

            'addCandidate[birthplace]' : {
                required: lang_birthPlaceRequired,
                maxlength: lang_tooLargeInput
            },

            'addCandidate[dob]' : {
                required: lang_dobRequired
            },

            'addCandidate[currentaddr]' : {
                required: lang_currentAddrRequired,
                maxlength: lang_tooLargeInput
            },

            'addCandidate[currentcity]' : {
                required: lang_currentCityRequired
            },

            'addCandidate[currentkecamatan]' : {
                required: lang_currentCityRequired
            },

            'addCandidate[currentkelurahan]' : {
                required: lang_currentCityRequired
            },

            'addCandidate[currentprovince]' : {
                required: lang_currentProvinceRequired
            },

            'addCandidate[currentzip]' : {
                required: lang_currentZipRequired,
                maxlength: lang_tooLargeInput
            },

            'addCandidate[permanentaddr]' : {
                required: lang_permanentAddrRequired,
                maxlength: lang_tooLargeInput
            },

            'addCandidate[permanentcity]' : {
                required: lang_permanentCityRequired
            },

            'addCandidate[permanentkecamatan]' : {
                required: lang_permanentCityRequired
            },

            'addCandidate[permanentkelurahan]' : {
                required: lang_permanentCityRequired
            },

            'addCandidate[permanentprovince]' : {
                required: lang_permanentProvinceRequired
            },

            'addCandidate[permanentzip]' : {
                required: lang_permanentZipRequired,
                maxlength: lang_tooLargeInput
            },

            'addCandidate[religion]' : {
                required: lang_religionRequired
            },

            'addCandidate[marital]' : {
                required: lang_maritalRequired
            },

            'addCandidate[phone]' : {
                required: lang_phoneRequired,
                maxlength: lang_tooLargeInput
            },

            'addCandidate[mobile]' : {
                required: lang_mobileRequired,
                maxlength: lang_tooLargeInput
            },

            'addCandidate[ektp]' : {
                required: lang_ektpRequired,
                maxlength: lang_tooLargeInput
            },

            'addCandidate[contactNo]': {
                phone: lang_validPhoneNo,
                maxlength:lang_tooLargeInput
            },

            'addCandidate[email]' : {
                required: lang_emailRequired,
                email: lang_validEmail,
                maxlength: lang_tooLargeInput
            },

            'addCandidate[photo]' : {
                required:lang_resumeRequired
            },

            'addCandidate[resume]' : {
                required:lang_resumeRequired
            },

            'addCandidate[keyWords]': {
                maxlength:lang_noMoreThan250
            },
            'addCandidate[comment]' :{
                maxlength:lang_noMoreThan250
            },

            'addCandidate[wantsalary]': {
                required:lang_WantsalaryRequired,
                number:lang_WantsalaryNumber
            },

            'addCandidate[vacancy]': {
                required: lang_vacancyRequired
            },

            'addCandidate[bloodtype]': {
                required: lang_bloodTypeRequired
            },
            'addCandidate[heght]': {
                required: lang_heightRequired
            },
            'addCandidate[weight]': {
                required: lang_weightRequired
            },
            'addCandidate[npwp]': {
                required: lang_npwpRequired
            },
            'addCandidate[bpjsnaker]': {
                required: lang_bpjsNakerRequired
            },
            'addCandidate[bpjskes]': {
                required: lang_bpjsKesRequired
            },
            'addCandidate[faskes]': {
                required: lang_faskesRequired
            },
            'addCandidate[kk]': {
                required: lang_kkRequired
            },
            'addCandidate[familyhead]': {
                required: lang_familyHeadRequired
            },

            'addCandidate[emergencyname]': {
                required: lang_EmergencyRequired
            },

            'addCandidate[emergencyaddr]': {
                required: lang_EmergencyRequired
            },

            'addCandidate[emergencyprovince]': {
                required: lang_EmergencyRequired
            },

            'addCandidate[emergencycity]': {
                required: lang_EmergencyRequired
            },

            'addCandidate[emergencykecamatan]': {
                required: lang_EmergencyRequired
            },

            'addCandidate[emergencykelurahan]': {
                required: lang_EmergencyRequired
            },

            'addCandidate[emergencyzip]': {
                required: lang_EmergencyRequired
            },

            'addCandidate[emergencyrelation]': {
                required: lang_EmergencyRequired
            },

            'addCandidate[emergencyphone]': {
                required: lang_EmergencyRequired
            },

            'addCandidate[appliedDate]' : {
                valid_date: lang_validDateMsg,
                date_range_comp:lang_dateValidation
            }

        }

    });
    return true;
}
