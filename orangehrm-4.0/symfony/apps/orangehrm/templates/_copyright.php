<?php 
$version = '1.0';
$prodName = 'CrossHRMS';
$copyrightYear = date('Y');

?>
<?php echo $prodName . ' ' . $version;?><br/>
&copy; 2017 - <?php echo $copyrightYear;?> <a href="http://www.uxspecialty.com" target="_blank">UX Specialty, Inc</a>. All rights reserved.
