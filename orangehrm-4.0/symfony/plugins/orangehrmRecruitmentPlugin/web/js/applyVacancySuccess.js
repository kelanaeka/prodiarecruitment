$(document).ready(function() {
	$("#uploaded").hide();
    if(candidateId > 0) {
        $(".formInputText").attr('disabled', 'disabled');
        $(".formInput").attr('disabled', 'disabled');
        $(".contactNo").attr('disabled', 'disabled');
        $(".keyWords").attr('disabled', 'disabled');
        $("#cvHelp").hide();
        $("#uploaded").show();
        $("#btnSave").hide();
    }	
	var isCollapse = false;
	$("#txtArea").attr('disabled', 'disabled');
	$("#txtArea").hide();

	$('#extend').click(function(){
		if(!isCollapse){
			$("#txtArea").show();
			isCollapse = true;
			$('#extend').text('[-]');
		} else {
			$("#txtArea").hide();
			isCollapse = false;
			$('#extend').text('[+]');
		}
	});

    var isEduCollapse = false;
    $("#education").attr('disabled', 'disabled');
    $("#education").hide();

    $('#eduExtend').click(function(){
        if(!isEduCollapse){
            $("#education").show();
            isEduCollapse = true;
            $('#eduExtend').text('[-]');
        } else {
            $("#education").hide();
            isEduCollapse = false;
            $('#eduExtend').text('[+]');
        }
    });

    var isWeCollapse = false;
    $("#we").attr('disabled', 'disabled');
    $("#we").hide();

    $('#weExtend').click(function(){
        if(!isWeCollapse){
            $("#we").show();
            isWeCollapse = true;
            $('#weExtend').text(' [-]');
        } else {
            $("#we").hide();
            isWeCollapse = false;
            $('#weExtend').text(' [+]');
        }
    });

    var isDocCollapse = false;
    $("#doctor").attr('disabled', 'disabled');
    $("#doctor").hide();

    $('#docExtend').click(function(){
        if(!isDocCollapse){
            $("#doctor").show();
            isDocCollapse = true;
            $('#docExtend').text(' [-]');
        } else {
            $("#doctor").hide();
            isDocCollapse = false;
            $('#docExtend').text(' [+]');
        }
    });

    var isNurseCollapse = false;
    $("#nurse").attr('disabled', 'disabled');
    $("#nurse").hide();

    $('#nurseExtend').click(function(){
        if(!isNurseCollapse){
            $("#nurse").show();
            isNurseCollapse = true;
            $('#nurseExtend').text(' [-]');
        } else {
            $("#nurse").hide();
            isNurseCollapse = false;
            $('#nurseExtend').text(' [+]');
        }
    });

    $('#btnSave').click(function() {
           
		if(isValidForm()){ 
			$('#addCandidate_vacancyList').val(vacancyId);
			$('#addCandidate_keyWords.inputFormatHint').val('');
			$('form#frmAddCandidate').attr({
				action:linkForApplyVacancy+"?id="+vacancyId
			});
			$('form#frmAddCandidate').submit();
		}
	});
        

    $('#backLink').click(function(){
        window.location.replace(linkForViewJobs);
    });
	if ($("#addCandidate_keyWords").val() == '') {
		$("#addCandidate_keyWords").val(lang_commaSeparated).addClass("inputFormatHint");
	}

	$("#addCandidate_keyWords").one('focus', function() {

		if ($(this).hasClass("inputFormatHint")) {
			$(this).val("");
			$(this).removeClass("inputFormatHint");
		}
	});

	
});

function stringToDate(_date,_format)
{
    var _delimiter = _format.match(/\W/g)[0];
    var formatLowerCase=_format.toLowerCase();
    var formatItems=formatLowerCase.split(_delimiter);
    var dateItems=_date.split(_delimiter);
    var monthIndex=formatItems.indexOf("m");
    var dayIndex=formatItems.indexOf("d");
    var yearIndex=formatItems.indexOf("y");
    var month=parseInt(dateItems[monthIndex]);
    month-=1;
    //alert(dateItems[yearIndex]+','+dateItems[monthIndex]+','+dateItems[dayIndex]);

    var formatedDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
    return formatedDate;
}


function isValidForm(){

    jQuery.validator.addMethod("greaterThan",
        function(value, element, param){
            var dateValue = stringToDate(value, dateFormat);

            if (!/Invalid|NaN/.test(dateValue)) {
                return dateValue > param;
            }
            //return isNaN(value) && isNaN($(params).val())
            //    || (Number(value) > Number($(params).val()));
        }, ""
    );

    var todayDate = new Date();

    var validator = $("#frmAddCandidate").validate({

		rules: {
            'addCandidate[doctor_strexpire]' : {
                greaterThan:todayDate
            },

			'addCandidate[firstName]' : {
				maxlength:30
			},

			'addCandidate[middleName]' : {
				required:true,
				maxlength:30
			},

			'addCandidate[lastName]' : {
				required:true,
				maxlength:30
			},

            'addCandidate[gender]' : {
                required:true
            },

            'addCandidate[birthplace]' : {
                required:true,
                maxlength:255
            },

            'addCandidate[dob]' : {
                required:true
            },

            'addCandidate[currentaddr]' : {
                required:true,
                maxlength:255
            },

            'addCandidate[currentcity]' : {
                required:true
            },

            'addCandidate[currentkecamatan]' : {
                required:true
            },

            'addCandidate[currentkelurahan]' : {
                required:true
            },

            'addCandidate[currentprovince]' : {
                required:true
            },

            'addCandidate[currentzip]' : {
                required:true,
                maxlength:10
            },

            'addCandidate[permanentaddr]' : {
                required:true,
                maxlength:255
            },

            'addCandidate[permanentcity]' : {
                required:true
            },

            'addCandidate[permanentkecamatan]' : {
                required:true
            },

            'addCandidate[permanentkelurahan]' : {
                required:true
            },

            'addCandidate[permanentprovince]' : {
                required:true
            },

            'addCandidate[permanentzip]' : {
                required:true,
                maxlength:10
            },

            'addCandidate[religion]' : {
                required:true
            },

            'addCandidate[marital]' : {
                required:true
            },

            'addCandidate[phone]' : {
                required:true,
                maxlength:45
            },

            'addCandidate[mobile]' : {
                required:true,
                maxlength:45
            },

            'addCandidate[ektp]' : {
                required:true,
                maxlength:45
            },

            'addCandidate[email]' : {
				required:true,
				email:true,
				maxlength:100

			},

			'addCandidate[contactNo]': {
				phone: true,
				maxlength:30
			},

            'addCandidate[photo]' : {
                required:true
            },

			'addCandidate[resume]' : {
				required:true
			},

			'addCandidate[keyWords]': {
				maxlength:250
			},

            'addCandidate[wantsalary]': {
			    required:true,
                number:true
            }
		},
		messages: {
            'addCandidate[doctor_strexpire]' : {
                greaterThan: lang_Strexpire
            },

			'addCandidate[firstName]' : {
				required: lang_firstNameRequired,
				maxlength: lang_tooLargeInput
			},

			'addCandidate[middleName]' : {
				required: lang_middleNameRequired,
				maxlength: lang_tooLargeInput
			},

			'addCandidate[lastName]' : {
				required: lang_lastNameRequired,
				maxlength: lang_tooLargeInput
			},

            'addCandidate[gender]' : {
                required: lang_genderRequired
            },

            'addCandidate[birthplace]' : {
                required: lang_birthPlaceRequired,
                maxlength: lang_tooLargeInput
            },

            'addCandidate[dob]' : {
                required: lang_dobRequired
            },

            'addCandidate[currentaddr]' : {
                required: lang_currentAddrRequired,
                maxlength: lang_tooLargeInput
            },

            'addCandidate[currentcity]' : {
                required: lang_currentCityRequired
            },

            'addCandidate[currentkecamatan]' : {
                required: lang_currentCityRequired
            },

            'addCandidate[currentkelurahan]' : {
                required: lang_currentCityRequired
            },

            'addCandidate[currentprovince]' : {
                required: lang_currentProvinceRequired
            },

            'addCandidate[currentzip]' : {
                required: lang_currentZipRequired,
                maxlength: lang_tooLargeInput
            },

            'addCandidate[permanentaddr]' : {
                required: lang_permanentAddrRequired,
                maxlength: lang_tooLargeInput
            },

            'addCandidate[permanentcity]' : {
                required: lang_permanentCityRequired
            },

            'addCandidate[permanentkecamatan]' : {
                required: lang_permanentCityRequired
            },

            'addCandidate[permanentkelurahan]' : {
                required: lang_permanentCityRequired
            },

            'addCandidate[permanentprovince]' : {
                required: lang_permanentProvinceRequired
            },

            'addCandidate[permanentzip]' : {
                required: lang_permanentZipRequired,
                maxlength: lang_tooLargeInput
            },

            'addCandidate[religion]' : {
                required: lang_religionRequired
            },

            'addCandidate[marital]' : {
                required: lang_maritalRequired
            },

            'addCandidate[phone]' : {
                required: lang_phoneRequired,
                maxlength: lang_tooLargeInput
            },

            'addCandidate[mobile]' : {
                required: lang_mobileRequired,
                maxlength: lang_tooLargeInput
            },

            'addCandidate[ektp]' : {
                required: lang_ektpRequired,
                maxlength: lang_tooLargeInput
            },

            'addCandidate[email]' : {
				required: lang_emailRequired,
				email: lang_validEmail,
				maxlength: lang_tooLargeInput
			},
            
            'addCandidate[contactNo]': {
				phone: lang_validPhoneNo,
				maxlength:lang_tooLargeInput
			},

            'addCandidate[photo]' : {
                required:lang_resumeRequired
            },

			'addCandidate[resume]' : {
				required:lang_resumeRequired
			},

			'addCandidate[keyWords]': {
				maxlength:lang_noMoreThan250
			},

            'addCandidate[wantsalary]': {
			    required:lang_WantsalaryRequired,
                number:lang_WantsalaryNumber
            }
		}

	});
	return true;
}