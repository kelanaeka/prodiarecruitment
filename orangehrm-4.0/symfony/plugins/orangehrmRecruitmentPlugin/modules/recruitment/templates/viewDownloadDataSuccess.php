
<?php
/**
 * Created by PhpStorm.
 * User: kelanaeka
 * Date: 5/6/2020
 * Time: 8:17 PM
 */
?>

<table border="1">
    <tr>
        <th>NO.</th>
        <th>NAME</th>
        <th>FRONT TITLE</th>
        <th>END TITLE</th>
        <th>GENDER</th>
        <th>BIRTH PLACE</th>
        <th>DATE OF BIRTH</th>
        <th>CURRENT ADDRESS</th>
        <th>CURRENT PROVINCE</th>
        <th>CURRENT CITY</th>
        <th>CURRENT KECAMATAN</th>
        <th>CURRENT KELURAHAN</th>
        <th>CURRENT ZIPCODE</th>
        <th>PERMANENT ADDRESS</th>
        <th>PERMANENT PROVINCE</th>
        <th>PERMANENT CITY</th>
        <th>PERMANENT KECAMATAN</th>
        <th>PERMANENT KELURAHAN</th>
        <th>PERMANENT ZIPCODE</th>
        <th>RELIGION</th>
        <th>MARITAL STATUS</th>
        <th>PHONE</th>
        <th>MOBILE PHONE</th>
        <th>E-MAIL ADDRESS</th>
        <th>e-KTP ID</th>
    </tr>

<?php
$no = 1;
foreach ($candidates as $candidate){
?>
		<tr>
			<td><?php echo $no ?></td>
			<td><?php echo $candidate->getMiddleName(); ?></td>
			<td><?php echo $candidate->getFirstName(); ?></td>
			<td><?php echo $candidate->getLastName(); ?></td>
            <td><?php echo $candidate->getGender(); ?></td>
            <td><?php echo $candidate->getBirthplace(); ?></td>
            <td><?php echo $candidate->getDob(); ?></td>
            <td><?php echo $candidate->getCurrentaddr(); ?></td>
            <td><?php echo $candidate->getCurrentprovince(); ?></td>
            <td><?php echo $candidate->getCurrentcity(); ?></td>
            <td><?php echo $candidate->getCurrentkecamatan(); ?></td>
            <td><?php echo $candidate->getCurrentkelurahan(); ?></td>
            <td><?php echo $candidate->getCurrentzip(); ?></td>
            <td><?php echo $candidate->getPermanentaddr(); ?></td>
            <td><?php echo $candidate->getPermanentprovince(); ?></td>
            <td><?php echo $candidate->getPermanentcity(); ?></td>
            <td><?php echo $candidate->getPermanentkecamatan(); ?></td>
            <td><?php echo $candidate->getPermanentkelurahan(); ?></td>
            <td><?php echo $candidate->getPermanentzip(); ?></td>
            <td><?php echo $candidate->getReligion(); ?></td>
            <td><?php echo $candidate->getMarital(); ?></td>
            <td><?php echo $candidate->getPhone(); ?></td>
            <td><?php echo $candidate->getMobile(); ?></td>
            <td><?php echo $candidate->getEmail(); ?></td>
            <td><?php echo $candidate->getEktp(); ?></td>
		</tr>
<?php
    $no++;
}
?>
</table>

