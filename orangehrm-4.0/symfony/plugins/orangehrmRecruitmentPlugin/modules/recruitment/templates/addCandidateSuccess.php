<?php
/**
 * OrangeHRM is a comprehensive Human Resource Management (HRM) System that captures
 * all the essential functionalities required for any enterprise.
 * Copyright (C) 2006 OrangeHRM Inc., http://www.orangehrm.com
 *
 * OrangeHRM is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * OrangeHRM is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA
 *
 */
?>
<?php use_javascripts_for_form($form) ?>
<?php use_stylesheets_for_form($form) ?>

<?php use_javascript(plugin_web_path('orangehrmRecruitmentPlugin', 'js/addCandidateSuccess')); ?>
<?php $title = ($candidateId > 0) ? __('Candidate') : __('Add Candidate'); ?>
<?php
$dateFormat = get_datepicker_date_format($sf_user->getDateFormat());
$allVacancylist[] = array("id" => "", "name" => __('-- Select --'));
$allowedVacancylist[] = array("id" => "", "name" => __('-- Select --'));
$allowedVacancylistWithClosedVacancies[] = array("id" => "", "name" => __('-- Select --'));
$allowedVacancyIdArray[] = array();
$closedVacancyIdArray[] = array();
foreach ($jobVacancyList as $vacancy) {
    $newVacancyId = $vacancy['id'];
    $newVacancyName = ($vacancy['status'] == JobVacancy::CLOSED) ? $vacancy['name'] . " (" . __('Closed') . ")" : $vacancy['name'];
    $allVacancylist[] = array("id" => $newVacancyId, "name" => $newVacancyName);
    if (in_array($vacancy['id'], $form->allowedVacancyList)) {
        $allowedVacancylistWithClosedVacancies[] = array("id" => $newVacancyId, "name" => $newVacancyName);
        $allowedVacancyIdArray[] = $newVacancyId;
        if ($vacancy['status'] == JobVacancy::ACTIVE) {
            $allowedVacancylist[] = array("id" => $newVacancyId, "name" => $newVacancyName);
        } else {
            $closedVacancyIdArray[] = $newVacancyId;
        }
    }
}
?>
<style type="text/css">
.actionDrpDown {
    width: 170px;
    margin:1px 10px 0 10px;
}    
</style>
<?php if($candidatePermissions->canRead()){?>
<div class="box" id="addCandidate">

    <?php
    //$psiFileName = "";
    $psiFileId = "";
    //$kontrakFileName = "";
    $kontrakFileId = "";
    $gajiFileName = "";
    $gajiFileId = "";
    $attachmentExists = "";
    $finalLinkHtml = array();
    $noAttachment = array(
        AddCandidateForm::KTP_FILE,
        AddCandidateForm::PHOTO_FILE,
        AddCandidateForm::EDU_FILE,
        AddCandidateForm::JOBREF_FILE,
        AddCandidateForm::DSTR_FILE,
        AddCandidateForm::IDI_FILE,
        AddCandidateForm::DHIPERKES_FILE,
        AddCandidateForm::ACLS_FILE,
        AddCandidateForm::ATLS_FILE,
        AddCandidateForm::NSTR_FILE,
        AddCandidateForm::PPNI_FILE,
        AddCandidateForm::NHIPERKES_FILE,
        AddCandidateForm::PPGD_FILE,
        AddCandidateForm::PSIKOTEST_FILE,
        AddCandidateForm::CONTRACT_FILE,
        AddCandidateForm::TRANSCRIPT_FILE
    );
    //current design doesnt need this array anymore
    //$fileTitleArray = array(AddCandidateForm::PSIKOTEST_FILE, AddCandidateForm::CONTRACT_FILE);
    if (count($form->attachment) > 0) {
        //KTP
        //$linkHtml = "<table style='border-spacing: 50px'><tr>";
        //$fileTitleArray = array(AddCandidateForm::PSIKOTEST_FILE, AddCandidateForm::CONTRACT_FILE, AddCandidateForm::SALARY_FILE);
        $attachmentExists = "yes";

        for($i = 0; $i < count($form->attachment); $i++){
            //$linkHtml = "<li>";
            $linkHtml = "";
            $attachment = $form->attachment[$i];
            $fileName = "";
            $color = "color:black;";
            $duplexBox = "";
            $radioName = "";
            switch($attachment->getFiletitle()){
                case AddCandidateForm::KTP_FILE:
                    //$linkHtml .= $form['resume']->renderLabel(__('Attachment eKTP '));
                    $duplexBox = $form['resume']->render(array("class" => "duplexBox", "style" => "display: none"));
                    $fileName = "KTP";
                    $theKey = array_search(AddCandidateForm::KTP_FILE, $noAttachment);
                    unset($noAttachment[$theKey]);
                    $radioName = "resumeUpdate";
                    break;
                case AddCandidateForm::PHOTO_FILE:
                    //$linkHtml .= $form['photo']->renderLabel(__('Attachment Photo '));
                    $duplexBox = $form['photo']->render(array("class" => "duplexBox", "style" => "display: none"));
                    $fileName = "Photo";
                    $theKey = array_search(AddCandidateForm::PHOTO_FILE, $noAttachment);
                    unset($noAttachment[$theKey]);
                    $radioName = "photoUpdate";
                    break;
                case AddCandidateForm::EDU_FILE:
                    $duplexBox = $form['graduatecertificate']->render(array("class" => "duplexBox", "style" => "display: none"));
                    $fileName = "Ijazah";
                    $theKey = array_search(AddCandidateForm::EDU_FILE, $noAttachment);
                    unset($noAttachment[$theKey]);
                    $radioName = "graduatecertificateUpdate";
                    break;
                case AddCandidateForm::JOBREF_FILE:
                    $duplexBox = $form['refletter']->render(array("class" => "duplexBox", "style" => "display: none"));
                    $fileName = "Reference";
                    $theKey = array_search(AddCandidateForm::JOBREF_FILE, $noAttachment);
                    unset($noAttachment[$theKey]);
                    $radioName = "refletterUpdate";
                    break;
                case AddCandidateForm::DSTR_FILE:
                    $duplexBox = $form['dstrfile']->render(array("class" => "duplexBox", "style" => "display: none"));
                    $fileName = "Doctor STR";
                    $theKey = array_search(AddCandidateForm::DSTR_FILE, $noAttachment);
                    unset($noAttachment[$theKey]);
                    $radioName = "dstrfileUpdate";
                    break;
                case AddCandidateForm::IDI_FILE:
                    $duplexBox = $form['idicardfile']->render(array("class" => "duplexBox", "style" => "display: none"));
                    $fileName = "IDI Card";
                    $theKey = array_search(AddCandidateForm::IDI_FILE, $noAttachment);
                    unset($noAttachment[$theKey]);
                    $radioName = "idicardfileUpdate";
                    break;
                case AddCandidateForm::DHIPERKES_FILE:
                    $duplexBox = $form['dhiperkesfile']->render(array("class" => "duplexBox", "style" => "display: none"));
                    $fileName = "Doctor Hiperkes";
                    $theKey = array_search(AddCandidateForm::DHIPERKES_FILE, $noAttachment);
                    unset($noAttachment[$theKey]);
                    $radioName = "dhiperkesfileUpdate";
                    break;
                case AddCandidateForm::ACLS_FILE:
                    $duplexBox = $form['aclsfile']->render(array("class" => "duplexBox", "style" => "display: none"));
                    $fileName = "ACLS";
                    if($aclsexpire == 0){
                        $fileName .= "(expire)";
                        $color = "color:red;";
                    }
                    $theKey = array_search(AddCandidateForm::ACLS_FILE, $noAttachment);
                    unset($noAttachment[$theKey]);
                    $radioName = "aclsfileUpdate";
                    break;
                case AddCandidateForm::ATLS_FILE:
                    $duplexBox = $form['atlsfile']->render(array("class" => "duplexBox", "style" => "display: none"));
                    $fileName = "ATLS";
                    if($atlsexpire == 0){
                        $fileName .= "(expire)";
                        $color = "color:red;";
                    }
                    $theKey = array_search(AddCandidateForm::ATLS_FILE, $noAttachment);
                    unset($noAttachment[$theKey]);
                    $radioName = "atlsfileUpdate";
                    break;
                case AddCandidateForm::NSTR_FILE:
                    $duplexBox = $form['nstrfile']->render(array("class" => "duplexBox", "style" => "display: none"));
                    $fileName = "Nurse STR";
                    $theKey = array_search(AddCandidateForm::NSTR_FILE, $noAttachment);
                    unset($noAttachment[$theKey]);
                    $radioName = "nstrfileUpdate";
                    break;
                case AddCandidateForm::PPNI_FILE:
                    $duplexBox = $form['ppnifile']->render(array("class" => "duplexBox", "style" => "display: none"));
                    $fileName = "PPNI";
                    $theKey = array_search(AddCandidateForm::PPNI_FILE, $noAttachment);
                    unset($noAttachment[$theKey]);
                    $radioName = "ppnifileUpdate";
                    break;
                case AddCandidateForm::NHIPERKES_FILE:
                    $duplexBox = $form['nhiperkesfile']->render(array("class" => "duplexBox", "style" => "display: none"));
                    $fileName = "Nurse Hiperkes";
                    $theKey = array_search(AddCandidateForm::NHIPERKES_FILE, $noAttachment);
                    unset($noAttachment[$theKey]);
                    $radioName = "nhiperkesfileUpdate";
                    break;
                case AddCandidateForm::PPGD_FILE:
                    $duplexBox = $form['ppgdfile']->render(array("class" => "duplexBox", "style" => "display: none"));
                    $fileName = "PPGD";
                    if($ppgdexpire == 0){
                        $fileName .= "(expire)";
                        $color = "color:red;";
                    }
                    $theKey = array_search(AddCandidateForm::PPGD_FILE, $noAttachment);
                    unset($noAttachment[$theKey]);
                    $radioName = "ppgdfileUpdate";
                    break;
                case AddCandidateForm::PSIKOTEST_FILE:
                    $duplexBox = $form['psikotest']->render(array("class" => "duplexBox", "style" => "display: none"));
                    //$psiFileName = "Hasil Psikotest";
                    $fileName = "Hasil Psikotest";
                    $psiFileId = $attachment->getId();
                    $theKey = array_search(AddCandidateForm::PSIKOTEST_FILE, $noAttachment);
                    unset($noAttachment[$theKey]);
                    $radioName = "psikotestUpdate";
                    break;
                case AddCandidateForm::CONTRACT_FILE:
                    $duplexBox = $form['kontrak']->render(array("class" => "duplexBox", "style" => "display: none"));
                    //$kontrakFileName = "Kontrak";
                    $fileName = "Kontrak";
                    $kontrakFileId = $attachment->getId();
                    $theKey = array_search(AddCandidateForm::CONTRACT_FILE, $noAttachment);
                    unset($noAttachment[$theKey]);
                    $radioName = "kontrakUpdate";
                    break;
                case AddCandidateForm::TRANSCRIPT_FILE:
                    $duplexBox = $form['transcript']->render(array("class" => "duplexBox", "style" => "display: none"));
                    $fileName = "Transcript";
                    $theKey = array_search(AddCandidateForm::TRANSCRIPT_FILE, $noAttachment);
                    unset($noAttachment[$theKey]);
                    $radioName = "transcriptUpdate";
                    break;
                /*case AddCandidateForm::SALARY_FILE:
                    $gajiFileName = "Gaji";
                    $gajiFileId = $attachment->getId();
                    break;*/
            }

            $fileTitle = $attachment->getFiletitle();
            //if(!in_array($fileTitle, $fileTitleArray)){
                //$linkHtml .= "<td style='padding: 20px; text-align: center;'><div id=\"fileLink\"><a target=\"_blank\" class=\"fileLink\" href=\"";
                $linkHtml .= "<table><tr><td style='text-align: center;'><div id=\"fileLink\"><a target=\"_blank\" class=\"fileLink\" href=\"";
                $linkHtml .= url_for('recruitment/viewCandidateAttachment?attachId=' . $attachment->getId());
                //$linkHtml .= "\"><i class=\"material-icons\" style=\"font-size:48px;{$color}\">description</i><br>{$fileName}</a></div></td>";
                $linkHtml .= "\"><i class=\"material-icons\" style=\"font-size:48px;{$color}\">description</i><br>{$fileName}</a></div></td>";
                $linkHtml .= "<td>" . $duplexBox . "<label style=\"display: none;\" id=\"fieldHelpBottomSpec_". $radioName ."\" class=\"fieldHelpBottomSpec\">". __(CommonMessages::FILE_LABEL_JPEG) . "</label>";
                $linkHtml .= "</td></tr></table>";
                $linkHtml .= "<li class=\"radio noLabel\" id=\"radio\">";
                $linkHtml .= $form[$radioName]->render(array("class" => "fileEditOptions"));
                $linkHtml .= "</li>";
            //}
            //$linkHtml .= "</li>";
            $finalLinkHtml[$attachment->getFiletitle()] = $linkHtml;
        }
        //$linkHtml .= "</tr></table>";
        //echo $form['resumeUpdate']->renderLabel(__('Attachment'));
        //echo $finalLinkHtml;
        /*echo "<li class=\"radio noLabel\" id=\"radio\">";
        echo $form['resumeUpdate']->render(array("class" => "fileEditOptions"));
        echo "</li>";
        echo "<li id=\"fileUploadSection\" class=\"noLabel\">";
        echo $form['resume']->renderLabel(' ');
        echo $form['resume']->render(array("class " => "duplexBox"));
        echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_DOC) . "</label>";
        echo "</li>";*/
    } //else {
        //echo $form['resume']->renderLabel(__('Attachment'), array("class " => "resume"));
        //echo $form['resume']->render();
        //echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_DOC) . "</label>";
    //}
    $noAttachment = array_values($noAttachment);
    for($i = 0;$i <= sizeof($noAttachment);$i++){
        $linkHtml = "";
        $duplexBox = "";
        switch($noAttachment[$i]){
            case AddCandidateForm::KTP_FILE:
                $duplexBox = $form['resume']->render(array("class" => "duplexBox"));
                break;
            case AddCandidateForm::PHOTO_FILE:
                $duplexBox = $form['photo']->render(array("class" => "duplexBox"));
                break;
            case AddCandidateForm::EDU_FILE:
                $duplexBox = $form['graduatecertificate']->render(array("class" => "duplexBox"));
                break;
            case AddCandidateForm::JOBREF_FILE:
                $duplexBox = $form['refletter']->render(array("class" => "duplexBox"));
                break;
            case AddCandidateForm::DSTR_FILE:
                $duplexBox = $form['dstrfile']->render(array("class" => "duplexBox"));
                break;
            case AddCandidateForm::IDI_FILE:
                $duplexBox = $form['idicardfile']->render(array("class" => "duplexBox"));
                break;
            case AddCandidateForm::DHIPERKES_FILE:
                $duplexBox = $form['dhiperkesfile']->render(array("class" => "duplexBox"));
                break;
            case AddCandidateForm::ACLS_FILE:
                $duplexBox = $form['aclsfile']->render(array("class" => "duplexBox"));
                break;
            case AddCandidateForm::ATLS_FILE:
                $duplexBox = $form['atlsfile']->render(array("class" => "duplexBox"));
                break;
            case AddCandidateForm::NSTR_FILE:
                $duplexBox = $form['nstrfile']->render(array("class" => "duplexBox"));
                break;
            case AddCandidateForm::PPNI_FILE:
                $duplexBox = $form['ppnifile']->render(array("class" => "duplexBox"));
                break;
            case AddCandidateForm::NHIPERKES_FILE:
                $duplexBox = $form['nhiperkesfile']->render(array("class" => "duplexBox"));
                break;
            case AddCandidateForm::PPGD_FILE:
                $duplexBox = $form['ppgdfile']->render(array("class" => "duplexBox"));
                break;
            case AddCandidateForm::PSIKOTEST_FILE:
                $duplexBox = $form['psikotest']->render(array("class" => "duplexBox"));
                break;
            case AddCandidateForm::CONTRACT_FILE:
                $duplexBox = $form['kontrak']->render(array("class" => "duplexBox"));
                break;
            case AddCandidateForm::TRANSCRIPT_FILE:
                $duplexBox = $form['transcript']->render(array("class" => "duplexBox"));
                break;
        }

        if(!in_array($noAttachment[$i], $fileTitleArray)){
            $linkHtml .= $duplexBox;
            $linkHtml .= "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_JPEG) . "</label>";
        }
        //$linkHtml .= "</li>";
        $finalLinkHtml[$noAttachment[$i]] = $linkHtml;
    }
    //print_r($noAttachment);die;


    ?>

    <div class="head"><h1 id="addCandidateHeading"><?php echo $title; ?></h1></div>
    <div class="inner">
        <?php include_partial('global/flash_messages', array('prefix' => 'addcandidate')); ?>
        <form name="frmAddCandidate" id="frmAddCandidate" method="post" action="<?php echo url_for('recruitment/addCandidate?id=' . $candidateId); ?>" enctype="multipart/form-data">

            <?php echo $form['_csrf_token']; ?>
            <fieldset>
                <ol>
                    <li>
                        <?php
                            if($shortlisted){
                                echo "<div class=\"message warning\">";
                                echo "This candidate has been shortlisted by other Hiring Manager";
                                echo "<a href=\"#\" class=\"messageCloseButton\"><?php echo __('Close');?></a>";
                                echo "</div>";
                            }
                        ?>
                    </li>
                    <li class="line nameContainer">

                        <label class="hasTopFieldHelp"><?php echo __('Full Name'); ?></label>
                        <ol class="fieldsInLine">
                            <li>
                                <div class="fieldDescription"> <?php echo __('Front Title'); ?></div>
                                <?php echo $form['firstName']->render(array("class" => "formInputText", "maxlength" => 35)); ?>
                            </li>
                            <li>
                                <div class="fieldDescription"><em>*</em> <?php echo __('Full Name'); ?></div>
                                 <?php echo $form['middleName']->render(array("class" => "formInputText", "maxlength" => 35)); ?>
                            </li>
                            <li>
                                <div class="fieldDescription"><em>*</em> <?php echo __('End Title'); ?></div>
                                <?php echo $form['lastName']->render(array("class" => "formInputText", "maxlength" => 35)); ?>
                            </li>
                        </ol>

                    </li>
                    <!--<li>
                        <?php //echo $form['firstName']->renderLabel(__('Full Name')); ?>
                        <span style="font-weight: bold"><?php //echo ($firstName . " " . $middleName . " " . $lastName); ?></span>
                    </li>-->
                    <!--<li>
                        <?php echo $form['gender']->renderLabel(__('Gender')). ' <span class="required">*</span>'; ?>
                        <span style="font-weight: bold"><?php echo ($gender); ?></span>
                    </li>
                    <li>
                        <?php echo $form['birthplace']->renderLabel(__('Birth Place')); ?>
                        <span style="font-weight: bold"><?php echo ($birthplace); ?></span>
                    </li>
                    <li>
                        <?php echo $form['dob']->renderLabel(__('Date of Birth')); ?>
                        <span style="font-weight: bold"><?php echo date('d-m-Y', strtotime($dob)); ?></span>
                    </li>
                    <li>
                        <?php echo $form['currentaddr']->renderLabel(__('Current Address')); ?>
                        <span style="font-weight: bold"><?php echo ($currentaddr); ?></span>
                    </li>
                    <li>
                        <?php echo $form['currentprovince']->renderLabel(__('Current Province')); ?>
                        <span style="font-weight: bold"><?php echo ($currentprovince); ?></span>
                    </li>
                    <li>
                        <?php echo $form['currentcity']->renderLabel(__('Current City')); ?>
                        <span style="font-weight: bold"><?php echo ($currentcity); ?></span>
                    </li>
                    <li>
                        <?php echo $form['currentkecamatan']->renderLabel(__('Current Kecamatan')); ?>
                        <span style="font-weight: bold"><?php echo ($currentkecamatan); ?></span>
                    </li>
                    <li>
                        <?php echo $form['currentkelurahan']->renderLabel(__('Current Kelurahan')); ?>
                        <span style="font-weight: bold"><?php echo ($currentkelurahan); ?></span>
                    </li>
                    <li>
                        <?php echo $form['currentzip']->renderLabel(__('Current Zip')); ?>
                        <span style="font-weight: bold"><?php echo ($currentzip); ?></span>
                    </li>
                    <li>
                        <?php echo $form['permanentaddr']->renderLabel(__('Permanent Address')); ?>
                        <span style="font-weight: bold"><?php echo ($permanentaddr); ?></span>
                    </li>
                    <li>
                        <?php echo $form['permanentprovince']->renderLabel(__('Permanent Province')); ?>
                        <span style="font-weight: bold"><?php echo ($permanentprovince); ?></span>
                    </li>
                    <li>
                        <?php echo $form['permanentcity']->renderLabel(__('Permanent City')); ?>
                        <span style="font-weight: bold"><?php echo ($permanentcity); ?></span>
                    </li>
                    <li>
                        <?php echo $form['permanentkecamatan']->renderLabel(__('Permanent Kecamatan')); ?>
                        <span style="font-weight: bold"><?php echo ($permanentkecamatan); ?></span>
                    </li>
                    <li>
                        <?php echo $form['permanentkelurahan']->renderLabel(__('Permanent Kelurahan')); ?>
                        <span style="font-weight: bold"><?php echo ($permanentkelurahan); ?></span>
                    </li>
                    <li>
                        <?php echo $form['permanentzip']->renderLabel(__('Permanent Zip')); ?>
                        <span style="font-weight: bold"><?php echo ($permanentzip); ?></span>
                    </li>
                    <li>
                        <?php echo $form['religion']->renderLabel(__('Religion')); ?>
                        <span style="font-weight: bold"><?php echo ($religion); ?></span>
                    </li>
                    <li>
                        <?php echo $form['marital']->renderLabel(__('Marital Status')); ?>
                        <span style="font-weight: bold"><?php echo ($marital); ?></span>
                    </li>
                    <li>
                        <?php echo $form['phone']->renderLabel(__('Phone')); ?>
                        <span style="font-weight: bold"><?php echo ($phone); ?></span>
                    </li>
                    <li>
                        <?php echo $form['mobile']->renderLabel(__('Mobile Phone')); ?>
                        <span style="font-weight: bold"><?php echo ($mobile); ?></span>
                    </li>
                    <li>
                        <?php echo $form['email']->renderLabel(__('Email Address')); ?>
                        <span style="font-weight: bold"><?php echo ($email); ?></span>
                    </li>
                    <li>
                        <?php echo $form['ektp']->renderLabel(__('Identity Number (eKTP) ')); ?>
                        <span style="font-weight: bold"><?php echo ($ektp); ?></span>
                    </li>-->
                    <li  class="new radio">
                        <?php echo $form['gender']->renderLabel(__('Gender'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['gender']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['birthplace']->renderLabel(__('Birth place'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['birthplace']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['dob']->renderLabel(__('Date of Birth'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['dob']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['currentaddr']->renderLabel(__('Current Address'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['currentaddr']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['currentprovince']->renderLabel(__('Current Province'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['currentprovince']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['currentcity']->renderLabel(__('Current City'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['currentcity']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['currentkecamatan']->renderLabel(__('Current Kecamatan'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['currentkecamatan']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['currentkelurahan']->renderLabel(__('Current Kelurahan'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['currentkelurahan']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['currentzip']->renderLabel(__('Current Zip'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['currentzip']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['permanentaddr']->renderLabel(__('Permanent Addr'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['permanentaddr']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['permanentprovince']->renderLabel(__('Permanent Province'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['permanentprovince']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['permanentcity']->renderLabel(__('Permanent City'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['permanentcity']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['permanentkecamatan']->renderLabel(__('Permanent Kecamatan'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['permanentkecamatan']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['permanentkelurahan']->renderLabel(__('Permanent Kelurahan'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['permanentkelurahan']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['permanentzip']->renderLabel(__('Permanent Zip'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['permanentzip']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['religion']->renderLabel(__('Religion'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['religion']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['marital']->renderLabel(__('Marital Status'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['marital']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['phone']->renderLabel(__('Phone'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['phone']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['mobile']->renderLabel(__('Mobile Phone'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['mobile']->render(array("class" => "formInputText")); ?>
                    </li>

                    <li>
                        <?php echo $form['email']->renderLabel(__('Email Address'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['email']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['ektp']->renderLabel(__('Identity Number (eKTP)'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['ektp']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li class="fieldHelpContainer">
                        <?php if ($candidateId == "") : ?>

                            <?php echo $form['photo']->renderLabel(__('Upload Photo') . ' <span class="required">*</span>'); ?>
                            <?php echo $form['photo']->render(array("class " => "duplexBox")); ?>
                            <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_JPEG) . "</label>"; ?>

                        <?php else : ?>
                            <?php echo $form['photo']->renderLabel(__('Photo')); ?>
                            <?php if (array_key_exists(AddCandidateForm::PHOTO_FILE, $finalLinkHtml)) {
                                echo $finalLinkHtml[AddCandidateForm::PHOTO_FILE];
                            }
                            ?>
                            <?php //echo "<span class=\"fileLink\">".__('Uploaded')."</span>"; ?>

                        <?php endif; ?>
                    </li>

                    <li class="fieldHelpContainer">
                        <?php if ($candidateId == "") : ?>

                            <?php echo $form['resume']->renderLabel(__('Upload eKTP') . ' <span class="required">*</span>'); ?>
                            <?php echo $form['resume']->render(array("class " => "duplexBox")); ?>
                            <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_JPEG) . "</label>"; ?>

                        <?php else : ?>

                            <?php echo $form['resume']->renderLabel(__('eKTP')); ?>
                            <?php if (array_key_exists(AddCandidateForm::KTP_FILE, $finalLinkHtml)) {
                                echo $finalLinkHtml[AddCandidateForm::KTP_FILE];
                            }
                            ?>
                            <?php //echo "<span class=\"fileLink\">".__('Uploaded')."</span>"; ?>

                        <?php endif; ?>
                    </li>
                </ol>
                <label><span class="keywrd" style="font-weight: bold">EDUCATION</span> <span  id="eduExtend">[+]</span></label>
                <!--<p style="font-weight: bold">EDUCATION</p>-->
                <div id="education">
                    <ol>
                        <li>
                        </li>
                        <li>
                            <?php echo $form['elementary']->renderLabel(__('Elementary School')); ?>
                            <?php echo $form['elementary']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['elementary_startyear']->renderLabel(__('Elementary School Start Year')); ?>
                            <?php echo $form['elementary_startyear']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['elementary_endyear']->renderLabel(__('Elementary School End Year')); ?>
                            <?php echo $form['elementary_endyear']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['juniorhigh']->renderLabel(__('Junior High School')); ?>
                            <?php echo $form['juniorhigh']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['juniorhigh_startyear']->renderLabel(__('Junior High School Start Year')); ?>
                            <?php echo $form['juniorhigh_startyear']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['juniorhigh_endyear']->renderLabel(__('Junior High School End Year')); ?>
                            <?php echo $form['juniorhigh_endyear']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['seniorhigh']->renderLabel(__('Senior High School')); ?>
                            <?php echo $form['seniorhigh']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['seniorhigh_startyear']->renderLabel(__('Senior High School Start Year')); ?>
                            <?php echo $form['seniorhigh_startyear']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['seniorhigh_endyear']->renderLabel(__('Senior High School End Year')); ?>
                            <?php echo $form['seniorhigh_endyear']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['associate']->renderLabel(__('Associate Degree (Diploma)')); ?>
                            <?php echo $form['associate']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['associate_startyear']->renderLabel(__('Associate Degree (Diploma) Start Year')); ?>
                            <?php echo $form['associate_startyear']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['associate_endyear']->renderLabel(__('Associate Degree (Diploma) End Year')); ?>
                            <?php echo $form['associate_endyear']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['associate_desc']->renderLabel(__('Associate Degree (Diploma) Required Information')); ?>
                            <?php echo $form['associate_desc']->render(array("cols" => 20, "rows" => 9, "class" => "editabletextarea")); ?>
                            <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::DEGREE_DESC) . "</label>"; ?>
                        </li>
                        <li>
                            <?php echo $form['bachelor']->renderLabel(__('Bachelor Degree (S1)')); ?>
                            <?php echo $form['bachelor']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['bachelor_startyear']->renderLabel(__('Bachelor Degree (S1) Start Year')); ?>
                            <?php echo $form['bachelor_startyear']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['bachelor_endyear']->renderLabel(__('Bachelor Degree (S1) End Year')); ?>
                            <?php echo $form['bachelor_endyear']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['bachelor_desc']->renderLabel(__('Bachelor Degree (S1) Required Information')); ?>
                            <?php echo $form['bachelor_desc']->render(array("cols" => 20, "rows" => 9, "class" => "editabletextarea")); ?>
                            <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::DEGREE_DESC) . "</label>"; ?>
                        </li>
                        <li>
                            <?php echo $form['master']->renderLabel(__('Master Degree (S2)')); ?>
                            <?php echo $form['master']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['master_startyear']->renderLabel(__('Master Degree (S2) Start Year')); ?>
                            <?php echo $form['master_startyear']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['master_endyear']->renderLabel(__('Master Degree (S2) End Year')); ?>
                            <?php echo $form['master_endyear']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['master_desc']->renderLabel(__('Master Degree (S2) Required Information')); ?>
                            <?php echo $form['master_desc']->render(array("cols" => 20, "rows" => 9, "class" => "editabletextarea")); ?>
                            <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::DEGREE_DESC) . "</label>"; ?>
                        </li>
                        <li>
                            <?php echo $form['doctoral']->renderLabel(__('Doctoral Degree (Ph.D)')); ?>
                            <?php echo $form['doctoral']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['doctoral_startyear']->renderLabel(__('Doctoral Degree (Ph.D) Start Year')); ?>
                            <?php echo $form['doctoral_startyear']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['doctoral_endyear']->renderLabel(__('Doctoral Degree (Ph.D) End Year')); ?>
                            <?php echo $form['doctoral_endyear']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['doctoral_desc']->renderLabel(__('Doctoral Degree (Ph.D) Required Information')); ?>
                            <?php echo $form['doctoral_desc']->render(array("cols" => 20, "rows" => 9, "class" => "editabletextarea")); ?>
                            <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::DEGREE_DESC) . "</label>"; ?>
                        </li>
                        <li class="fieldHelpContainer">
                            <?php if ($candidateId == "") : ?>

                                <?php echo $form['graduatecertificate']->renderLabel(__('Upload last graduate certificate')); ?>
                                <?php echo $form['graduatecertificate']->render(array("class " => "duplexBox")); ?>
                                <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_JPEG) . "</label>"; ?>

                            <?php else : ?>

                                <?php echo $form['graduatecertificate']->renderLabel(__('Last graduate certificate')); ?>
                                <?php if (array_key_exists(AddCandidateForm::EDU_FILE, $finalLinkHtml)) {
                                    echo $finalLinkHtml[AddCandidateForm::EDU_FILE];
                                }
                                ?>
                                <?php //echo "<span class=\"fileLink\">".__('Uploaded')."</span>"; ?>

                            <?php endif; ?>
                        </li>
                        <li class="fieldHelpContainer">
                            <?php if ($candidateId == "") : ?>

                                <?php echo $form['transcript']->renderLabel(__('Upload last graduate transcript')); ?>
                                <?php echo $form['transcript']->render(array("class " => "duplexBox")); ?>
                                <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_JPEG) . "</label>"; ?>

                            <?php else : ?>

                                <?php echo $form['transcript']->renderLabel(__('Last graduate transcript')); ?>
                                <?php if (array_key_exists(AddCandidateForm::TRANSCRIPT_FILE, $finalLinkHtml)) {
                                    echo $finalLinkHtml[AddCandidateForm::TRANSCRIPT_FILE];
                                }
                                ?>
                                <?php //echo "<span class=\"fileLink\">".__('Uploaded')."</span>"; ?>

                            <?php endif; ?>
                        </li>
                    </ol>
                </div>
                <hr style="border: 0px dashed #727272;">
                <label><span class="keywrd" style="font-weight: bold">WORKING EXPERIENCE</span><span id="weExtend"> [+]</span></label>
                <div id="we">
                    <ol>
                        <!--
                        <li>
                            <p class="keywrd" style="font-weight: bold">WORKING EXPERIENCE</p>
                        </li>-->
                        <li>
                        </li>
                        <li>
                            <?php echo $form['company1']->renderLabel(__('Company Name')); ?>
                            <?php echo $form['company1']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['position1']->renderLabel(__('Position')); ?>
                            <?php echo $form['position1']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['start1']->renderLabel(__('Start')); ?>
                            <?php echo $form['start1']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['end1']->renderLabel(__('End')); ?>
                            <?php echo $form['end1']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="largeTextBox">
                            <?php echo $form['jobdesc1']->renderLabel(__('Job Description')); ?>
                            <?php echo $form['jobdesc1']->render(array("cols" => 20, "rows" => 9, "class" => "editabletextarea")); ?>
                        </li>
                        <li>
                            <?php echo $form['refname1']->renderLabel(__('Reference Name')); ?>
                            <?php echo $form['refname1']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['reftelp1']->renderLabel(__('Reference Phone')); ?>
                            <?php echo $form['reftelp1']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li><hr style="border: 0.4px dashed #727272;"></li>
                        <li>
                            <?php echo $form['company2']->renderLabel(__('Company Name')); ?>
                            <?php echo $form['company2']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['position2']->renderLabel(__('Position')); ?>
                            <?php echo $form['position2']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['start2']->renderLabel(__('Start')); ?>
                            <?php echo $form['start2']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['end2']->renderLabel(__('End')); ?>
                            <?php echo $form['end2']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="largeTextBox">
                            <?php echo $form['jobdesc2']->renderLabel(__('Job Description')); ?>
                            <?php echo $form['jobdesc2']->render(array("cols" => 20, "rows" => 9, "class" => "editabletextarea")); ?>
                        </li>
                        <li>
                            <?php echo $form['refname2']->renderLabel(__('Reference Name')); ?>
                            <?php echo $form['refname2']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['reftelp2']->renderLabel(__('Reference Phone')); ?>
                            <?php echo $form['reftelp2']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li><hr style="border: 0.4px dashed #727272;"></li>
                        <li>
                            <?php echo $form['company3']->renderLabel(__('Company Name')); ?>
                            <?php echo $form['company3']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['position3']->renderLabel(__('Position')); ?>
                            <?php echo $form['position3']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['start3']->renderLabel(__('Start')); ?>
                            <?php echo $form['start3']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['end3']->renderLabel(__('End')); ?>
                            <?php echo $form['end3']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="largeTextBox">
                            <?php echo $form['jobdesc3']->renderLabel(__('Job Description')); ?>
                            <?php echo $form['jobdesc3']->render(array("cols" => 20, "rows" => 9, "class" => "editabletextarea")); ?>
                        </li>
                        <li>
                            <?php echo $form['refname3']->renderLabel(__('Reference Name')); ?>
                            <?php echo $form['refname3']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['reftelp3']->renderLabel(__('Reference Phone')); ?>
                            <?php echo $form['reftelp3']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li><hr style="border: 0.2px dashed #727272;"></li>
                        <li>
                            <?php echo $form['company4']->renderLabel(__('Company Name')); ?>
                            <?php echo $form['company4']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['position4']->renderLabel(__('Position')); ?>
                            <?php echo $form['position4']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['start4']->renderLabel(__('Start')); ?>
                            <?php echo $form['start4']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['end4']->renderLabel(__('End')); ?>
                            <?php echo $form['end4']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="largeTextBox">
                            <?php echo $form['jobdesc4']->renderLabel(__('Job Description')); ?>
                            <?php echo $form['jobdesc4']->render(array("cols" => 20, "rows" => 9, "class" => "editabletextarea")); ?>
                        </li>
                        <li>
                            <?php echo $form['refname4']->renderLabel(__('Reference Name')); ?>
                            <?php echo $form['refname4']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['reftelp4']->renderLabel(__('Reference Phone')); ?>
                            <?php echo $form['reftelp4']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li><hr style="border: 0.2px dashed #727272;"></li>
                        <li>
                            <?php echo $form['company5']->renderLabel(__('Company Name')); ?>
                            <?php echo $form['company5']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['position5']->renderLabel(__('Position')); ?>
                            <?php echo $form['position5']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['start5']->renderLabel(__('Start')); ?>
                            <?php echo $form['start5']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['end5']->renderLabel(__('End')); ?>
                            <?php echo $form['end5']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="largeTextBox">
                            <?php echo $form['jobdesc5']->renderLabel(__('Job Description')); ?>
                            <?php echo $form['jobdesc5']->render(array("cols" => 20, "rows" => 9, "class" => "editabletextarea")); ?>
                        </li>
                        <li>
                            <?php echo $form['refname5']->renderLabel(__('Reference Name')); ?>
                            <?php echo $form['refname5']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['reftelp5']->renderLabel(__('Reference Phone')); ?>
                            <?php echo $form['reftelp5']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li class="fieldHelpContainer">
                            <?php if ($candidateId == "") : ?>

                                <?php echo $form['refletter']->renderLabel(__('Upload references letter from last job')); ?>
                                <?php echo $form['refletter']->render(array("class " => "duplexBox")); ?>
                                <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_JPEG) . "</label>"; ?>

                            <?php else : ?>

                                <?php echo $form['refletter']->renderLabel(__('Reference letter')); ?>
                                <?php if (array_key_exists(AddCandidateForm::JOBREF_FILE, $finalLinkHtml)) {
                                    echo $finalLinkHtml[AddCandidateForm::JOBREF_FILE];
                                }
                                ?>
                                <?php //echo "<span class=\"fileLink\">".__('Uploaded')."</span>"; ?>

                            <?php endif; ?>
                        </li>
                    </ol>
                </div>
                <hr style="border: 0px dashed #727272;">
                <label><span class="keywrd" style="font-weight: bold">OTHERS</span></label>
                <!--<p class="keywrd" style="font-weight: bold">OTHERS</p>-->
                <ol>
                    <li>

                    </li>
                    <li>
                        <?php echo $form['currentsalary']->renderLabel(__('Current Salary')); ?>
                        <?php echo $form['currentsalary']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['wantsalary']->renderLabel(__('Expected Salary'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['wantsalary']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['prefworkplace']->renderLabel(__('Preference Work Place')); ?>
                        <?php echo $form['prefworkplace']->render(array("class" => "formInputText")); ?>
                    </li>
                </ol>
                <hr style="border: 0px dashed #727272;">
                <label><span class="keywrd" style="font-weight: bold">LEGALITY (DOCTOR)</span><span id="docExtend"> [+]</span></label>
                <div id="doctor">
                    <ol>
                        <li>
                            <!--<p class="keywrd" style="font-weight: bold">LEGALITY (DOCTOR)</p>-->
                        </li>
                        <li><hr style="border: 0.4px dashed #727272;"></li>
                        <li>
                            <?php echo $form['doctor_str']->renderLabel(__('STR No.')); ?>
                            <?php echo $form['doctor_str']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['doctor_strissued']->renderLabel(__('STR Issue Date')); ?>
                            <?php echo $form['doctor_strissued']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['doctor_strexpire']->renderLabel(__('STR Expire Date')); ?>
                            <?php echo $form['doctor_strexpire']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="fieldHelpContainer">
                            <?php if ($candidateId == "") : ?>

                                <?php echo $form['dstrfile']->renderLabel(__('Upload STR')); ?>
                                <?php echo $form['dstrfile']->render(array("class " => "duplexBox")); ?>
                                <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_JPEG) . "</label>"; ?>

                            <?php else : ?>

                                <?php echo $form['dstrfile']->renderLabel(__('STR')); ?>
                                <?php if (array_key_exists(AddCandidateForm::DSTR_FILE, $finalLinkHtml)) {
                                    echo $finalLinkHtml[AddCandidateForm::DSTR_FILE];
                                }
                                ?>
                                <?php //echo "<span class=\"fileLink\">".__('Uploaded')."</span>"; ?>

                            <?php endif; ?>
                        </li>
                        <li><hr style="border: 0.4px dashed #727272;"></li>
                        <li>
                            <?php echo $form['doctor_idicard']->renderLabel(__('IDI Card No.')); ?>
                            <?php echo $form['doctor_idicard']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['doctor_idicardexpire']->renderLabel(__('IDI Card Expire Date')); ?>
                            <?php echo $form['doctor_idicardexpire']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="fieldHelpContainer">
                            <?php if ($candidateId == "") : ?>

                                <?php echo $form['idicardfile']->renderLabel(__('Upload IDI Card')); ?>
                                <?php echo $form['idicardfile']->render(array("class " => "duplexBox")); ?>
                                <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_JPEG) . "</label>"; ?>

                            <?php else : ?>

                                <?php echo $form['idicardfile']->renderLabel(__('IDI Card')); ?>
                                <?php if (array_key_exists(AddCandidateForm::IDI_FILE, $finalLinkHtml)) {
                                    echo $finalLinkHtml[AddCandidateForm::IDI_FILE];
                                }
                                ?>
                                <?php //echo "<span class=\"fileLink\">".__('Uploaded')."</span>"; ?>

                            <?php endif; ?>
                        </li>
                        <li><hr style="border: 0.4px dashed #727272;"></li>
                        <li>
                            <?php echo $form['doctor_hiperkes']->renderLabel(__('Hiperkes Certificate')); ?>
                            <?php echo $form['doctor_hiperkes']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li class="fieldHelpContainer">
                            <?php if ($candidateId == "") : ?>

                                <?php echo $form['dhiperkesfile']->renderLabel(__('Upload Hiperkes Certificate')); ?>
                                <?php echo $form['dhiperkesfile']->render(array("class " => "duplexBox")); ?>
                                <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_JPEG) . "</label>"; ?>

                            <?php else : ?>

                                <?php echo $form['dhiperkesfile']->renderLabel(__('Hiperkes Certificate')); ?>
                                <?php if (array_key_exists(AddCandidateForm::DHIPERKES_FILE, $finalLinkHtml)) {
                                    echo $finalLinkHtml[AddCandidateForm::DHIPERKES_FILE];
                                }
                                ?>
                                <?php //echo "<span class=\"fileLink\">".__('Uploaded')."</span>"; ?>

                            <?php endif; ?>
                        </li>
                        <li><hr style="border: 0.4px dashed #727272;"></li>
                        <li>
                            <?php echo $form['doctor_acls']->renderLabel(__('ACLS Certificate')); ?>
                            <?php echo $form['doctor_acls']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['doctor_aclsissued']->renderLabel(__('ACLS Certificate Issue Date')); ?>
                            <?php echo $form['doctor_aclsissued']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['doctor_aclsexpire']->renderLabel(__('ACLS Certificate Expire Date')); ?>
                            <?php echo $form['doctor_aclsexpire']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="fieldHelpContainer">
                            <?php if ($candidateId == "") : ?>

                                <?php echo $form['aclsfile']->renderLabel(__('Upload ACLS Certificate')); ?>
                                <?php echo $form['aclsfile']->render(array("class " => "duplexBox")); ?>
                                <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_JPEG) . "</label>"; ?>

                            <?php else : ?>

                                <?php echo $form['aclsfile']->renderLabel(__('ACLS Certificate')); ?>
                                <?php if (array_key_exists(AddCandidateForm::ACLS_FILE, $finalLinkHtml)) {
                                    echo $finalLinkHtml[AddCandidateForm::ACLS_FILE];
                                }
                                ?>
                                <?php //echo "<span class=\"fileLink\">".__('Uploaded')."</span>"; ?>

                            <?php endif; ?>
                        </li>
                        <li><hr style="border: 0.4px dashed #727272;"></li>
                        <li>
                            <?php echo $form['doctor_atls']->renderLabel(__('ATLS Certificate')); ?>
                            <?php echo $form['doctor_atls']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['doctor_atlsissued']->renderLabel(__('ATLS Certificate Issue Date')); ?>
                            <?php echo $form['doctor_atlsissued']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['doctor_atlsexpire']->renderLabel(__('ATLS Certificate Expire Date')); ?>
                            <?php echo $form['doctor_atlsexpire']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="fieldHelpContainer">
                            <?php if ($candidateId == "") : ?>

                                <?php echo $form['atlsfile']->renderLabel(__('Upload ATLS Certificate')); ?>
                                <?php echo $form['atlsfile']->render(array("class " => "duplexBox")); ?>
                                <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_JPEG) . "</label>"; ?>

                            <?php else : ?>

                                <?php echo $form['atlsfile']->renderLabel(__('ATLS Certificate')); ?>
                                <?php if (array_key_exists(AddCandidateForm::ATLS_FILE, $finalLinkHtml)) {
                                    echo $finalLinkHtml[AddCandidateForm::ATLS_FILE];
                                }
                                ?>
                                <?php //echo "<span class=\"fileLink\">".__('Uploaded')."</span>"; ?>

                            <?php endif; ?>
                        </li>

                    </ol>
                </div>
                <hr style="border: 0px dashed #727272;">
                <label><span class="keywrd" style="font-weight: bold">LEGALITY (NURSE)</span><span id="nurseExtend"> [+]</span></label>
                <div id="nurse">
                    <ol>
                        <li>
                            <!--<p class="keywrd" style="font-weight: bold">LEGALITY (NURSE)</p>-->
                        </li>
                        <li><hr style="border: 0.4px dashed #727272;"></li>
                        <li>
                            <?php echo $form['nurse_str']->renderLabel(__('STR No.')); ?>
                            <?php echo $form['nurse_str']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['nurse_strissued']->renderLabel(__('STR Issue Date')); ?>
                            <?php echo $form['nurse_strissued']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['nurse_strexpire']->renderLabel(__('STR Expire Date')); ?>
                            <?php echo $form['nurse_strexpire']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="fieldHelpContainer">
                            <?php if ($candidateId == "") : ?>

                                <?php echo $form['nstrfile']->renderLabel(__('Upload STR')); ?>
                                <?php echo $form['nstrfile']->render(array("class " => "duplexBox")); ?>
                                <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_JPEG) . "</label>"; ?>

                            <?php else : ?>

                                <?php echo $form['nstrfile']->renderLabel(__('STR')); ?>
                                <?php if (array_key_exists(AddCandidateForm::NSTR_FILE, $finalLinkHtml)) {
                                    echo $finalLinkHtml[AddCandidateForm::NSTR_FILE];
                                }
                                ?>
                                <?php //echo "<span class=\"fileLink\">".__('Uploaded')."</span>"; ?>

                            <?php endif; ?>
                        </li>
                        <li><hr style="border: 0.4px dashed #727272;"></li>
                        <li>
                            <?php echo $form['nurse_ppni']->renderLabel(__('PPNI Card No.')); ?>
                            <?php echo $form['nurse_ppni']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['nurse_ppniexpire']->renderLabel(__('PPNI Card Expire Date')); ?>
                            <?php echo $form['nurse_ppniexpire']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="fieldHelpContainer">
                            <?php if ($candidateId == "") : ?>

                                <?php echo $form['ppnifile']->renderLabel(__('Upload PPNI Card')); ?>
                                <?php echo $form['ppnifile']->render(array("class " => "duplexBox")); ?>
                                <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_JPEG) . "</label>"; ?>

                            <?php else : ?>

                                <?php echo $form['ppnifile']->renderLabel(__('PPNI Card')); ?>
                                <?php if (array_key_exists(AddCandidateForm::PPNI_FILE, $finalLinkHtml)) {
                                    echo $finalLinkHtml[AddCandidateForm::PPNI_FILE];
                                }
                                ?>
                                <?php //echo "<span class=\"fileLink\">".__('Uploaded')."</span>"; ?>

                            <?php endif; ?>
                        </li>
                        <li><hr style="border: 0.4px dashed #727272;"></li>
                        <li>
                            <?php echo $form['nurse_hiperkes']->renderLabel(__('Hiperkes Certificate')); ?>
                            <?php echo $form['nurse_hiperkes']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li class="fieldHelpContainer">
                            <?php if ($candidateId == "") : ?>

                                <?php echo $form['nhiperkesfile']->renderLabel(__('Upload Hiperkes Certificate')); ?>
                                <?php echo $form['nhiperkesfile']->render(array("class " => "duplexBox")); ?>
                                <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_JPEG) . "</label>"; ?>

                            <?php else : ?>

                                <?php echo $form['nhiperkesfile']->renderLabel(__('Hiperkes Certificate')); ?>
                                <?php if (array_key_exists(AddCandidateForm::NHIPERKES_FILE, $finalLinkHtml)) {
                                    echo $finalLinkHtml[AddCandidateForm::NHIPERKES_FILE];
                                }
                                ?>
                                <?php //echo "<span class=\"fileLink\">".__('Uploaded')."</span>"; ?>

                            <?php endif; ?>
                        </li>
                        <li><hr style="border: 0.4px dashed #727272;"></li>
                        <li>
                            <?php echo $form['nurse_ppgd']->renderLabel(__('PPGD Certificate')); ?>
                            <?php echo $form['nurse_ppgd']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['nurse_ppgdissued']->renderLabel(__('PPGD Certificate Issue Date')); ?>
                            <?php echo $form['nurse_ppgdissued']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['nurse_ppgdexpire']->renderLabel(__('PPGD Certificate Expire Date')); ?>
                            <?php echo $form['nurse_ppgdexpire']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="fieldHelpContainer">
                            <?php if ($candidateId == "") : ?>

                                <?php echo $form['ppgdfile']->renderLabel(__('Upload PPGD Certificate')); ?>
                                <?php echo $form['ppgdfile']->render(array("class " => "duplexBox")); ?>
                                <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_JPEG) . "</label>"; ?>

                            <?php else : ?>

                                <?php echo $form['ppgdfile']->renderLabel(__('PPGD Certificate')); ?>
                                <?php if (array_key_exists(AddCandidateForm::PPGD_FILE, $finalLinkHtml)) {
                                    echo $finalLinkHtml[AddCandidateForm::PPGD_FILE];
                                }
                                ?>
                                <?php //echo "<span class=\"fileLink\">".__('Uploaded')."</span>"; ?>

                            <?php endif; ?>
                        </li>
                    </ol>
                </div>
                <?php if(($candidateId > 0) && ($vacancyStatusTxt != 'APPLICATION INITIATED')){ ?>
                <hr style="border: 0px dashed #727272;">
                <label><span class="keywrd" style="font-weight: bold">ADDITIONAL DATA</span></label>
                <div id="additional">
                <ol>
                    <li>

                    </li>
                    <!--<li>
                        <?php //echo $form['bloodtype']->renderLabel(__('Blood Type ')); ?>
                        <span style="font-weight: bold"><?php //echo ($bloodtype); ?></span>
                    </li>
                    <li>
                        <?php //echo $form['heght']->renderLabel(__('Height ')); ?>
                        <span style="font-weight: bold"><?php //echo ($heght); ?></span>
                    </li>
                    <li>
                        <?php //echo $form['weight']->renderLabel(__('Weight ')); ?>
                        <span style="font-weight: bold"><?php //echo ($weight); ?></span>
                    </li>
                    <li>
                        <?php //echo $form['npwp']->renderLabel(__('NPWP ')); ?>
                        <span style="font-weight: bold"><?php //echo ($npwp); ?></span>
                    </li>
                    <li>
                        <?php //echo $form['bpjsnaker']->renderLabel(__('BPJS Tenaga Kerja ')); ?>
                        <span style="font-weight: bold"><?php //echo ($bpjsnaker); ?></span>
                    </li>
                    <li>
                        <?php //echo $form['bpjskes']->renderLabel(__('BPJS Kesehatan ')); ?>
                        <span style="font-weight: bold"><?php //echo ($bpjskes); ?></span>
                    </li>
                    <li>
                        <?php //echo $form['faskes']->renderLabel(__('Fasilitas Kesehatan ')); ?>
                        <span style="font-weight: bold"><?php //echo ($faskes); ?></span>
                    </li>
                    <li>
                        <?php //echo $form['kk']->renderLabel(__('Kartu Keluarga ')); ?>
                        <span style="font-weight: bold"><?php //echo ($kk); ?></span>
                    </li>
                    <li>
                        <?php //echo $form['familyhead']->renderLabel(__('Kepala Keluarga ')); ?>
                        <span style="font-weight: bold"><?php //echo ($familyhead == 2)? 'Yes':'No'; ?></span>
                    </li>-->
                    <li>
                        <?php echo $form['bloodtype']->renderLabel(__('Blood Type'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['bloodtype']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['heght']->renderLabel(__('Height (cm)'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['heght']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['weight']->renderLabel(__('Weight (kg)'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['weight']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['npwp']->renderLabel(__('NPWP'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['npwp']->render(array("class" => "formInputText")); ?>
                        <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::NUMBER_ALT) . "</label>"; ?>
                    </li>
                    <li>
                        <?php echo $form['bpjsnaker']->renderLabel(__('BPJS Ketenagakerjaan Card Number'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['bpjsnaker']->render(array("class" => "formInputText")); ?>
                        <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::NUMBER_ALT) . "</label>"; ?>
                    </li>
                    <li>
                        <?php echo $form['bpjskes']->renderLabel(__('BPJS Kesehatan Card Number'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['bpjskes']->render(array("class" => "formInputText")); ?>
                        <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::NUMBER_ALT) . "</label>"; ?>
                    </li>
                    <li>
                        <?php echo $form['faskes']->renderLabel(__('Fasilitas Kesehatan'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['faskes']->render(array("cols" => 20, "rows" => 9, "class" => "editabletextarea", "class" => "editabletextarea")); ?>
                    </li>
                    <li>
                        <?php echo $form['kk']->renderLabel(__('Kartu Keluarga Number'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['kk']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['familyhead']->renderLabel(__('As Head of Family (based on Kartu Keluarga) '). ' <span class="required">*</span>'); ?>
                        <?php echo $form['familyhead']->render(array("class"=>"editable")); ?>
                    </li>
                </ol>
                </div>
                <label><span class="keywrd" style="font-weight: bold">EMERGENCY</span></label>
                <div id="emergency">
                    <ol>
                        <li></li>
                        <li>
                            <?php echo $form['emergencyname']->renderLabel(__('Nama'). ' <span class="required">*</span>'); ?>
                            <?php echo $form['emergencyname']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['emergencyaddr']->renderLabel(__('Current Address'). ' <span class="required">*</span>'); ?>
                            <?php echo $form['emergencyaddr']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['emergencyprovince']->renderLabel(__('Province'). ' <span class="required">*</span>'); ?>
                            <?php echo $form['emergencyprovince']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['emergencycity']->renderLabel(__('City'). ' <span class="required">*</span>'); ?>
                            <?php echo $form['emergencycity']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['emergencykecamatan']->renderLabel(__('Kecamatan'). ' <span class="required">*</span>'); ?>
                            <?php echo $form['emergencykecamatan']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['emergencykelurahan']->renderLabel(__('Kelurahan'). ' <span class="required">*</span>'); ?>
                            <?php echo $form['emergencykelurahan']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['emergencyzip']->renderLabel(__('Zip Code'). ' <span class="required">*</span>'); ?>
                            <?php echo $form['emergencyzip']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['emergencyrelation']->renderLabel(__('Hubungan'). ' <span class="required">*</span>'); ?>
                            <?php echo $form['emergencyrelation']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['emergencyphone']->renderLabel(__('Nomor Telpon'). ' <span class="required">*</span>'); ?>
                            <?php echo $form['emergencyphone']->render(array("class" => "formInputText")); ?>
                        </li>
                    </ol>
                </div>
                <hr style="border: 0px dashed #727272;">
                <label><span class="keywrd" style="font-weight: bold">FAMILY</span><span id="familyExtend"> [+]</span></label>
                <div id="family">
                    <ol>
                        <li></li>
                        <li>
                            <?php echo $form['spouse']->renderLabel(__('Spouse Name')); ?>
                            <?php echo $form['spouse']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['spouseage']->renderLabel(__('Spouse Age')); ?>
                            <?php echo $form['spouseage']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['spouseedu']->renderLabel(__('Spouse Education')); ?>
                            <?php echo $form['spouseedu']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['spouseoccupation']->renderLabel(__('Spouse Occupation')); ?>
                            <?php echo $form['spouseoccupation']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['firstchildname']->renderLabel(__('First Child Name')); ?>
                            <?php echo $form['firstchildname']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['firstchildage']->renderLabel(__('First Child Age')); ?>
                            <?php echo $form['firstchildage']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['firstchildedu']->renderLabel(__('First Child Education')); ?>
                            <?php echo $form['firstchildedu']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['firstchildoccupation']->renderLabel(__('First Child Occupation')); ?>
                            <?php echo $form['firstchildoccupation']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['secondchildname']->renderLabel(__('Second Child Name')); ?>
                            <?php echo $form['secondchildname']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['secondchildage']->renderLabel(__('Second Child Age')); ?>
                            <?php echo $form['secondchildage']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['secondchildedu']->renderLabel(__('Second Child Education')); ?>
                            <?php echo $form['secondchildedu']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['secondchildoccupation']->renderLabel(__('Second Child Occupation')); ?>
                            <?php echo $form['secondchildoccupation']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['thirdchildname']->renderLabel(__('Third Child Name')); ?>
                            <?php echo $form['thirdchildname']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['thirdchildage']->renderLabel(__('Third Child Age')); ?>
                            <?php echo $form['thirdchildage']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['thirdchildedu']->renderLabel(__('Third Child Education')); ?>
                            <?php echo $form['thirdchildedu']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['thirdchildoccupation']->renderLabel(__('Third Child Occupation')); ?>
                            <?php echo $form['thirdchildoccupation']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['fathername']->renderLabel(__('Father Name')); ?>
                            <?php echo $form['fathername']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['fatherage']->renderLabel(__('Father Age')); ?>
                            <?php echo $form['fatherage']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['fatheredu']->renderLabel(__('Father Education')); ?>
                            <?php echo $form['fatheredu']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['fatheroccupation']->renderLabel(__('Father Occupation')); ?>
                            <?php echo $form['fatheroccupation']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['mothername']->renderLabel(__('Mother Name')); ?>
                            <?php echo $form['mothername']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['motherage']->renderLabel(__('Mother Age')); ?>
                            <?php echo $form['motherage']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['motheredu']->renderLabel(__('Mother Education')); ?>
                            <?php echo $form['motheredu']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['motheroccupation']->renderLabel(__('Mother Occupation')); ?>
                            <?php echo $form['motheroccupation']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['siblingname1']->renderLabel(__('Brother/Sister Name (1)')); ?>
                            <?php echo $form['siblingname1']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['siblingage1']->renderLabel(__('Brother/Sister Age (1)')); ?>
                            <?php echo $form['siblingage1']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['siblingedu1']->renderLabel(__('Brother/Sister Education (1)')); ?>
                            <?php echo $form['siblingedu1']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['siblingoccupation1']->renderLabel(__('Brother/Sister Occupation (1)')); ?>
                            <?php echo $form['siblingoccupation1']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['siblingname2']->renderLabel(__('Brother/Sister Name (2)')); ?>
                            <?php echo $form['siblingname2']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['siblingage2']->renderLabel(__('Brother/Sister Age (2)')); ?>
                            <?php echo $form['siblingage2']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['siblingedu2']->renderLabel(__('Brother/Sister Education (2)')); ?>
                            <?php echo $form['siblingedu2']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['siblingoccupation2']->renderLabel(__('Brother/Sister Occupation (2)')); ?>
                            <?php echo $form['siblingoccupation2']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['siblingname3']->renderLabel(__('Brother/Sister Name (3)')); ?>
                            <?php echo $form['siblingname3']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['siblingage3']->renderLabel(__('Brother/Sister Age (3)')); ?>
                            <?php echo $form['siblingage3']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['siblingedu3']->renderLabel(__('Brother/Sister Education (3)')); ?>
                            <?php echo $form['siblingedu3']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['siblingoccupation3']->renderLabel(__('Brother/Sister Occupation (3)')); ?>
                            <?php echo $form['siblingoccupation3']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['siblingname4']->renderLabel(__('Brother/Sister Name (4)')); ?>
                            <?php echo $form['siblingname4']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['siblingage4']->renderLabel(__('Brother/Sister Age (4)')); ?>
                            <?php echo $form['siblingage4']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['siblingedu4']->renderLabel(__('Brother/Sister Education (4)')); ?>
                            <?php echo $form['siblingedu4']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['siblingoccupation4']->renderLabel(__('Brother/Sister Occupation (4)')); ?>
                            <?php echo $form['siblingoccupation4']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['siblingname5']->renderLabel(__('Brother/Sister Name (5)')); ?>
                            <?php echo $form['siblingname5']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['siblingage5']->renderLabel(__('Brother/Sister Age (5)')); ?>
                            <?php echo $form['siblingage5']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['siblingedu5']->renderLabel(__('Brother/Sister Education (5)')); ?>
                            <?php echo $form['siblingedu5']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['siblingoccupation5']->renderLabel(__('Brother/Sister Occupation (5)')); ?>
                            <?php echo $form['siblingoccupation5']->render(array("class" => "formInputText")); ?>
                        </li>
                    </ol>
                </div>
                <hr style="border: 0px dashed #727272;">
                <!-- paste here -->

                <label><span class="keywrd" style="font-weight: bold">ORGANIZATIONAL EXPERIENCE</span><span id="orgExtend"> [+]</span></label>
                <div id="org">
                    <ol>
                        <li></li>
                        <li>
                            <?php echo $form['orgname1']->renderLabel(__('Organization Name (1)')); ?>
                            <?php echo $form['orgname1']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['orgyear1']->renderLabel(__('Year')); ?>
                            <?php echo $form['orgyear1']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['orgposition1']->renderLabel(__('Position')); ?>
                            <?php echo $form['orgposition1']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['orgname2']->renderLabel(__('Organization Name (2)')); ?>
                            <?php echo $form['orgname2']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['orgyear2']->renderLabel(__('Year')); ?>
                            <?php echo $form['orgyear2']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['orgposition2']->renderLabel(__('Position')); ?>
                            <?php echo $form['orgposition2']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['orgname3']->renderLabel(__('Organization Name (3)')); ?>
                            <?php echo $form['orgname3']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['orgyear3']->renderLabel(__('Year')); ?>
                            <?php echo $form['orgyear3']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['orgposition3']->renderLabel(__('Position')); ?>
                            <?php echo $form['orgposition3']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['orgname4']->renderLabel(__('Organization Name (4)')); ?>
                            <?php echo $form['orgname4']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['orgyear4']->renderLabel(__('Year')); ?>
                            <?php echo $form['orgyear4']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['orgposition4']->renderLabel(__('Position')); ?>
                            <?php echo $form['orgposition4']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['orgname5']->renderLabel(__('Organization Name (5)')); ?>
                            <?php echo $form['orgname5']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['orgyear5']->renderLabel(__('Year')); ?>
                            <?php echo $form['orgyear5']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['orgposition5']->renderLabel(__('Position')); ?>
                            <?php echo $form['orgposition5']->render(array("class" => "formInputText")); ?>
                        </li>
                    </ol>
                </div>
                <hr style="border: 0px dashed #727272;">
                <label><span class="keywrd" style="font-weight: bold">MEDICAL HISTORY</span><span id="medExtend"> [+]</span></label>
                <div id="medhistory">
                    <p>Choose according to the medical history that has been or is being suffered during the last 3 months.</p>
                    <hr style="border: 0px dashed #727272;">
                    <p style="font-weight: bold">Respiratory tract disease</p>
                    <ol>
                        <li></li>
                        <li class="new radio">
                            <?php echo $form['diphteria']->renderLabel(__('Diphteria')); ?>
                            <?php echo $form['diphteria']->render(array("class"=>"editable")); ?>
                        </li class="new radio">
                        <li class="new radio">
                            <?php echo $form['sinusitis']->renderLabel(__('Sinusitis')); ?>
                            <?php echo $form['sinusitis']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['bronchitis']->renderLabel(__('Bronchitis')); ?>
                            <?php echo $form['bronchitis']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['hemoptoe']->renderLabel(__('Hemoptoe')); ?>
                            <?php echo $form['hemoptoe']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['tbc']->renderLabel(__('TBC')); ?>
                            <?php echo $form['tbc']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['lunginfection']->renderLabel(__('Lung Infection')); ?>
                            <?php echo $form['lunginfection']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['asthma']->renderLabel(__('Asthma')); ?>
                            <?php echo $form['asthma']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['dyspnoea']->renderLabel(__('Dyspnoea')); ?>
                            <?php echo $form['dyspnoea']->render(array("class"=>"editable")); ?>
                        </li>
                    </ol>
                    <p style="font-weight: bold">Kidney and urinary tract disease</p>
                    <ol>
                        <li></li>
                        <li class="new radio">
                            <?php echo $form['urinateproblem']->renderLabel(__('Difficult to urinate')); ?>
                            <?php echo $form['urinateproblem']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['urinatedisorder']->renderLabel(__('Urinary tract disorder')); ?>
                            <?php echo $form['urinatedisorder']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['kidneydisease']->renderLabel(__('Kidney disease')); ?>
                            <?php echo $form['kidneydisease']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['kidneystone']->renderLabel(__('Kidney stone')); ?>
                            <?php echo $form['kidneystone']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['frequenturinate']->renderLabel(__('Frequent urination')); ?>
                            <?php echo $form['frequenturinate']->render(array("class"=>"editable")); ?>
                        </li>
                    </ol>
                    <p style="font-weight: bold">Nervous system disease</p>
                    <ol>
                        <li></li>
                        <li class="new radio">
                            <?php echo $form['meningitis']->renderLabel(__('Meningitis')); ?>
                            <?php echo $form['meningitis']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['cerebralconcussion']->renderLabel(__('Cerebral concussion')); ?>
                            <?php echo $form['cerebralconcussion']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['poliomyelitis']->renderLabel(__('Poliomyelitis')); ?>
                            <?php echo $form['poliomyelitis']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['epilepsy']->renderLabel(__('Epilepsy')); ?>
                            <?php echo $form['epilepsy']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['stroke']->renderLabel(__('Stroke')); ?>
                            <?php echo $form['stroke']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['headache']->renderLabel(__('Headache')); ?>
                            <?php echo $form['headache']->render(array("class"=>"editable")); ?>
                        </li>
                    </ol>
                    <p style="font-weight: bold">Digestive tract disease</p>
                    <ol>
                        <li></li>
                        <li class="new radio">
                            <?php echo $form['typhoid']->renderLabel(__('Typhoid')); ?>
                            <?php echo $form['typhoid']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['bloodvomit']->renderLabel(__('Blood vomiting')); ?>
                            <?php echo $form['bloodvomit']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['obstipation']->renderLabel(__('Obstipation')); ?>
                            <?php echo $form['obstipation']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['dyspepsia']->renderLabel(__('Dyspepsia')); ?>
                            <?php echo $form['dyspepsia']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['jaundice']->renderLabel(__('Jaundice')); ?>
                            <?php echo $form['jaundice']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['gallbladder']->renderLabel(__('Gall bladder disease')); ?>
                            <?php echo $form['gallbladder']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['swallowingdisorder']->renderLabel(__('Swallowing disorder')); ?>
                            <?php echo $form['swallowingdisorder']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['incontinentiaalvi']->renderLabel(__('Incontinentia alvi')); ?>
                            <?php echo $form['incontinentiaalvi']->render(array("class"=>"editable")); ?>
                        </li>
                    </ol>
                    <p style="font-weight: bold">Skin / sexual disesase</p>
                    <ol>
                        <li></li>
                        <li class="new radio">
                            <?php echo $form['chickenpox']->renderLabel(__('Chicken pox')); ?>
                            <?php echo $form['chickenpox']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['fungalskin']->renderLabel(__('Fungal skin disease')); ?>
                            <?php echo $form['fungalskin']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['std']->renderLabel(__('Sexually transmitted disease')); ?>
                            <?php echo $form['std']->render(array("class"=>"editable")); ?>
                        </li>
                    </ol>
                    <p style="font-weight: bold">Heart disease</p>
                    <ol>
                        <li></li>
                        <li class="new radio">
                            <?php echo $form['heartattack']->renderLabel(__('Heart attack')); ?>
                            <?php echo $form['heartattack']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['chestpains']->renderLabel(__('Chest pains')); ?>
                            <?php echo $form['chestpains']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['palpitation']->renderLabel(__('Palpitation')); ?>
                            <?php echo $form['palpitation']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['hypertension']->renderLabel(__('Hypertension')); ?>
                            <?php echo $form['hypertension']->render(array("class"=>"editable")); ?>
                        </li>
                    </ol>
                    <p style="font-weight: bold">Vascular system disease</p>
                    <ol>
                        <li></li>
                        <li class="new radio">
                            <?php echo $form['hemorrhoid']->renderLabel(__('Hemorrhoid')); ?>
                            <?php echo $form['hemorrhoid']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['varicoseveins']->renderLabel(__('Varicose veins')); ?>
                            <?php echo $form['varicoseveins']->render(array("class"=>"editable")); ?>
                        </li>
                    </ol>
                    <p style="font-weight: bold">Glands disease</p>
                    <ol>
                        <li></li>
                        <li class="new radio">
                            <?php echo $form['thyroiddisease']->renderLabel(__('Thyroid disease')); ?>
                            <?php echo $form['thyroiddisease']->render(array("class"=>"editable")); ?>
                        </li>
                    </ol>
                    <p style="font-weight: bold">Join and bones disease</p>
                    <ol>
                        <li></li>
                        <li class="new radio">
                            <?php echo $form['rhematoidsarthritis']->renderLabel(__('Rhematoids arthritis')); ?>
                            <?php echo $form['rhematoidsarthritis']->render(array("class"=>"editable")); ?>
                        </li>
                    </ol>
                    <p style="font-weight: bold">Other disease</p>
                    <ol>
                        <li></li>
                        <li class="new radio">
                            <?php echo $form['foodallergy']->renderLabel(__('Food allergy')); ?>
                            <?php echo $form['foodallergy']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['foodallergylist']->renderLabel(__('Mention it')); ?>
                            <?php echo $form['foodallergylist']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['drugallergy']->renderLabel(__('Drug allergy')); ?>
                            <?php echo $form['drugallergy']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['drugallergylist']->renderLabel(__('Mention it')); ?>
                            <?php echo $form['drugallergylist']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['tetanus']->renderLabel(__('Tetanus')); ?>
                            <?php echo $form['tetanus']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['fainting']->renderLabel(__('Fainting')); ?>
                            <?php echo $form['fainting']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['oblivion']->renderLabel(__('Oblivion')); ?>
                            <?php echo $form['oblivion']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['consentrationdif']->renderLabel(__('Dificulty to consentration')); ?>
                            <?php echo $form['consentrationdif']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['visiondisorder']->renderLabel(__('Vision disorder')); ?>
                            <?php echo $form['visiondisorder']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['hearingdisorder']->renderLabel(__('Hearing disorder')); ?>
                            <?php echo $form['hearingdisorder']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['lumbago']->renderLabel(__('Lumbago')); ?>
                            <?php echo $form['lumbago']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['neoplasm']->renderLabel(__('Neoplasm')); ?>
                            <?php echo $form['neoplasm']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['mentalillness']->renderLabel(__('Mental illness')); ?>
                            <?php echo $form['mentalillness']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['skintuberculose']->renderLabel(__('Skin tuberculose')); ?>
                            <?php echo $form['skintuberculose']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['bonetuberculose']->renderLabel(__('Bone/Other tuberculose')); ?>
                            <?php echo $form['bonetuberculose']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['measles']->renderLabel(__('Measles')); ?>
                            <?php echo $form['measles']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['malaria']->renderLabel(__('Malaria')); ?>
                            <?php echo $form['malaria']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['diabetes']->renderLabel(__('Diabetes')); ?>
                            <?php echo $form['diabetes']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['sleepdisorder']->renderLabel(__('Sleep disorder')); ?>
                            <?php echo $form['sleepdisorder']->render(array("class"=>"editable")); ?>
                        </li>
                    </ol>
                    <ol>
                        <li class="new radio">
                            <?php echo $form['hospitalized']->renderLabel(__('Have you ever been hospitalized?')); ?>
                            <?php echo $form['hospitalized']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['hospitalizeddate']->renderLabel(__('Mention when')); ?>
                            <?php echo $form['hospitalizeddate']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['hospitalizedlength']->renderLabel(__('How long')); ?>
                            <?php echo $form['hospitalizedlength']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['hospitalizedwhy']->renderLabel(__('Why')); ?>
                            <?php echo $form['hospitalizedwhy']->render(array("cols" => 20, "rows" => 9, "class" => "editabletextarea")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['accident']->renderLabel(__('Have you ever had accident?')); ?>
                            <?php echo $form['accident']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['accidentdate']->renderLabel(__('Mention when')); ?>
                            <?php echo $form['accidentdate']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['accidenttxt']->renderLabel(__('What kind of accident')); ?>
                            <?php echo $form['accidenttxt']->render(array("cols" => 20, "rows" => 9, "class" => "editabletextarea")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['surgery']->renderLabel(__('Have you ever been on surgical operation?')); ?>
                            <?php echo $form['surgery']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['surgerydate']->renderLabel(__('Mention when')); ?>
                            <?php echo $form['surgerydate']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['surgerytxt']->renderLabel(__('What kind of surgery')); ?>
                            <?php echo $form['surgerytxt']->render(array("cols" => 20, "rows" => 9, "class" => "editabletextarea")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['smoker']->renderLabel(__('Are you smoker?')); ?>
                            <?php echo $form['smoker']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['smokernumber']->renderLabel(__('Cigarettes per day')); ?>
                            <?php echo $form['smokernumber']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['smokerage']->renderLabel(__('Begin to smoke at age')); ?>
                            <?php echo $form['smokerage']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['smokerstop']->renderLabel(__('Stop smoking in')); ?>
                            <?php echo $form['smokerstop']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['alcohol']->renderLabel(__('Do you drink alcohol?')); ?>
                            <?php echo $form['alcohol']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['alcoholperweek']->renderLabel(__('Drink per week')); ?>
                            <?php echo $form['alcoholperweek']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['alcoholslokyperweek']->renderLabel(__('Sloky per week')); ?>
                            <?php echo $form['alcoholslokyperweek']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['exercise']->renderLabel(__('Do you like to exercise?')); ?>
                            <?php echo $form['exercise']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['exercisetxt']->renderLabel(__('What kind of exercise and exercise per week')); ?>
                            <?php echo $form['exercisetxt']->render(array("cols" => 20, "rows" => 9, "class" => "editabletextarea")); ?>
                        </li>
                        <li>
                            <?php echo $form['medicinelist']->renderLabel(__('What medicines you use frequently?')); ?>
                            <?php echo $form['medicinelist']->render(array("class" => "formInputText")); ?>
                        </li>
                    </ol>
                </div>
                <hr style="border: 0px dashed #727272;">
                <label><span class="keywrd" style="font-weight: bold">VACCINATION HISTORY</span><span id="vacExtend"> [+]</span></label>
                <div id="vachistory">
                    <ol>
                        <li></li>
                        <li class="new radio">
                            <?php echo $form['vacdiptheri']->renderLabel(__('Diptheri')); ?>
                            <?php echo $form['vacdiptheri']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['vactetanus']->renderLabel(__('Tetanus')); ?>
                            <?php echo $form['vactetanus']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['vacpolio']->renderLabel(__('Polio')); ?>
                            <?php echo $form['vacpolio']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['vacbcg']->renderLabel(__('BCG')); ?>
                            <?php echo $form['vacbcg']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['vacmmr']->renderLabel(__('MMR')); ?>
                            <?php echo $form['vacmmr']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['vacinfluenza']->renderLabel(__('Influenza')); ?>
                            <?php echo $form['vacinfluenza']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['vachepatitisa']->renderLabel(__('Hepatitis A')); ?>
                            <?php echo $form['vachepatitisa']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['vachepatitisb']->renderLabel(__('Hepatitis B')); ?>
                            <?php echo $form['vachepatitisb']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['vacvarisela']->renderLabel(__('Varisela')); ?>
                            <?php echo $form['vacvarisela']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['vactyphoid']->renderLabel(__('Typhoid')); ?>
                            <?php echo $form['vactyphoid']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['vachpv']->renderLabel(__('HPV')); ?>
                            <?php echo $form['vachpv']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['vacmeningokok']->renderLabel(__('Meningokok')); ?>
                            <?php echo $form['vacmeningokok']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['vacyellowfever']->renderLabel(__('Yellow fever')); ?>
                            <?php echo $form['vacyellowfever']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="new radio">
                            <?php echo $form['vacjapencephalitis']->renderLabel(__('Japanese encephalitis')); ?>
                            <?php echo $form['vacjapencephalitis']->render(array("class"=>"editable")); ?>
                        </li>
                    </ol>
                </div>
                <hr style="border: 0px dashed #727272;">
                <?php } ?>
                <!--<ol>
                    <li>
                        <?php //echo $form['wantsalary']->renderLabel(__('Expected Salary ')); ?>
                        <span style="font-weight: bold"><?php //echo "Rp " . number_format($wantsalary,2,",","."); ?></span>
                    </li>
                </ol>-->
                <ol>

                    <!-- Resume block : Begins -->

                    <!--<li>-->

                    <!--</li>-->

                    <!-- Resume block : Ends -->

                    <li  class="line">
                        <?php echo $form['vacancy']->renderLabel(__('Job Vacancy') . ' <span class="required">*</span>', array("class" => "vacancyDrpLabel")); ?>
                        <?php echo $form['vacancy']->render(array("class" => "vacancyDrp")); ?>
                        <!--<span style="font-weight: bold"><?php //echo ($vacancyName); ?></span>-->
                    </li>
                    <?php if ($candidateId > 0) : ?>
                    <li>
                        <?php echo $form['vacancy']->renderLabel(__('Status'), array("class" => "vacancyDrpLabel")); ?>
                            <?php $existingVacancyList = $actionForm->candidate->getJobCandidateVacancy(); ?>
                            <?php if ($existingVacancyList[0]->getVacancyId() > 0) : ?>
                        <div id="actionPane" style="float:left; width:600px; padding-top:0px">
                                <?php $i = 0 ?>
                                <?php foreach ($existingVacancyList as $candidateVacancy) {
                                    ?>
                                    <div id="<?php echo $i ?>">
                                        <table>
                                            <tr>
                                                <td style="vertical-align: top"><span class="status" style="font-weight: bold"><?php echo __(ucwords(strtolower($candidateVacancy->getStatus()))); ?></span></td>
                                                <td>
                                                    <?php
                                                    $widgetName = $candidateVacancy->getId();
                                                    echo $actionForm[$widgetName]->render(array("class" => "actionDrpDown"));
                                                    ?>
                                                </td>
                                            </tr>
                                        </table>

                                <!--<span class="status" style="font-weight: bold"><?php //echo __("Status") . ": " . __(ucwords(strtolower($candidateVacancy->getStatus()))); ?></span>-->
                                    <?php
                                }
                                $i++;
                                ?>

                                <?php //} ?>

                            <?php endif; ?>
                    </li>
                    <li>
                        <?php echo $form['keyWords']->renderLabel(__('Online Interview Link'), array("class " => "keywrd")); ?>
                        <a href="<?php echo htmlspecialchars_decode($interviewUrl); ?>"><?php echo $interviewUrl; ?></a>
                    </li>
                    <?php endif; ?>

                    <li>
                        <?php echo $form['keyWords']->renderLabel(__('Keywords'), array("class " => "keywrd")); ?>
                        <?php echo $form['keyWords']->render(array("class" => "keyWords")); ?>
                        <?php //echo ($keyWords); ?>
                    </li>
                    <li>
                        <?php echo $form['comment']->renderLabel(__('Comment'), array("class " => "comment")); ?>
                        <?php echo $form['comment']->render(array("class" => "formInputText", "cols" => 35, "rows" => 4)); ?>
                        <?php //echo ($comment); ?>
                    </li>
                    <li>
                        <?php echo $form['appliedDate']->renderLabel(__('Date of Application'), array("class " => "appDate")); ?>
                        <?php //if ($candidateId > 0) { ?>
                            <!--<span style="font-weight: bold"><?php //echo date('d-m-Y', strtotime($appliedDate)); ?></span>-->
                        <?php //} else { ?>
                            <?php echo $form['appliedDate']->render(array("class" => "formDateInput")); ?>
                        <?php //}?>
                    </li>

                    <li class="required new">
                        <em>*</em> <?php echo __(CommonMessages::REQUIRED_FIELD); ?>
                    </li>
                </ol>
                <ol>
                    <li>
                        <?php
                            if(!empty($psiFileId)){
                                echo $form['psikotest']->renderLabel(__('File Hasil Psikotest'));
                                if (array_key_exists(AddCandidateForm::PSIKOTEST_FILE, $finalLinkHtml)) {
                                    echo $finalLinkHtml[AddCandidateForm::PSIKOTEST_FILE];
                                }
                                //$linkHtmlPsikotest = "<table style='border-spacing: 50px'><tr><td style='padding: 20px; text-align: center;'>";
                                //$linkHtmlPsikotest .= "<div id=\"psiLink\"><a target=\"_blank\" class=\"psiLink\" href=\"";
                                //$linkHtmlPsikotest .= url_for('recruitment/viewCandidateAttachment?attachId=' . $psiFileId);
                                //$linkHtmlPsikotest .= "\"><i class=\"material-icons\" style=\"font-size:48px;\">description</i><br>{$psiFileName}</a></div>";
                                //$linkHtmlPsikotest .= "</td></tr></table>";
                            } else {
                                if($candidateVacancyStatus == "EXTACTION"){
                                    echo $form['psikotest']->renderLabel(__('File Hasil Psikotest'));
                                    echo $form['psikotest']->render(array("class " => "duplexBox"));
                                    echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_DOC) . "</label>";
                                } else {
                                    echo $form['psikotest']->renderLabel(__('File Hasil Psikotest'));
                                    echo "N/A";
                                }
                            }
                                //$linkHtmlPsikotest = "N/A"
                        ?>
                        <?php //echo ($linkHtmlPsikotest); ?>
                    </li>
                    <!--<li id="fileUploadSection" class="noLabel">
                        <?php
                            //echo $form['psikotest']->renderLabel(' ');
                            //echo $form['psikotest']->render(array("class " => "duplexBox"));
                            //echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_DOC) . "</label>";
                        ?>
                    </li>-->
                </ol>
                <ol>
                    <li>
                        <?php
                        if(!empty($kontrakFileId)){
                            echo $form['kontrak']->renderLabel(__('File Dokumen Kontrak'));
                            if (array_key_exists(AddCandidateForm::CONTRACT_FILE, $finalLinkHtml)) {
                                echo $finalLinkHtml[AddCandidateForm::CONTRACT_FILE];
                            }
                            //$linkHtmlKontrak = "<table style='border-spacing: 50px'><tr><td style='padding: 20px; text-align: center;'>";
                            //$linkHtmlKontrak .= "<div id=\"kontrakLink\"><a target=\"_blank\" class=\"kontrakLink\" href=\"";
                            //$linkHtmlKontrak .= url_for('recruitment/viewCandidateAttachment?attachId=' . $kontrakFileId);
                            //$linkHtmlKontrak .= "\"><i class=\"material-icons\" style=\"font-size:48px;\">description</i><br>{$kontrakFileName}</a></div>";
                            //$linkHtmlKontrak .= "</td></tr></table>";
                        } else {
                            echo $form['kontrak']->renderLabel(__('File Dokumen Kontrak'));
                            echo "N/A";
                        }
                            //$linkHtmlKontrak = "N/A"
                        ?>
                        <?php //echo $form['kontrak']->renderLabel(__('File Dokumen Kontrak')); ?>
                        <?php //echo ($linkHtmlKontrak); ?>
                    </li>
                    <!--<li>
                        <?php
                        /*if(!empty($gajiFileId)){
                            $linkHtmlGaji = "<table style='border-spacing: 50px'><tr><td style='padding: 20px; text-align: center;'>";
                            $linkHtmlGaji .= "<div id=\"gajiLink\"><a target=\"_blank\" class=\"gajiLink\" href=\"";
                            $linkHtmlGaji .= url_for('recruitment/viewCandidateAttachment?attachId=' . $gajiFileId);
                            $linkHtmlGaji .= "\"><i class=\"material-icons\" style=\"font-size:48px;\">description</i><br>{$gajiFileName}</a></div>";
                            $linkHtmlGaji .= "</td></tr></table>";
                        } else
                            $linkHtmlGaji = "N/A"*/
                        ?>
                        <?php //echo $form['gaji']->renderLabel(__('File Rincian Gaji')); ?>
                        <?php //echo ($linkHtmlGaji); ?>
                    </li>-->
                </ol>
                <p>

                    <?php if ($edit): ?>
                        <?php if(($candidatePermissions->canCreate() && empty($candidateId)) || ($candidatePermissions->canUpdate() && $candidateId > 0)) {?>
                        <input type="button"id="btnSave" value="<?php echo __("Save"); ?>"/>
                        <?php }?>
                    <?php endif; ?>
                    <?php if ($candidateId > 0): ?>
                        <input type="button" class="cancel" id="btnBack" value="<?php echo __("Back"); ?>"/>
                    <?php endif; ?>

                </p>
            </fieldset>
        </form>
    </div>

</div>

<?php if ($candidateId > 0) : ?>
    <?php $existingVacancyList = $actionForm->candidate->getJobCandidateVacancy(); ?>
    <?php include_component('core', 'ohrmList', $parmetersForListCompoment); ?>
<?php endif; ?>

<!-- Confirmation box - delete HTML: Begins -->
<div class="modal hide" id="deleteConfirmation">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">×</a>
        <h3><?php echo __('OrangeHRM - Confirmation Required'); ?></h3>
    </div>
    <div class="modal-body">
        <p><?php echo __(CommonMessages::DELETE_CONFIRMATION); ?></p>
    </div>
    <div class="modal-footer">
        <input type="button" class="btn" data-dismiss="modal" id="dialogDeleteBtn" value="<?php echo __('Ok'); ?>" />
        <input type="button" class="btn reset" data-dismiss="modal" value="<?php echo __('Cancel'); ?>" />
    </div>
</div>
<!-- Confirmation box HTML: Ends -->

<!-- Confirmation box - remove vacancies & save HTML: Begins -->
<div class="modal hide" id="deleteConfirmationForSave">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">×</a>
        <h3><?php echo __('OrangeHRM - Confirmation Required'); ?></h3>
    </div>
    <div class="modal-body">
        <p><?php echo __("This action will remove previous vacancy"); ?></p>
        <br>
        <p><?php echo __('Remove?'); ?></p>
    </div>
    <div class="modal-footer">
        <input type="button" class="btn" data-dismiss="modal" id="dialogSaveButton" value="<?php echo __('Ok'); ?>" />
        <input type="button" class="btn reset" data-dismiss="modal" id="dialogCancelButton" value="<?php echo __('Cancel'); ?>" />
    </div>
</div>
<!-- Confirmation box remove vacancies & save HTML: Ends -->
<?php }?>
<script type="text/javascript">
    //<![CDATA[
    var datepickerDateFormat = '<?php echo get_datepicker_date_format($sf_user->getDateFormat()); ?>';
    var lang_firstNameRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_lastNameRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_emailRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_validDateMsg = '<?php echo __(ValidationMessages::DATE_FORMAT_INVALID, array('%format%' => str_replace('yy', 'yyyy', get_datepicker_date_format($sf_user->getDateFormat())))) ?>';
    var lang_validEmail = '<?php echo __(ValidationMessages::EMAIL_INVALID); ?>';
    var list = <?php echo json_encode($allVacancylist); ?>;
    var allowedVacancylistWithClosedVacancies = <?php echo json_encode($allowedVacancylistWithClosedVacancies); ?>;
    var allowedVacancylist = <?php echo json_encode($allowedVacancylist); ?>;
    var allowedVacancyIdArray = <?php echo json_encode($allowedVacancyIdArray); ?>;
    var closedVacancyIdArray = <?php echo json_encode($closedVacancyIdArray); ?>;
    var lang_identical_rows = "<?php echo __('Cannot assign same vacancy twice'); ?>";
    var lang_tooLargeInput = "<?php echo __(ValidationMessages::TEXT_LENGTH_EXCEEDS, array('%amount%' => 30)); ?>";
    var lang_commaSeparated = "<?php echo __('Enter comma separated words') . '...'; ?>";
    var currentDate = '<?php echo set_datepicker_date_format(date("Y-m-d")); ?>';
    var lang_dateValidation = "<?php echo __("Should be less than current date"); ?>";
    var lang_validPhoneNo = "<?php echo __(ValidationMessages::TP_NUMBER_INVALID); ?>";
    var lang_noMoreThan250 = "<?php echo __(ValidationMessages::TEXT_LENGTH_EXCEEDS, array('%amount%' => 250)); ?>";
    var lang_edit = "<?php echo __("Edit"); ?>";
    var lang_save = "<?php echo __("Save"); ?>";
    var lang_cancel = "<?php echo __("Cancel"); ?>";
    var candidateId = "<?php echo $candidateId; ?>";
    var attachment = "<?php echo $attachmentExists; ?>"
    var changeStatusUrl = '<?php echo url_for('recruitment/changeCandidateVacancyStatus?'); ?>';
    var backBtnUrl = '<?php echo url_for('recruitment/viewCandidates?'); ?>';
    var cancelBtnUrl = '<?php echo url_for('recruitment/addCandidate?'); ?>';
    var interviewUrl = '<?php echo url_for('recruitment/jobInterview?'); ?>';
    var interviewAction = '<?php echo WorkflowStateMachine::RECRUITMENT_APPLICATION_ACTION_SHEDULE_INTERVIEW; ?>';
    var interviewAction2 = '<?php echo WorkflowStateMachine::RECRUITMENT_APPLICATION_ACTION_SHEDULE_2ND_INTERVIEW; ?>';
    var removeAction = '<?php echo JobCandidateVacancy::REMOVE; ?>';
    var lang_remove =  '<?php echo __("Remove"); ?>';
    var lang_editCandidateTitle = "<?php echo __('Edit Candidate'); ?>";
    var editRights = "<?php echo $edit; ?>";
    var activeStatus = "<?php echo JobCandidate::ACTIVE; ?>";
    var candidateStatus = "<?php echo $candidateStatus; ?>";
    var invalidFile = "<?php echo $invalidFile; ?>";
    var candidateVacancyStatus = "<?php echo $candidateVacancyStatus; ?>";
    var psiFileId = "<?php echo $psiFileId;?>";
    var lang_Strexpire = "<?php echo(ValidationMessages::STR_EXPIRE);?>";
    var lang_genderRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_birthPlaceRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_dobRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_currentAddrRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_currentCityRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_currentProvinceRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_currentZipRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_permanentAddrRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_permanentCityRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_permanentProvinceRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_permanentZipRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_religionRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_maritalRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_phoneRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_mobileRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_ektpRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_resumeRequired = "<?php echo __(ValidationMessages::REQUIRED); ?>";
    var lang_WantsalaryRequired = "<?php echo(ValidationMessages::REQUIRED);?>";
    var lang_WantsalaryNumber = "<?php echo(ValidationMessages::VALID_NUMBER);?>";
    var lang_bloodTypeRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_heightRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_weightRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_npwpRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_bpjsNakerRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_bpjsKesRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_faskesRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_kkRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_familyHeadRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_EmergencyRequired = "<?php echo(ValidationMessages::REQUIRED);?>";
    var lang_vacancyRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var dateFormat = "<?php echo $dateFormat?>";

    //Eka: add ajax for retrieving nomornet data
    var provinsi = "";
    var city = "";
    var kecamatan = "";
    var desaKodepos = {};

    //Eka: address domisili
    var domProvinsi = "";
    var domCity = "";
    var domKecamatan = "";
    var domDesaKodepos = {};

    //var nomornetHost = "http://localhost:8880";
    <?php
    //getting server's ip addr
        $host= gethostname();
        $ip_server = gethostbyname($host);
    //$ip_server = $_SERVER['SERVER_ADDR'];
        $nomornetHost = "http://" . $ip_server . "/nomornet_api";
    ?>
    var nomornetHost = "<?php echo $nomornetHost ?>";
    //var nomornetHost = "http://localhost/nomornet_api";
    //var nomornetHost = "http://118.97.180.122:8880";

    //current address
    $("#addCandidate_currentprovince").change(function (){
        //alert($(this).val());
        provinsi = $(this).val();
        var $dropdown = $("#addCandidate_currentkecamatan");
        $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
        var $dropdown2 = $("#addCandidate_currentkelurahan");
        $dropdown2.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
        $("#addCandidate_currentzip").val("");
        $.ajax({
            type:"GET",
            dataType: "json",
            url: nomornetHost + "/nomornet/kotakabupaten/" + provinsi,
            success: function(result){
                console.log(result);
                resultArray = result['records'];
                var $dropdown = $("#addCandidate_currentcity");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
                for(var i in resultArray){
                    console.log(resultArray[i].kotakabupaten);
                    $dropdown.append($("<option />").val(resultArray[i].kotakabupaten).text(resultArray[i].kotakabupaten));
                }
                //alert(result['records'][0].kotakabupaten);
            },
            error: function(jqxhrTxt, statusTxt, errTxt){
                console.log(statusTxt);
                var $dropdown = $("#addCandidate_currentcity");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
            }
        })
    });

    $("#addCandidate_currentcity").change(function (){
        //alert($(this).val());
        provinsi = $("#addCandidate_currentprovince").val();
        city = $(this).val();
        var $dropdown = $("#addCandidate_currentkelurahan");
        $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
        $("#addCandidate_currentzip").val("");
        $.ajax({
            type:"GET",
            dataType: "json",
            url: nomornetHost + "/nomornet/kecamatan/" + provinsi + "/" + city,
            success: function(result){
                console.log(result);
                resultArray = result['records'];
                var $dropdown = $("#addCandidate_currentkecamatan");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
                for(var i in resultArray){
                    console.log(resultArray[i].kecamatan);
                    $dropdown.append($("<option />").val(resultArray[i].kecamatan).text(resultArray[i].kecamatan));
                }
                //alert(result['records'][0].kotakabupaten);
            },
            error: function(jqxhrTxt, statusTxt, errTxt){
                console.log(statusTxt);
                var $dropdown = $("#addCandidate_currentkecamatan");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
            }
        })
    });

    $("#addCandidate_currentkecamatan").change(function (){
        //alert($(this).val());
        provinsi = $("#addCandidate_currentprovince").val();
        city = $("#addCandidate_currentcity").val();
        kecamatan = $(this).val();
        $("#addCandidate_currentzip").val("");
        $.ajax({
            type:"GET",
            dataType: "json",
            url: nomornetHost + "/nomornet/kodeposdesa/" + provinsi + "/" + city + "/" + kecamatan,
            success: function(result){
                console.log(result);
                resultArray = result['records'];
                var $dropdown = $("#addCandidate_currentkelurahan");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
                for(var i in resultArray){
                    console.log(resultArray[i].desakelurahan + " - " + resultArray[i].kodepos);
                    $dropdown.append($("<option />").val(resultArray[i].desakelurahan).text(resultArray[i].desakelurahan));
                    desaKodepos[resultArray[i].desakelurahan] = resultArray[i].kodepos;
                }
                //alert(result['records'][0].kotakabupaten);
            },
            error: function(jqxhrTxt, statusTxt, errTxt){
                console.log(statusTxt);
                var $dropdown = $("#addCandidate_currentkelurahan");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
            }
        })
    });

    $("#addCandidate_currentkelurahan").change(function (){
        //alert($(this).val());
        kelurahan = $(this).val();
        $("#addCandidate_currentzip").val(desaKodepos[kelurahan]);
    });

    //permanent address
    $("#addCandidate_permanentprovince").change(function (){
        //alert($(this).val());
        provinsi = $(this).val();
        var $dropdown = $("#addCandidate_permanentkecamatan");
        $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
        var $dropdown2 = $("#addCandidate_permanentkelurahan");
        $dropdown2.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
        $("#addCandidate_permanentzip").val("");
        $.ajax({
            type:"GET",
            dataType: "json",
            url: nomornetHost + "/nomornet/kotakabupaten/" + provinsi,
            success: function(result){
                console.log(result);
                resultArray = result['records'];
                var $dropdown = $("#addCandidate_permanentcity");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
                for(var i in resultArray){
                    console.log(resultArray[i].kotakabupaten);
                    $dropdown.append($("<option />").val(resultArray[i].kotakabupaten).text(resultArray[i].kotakabupaten));
                }
                //alert(result['records'][0].kotakabupaten);
            },
            error: function(jqxhrTxt, statusTxt, errTxt){
                console.log(statusTxt);
                var $dropdown = $("#addCandidate_permanentcity");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
            }
        })
    });

    $("#addCandidate_permanentcity").change(function (){
        //alert($(this).val());
        provinsi = $("#addCandidate_permanentprovince").val();
        city = $(this).val();
        var $dropdown = $("#addCandidate_permanentkelurahan");
        $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
        $("#addCandidate_permanentzip").val("");
        $.ajax({
            type:"GET",
            dataType: "json",
            url: nomornetHost + "/nomornet/kecamatan/" + provinsi + "/" + city,
            success: function(result){
                console.log(result);
                resultArray = result['records'];
                var $dropdown = $("#addCandidate_permanentkecamatan");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
                for(var i in resultArray){
                    console.log(resultArray[i].kecamatan);
                    $dropdown.append($("<option />").val(resultArray[i].kecamatan).text(resultArray[i].kecamatan));
                }
                //alert(result['records'][0].kotakabupaten);
            },
            error: function(jqxhrTxt, statusTxt, errTxt){
                console.log(statusTxt);
                var $dropdown = $("#addCandidate_permanentkecamatan");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
            }
        })
    });

    $("#addCandidate_permanentkecamatan").change(function (){
        //alert($(this).val());
        provinsi = $("#addCandidate_permanentprovince").val();
        city = $("#addCandidate_permanentcity").val();
        kecamatan = $(this).val();
        $("#addCandidate_permanentzip").val("");
        $.ajax({
            type:"GET",
            dataType: "json",
            url: nomornetHost + "/nomornet/kodeposdesa/" + provinsi + "/" + city + "/" + kecamatan,
            success: function(result){
                console.log(result);
                resultArray = result['records'];
                var $dropdown = $("#addCandidate_permanentkelurahan");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
                for(var i in resultArray){
                    console.log(resultArray[i].desakelurahan + " - " + resultArray[i].kodepos);
                    $dropdown.append($("<option />").val(resultArray[i].desakelurahan).text(resultArray[i].desakelurahan));
                    desaKodepos[resultArray[i].desakelurahan] = resultArray[i].kodepos;
                }
                //alert(result['records'][0].kotakabupaten);
            },
            error: function(jqxhrTxt, statusTxt, errTxt){
                console.log(statusTxt);
                var $dropdown = $("#addCandidate_permanentkelurahan");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
            }
        })
    });

    $("#addCandidate_permanentkelurahan").change(function (){
        //alert($(this).val());
        kelurahan = $(this).val();
        $("#addCandidate_permanentzip").val(desaKodepos[kelurahan]);
    });
//from applyVacancySuccess
    $(document).ready(function() {
        /*$("#uploaded").hide();
        if(candidateId > 0) {
            $(".formInputText").attr('disabled', 'disabled');
            $(".formInput").attr('disabled', 'disabled');
            $(".contactNo").attr('disabled', 'disabled');
            $(".keyWords").attr('disabled', 'disabled');
            $("#cvHelp").hide();
            $("#uploaded").show();
            $("#btnSave").hide();
        }*/
        var isCollapse = false;
        $("#txtArea").attr('disabled', 'disabled');
        $("#txtArea").hide();

        $('#extend').click(function(){
            if(!isCollapse){
                $("#txtArea").show();
                isCollapse = true;
                $('#extend').text('[-]');
            } else {
                $("#txtArea").hide();
                isCollapse = false;
                $('#extend').text('[+]');
            }
        });

        var isEduCollapse = false;
        $("#education").attr('disabled', 'disabled');
        $("#education").hide();

        $('#eduExtend').click(function(){
            if(!isEduCollapse){
                $("#education").show();
                isEduCollapse = true;
                $('#eduExtend').text('[-]');
            } else {
                $("#education").hide();
                isEduCollapse = false;
                $('#eduExtend').text('[+]');
            }
        });

        var isWeCollapse = false;
        $("#we").attr('disabled', 'disabled');
        $("#we").hide();

        $('#weExtend').click(function(){
            if(!isWeCollapse){
                $("#we").show();
                isWeCollapse = true;
                $('#weExtend').text(' [-]');
            } else {
                $("#we").hide();
                isWeCollapse = false;
                $('#weExtend').text(' [+]');
            }
        });

        var isDocCollapse = false;
        $("#doctor").attr('disabled', 'disabled');
        $("#doctor").hide();

        $('#docExtend').click(function(){
            if(!isDocCollapse){
                $("#doctor").show();
                isDocCollapse = true;
                $('#docExtend').text(' [-]');
            } else {
                $("#doctor").hide();
                isDocCollapse = false;
                $('#docExtend').text(' [+]');
            }
        });

        var isNurseCollapse = false;
        $("#nurse").attr('disabled', 'disabled');
        $("#nurse").hide();

        $('#nurseExtend').click(function(){
            if(!isNurseCollapse){
                $("#nurse").show();
                isNurseCollapse = true;
                $('#nurseExtend').text(' [-]');
            } else {
                $("#nurse").hide();
                isNurseCollapse = false;
                $('#nurseExtend').text(' [+]');
            }
        });

        /*var isEmergencyCollapse = false;
        $("#emergency").attr('disabled', 'disabled');
        $("#emergency").hide();

        $('#emergencyExtend').click(function(){
            if(!isEmergencyCollapse){
                $("#emergency").show();
                isEmergencyCollapse = true;
                $('#emergencyExtend').text(' [-]');
            } else {
                $("#emergency").hide();
                isEmergencyCollapse = false;
                $('#emergencyExtend').text(' [+]');
            }
        });*/

        var isFamilyCollapse = false;
        $("#family").attr('disabled', 'disabled');
        $("#family").hide();

        $('#familyExtend').click(function(){
            if(!isFamilyCollapse){
                $("#family").show();
                isFamilyCollapse = true;
                $('#familyExtend').text(' [-]');
            } else {
                $("#family").hide();
                isFamilyCollapse = false;
                $('#familyExtend').text(' [+]');
            }
        });

        var isOrgCollapse = false;
        $("#org").attr('disabled', 'disabled');
        $("#org").hide();

        $('#orgExtend').click(function(){
            if(!isOrgCollapse){
                $("#org").show();
                isOrgCollapse = true;
                $('#orgExtend').text(' [-]');
            } else {
                $("#org").hide();
                isOrgCollapse = false;
                $('#orgExtend').text(' [+]');
            }
        });

        var isMedCollapse = false;
        $("#medhistory").attr('disabled', 'disabled');
        $("#medhistory").hide();

        $('#medExtend').click(function(){
            if(!isMedCollapse){
                $("#medhistory").show();
                isMedCollapse = true;
                $('#medExtend').text(' [-]');
            } else {
                $("#medhistory").hide();
                isMedCollapse = false;
                $('#medExtend').text(' [+]');
            }
        });

        var isVacCollapse = false;
        $("#vachistory").attr('disabled', 'disabled');
        $("#vachistory").hide();

        $('#vacExtend').click(function(){
            if(!isVacCollapse){
                $("#vachistory").show();
                isVacCollapse = true;
                $('#vacExtend').text(' [-]');
            } else {
                $("#vachistory").hide();
                isVacCollapse = false;
                $('#vacExtend').text(' [+]');
            }
        });

        /*$('#btnSave').click(function() {

            if(isValidForm()){
                $('#addCandidate_vacancyList').val(vacancyId);
                $('#addCandidate_keyWords.inputFormatHint').val('');
                $('form#frmAddCandidate').attr({
                    action:linkForApplyVacancy+"?id="+vacancyId
                });
                $('form#frmAddCandidate').submit();
            }
        });*/


        $('#backLink').click(function(){
            window.location.replace(linkForViewJobs);
        });
        if ($("#addCandidate_keyWords").val() == '') {
            $("#addCandidate_keyWords").val(lang_commaSeparated).addClass("inputFormatHint");
        }

        $("#addCandidate_keyWords").one('focus', function() {

            if ($(this).hasClass("inputFormatHint")) {
                $(this).val("");
                $(this).removeClass("inputFormatHint");
            }
        });

        /*if(candidateId > 0){
            var photo = document.getElementById('addCandidate_photo');
            photo.style.display = "none";
        }*/

    });

    //emergency address
    $("#addCandidate_emergencyprovince").change(function (){
        //alert($(this).val());
        provinsi = $(this).val();
        var $dropdown = $("#addCandidate_emergencykecamatan");
        $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
        var $dropdown2 = $("#addCandidate_emergencykelurahan");
        $dropdown2.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
        $("#addCandidate_emergencyzip").val("");
        $.ajax({
            type:"GET",
            dataType: "json",
            url: nomornetHost + "/nomornet/kotakabupaten/" + provinsi,
            success: function(result){
                console.log(result);
                resultArray = result['records'];
                var $dropdown = $("#addCandidate_emergencycity");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
                for(var i in resultArray){
                    console.log(resultArray[i].kotakabupaten);
                    $dropdown.append($("<option />").val(resultArray[i].kotakabupaten).text(resultArray[i].kotakabupaten));
                }
                //alert(result['records'][0].kotakabupaten);
            },
            error: function(jqxhrTxt, statusTxt, errTxt){
                console.log(statusTxt);
                var $dropdown = $("#addCandidate_emergencycity");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
            }
        })
    });

    $("#addCandidate_emergencycity").change(function (){
        //alert($(this).val());
        provinsi = $("#addCandidate_emergencyprovince").val();
        city = $(this).val();
        var $dropdown = $("#addCandidate_emergencykelurahan");
        $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
        $("#addCandidate_emergencyzip").val("");
        $.ajax({
            type:"GET",
            dataType: "json",
            url: nomornetHost + "/nomornet/kecamatan/" + provinsi + "/" + city,
            success: function(result){
                console.log(result);
                resultArray = result['records'];
                var $dropdown = $("#addCandidate_emergencykecamatan");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
                for(var i in resultArray){
                    console.log(resultArray[i].kecamatan);
                    $dropdown.append($("<option />").val(resultArray[i].kecamatan).text(resultArray[i].kecamatan));
                }
                //alert(result['records'][0].kotakabupaten);
            },
            error: function(jqxhrTxt, statusTxt, errTxt){
                console.log(statusTxt);
                var $dropdown = $("#addCandidate_emergencykecamatan");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
            }
        })
    });

    $("#addCandidate_emergencykecamatan").change(function (){
        //alert($(this).val());
        provinsi = $("#addCandidate_emergencyprovince").val();
        city = $("#addCandidate_emergencycity").val();
        kecamatan = $(this).val();
        $("#addCandidate_emergencyzip").val("");
        $.ajax({
            type:"GET",
            dataType: "json",
            url: nomornetHost + "/nomornet/kodeposdesa/" + provinsi + "/" + city + "/" + kecamatan,
            success: function(result){
                console.log(result);
                resultArray = result['records'];
                var $dropdown = $("#addCandidate_emergencykelurahan");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
                for(var i in resultArray){
                    console.log(resultArray[i].desakelurahan + " - " + resultArray[i].kodepos);
                    $dropdown.append($("<option />").val(resultArray[i].desakelurahan).text(resultArray[i].desakelurahan));
                    desaKodepos[resultArray[i].desakelurahan] = resultArray[i].kodepos;
                }
                //alert(result['records'][0].kotakabupaten);
            },
            error: function(jqxhrTxt, statusTxt, errTxt){
                console.log(statusTxt);
                var $dropdown = $("#addCandidate_emergencykelurahan");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
            }
        })
    });

    $("#addCandidate_emergencykelurahan").change(function (){
        //alert($(this).val());
        kelurahan = $(this).val();
        $("#addCandidate_emergencyzip").val(desaKodepos[kelurahan]);
    });

</script>
