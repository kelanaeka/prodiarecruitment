<?php

/**
 * OrangeHRM is a comprehensive Human Resource Management (HRM) System that captures
 * all the essential functionalities required for any enterprise.
 * Copyright (C) 2006 OrangeHRM Inc., http://www.orangehrm.com
 *
 * OrangeHRM is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * OrangeHRM is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA
 *
 */
class changeCandidateVacancyStatusAction extends baseRecruitmentAction {

    private $performedAction;
    public $interviewDate;
    public $interviewNote;

    /**
     * @param sfForm $form
     * @return
     */
    public function setForm(sfForm $form) {
        if (is_null($this->form)) {
            $this->form = $form;
        }
    }

    /**
     *
     * @return <type>
     */
    public function getCandidateService() {
        if (is_null($this->candidateService)) {
            $this->candidateService = new CandidateService();
            $this->candidateService->setCandidateDao(new CandidateDao());
        }
        return $this->candidateService;
    }

    /**
     *
     * @return <type>
     */
    public function getInterviewService() {
        if (is_null($this->interviewService)) {
            $this->interviewService = new JobInterviewService();
            $this->interviewService->setJobInterviewDao(new JobInterviewDao());
        }
        return $this->interviewService;
    }

    public function sendNotification($emailAddr, $message, $subject){
        //$smtp2goUrl = "https://api.smtp2go.com/v3/email/send";
        $smtpBbUrl = "https://api.thebigbox.id/mail-sender/0.0.2/mails";

        /*$emailBodyArray = array(
            "api_key" => "api-CAF05D60704811EA8980F23C91C88F4E",
            "sender" => "kelanaeka73@nomornet.info",
            "to" => array($emailAddr),
            "subject" => $subject,
            "html_body" => "",
            "text_body" => $message
        );*/
        $emailBodyArray = array(
            "subject" => $subject,
            "message" => $message,
            "recipient" => $emailAddr
        );

        //$emailBodyJson = json_encode($emailBodyArray);
        $emailBodyUrlEncoded = http_build_query($emailBodyArray);

        //$curl = curl_init($smtp2goUrl);
        $curl = curl_init($smtpBbUrl);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        //curl_setopt($curl, CURLOPT_POSTFIELDS, $emailBodyJson);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $emailBodyUrlEncoded);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded',
            'accept: application/json',
            'x-api-key: Xy9q3r34G6ztzfxrUVG9ralextx1rkEQ'
        ));
        $result = curl_exec($curl);
        $errno = curl_errno($curl);
        if($errno){
            die("Connection Failure");
        }
        curl_close($curl);
        return $result;
    }

    /**
     *
     * @param <type> $request
     */
    public function execute($request) {

        /* For highlighting corresponding menu item */
        $request->setParameter('initialActionName', 'viewCandidates');
        $userRoleManager = $this->getContext()->getUserRoleManager();

        $this->candidatePermissions = $this->getDataGroupPermissions('recruitment_candidates');

        $usrObj = $this->getUser()->getAttribute('user');
        if (!($usrObj->isAdmin() || $usrObj->isHiringManager() || $usrObj->isInterviewer() || $this->candidatePermissions->canRead())) {
            $this->redirect('pim/viewPersonalDetails');
        }
        $allowedCandidateList = $usrObj->getAllowedCandidateList();
        $allowedVacancyList = $usrObj->getAllowedVacancyList();
        $allowedCandidateListToDelete = $usrObj->getAllowedCandidateListToDelete();
        $this->enableEdit = true;
        if ($this->getUser()->hasFlash('templateMessage')) {
            list($this->messageType, $this->message) = $this->getUser()->getFlash('templateMessage');
        }

        $id = $request->getParameter('id');
        $this->id = $id;
        if (!empty($id)) {
            $history = $this->getCandidateService()->getCandidateHistoryById($id);
            $action = $history->getAction();
            
            // check if user can perform this history action
            $allowedStates = $userRoleManager->getActionableStates(WorkflowStateMachine::FLOW_RECRUITMENT, 
                    array($action));
            if (!empty($allowedStates)) {
                $this->candidatePermissions = new ResourcePermission(true, true, true, true);
            }

            $this->interviewId = $history->getInterviewId();
            //if(!empty($this->interviewId)){
            //    $this->interviewDate = $this->getInterviewService()->getInterviewById($this->interviewId)->getInterviewDate();
            //    $this->interviewNote = $this->getInterviewService()->getInterviewById($this->interviewId)->getNote();
            //}

            if ($action == WorkflowStateMachine::RECRUITMENT_APPLICATION_ACTION_SHEDULE_INTERVIEW || $action == WorkflowStateMachine::RECRUITMENT_APPLICATION_ACTION_SHEDULE_2ND_INTERVIEW) {
                if ($this->getUser()->hasFlash('templateMessage')) {
                    list($this->messageType, $this->message) = $this->getUser()->getFlash('templateMessage');
                    $this->getUser()->setFlash($this->messageType, $this->message);
                }
                $this->redirect('recruitment/jobInterview?historyId=' . $id . '&interviewId=' . $this->interviewId);
            }
            $this->performedAction = $action;
            if ($this->getCandidateService()->isInterviewer($this->getCandidateService()->getCandidateVacancyByCandidateIdAndVacancyId($history->getCandidateId(), $history->getVacancyId()), $usrObj->getEmployeeNumber())) {
                $this->enableEdit = false;
                if ($action == WorkflowStateMachine::RECRUITMENT_APPLICATION_ACTION_MARK_INTERVIEW_PASSED || $action == WorkflowStateMachine::RECRUITMENT_APPLICATION_ACTION_MARK_INTERVIEW_FAILED) {
                    $this->enableEdit = true;
                }
            }
            
        }

        $candidateVacancyId = $request->getParameter('candidateVacancyId');
        $this->selectedAction = $request->getParameter('selectedAction');
        $param = array();
        if ($id > 0) {
            $param = array('id' => $id, 'candidatePermissions' => $this->candidatePermissions);
        }
        if ($candidateVacancyId > 0 && $this->selectedAction != "") {

            $candidateVacancy = $this->getCandidateService()->getCandidateVacancyById($candidateVacancyId);
            $nextActionList = $this->getCandidateService()->getNextActionsForCandidateVacancy($candidateVacancy->getStatus(), $usrObj);
            if ($nextActionList[$this->selectedAction] == "" || !in_array($candidateVacancy->getCandidateId(), $allowedCandidateList)) {
                $this->redirect('recruitment/viewCandidates');
            }
            
            // check if user can perform action on candidate
            $actionAllowed = $userRoleManager->isActionAllowed(WorkflowStateMachine::FLOW_RECRUITMENT, 
                    $candidateVacancy->getStatus(), $this->selectedAction);
           
            if ($actionAllowed) {
                $this->candidatePermissions = new ResourcePermission(true, true, true, false);
            }
            
            $param = array('candidateVacancyId' => $candidateVacancyId, 'selectedAction' => $this->selectedAction, 'candidatePermissions' => $this->candidatePermissions);
            $this->performedAction = $this->selectedAction;
        }


        $this->setForm(new CandidateVacancyStatusForm(array(), $param, true));
//        if (!in_array($this->form->candidateId, $allowedCandidateList) && !in_array($this->form->vacancyId, $allowedVacancyList)) {
//            $this->redirect('recruitment/viewCandidates');
//        }
//        if (!in_array($this->form->candidateId, $allowedCandidateListToDelete)) {
//            $this->enableEdit = false;
//        }        
        if ($request->isMethod('post')) {
            if ($this->candidatePermissions->canUpdate()) {
                if($this->selectedAction == WorkflowStateMachine::RECRUITMENT_APPLICATION_ACTION_HIRE)
                    $this->form->bind($request->getParameter($this->form->getName()), $request->getFiles($this->form->getName()));
                else
                    $this->form->bind($request->getParameter($this->form->getName()));

                if ($this->form->isValid()) {
                    $candidateId = $this->form->candidateId;
                    $applicationId = $this->form->candidateVacancyId;
                    $result = $this->form->performAction();

                    //send email for shortlist action
                    if($this->selectedAction == WorkflowStateMachine::RECRUITMENT_APPLICATION_ACTION_SHORTLIST){
                        $candidate = $this->getCandidateService()->getCandidateById($candidateId);
                        $subject = "Aplikasi Anda Dengan ID 0" . $applicationId . " Telah Terpilih";
                        $message = "Selamat! Aplikasi anda telah kami pilih untuk masuk ke tahap selanjutnya. Silahkan isi data lanjutan pelamar kerja dengan memasukkan ID Aplikasi anda.";
                        $this->sendNotification($candidate->getEmail(), $message, $subject);
                    }

                    //send email for interview passed
                    if ($this->selectedAction == WorkflowStateMachine::RECRUITMENT_APPLICATION_ACTION_MARK_INTERVIEW_PASSED){
                        $candidate = $this->getCandidateService()->getCandidateById($candidateId);
                        $subject = "Hasil Interview ID 0" . $applicationId;
                        $message = "Selamat! Berdasarkan hasil interview, Anda telah kami pilih untuk diterima sebagai pegawai. Kami akan segera menghubungi Anda untuk proses selanjutnya.";
                        $this->sendNotification($candidate->getEmail(), $message, $subject);
                    }

                    //send email for interview failed
                    if ($this->selectedAction == WorkflowStateMachine::RECRUITMENT_APPLICATION_ACTION_MARK_INTERVIEW_FAILED) {
                        $candidate = $this->getCandidateService()->getCandidateById($candidateId);
                        $subject = "Hasil Interview ID 0" . $applicationId;
                        $message = "Mohon maaf. Berdasarkan hasil interview, kami belum dapat melanjutkan proses penerimaan Anda sebagai pegawai. Tetap optimis, kami akan menginformasikan bila ada peluang lainnya untuk Anda dapat bekerja bersama kami.";
                        $this->sendNotification($candidate->getEmail(), $message, $subject);
                    }

                    //send email for rejected
                    if ($this->selectedAction == WorkflowStateMachine::RECRUITMENT_APPLICATION_ACTION_REJECT) {
                        $candidate = $this->getCandidateService()->getCandidateById($candidateId);
                        $subject = "Aplikasi Anda Dengan ID 0" . $applicationId . " Ditolak";
                        $message = "Mohon maaf. Kami belum dapat melanjutkan proses penerimaan Anda sebagai pegawai. Tetap optimis, kami akan menginformasikan bila ada peluang lainnya untuk Anda dapat bekerja bersama kami.";
                        $this->sendNotification($candidate->getEmail(), $message, $subject);
                    }

                    if (isset($result['messageType'])) {
                        $this->getUser()->setFlash($result['messageType'], $result['message']);
                    } else {
                        $message = __(TopLevelMessages::UPDATE_SUCCESS);
                        $this->getUser()->setFlash('success', $message);
                    }
                    $this->redirect('recruitment/changeCandidateVacancyStatus?id=' . $this->form->historyId);
                }
            }
        }
    }

}