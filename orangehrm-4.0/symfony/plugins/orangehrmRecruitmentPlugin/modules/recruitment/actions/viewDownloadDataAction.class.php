<?php
/**
 * Created by PhpStorm.
 * User: kelanaeka
 * Date: 5/5/2020
 * Time: 8:32 PM
 */

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class viewDownloadDataAction extends sfAction
{
    /**
     *
     * @return <type>
     */
    public function getCandidateService() {
        if (is_null($this->candidateService)) {
            $this->candidateService = new CandidateService();
            $this->candidateService->setCandidateDao(new CandidateDao());
        }
        return $this->candidateService;
    }


    /**
     *
     * @return <type>
     */
    public function getYesNo($input){
        $answer = '';
        if($input != null){
            if($input == 1)
                $answer = 'No';
            else if($input == 2)
                $answer = 'Yes';
            else
                $answer = '';
        }

        return $answer;
    }
    /**
     *
     * @param <type> $request
     * @return <type>
     */
    public function execute($request){
        //$this->candidates = $this->getCandidateService()->getCandidates();
        $this->candidates = $this->getCandidateService()->getCandidateVacData();
        //print_r($this->candidates);die;
        $spr = new Spreadsheet();
        $sheet = $spr->getActiveSheet();
        $sheet->setTitle("Data Pelamar");

        //csv
        /*$csvHeader = array('NO','NAME','FRONT TITLE','END TITLE','GENDER','BIRTH PLACE','DATE OF BIRTH','CURRENT ADDRESS','CURRENT PROVINCE','CURRENT CITY',
                        'CURRENT KECAMATAN','CURRENT KELURAHAN','CURRENT ZIPCODE','PERMANENT ADDRESS','PERMANENT PROVINCE','PERMANENT CITY','PERMANENT KECAMATAN',
                        'PERMANENT KELURAHAN','PERMANENT ZIPCODE','RELIGION','MARITAL STATUS','PHONE','MOBILE PHONE','E-MAIL ADDRESS','e-KTP ID', 'Elementary',
                        'Elementary Start Year','Elementary End Year','Junior High','Junior High Start Year','Junior High End Year','Senior High',
                        'Senior High Start Year','Senior High End Year','Associate','Associate Start Year','Associate End Year','Bachelor','Bachelor Start Year',
                        'Bachelor End Year','Master','Master Start Year','Master End Year','Doctoral','Doctoral Start Year','Doctoral End Year','Company 1',
                        'Position 1','Start Year 1','End Year 1','Job Desc 1','Reference 1','Reference Telp 1','Company 2','Position 2','Start Year 2',
                        'End Year 2','Job Desc 2','Reference Name 2','Reference Telp 2','Company 3','Position 3','Start Year 3','End Year 3','Job Desc 3',
                        'Reference Name 3','Reference Telp 3','Company 4','Position 4','Start Year 4','End Year 4','Job Desc 4','Reference Name 4','Reference Telp 4',
                        'Company 5','Position 5','Start Year 5','End Year 5','Job Desc 5','Reference Name 5','Reference Telp 5','Current Salary','Expected Salary',
                        'Prefered Workplace','Doctor STR','Doctor STR Issued','Doctor STR Expire',
                        'Doctor IDI card',
                        'Doctor IDI card expire',
                        'Doctor Hiperkes',
                        'Doctor ACLS',
                        'Doctor ACLS issued',
                        'Doctor ACLS expire date',
                        'Doctor ACLS expire',
                        'Doctor ATLS',
                        'Doctor ATLS issued',
                        'Doctor ATLS expire date',
                        'Doctor ATLS expire',
                        'Nurse STR',
                        'Nurse STR issued',
                        'Nurse STR expire date',
                        'Nurse PPNI',
                        'Nurse PPNI expire date',
                        'Nurse Hiperkes',
                        'Nurse PPGD',
                        'Nurse PPGD issued',
                        'Nurse PPGD expire date',
                        'Nurse PPGD expire',
                        'Blood type',
                        'Heght',
                        'Weight',
                        'NPWP',
                        'BPJS naker',
                        'BPJS kes',
                        'Faskes',
                        'KK',
                        'Family head',
                        'Emergency name',
                        'Emergency addr',
                        'Emergency province',
                        'Emergency city',
                        'Emergency kecamatan',
                        'Emergency kelurahan',
                        'Emergency zip',
                        'Emergency relation',
                        'Emergency phone',
                        'Spouse',
                        'Spouseage',
                        'Spouseedu',
                        'Spouseoccupation',
                        'First child name',
                        'First child age',
                        'First child edu',
                        'First child occupation',
                        'Second child name',
                        'Second child age',
                        'Second child edu',
                        'Second child occupation',
                        'Third child name',
                        'Third child age',
                        'Third child edu',
                        'Third child occupation',
                        'Father name',
                        'Father age',
                        'Father edu',
                        'Father occupation',
                        'Mother name',
                        'Mother age',
                        'Mother edu',
                        'Mother occupation',
                        'Sibling name 1',
                        'Sibling age 1',
                        'Sibling edu 1',
                        'Sibling occupation 1',
                        'Sibling name 2',
                        'Sibling age 2',
                        'Sibling edu 2',
                        'Sibling occupation 2',
                        'Sibling name 3',
                        'Sibling age 3',
                        'Sibling edu 3',
                        'Sibling occupation 3',
                        'Sibling name 4',
                        'Sibling age 4',
                        'Sibling edu 4',
                        'Sibling occupation 4',
                        'Sibling name 5',
                        'Sibling age 5',
                        'Sibling edu 5',
                        'Sibling occupation 5',
                        'Org name 1',
                        'Org year 1',
                        'Org position 1',
                        'Org name 2',
                        'Org year 2',
                        'Org position 2',
                        'Org name 3',
                        'Org year 3',
                        'Org position 3',
                        'Org name 4',
                        'Org year 4',
                        'Org position 4',
                        'Org name 5',
                        'Org year 5',
                        'Org position 5',
                        'Diphteria',
                        'Sinusitis',
                        'Bronchitis',
                        'Hemoptoe',
                        'TBC',
                        'Lung infection',
                        'Asthma',
                        'Dyspnoea',
                        'Urinate problem',
                        'Urinate disorder',
                        'Kidney disease',
                        'Kidney stone',
                        'Frequent urinate',
                        'Meningitis',
                        'Cerebral concussion',
                        'Poliomyelitis',
                        'Epilepsy',
                        'Stroke',
                        'Headache',
                        'Typhoid',
                        'Bloodvomit',
                        'Obstipation',
                        'Dyspepsia',
                        'Jaundice',
                        'Gallbladder',
                        'Swallowing disorder',
                        'Incontinentia alvi',
                        'Chickenpox',
                        'Fungal skin',
                        'STD',
                        'Heart attack',
                        'Chest pains',
                        'Palpitation',
                        'hypertension',
                        'Hemorrhoid',
                        'Varicose veins',
                        'Thyroid disease',
                        'Rhematoids arthritis',
                        'Foodallergy',
                        'Foodallergy list',
                        'Drugallergy',
                        'Drugallergy list',
                        'Tetanus',
                        'Fainting',
                        'Oblivion',
                        'Consentration difficulties',
                        'Vision disorder',
                        'Hearing disorder',
                        'Lumbago',
                        'Neoplasm',
                        'Mental illness',
                        'Skin tuberculose',
                        'Bone tuberculose',
                        'Measles',
                        'Malaria',
                        'Diabetes',
                        'Sleep disorder',
                        'Hospitalized',
                        'Hospitalized date',
                        'Hospitalized length',
                        'Hospitalized reason',
                        'Accident',
                        'Accident date',
                        'Accident description',
                        'Surgery',
                        'Surgery date',
                        'Surgery description',
                        'Smoker',
                        'Cigarettes per day',
                        'Smoke start age',
                        'Smoke stop since',
                        'Alcohol',
                        'Alcohol per week',
                        'Alcohol sloky per week',
                        'Exercise',
                        'Exercise description',
                        'Medicine list',
                        'Vac diptheri',
                        'Vac tetanus',
                        'Vac polio',
                        'Vac bcg',
                        'Vac mmr',
                        'Vac influenza',
                        'Vac hepatitisa',
                        'Vac hepatitisb',
                        'Vac varisela',
                        'Vac typhoid',
                        'Vac hpv',
                        'Vac meningokok',
                        'Vac yellowfever',
                        'Vac japencephalitis',
                        'Consent');*/

        //Header
        $sheet->setCellValue('A1','NO');
        $sheet->setCellValue('B1','VACANCY');
        $sheet->setCellValue('C1','STATUS');
        $sheet->setCellValue('D1','APPLICATION ID');
        $sheet->setCellValue('E1','NAME');
        $sheet->setCellValue('F1','FRONT TITLE');
        $sheet->setCellValue('G1','END TITLE');
        $sheet->setCellValue('H1','GENDER');
        $sheet->setCellValue('I1','BIRTH PLACE');
        $sheet->setCellValue('J1','DATE OF BIRTH');
        $sheet->setCellValue('K1','CURRENT ADDRESS');
        $sheet->setCellValue('L1','CURRENT PROVINCE');
        $sheet->setCellValue('M1','CURRENT CITY');
        $sheet->setCellValue('N1','CURRENT KECAMATAN');
        $sheet->setCellValue('O1','CURRENT KELURAHAN');
        $sheet->setCellValue('P1','CURRENT ZIPCODE');
        $sheet->setCellValue('Q1','PERMANENT ADDRESS');
        $sheet->setCellValue('R1','PERMANENT PROVINCE');
        $sheet->setCellValue('S1','PERMANENT CITY');
        $sheet->setCellValue('T1','PERMANENT KECAMATAN');
        $sheet->setCellValue('U1','PERMANENT KELURAHAN');
        $sheet->setCellValue('V1','PERMANENT ZIPCODE');
        $sheet->setCellValue('W1','RELIGION');
        $sheet->setCellValue('X1','MARITAL STATUS');
        $sheet->setCellValue('Y1','PHONE');
        $sheet->setCellValue('Z1','MOBILE PHONE');
        $sheet->setCellValue('AA1','E-MAIL ADDRESS');
        $sheet->setCellValue('AB1','e-KTP ID');
        $sheet->setCellValue('AC1','Elementary');
        $sheet->setCellValue('AD1','Elementary Start Year');
        $sheet->setCellValue('AE1','Elementary End Year');
        $sheet->setCellValue('AF1','Junior High');
        $sheet->setCellValue('AG1','Junior High Start Year');
        $sheet->setCellValue('AH1','Junior High End Year');
        $sheet->setCellValue('AI1','Senior High');
        $sheet->setCellValue('AJ1','Senior High Start Year');
        $sheet->setCellValue('AK1','Senior High End Year');
        $sheet->setCellValue('AL1','Associate');
        $sheet->setCellValue('AM1','Associate Start Year');
        $sheet->setCellValue('AN1','Associate End Year');
        $sheet->setCellValue('AO1','Bachelor');
        $sheet->setCellValue('AP1','Bachelor Start Year');
        $sheet->setCellValue('AQ1','Bachelor End Year');
        $sheet->setCellValue('AR1','Master');
        $sheet->setCellValue('AS1','Master Start Year');
        $sheet->setCellValue('AT1','Master End Year');
        $sheet->setCellValue('AU1','Doctoral');
        $sheet->setCellValue('AV1','Doctoral Start Year');
        $sheet->setCellValue('AW1','Doctoral End Year');
        $sheet->setCellValue('AX1','Company 1');
        $sheet->setCellValue('AY1','Position 1');
        $sheet->setCellValue('AZ1','Start Year 1');
        $sheet->setCellValue('BA1','End Year 1');
        $sheet->setCellValue('BB1','Job Desc 1');
        $sheet->setCellValue('BC1','Reference 1');
        $sheet->setCellValue('BD1','Reference Telp 1');
        $sheet->setCellValue('BE1','Company 2');
        $sheet->setCellValue('BF1','Position 2');
        $sheet->setCellValue('BG1','Start Year 2');
        $sheet->setCellValue('BH1','End Year 2');
        $sheet->setCellValue('BI1','Job Desc 2');
        $sheet->setCellValue('BJ1','Reference Name 2');
        $sheet->setCellValue('BK1','Reference Telp 2');
        $sheet->setCellValue('BL1','Company 3');
        $sheet->setCellValue('BM1','Position 3');
        $sheet->setCellValue('BN1','Start Year 3');
        $sheet->setCellValue('BO1','End Year 3');
        $sheet->setCellValue('BP1','Job Desc 3');
        $sheet->setCellValue('BQ1','Reference Name 3');
        $sheet->setCellValue('BR1','Reference Telp 3');
        $sheet->setCellValue('BS1','Company 4');
        $sheet->setCellValue('BT1','Position 4');
        $sheet->setCellValue('BU1','Start Year 4');
        $sheet->setCellValue('BV1','End Year 4');
        $sheet->setCellValue('BW1','Job Desc 4');
        $sheet->setCellValue('BX1','Reference Name 4');
        $sheet->setCellValue('BY1','Reference Telp 4');
        $sheet->setCellValue('BZ1','Company 5');
        $sheet->setCellValue('CA1','Position 5');
        $sheet->setCellValue('CB1','Start Year 5');
        $sheet->setCellValue('CC1','End Year 5');
        $sheet->setCellValue('CD1','Job Desc 5');
        $sheet->setCellValue('CE1','Reference Name 5');
        $sheet->setCellValue('CF1','Reference Telp 5');
        $sheet->setCellValue('CG1','Current Salary');
        $sheet->setCellValue('CH1','Expected Salary');
        $sheet->setCellValue('CI1','Prefered Workplace');
        $sheet->setCellValue('CJ1','Doctor STR');
        $sheet->setCellValue('CK1','Doctor STR Issued');
        $sheet->setCellValue('CL1','Doctor STR Expire');
        $sheet->setCellValue('CM1','Doctor IDI card');
        $sheet->setCellValue('CN1','Doctor IDI card expire');
        $sheet->setCellValue('CO1','Doctor Hiperkes');
        $sheet->setCellValue('CP1','Doctor ACLS');
        $sheet->setCellValue('CQ1','Doctor ACLS issued');
        $sheet->setCellValue('CR1','Doctor ACLS expire date');
        $sheet->setCellValue('CS1','Doctor ACLS expire');
        $sheet->setCellValue('CT1','Doctor ATLS');
        $sheet->setCellValue('CU1','Doctor ATLS issued');
        $sheet->setCellValue('CV1','Doctor ATLS expire date');
        $sheet->setCellValue('CW1','Doctor ATLS expire');
        $sheet->setCellValue('CX1','Nurse STR');
        $sheet->setCellValue('CY1','Nurse STR issued');
        $sheet->setCellValue('CZ1','Nurse STR expire date');
        $sheet->setCellValue('DA1','Nurse PPNI');
        $sheet->setCellValue('DB1','Nurse PPNI expire date');
        $sheet->setCellValue('DC1','Nurse Hiperkes');
        $sheet->setCellValue('DD1','Nurse PPGD');
        $sheet->setCellValue('DE1','Nurse PPGD issued');
        $sheet->setCellValue('DF1','Nurse PPGD expire date');
        $sheet->setCellValue('DG1','Nurse PPGD expire');
        $sheet->setCellValue('DH1','NPWP');
        $sheet->setCellValue('DI1','BPJS naker');
        $sheet->setCellValue('DJ1','BPJS kes');
        $sheet->setCellValue('DK1','Faskes');
        $sheet->setCellValue('DL1','KK');
        $sheet->setCellValue('DM1','Family head');
        $sheet->setCellValue('DN1','Emergency name');
        $sheet->setCellValue('DO1','Emergency addr');
        $sheet->setCellValue('DP1','Emergency province');
        $sheet->setCellValue('DQ1','Emergency city');
        $sheet->setCellValue('DR1','Emergency kecamatan');
        $sheet->setCellValue('DS1','Emergency kelurahan');
        $sheet->setCellValue('DT1','Emergency zip');
        $sheet->setCellValue('DU1','Emergency relation');
        $sheet->setCellValue('DV1','Emergency phone');
        $sheet->setCellValue('DW1','Spouse');
        $sheet->setCellValue('DX1','Spouseage');
        $sheet->setCellValue('DY1','Spouseedu');
        $sheet->setCellValue('DZ1','Spouseoccupation');
        $sheet->setCellValue('EA1','First child name');
        $sheet->setCellValue('EB1','First child age');
        $sheet->setCellValue('EC1','First child edu');
        $sheet->setCellValue('ED1','First child occupation');
        $sheet->setCellValue('EE1','Second child name');
        $sheet->setCellValue('EF1','Second child age');
        $sheet->setCellValue('EG1','Second child edu');
        $sheet->setCellValue('EH1','Second child occupation');
        $sheet->setCellValue('EI1','Third child name');
        $sheet->setCellValue('EJ1','Third child age');
        $sheet->setCellValue('EK1','Third child edu');
        $sheet->setCellValue('EL1','Third child occupation');
        $sheet->setCellValue('EM1','Father name');
        $sheet->setCellValue('EN1','Father age');
        $sheet->setCellValue('EO1','Father edu');
        $sheet->setCellValue('EP1','Father occupation');
        $sheet->setCellValue('EQ1','Mother name');
        $sheet->setCellValue('ER1','Mother age');
        $sheet->setCellValue('ES1','Mother edu');
        $sheet->setCellValue('ET1','Mother occupation');
        $sheet->setCellValue('EU1','Sibling name 1');
        $sheet->setCellValue('EV1','Sibling age 1');
        $sheet->setCellValue('EW1','Sibling edu 1');
        $sheet->setCellValue('EX1','Sibling occupation 1');
        $sheet->setCellValue('EY1','Sibling name 2');
        $sheet->setCellValue('EZ1','Sibling age 2');
        $sheet->setCellValue('FA1','Sibling edu 2');
        $sheet->setCellValue('FB1','Sibling occupation 2');
        $sheet->setCellValue('FC1','Sibling name 3');
        $sheet->setCellValue('FD1','Sibling age 3');
        $sheet->setCellValue('FE1','Sibling edu 3');
        $sheet->setCellValue('FF1','Sibling occupation 3');
        $sheet->setCellValue('FG1','Sibling name 4');
        $sheet->setCellValue('FH1','Sibling age 4');
        $sheet->setCellValue('FI1','Sibling edu 4');
        $sheet->setCellValue('FJ1','Sibling occupation 4');
        $sheet->setCellValue('FK1','Sibling name 5');
        $sheet->setCellValue('FL1','Sibling age 5');
        $sheet->setCellValue('FM1','Sibling edu 5');
        $sheet->setCellValue('FN1','Sibling occupation 5');
        $sheet->setCellValue('FO1','Org name 1');
        $sheet->setCellValue('FP1','Org year 1');
        $sheet->setCellValue('FQ1','Org position 1');
        $sheet->setCellValue('FR1','Org name 2');
        $sheet->setCellValue('FS1','Org year 2');
        $sheet->setCellValue('FT1','Org position 2');
        $sheet->setCellValue('FU1','Org name 3');
        $sheet->setCellValue('FV1','Org year 3');
        $sheet->setCellValue('FW1','Org position 3');
        $sheet->setCellValue('FX1','Org name 4');
        $sheet->setCellValue('FY1','Org year 4');
        $sheet->setCellValue('FZ1','Org position 4');
        $sheet->setCellValue('GA1','Org name 5');
        $sheet->setCellValue('GB1','Org year 5');
        $sheet->setCellValue('GC1','Org position 5');
        $sheet->setCellValue('GD1','Diphteria');
        $sheet->setCellValue('GE1','Sinusitis');
        $sheet->setCellValue('GF1','Bronchitis');
        $sheet->setCellValue('GG1','Hemoptoe');
        $sheet->setCellValue('GH1','TBC');
        $sheet->setCellValue('GI1','Lung infection');
        $sheet->setCellValue('GJ1','Asthma');
        $sheet->setCellValue('GK1','Dyspnoea');
        $sheet->setCellValue('GL1','Urinate problem');
        $sheet->setCellValue('GM1','Urinate disorder');
        $sheet->setCellValue('GN1','Kidney disease');
        $sheet->setCellValue('GO1','Kidney stone');
        $sheet->setCellValue('GP1','Frequent urinate');
        $sheet->setCellValue('GQ1','Meningitis');
        $sheet->setCellValue('GR1','Cerebral concussion');
        $sheet->setCellValue('GS1','Poliomyelitis');
        $sheet->setCellValue('GT1','Epilepsy');
        $sheet->setCellValue('GU1','Stroke');
        $sheet->setCellValue('GV1','Headache');
        $sheet->setCellValue('GW1','Typhoid');
        $sheet->setCellValue('GX1','Bloodvomit');
        $sheet->setCellValue('GY1','Obstipation');
        $sheet->setCellValue('GZ1','Dyspepsia');
        $sheet->setCellValue('HA1','Jaundice');
        $sheet->setCellValue('HB1','Gallbladder');
        $sheet->setCellValue('HC1','Swallowing disorder');
        $sheet->setCellValue('HD1','Incontinentia alvi');
        $sheet->setCellValue('HE1','Chickenpox');
        $sheet->setCellValue('HF1','Fungal skin');
        $sheet->setCellValue('HG1','STD');
        $sheet->setCellValue('HH1','Heart attack');
        $sheet->setCellValue('HI1','Chest pains');
        $sheet->setCellValue('HJ1','Palpitation');
        $sheet->setCellValue('HK1','hypertension');
        $sheet->setCellValue('HL1','Hemorrhoid');
        $sheet->setCellValue('HM1','Varicose veins');
        $sheet->setCellValue('HN1','Thyroid disease');
        $sheet->setCellValue('HO1','Rhematoids arthritis');
        $sheet->setCellValue('HP1','Foodallergy');
        $sheet->setCellValue('HQ1','Foodallergy list');
        $sheet->setCellValue('HR1','Drugallergy');
        $sheet->setCellValue('HS1','Drugallergy list');
        $sheet->setCellValue('HT1','Tetanus');
        $sheet->setCellValue('HU1','Fainting');
        $sheet->setCellValue('HV1','Oblivion');
        $sheet->setCellValue('HW1','Consentration difficulties');
        $sheet->setCellValue('HX1','Vision disorder');
        $sheet->setCellValue('HY1','Hearing disorder');
        $sheet->setCellValue('HZ1','Lumbago');
        $sheet->setCellValue('IA1','Neoplasm');
        $sheet->setCellValue('IB1','Mental illness');
        $sheet->setCellValue('IC1','Skin tuberculose');
        $sheet->setCellValue('ID1','Bone tuberculose');
        $sheet->setCellValue('IE1','Measles');
        $sheet->setCellValue('IF1','Malaria');
        $sheet->setCellValue('IG1','Diabetes');
        $sheet->setCellValue('IH1','Sleep disorder');
        $sheet->setCellValue('II1','Hospitalized');
        $sheet->setCellValue('IJ1','Hospitalized date');
        $sheet->setCellValue('IK1','Hospitalized length');
        $sheet->setCellValue('IL1','Hospitalized reason');
        $sheet->setCellValue('IM1','Accident');
        $sheet->setCellValue('IN1','Accident date');
        $sheet->setCellValue('IO1','Accident description');
        $sheet->setCellValue('IP1','Surgery');
        $sheet->setCellValue('IQ1','Surgery date');
        $sheet->setCellValue('IR1','Surgery description');
        $sheet->setCellValue('IS1','Smoker');
        $sheet->setCellValue('IT1','Cigarettes per day');
        $sheet->setCellValue('IU1','Smoke start age');
        $sheet->setCellValue('IV1','Smoke stop since');
        $sheet->setCellValue('IW1','Alcohol');
        $sheet->setCellValue('IX1','Alcohol per week');
        $sheet->setCellValue('IY1','Alcohol sloky per week');
        $sheet->setCellValue('IZ1','Exercise');
        $sheet->setCellValue('JA1','Exercise description');
        $sheet->setCellValue('JB1','Medicine list');
        $sheet->setCellValue('JC1','Vac diptheri');
        $sheet->setCellValue('JD1','Vac tetanus');
        $sheet->setCellValue('JE1','Vac polio');
        $sheet->setCellValue('JF1','Vac bcg');
        $sheet->setCellValue('JG1','Vac mmr');
        $sheet->setCellValue('JH1','Vac influenza');
        $sheet->setCellValue('JI1','Vac hepatitisa');
        $sheet->setCellValue('JJ1','Vac hepatitisb');
        $sheet->setCellValue('JK1','Vac varisela');
        $sheet->setCellValue('JL1','Vac typhoid');
        $sheet->setCellValue('JM1','Vac hpv');
        $sheet->setCellValue('JN1','Vac meningokok');
        $sheet->setCellValue('JO1','Vac yellowfever');
        $sheet->setCellValue('JP1','Vac japencephalitis');
        $sheet->setCellValue('JQ1','Blood type');
        $sheet->setCellValue('JR1','Heght');
        $sheet->setCellValue('JS1','Weight');
        $sheet->setCellValue('JT1','Consent');

        $no = 2;
        //$contents = array();
        //$contents[0] = $csvHeader;
        foreach ($this->candidates as $candidate){
            //$csvContent = array(
            $sheet->setCellValue('A'.$no, $no - 1);
            $sheet->setCellValue('B'.$no, $candidate['lowongan']);
            $sheet->setCellValue('C'.$no, $candidate['statuslamaran']);
            $sheet->setCellValue('D'.$no, $candidate['idlamaran']);
            $sheet->setCellValue('E'.$no, $candidate['middle_name']);
            $sheet->setCellValue('F'.$no, $candidate['first_name']);
            $sheet->setCellValue('G'.$no, $candidate['last_name']);
            $sheet->setCellValue('H'.$no, $candidate['gender']);
            $sheet->setCellValue('I'.$no, $candidate['birthplace']);
            $sheet->setCellValue('J'.$no, $candidate['dob']);
            $sheet->setCellValue('K'.$no, $candidate['currentaddr']);
            $sheet->setCellValue('L'.$no, $candidate['currentprovince']);
            $sheet->setCellValue('M'.$no, $candidate['currentcity']);
            $sheet->setCellValue('N'.$no, $candidate['currentkecamatan']);
            $sheet->setCellValue('O'.$no, $candidate['currentkelurahan']);
            $sheet->setCellValue('P'.$no, $candidate['currentzip']);
            $sheet->setCellValue('Q'.$no, $candidate['permanentaddr']);
            $sheet->setCellValue('R'.$no, $candidate['permanentprovince']);
            $sheet->setCellValue('S'.$no, $candidate['permanentcity']);
            $sheet->setCellValue('T'.$no, $candidate['permanentkecamatan']);
            $sheet->setCellValue('U'.$no, $candidate['permanentkelurahan']);
            $sheet->setCellValue('V'.$no, $candidate['permanentzip']);
            $sheet->setCellValue('W'.$no, $candidate['religion']);
            $sheet->setCellValue('X'.$no, $candidate['marital']);
            $sheet->setCellValue('Y'.$no, $candidate['phone']);
            $sheet->setCellValue('Z'.$no, $candidate['mobile']);
            $sheet->setCellValue('AA'.$no, $candidate['email']);
            $sheet->setCellValue('AB'.$no, $candidate['ektp']);
            $sheet->setCellValue('AC'.$no, $candidate['elementary']);
            $sheet->setCellValue('AD'.$no, $candidate['elementary_startyear']);
            $sheet->setCellValue('AE'.$no, $candidate['elementary_endyear']);
            $sheet->setCellValue('AF'.$no, $candidate['juniorhigh']);
            $sheet->setCellValue('AG'.$no, $candidate['juniorhigh_startyear']);
            $sheet->setCellValue('AH'.$no, $candidate['juniorhigh_endyear']);
            $sheet->setCellValue('AI'.$no, $candidate['seniorhigh']);
            $sheet->setCellValue('AJ'.$no, $candidate['seniorhigh_startyear']);
            $sheet->setCellValue('AK'.$no, $candidate['seniorhigh_endyear']);
            $sheet->setCellValue('AL'.$no, $candidate['associate']);
            $sheet->setCellValue('AM'.$no, $candidate['associate_startyear']);
            $sheet->setCellValue('AN'.$no, $candidate['associate_endyear']);
            $sheet->setCellValue('AO'.$no, $candidate['bachelor']);
            $sheet->setCellValue('AP'.$no, $candidate['bachelor_startyear']);
            $sheet->setCellValue('AQ'.$no, $candidate['bachelor_endyear']);
            $sheet->setCellValue('AR'.$no, $candidate['master']);
            $sheet->setCellValue('AS'.$no, $candidate['master_startyear']);
            $sheet->setCellValue('AT'.$no, $candidate['master_endyear']);
            $sheet->setCellValue('AU'.$no, $candidate['doctoral']);
            $sheet->setCellValue('AV'.$no, $candidate['doctoral_startyear']);
            $sheet->setCellValue('AW'.$no, $candidate['doctoral_endyear']);
            $sheet->setCellValue('AX'.$no, $candidate['company1']);
            $sheet->setCellValue('AY'.$no, $candidate['position1']);
            $sheet->setCellValue('AZ'.$no, $candidate['start1']);
            $sheet->setCellValue('BA'.$no, $candidate['end1']);
            $sheet->setCellValue('BB'.$no, $candidate['jobdesc1']);
            $sheet->setCellValue('BC'.$no, $candidate['refname1']);
            $sheet->setCellValue('BD'.$no, $candidate['reftelp1']);
            $sheet->setCellValue('BE'.$no, $candidate['company2']);
            $sheet->setCellValue('BF'.$no, $candidate['position2']);
            $sheet->setCellValue('BG'.$no, $candidate['start2']);
            $sheet->setCellValue('BH'.$no, $candidate['end2']);
            $sheet->setCellValue('BI'.$no, $candidate['jobdesc2']);
            $sheet->setCellValue('BJ'.$no, $candidate['refname2']);
            $sheet->setCellValue('BK'.$no, $candidate['reftelp2']);
            $sheet->setCellValue('BL'.$no, $candidate['company3']);
            $sheet->setCellValue('BM'.$no, $candidate['position3']);
            $sheet->setCellValue('BN'.$no, $candidate['start3']);
            $sheet->setCellValue('BO'.$no, $candidate['end3']);
            $sheet->setCellValue('BP'.$no, $candidate['jobdesc3']);
            $sheet->setCellValue('BQ'.$no, $candidate['refname3']);
            $sheet->setCellValue('BR'.$no, $candidate['reftelp3']);
            $sheet->setCellValue('BS'.$no, $candidate['company4']);
            $sheet->setCellValue('BT'.$no, $candidate['position4']);
            $sheet->setCellValue('BU'.$no, $candidate['start4']);
            $sheet->setCellValue('BV'.$no, $candidate['end4']);
            $sheet->setCellValue('BW'.$no, $candidate['jobdesc4']);
            $sheet->setCellValue('BX'.$no, $candidate['refname4']);
            $sheet->setCellValue('BY'.$no, $candidate['reftelp4']);
            $sheet->setCellValue('BZ'.$no, $candidate['company5']);
            $sheet->setCellValue('CA'.$no, $candidate['position5']);
            $sheet->setCellValue('CB'.$no, $candidate['start5']);
            $sheet->setCellValue('CC'.$no, $candidate['end5']);
            $sheet->setCellValue('CD'.$no, $candidate['jobdesc5']);
            $sheet->setCellValue('CE'.$no, $candidate['refname5']);
            $sheet->setCellValue('CF'.$no, $candidate['reftelp5']);
            $sheet->setCellValue('CG'.$no, $candidate['currentsalary']);
            $sheet->setCellValue('CH'.$no, $candidate['wantsalary']);
            $sheet->setCellValue('CI'.$no, $candidate['prefworkplace']);
            $sheet->setCellValue('CJ'.$no, $candidate['doctor_str']);
            $sheet->setCellValue('CK'.$no, $candidate['doctor_strissued']);
            $sheet->setCellValue('CL'.$no, $candidate['doctor_strexpire']);
            $sheet->setCellValue('CM'.$no, $candidate['doctor_idicard']);
            $sheet->setCellValue('CN'.$no, $candidate['doctor_idicardexpire']);
            $sheet->setCellValue('CO'.$no, $candidate['doctor_hiperkes']);
            $sheet->setCellValue('CP'.$no, $candidate['doctor_acls']);
            $sheet->setCellValue('CQ'.$no, $candidate['doctor_aclsissued']);
            $sheet->setCellValue('CR'.$no, $candidate['doctor_aclsexpire']);
            $sheet->setCellValue('CS'.$no, $candidate['aclsexpire']);
            $sheet->setCellValue('CT'.$no, $candidate['doctor_atls']);
            $sheet->setCellValue('CU'.$no, $candidate['doctor_atlsissued']);
            $sheet->setCellValue('CV'.$no, $candidate['doctor_atlsexpire']);
            $sheet->setCellValue('CW'.$no, $candidate['atlsexpire']);
            $sheet->setCellValue('CX'.$no, $candidate['nurse_str']);
            $sheet->setCellValue('CY'.$no, $candidate['nurse_strissued']);
            $sheet->setCellValue('CZ'.$no, $candidate['nurse_strexpire']);
            $sheet->setCellValue('DA'.$no, $candidate['nurse_ppni']);
            $sheet->setCellValue('DB'.$no, $candidate['nurse_ppniexpire']);
            $sheet->setCellValue('DC'.$no, $candidate['nurse_hiperkes']);
            $sheet->setCellValue('DD'.$no, $candidate['nurse_ppgd']);
            $sheet->setCellValue('DE'.$no, $candidate['nurse_ppgdissued']);
            $sheet->setCellValue('DF'.$no, $candidate['nurse_ppgdexpire']);
            $sheet->setCellValue('DG'.$no, $candidate['ppgdexpire']);
            $sheet->setCellValue('DH'.$no, $candidate['npwp']);
            $sheet->setCellValue('DI'.$no, $candidate['bpjsnaker']);
            $sheet->setCellValue('DJ'.$no, $candidate['bpjskes']);
            $sheet->setCellValue('DK'.$no, $candidate['faskes']);
            $sheet->setCellValue('DL'.$no, $candidate['kk']);
            $sheet->setCellValue('DM'.$no, $this->getYesNo($candidate['familyhead']));
            $sheet->setCellValue('DN'.$no, $candidate['emergencyname']);
            $sheet->setCellValue('DO'.$no, $candidate['emergencyaddr']);
            $sheet->setCellValue('DP'.$no, $candidate['emergencyprovince']);
            $sheet->setCellValue('DQ'.$no, $candidate['emergencycity']);
            $sheet->setCellValue('DR'.$no, $candidate['emergencykecamatan']);
            $sheet->setCellValue('DS'.$no, $candidate['emergencykelurahan']);
            $sheet->setCellValue('DT'.$no, $candidate['emergencyzip']);
            $sheet->setCellValue('DU'.$no, $candidate['emergencyrelation']);
            $sheet->setCellValue('DV'.$no, $candidate['emergencyphone']);
            $sheet->setCellValue('DW'.$no, $candidate['spouse']);
            $sheet->setCellValue('DX'.$no, $candidate['spouseage']);
            $sheet->setCellValue('DY'.$no, $candidate['spouseedu']);
            $sheet->setCellValue('DZ'.$no, $candidate['spouseoccupation']);
            $sheet->setCellValue('EA'.$no, $candidate['firstchildname']);
            $sheet->setCellValue('EB'.$no, $candidate['firstchildage']);
            $sheet->setCellValue('EC'.$no, $candidate['firstchildedu']);
            $sheet->setCellValue('ED'.$no, $candidate['firstchildoccupation']);
            $sheet->setCellValue('EE'.$no, $candidate['secondchildname']);
            $sheet->setCellValue('EF'.$no, $candidate['secondchildage']);
            $sheet->setCellValue('EG'.$no, $candidate['secondchildedu']);
            $sheet->setCellValue('EH'.$no, $candidate['secondchildoccupation']);
            $sheet->setCellValue('EI'.$no, $candidate['thirdchildname']);
            $sheet->setCellValue('EJ'.$no, $candidate['thirdchildage']);
            $sheet->setCellValue('EK'.$no, $candidate['thirdchildedu']);
            $sheet->setCellValue('EL'.$no, $candidate['thirdchildoccupation']);
            $sheet->setCellValue('EM'.$no, $candidate['fathername']);
            $sheet->setCellValue('EN'.$no, $candidate['fatherage']);
            $sheet->setCellValue('EO'.$no, $candidate['fatheredu']);
            $sheet->setCellValue('EP'.$no, $candidate['fatheroccupation']);
            $sheet->setCellValue('EQ'.$no, $candidate['mothername']);
            $sheet->setCellValue('ER'.$no, $candidate['motherage']);
            $sheet->setCellValue('ES'.$no, $candidate['motheredu']);
            $sheet->setCellValue('ET'.$no, $candidate['motheroccupation']);
            $sheet->setCellValue('EU'.$no, $candidate['siblingname1']);
            $sheet->setCellValue('EV'.$no, $candidate['siblingage1']);
            $sheet->setCellValue('EW'.$no, $candidate['siblingedu1']);
            $sheet->setCellValue('EX'.$no, $candidate['siblingoccupation1']);
            $sheet->setCellValue('EY'.$no, $candidate['siblingname2']);
            $sheet->setCellValue('EZ'.$no, $candidate['siblingage2']);
            $sheet->setCellValue('FA'.$no, $candidate['siblingedu2']);
            $sheet->setCellValue('FB'.$no, $candidate['siblingoccupation2']);
            $sheet->setCellValue('FC'.$no, $candidate['siblingname3']);
            $sheet->setCellValue('FD'.$no, $candidate['siblingage3']);
            $sheet->setCellValue('FE'.$no, $candidate['siblingedu3']);
            $sheet->setCellValue('FF'.$no, $candidate['siblingoccupation3']);
            $sheet->setCellValue('FG'.$no, $candidate['siblingname4']);
            $sheet->setCellValue('FH'.$no, $candidate['siblingage4']);
            $sheet->setCellValue('FI'.$no, $candidate['siblingedu4']);
            $sheet->setCellValue('FJ'.$no, $candidate['siblingoccupation4']);
            $sheet->setCellValue('FK'.$no, $candidate['siblingname5']);
            $sheet->setCellValue('FL'.$no, $candidate['siblingage5']);
            $sheet->setCellValue('FM'.$no, $candidate['siblingedu5']);
            $sheet->setCellValue('FN'.$no, $candidate['siblingoccupation5']);
            $sheet->setCellValue('FO'.$no, $candidate['orgname1']);
            $sheet->setCellValue('FP'.$no, $candidate['orgyear1']);
            $sheet->setCellValue('FQ'.$no, $candidate['orgposition1']);
            $sheet->setCellValue('FR'.$no, $candidate['orgname2']);
            $sheet->setCellValue('FS'.$no, $candidate['orgyear2']);
            $sheet->setCellValue('FT'.$no, $candidate['orgposition2']);
            $sheet->setCellValue('FU'.$no, $candidate['orgname3']);
            $sheet->setCellValue('FV'.$no, $candidate['orgyear3']);
            $sheet->setCellValue('FW'.$no, $candidate['orgposition3']);
            $sheet->setCellValue('FX'.$no, $candidate['orgname4']);
            $sheet->setCellValue('FY'.$no, $candidate['orgyear4']);
            $sheet->setCellValue('FZ'.$no, $candidate['orgposition4']);
            $sheet->setCellValue('GA'.$no, $candidate['orgname5']);
            $sheet->setCellValue('GB'.$no, $candidate['orgyear5']);
            $sheet->setCellValue('GC'.$no, $candidate['orgposition5']);
            $sheet->setCellValue('GD'.$no, $this->getYesNo($candidate['diphteria']));
            $sheet->setCellValue('GE'.$no, $this->getYesNo($candidate['sinusitis']));
            $sheet->setCellValue('GF'.$no, $this->getYesNo($candidate['bronchitis']));
            $sheet->setCellValue('GG'.$no, $this->getYesNo($candidate['hemoptoe']));
            $sheet->setCellValue('GH'.$no, $this->getYesNo($candidate['tbc']));
            $sheet->setCellValue('GI'.$no, $this->getYesNo($candidate['lunginfection']));
            $sheet->setCellValue('GJ'.$no, $this->getYesNo($candidate['asthma']));
            $sheet->setCellValue('GK'.$no, $this->getYesNo($candidate['dyspnoea']));
            $sheet->setCellValue('GL'.$no, $this->getYesNo($candidate['urinateproblem']));
            $sheet->setCellValue('GM'.$no, $this->getYesNo($candidate['urinatedisorder']));
            $sheet->setCellValue('GN'.$no, $this->getYesNo($candidate['kidneydisease']));
            $sheet->setCellValue('GO'.$no, $this->getYesNo($candidate['kidneystone']));
            $sheet->setCellValue('GP'.$no, $this->getYesNo($candidate['frequenturinate']));
            $sheet->setCellValue('GQ'.$no, $this->getYesNo($candidate['meningitis']));
            $sheet->setCellValue('GR'.$no, $this->getYesNo($candidate['cerebralconcussion']));
            $sheet->setCellValue('GS'.$no, $this->getYesNo($candidate['poliomyelitis']));
            $sheet->setCellValue('GT'.$no, $this->getYesNo($candidate['epilepsy']));
            $sheet->setCellValue('GU'.$no, $this->getYesNo($candidate['stroke']));
            $sheet->setCellValue('GV'.$no, $this->getYesNo($candidate['headache']));
            $sheet->setCellValue('GW'.$no, $this->getYesNo($candidate['typhoid']));
            $sheet->setCellValue('GX'.$no, $this->getYesNo($candidate['bloodvomit']));
            $sheet->setCellValue('GY'.$no, $this->getYesNo($candidate['obstipation']));
            $sheet->setCellValue('GZ'.$no, $this->getYesNo($candidate['dyspepsia']));
            $sheet->setCellValue('HA'.$no, $this->getYesNo($candidate['jaundice']));
            $sheet->setCellValue('HB'.$no, $this->getYesNo($candidate['gallbladder']));
            $sheet->setCellValue('HC'.$no, $this->getYesNo($candidate['swallowingdisorder']));
            $sheet->setCellValue('HD'.$no, $this->getYesNo($candidate['incontinentiaalvi']));
            $sheet->setCellValue('HE'.$no, $this->getYesNo($candidate['chickenpox']));
            $sheet->setCellValue('HF'.$no, $this->getYesNo($candidate['fungalskin']));
            $sheet->setCellValue('HG'.$no, $this->getYesNo($candidate['std']));
            $sheet->setCellValue('HH'.$no, $this->getYesNo($candidate['heartattack']));
            $sheet->setCellValue('HI'.$no, $this->getYesNo($candidate['chestpains']));
            $sheet->setCellValue('HJ'.$no, $this->getYesNo($candidate['palpitation']));
            $sheet->setCellValue('HK'.$no, $this->getYesNo($candidate['hypertension']));
            $sheet->setCellValue('HL'.$no, $this->getYesNo($candidate['hemorrhoid']));
            $sheet->setCellValue('HM'.$no, $this->getYesNo($candidate['varicoseveins']));
            $sheet->setCellValue('HN'.$no, $this->getYesNo($candidate['thyroiddisease']));
            $sheet->setCellValue('HO'.$no, $this->getYesNo($candidate['rhematoidsarthritis']));
            $sheet->setCellValue('HP'.$no, $this->getYesNo($candidate['foodallergy']));
            $sheet->setCellValue('HQ'.$no, $candidate['foodallergylist']);
            $sheet->setCellValue('HR'.$no, $this->getYesNo($candidate['drugallergy']));
            $sheet->setCellValue('HS'.$no, $candidate['drugallergylist']);
            $sheet->setCellValue('HT'.$no, $this->getYesNo($candidate['tetanus']));
            $sheet->setCellValue('HU'.$no, $this->getYesNo($candidate['fainting']));
            $sheet->setCellValue('HV'.$no, $this->getYesNo($candidate['oblivion']));
            $sheet->setCellValue('HW'.$no, $this->getYesNo($candidate['consentrationdif']));
            $sheet->setCellValue('HX'.$no, $this->getYesNo($candidate['visiondisorder']));
            $sheet->setCellValue('HY'.$no, $this->getYesNo($candidate['hearingdisorder']));
            $sheet->setCellValue('HZ'.$no, $this->getYesNo($candidate['lumbago']));
            $sheet->setCellValue('IA'.$no, $this->getYesNo($candidate['neoplasm']));
            $sheet->setCellValue('IB'.$no, $this->getYesNo($candidate['mentalillness']));
            $sheet->setCellValue('IC'.$no, $this->getYesNo($candidate['skintuberculose']));
            $sheet->setCellValue('ID'.$no, $this->getYesNo($candidate['bonetuberculose']));
            $sheet->setCellValue('IE'.$no, $this->getYesNo($candidate['measles']));
            $sheet->setCellValue('IF'.$no, $this->getYesNo($candidate['malaria']));
            $sheet->setCellValue('IG'.$no, $this->getYesNo($candidate['diabetes']));
            $sheet->setCellValue('IH'.$no, $this->getYesNo($candidate['sleepdisorder']));
            $sheet->setCellValue('II'.$no, $this->getYesNo($candidate['hospitalized']));
            $sheet->setCellValue('IJ'.$no, $candidate['hospitalizeddate']);
            $sheet->setCellValue('IK'.$no, $candidate['hospitalizedlength']);
            $sheet->setCellValue('IL'.$no, $candidate['hospitalizedwhy']);
            $sheet->setCellValue('IM'.$no, $this->getYesNo($candidate['accident']));
            $sheet->setCellValue('IN'.$no, $candidate['accidentdate']);
            $sheet->setCellValue('IO'.$no, $candidate['accidenttxt']);
            $sheet->setCellValue('IP'.$no, $this->getYesNo($candidate['surgery']));
            $sheet->setCellValue('IQ'.$no, $candidate['surgerydate']);
            $sheet->setCellValue('IR'.$no, $candidate['surgerytxt']);
            $sheet->setCellValue('IS'.$no, $this->getYesNo($candidate['smoker']));
            $sheet->setCellValue('IT'.$no, $candidate['smokernumber']);
            $sheet->setCellValue('IU'.$no, $candidate['smokerage']);
            $sheet->setCellValue('IV'.$no, $candidate['smokerstop']);
            $sheet->setCellValue('IW'.$no, $this->getYesNo($candidate['alcohol']));
            $sheet->setCellValue('IX'.$no, $candidate['alcoholperweek']);
            $sheet->setCellValue('IY'.$no, $candidate['alcoholslokyperweek']);
            $sheet->setCellValue('IZ'.$no, $this->getYesNo($candidate['exercise']));
            $sheet->setCellValue('JA'.$no, $candidate['exercisetxt']);
            $sheet->setCellValue('JB'.$no, $candidate['medicinelist']);
            $sheet->setCellValue('JC'.$no, $this->getYesNo($candidate['vacdiptheri']));
            $sheet->setCellValue('JD'.$no, $this->getYesNo($candidate['vactetanus']));
            $sheet->setCellValue('JE'.$no, $this->getYesNo($candidate['vacpolio']));
            $sheet->setCellValue('JF'.$no, $this->getYesNo($candidate['vacbcg']));
            $sheet->setCellValue('JG'.$no, $this->getYesNo($candidate['vacmmr']));
            $sheet->setCellValue('JH'.$no, $this->getYesNo($candidate['vacinfluenza']));
            $sheet->setCellValue('JI'.$no, $this->getYesNo($candidate['vachepatitisa']));
            $sheet->setCellValue('JJ'.$no, $this->getYesNo($candidate['vachepatitisb']));
            $sheet->setCellValue('JK'.$no, $this->getYesNo($candidate['vacvarisela']));
            $sheet->setCellValue('JL'.$no, $this->getYesNo($candidate['vactyphoid']));
            $sheet->setCellValue('JM'.$no, $this->getYesNo($candidate['vachpv']));
            $sheet->setCellValue('JN'.$no, $this->getYesNo($candidate['vacmeningokok']));
            $sheet->setCellValue('JO'.$no, $this->getYesNo($candidate['vacyellowfever']));
            $sheet->setCellValue('JP'.$no, $this->getYesNo($candidate['vacjapencephalitis']));
            $sheet->setCellValue('JQ'.$no, $candidate['bloodtype']);
            $sheet->setCellValue('JR'.$no, $candidate['heght']);
            $sheet->setCellValue('JS'.$no, $candidate['weight']);
            $sheet->setCellValue('JT'.$no, $this->getYesNo($candidate['consent']));

            //);

            //$contents[$no] = $csvContent;
            $no++;
        }

        $writer = new Xlsx($spr);
        //header( 'Content-Type: text/csv' );
        //header( 'Content-Disposition: attachment;filename=candidatedata.csv');
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="candidatedata.xlsx"');
        header('Cache-Control: max-age=0');
        header('Expires: Fri, 11 Nov 2011 11:11:11 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        ob_end_clean();
        $writer->save('php://output');
        die();

        //$fp = fopen('php://output', 'w');
        //foreach($contents as $content){
        //    fputcsv($fp, $content);
        //}
        //fclose($fp);die;

        /*$response = $this->getResponse();

        if (!empty($this->candidates)) {
            //$contents = $attachment->getFileContent();
            $contentType = "text/csv";
            $fileName = "candidatedata.csv";
            //$fileLength = $attachment->getFileSize();

            $response->setHttpHeader('Pragma', 'must-revalidate');

            //$response->setHttpHeader('Expires', '0');
            //$response->setHttpHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0, max-age=0");
            $response->setHttpHeader("Cache-Control", "must-revalidate");
            //$response->setHttpHeader("Cache-Control", "private", false);
            $response->setHttpHeader("Content-type", $contentType);
            $response->setHttpHeader("Content-disposition", 'attachment; filename="' . $fileName . '";');
            //$response->setHttpHeader("Content-Transfer-Encoding", "binary");
            //$response->setHttpHeader("Content-Length", $fileLength);

            $response->setContent($content);
            $response->send();
        } else {
            $response->setStatusCode(404, 'This attachment does not exist');
        }

        return sfView::NONE;*/

    }
}