<?php

/**
 * OrangeHRM is a comprehensive Human Resource Management (HRM) System that captures
 * all the essential functionalities required for any enterprise.
 * Copyright (C) 2006 OrangeHRM Inc., http://www.orangehrm.com
 *
 * OrangeHRM is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * OrangeHRM is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA
 *
 */
class jobInterviewAction extends baseAction {

    /**
     * @param sfForm $form
     * @return
     */
    public function setForm(sfForm $form) {
        if (is_null($this->form)) {
            $this->form = $form;
        }
    }

    /**
     *
     * @return <type>
     */
    public function getJobInterviewService() {
        if (is_null($this->jobInterviewService)) {
            $this->jobInterviewService = new JobInterviewService();
            $this->jobInterviewService->setJobInterviewDao(new JobInterviewDao());
        }
        return $this->jobInterviewService;
    }

    public function getCandidateService() {
        if (is_null($this->candidateService)) {
            $this->candidateService = new CandidateService();
            $this->candidateService->setCandidateDao(new CandidateDao());
        }
        return $this->candidateService;
    }

    /**
     *
     * @return <type>
     */
    public function getInterviewService() {
        if (is_null($this->interviewService)) {
            $this->interviewService = new JobInterviewService();
            $this->interviewService->setJobInterviewDao(new JobInterviewDao());
        }
        return $this->interviewService;
    }

    public function sendNotification($emailAddr, $message, $subject){
        //$smtp2goUrl = "https://api.smtp2go.com/v3/email/send";
        $smtpBbUrl = "https://api.thebigbox.id/mail-sender/0.0.2/mails";

        /*$emailBodyArray = array(
            "api_key" => "api-CAF05D60704811EA8980F23C91C88F4E",
            "sender" => "kelanaeka73@nomornet.info",
            "to" => array($emailAddr),
            "subject" => $subject,
            "html_body" => "",
            "text_body" => $message
        );*/
        $emailBodyArray = array(
            "subject" => $subject,
            "message" => $message,
            "recipient" => $emailAddr
        );

        //$emailBodyJson = json_encode($emailBodyArray);
        $emailBodyUrlEncoded = http_build_query($emailBodyArray);

        //$curl = curl_init($smtp2goUrl);
        $curl = curl_init($smtpBbUrl);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        //curl_setopt($curl, CURLOPT_POSTFIELDS, $emailBodyJson);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $emailBodyUrlEncoded);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded',
            'accept: application/json',
            'x-api-key: Xy9q3r34G6ztzfxrUVG9ralextx1rkEQ'
        ));
        $result = curl_exec($curl);
        $errno = curl_errno($curl);
        if($errno){
            die("Connection Failure");
        }
        curl_close($curl);
    }


    /**
     *
     * @param <type> $request
     */
    public function execute($request) {

        /* For highlighting corresponding menu item */
        $request->setParameter('initialActionName', 'viewCandidates');
        $userRoleManager = $this->getContext()->getUserRoleManager();

        $this->candidatePermissions = $this->getDataGroupPermissions('recruitment_candidates');

        $usrObj = $this->getUser()->getAttribute('user');
        $allowedCandidateList = $usrObj->getAllowedCandidateList();
        $allowedVacancyList = $usrObj->getAllowedVacancyList();
        $empNumber = $usrObj->getEmployeeNumber();

        if ($this->getUser()->hasFlash('templateMessage')) {
            list($this->messageType, $this->message) = $this->getUser()->getFlash('templateMessage');
        }

        $this->historyId = $request->getParameter('historyId');
        $this->interviewId = $request->getParameter('interviewId');

        $candidateVacancyId = $request->getParameter('candidateVacancyId');
        $selectedAction = $request->getParameter('selectedAction');
        $this->editHiringManager = true;

        $param = array();
        if ($candidateVacancyId > 0 && $selectedAction != "") {
            $candidateVacancy = $this->getCandidateService()->getCandidateVacancyById($candidateVacancyId);

            // check if user can perform action on candidate
            if (!empty($this->interviewId) && $request->isMethod('post')) {
                $allowedStates = $userRoleManager->getActionableStates(WorkflowStateMachine::FLOW_RECRUITMENT, 
                        array($selectedAction));
                $actionAllowed = !empty($allowedStates);
            } else {
                $actionAllowed = $userRoleManager->isActionAllowed(WorkflowStateMachine::FLOW_RECRUITMENT, 
                        $candidateVacancy->getStatus(), $selectedAction);
            }
            if ($actionAllowed) {
                $this->candidatePermissions = new ResourcePermission(true, true, true, true);
            }
            
            $interviewHistory = $this->getJobInterviewService()->getInterviewScheduledHistoryByInterviewId($this->interviewId);

            $param = array('interviewId' => $this->interviewId, 'candidateVacancyId' => $candidateVacancyId, 'selectedAction' => $selectedAction, 'historyId' => (!empty($interviewHistory)) ? $interviewHistory->getId() : null, 'candidatePermissions' => $this->candidatePermissions);
        }

        if (!empty($this->historyId) && !empty($this->interviewId)) {
            $history = $this->getCandidateService()->getCandidateHistoryById($this->historyId);

            $candidateVacancyId = $this->getCandidateService()->getCandidateVacancyByCandidateIdAndVacancyId($history->getCandidateId(), $history->getVacancyId());
            $selectedAction = $history->getAction();
            
            // check if user can perform this history action
            $allowedStates = $userRoleManager->getActionableStates(WorkflowStateMachine::FLOW_RECRUITMENT, 
                    array($selectedAction));
            if (!empty($allowedStates)) {
                $this->candidatePermissions = new ResourcePermission(true, true, true, true);
            }
            
            $param = array('id' => $this->interviewId, 'candidateVacancyId' => $candidateVacancyId, 'selectedAction' => $selectedAction, 'candidatePermissions' => $this->candidatePermissions);
        }
        if (!$this->getCandidateService()->isHiringManager($candidateVacancyId, $empNumber) && $this->getCandidateService()->isInterviewer($candidateVacancyId, $empNumber)) {
            $this->editHiringManager = false;
        }
//        $lastAction = $this->getCandidateService()->getLastPerformedActionByCandidateVAcancyId($candidateVacancyId);
        $this->setForm(new JobInterviewForm(array(), $param, true));

        if (!in_array($this->form->candidateId, $allowedCandidateList) && !in_array($this->form->vacancyId, $allowedVacancyList)) {
            $this->redirect('recruitment/viewCandidates');
        }

        $candidateId = $this->form->candidateId;
        $interviewDate = "N/A";
        $interviewNote = "N/A";
        //if(!empty($this->interviewId)){
        //    $interviewDate = $this->getInterviewService()->getInterviewById($this->interviewId)->getInterviewDate();
        //    $interviewNote = $this->getInterviewService()->getInterviewById($this->interviewId)->getNote();
        //}
        //$this->interviewId = $history->getInterviewId();

        if ($request->isMethod('post')) {
            if ($this->candidatePermissions->canUpdate()) {
                $this->form->bind($request->getParameter($this->form->getName()));
                if ($this->form->isValid()) {
                    $result = $this->form->save();
                    if (isset($result['messageType'])) {
                        $this->getUser()->setFlash('templateMessage', array($result['messageType'], $result['message']));
                    } else {
                        //send email for interview action
                        $candidate = $this->getCandidateService()->getCandidateById($candidateId);
                        $interview = $this->getInterviewService()->getInterviewsByCandidateVacancyId($candidateVacancyId);
                        $interviewDateFormat = strtotime($interview->getData()[0]->getInterviewDate());
                        $interviewDate = date('d-m-Y', $interviewDateFormat) . " pukul " . $interview->getData()[0]->getInterviewTime();
                        $interviewUrl = "https://conf.umeetme.id/simpeginterview" . $candidateVacancyId;
                        $interviewNote = $interview->getData()[0]->getNote();
                        $subject = "Undangan Interview";
                        $message = "Selamat! Aplikasi anda telah kami pilih untuk melanjutkan ke tahap interview. \n";
                        $message .= "Interview anda dijadwalkan pada tanggal " . $interviewDate . "\n";
                        $message .= "Interview anda dapat dilakukan secara online melalui link berikut: " . $interviewUrl . "\n";
                        $message .= "Catatan: " . $interviewNote;
                        $this->sendNotification($candidate->getEmail(), $message, $subject);

                        $this->getUser()->setFlash('templateMessage', array('success', __('Successfully Scheduled')));
                    }
                    $this->redirect('recruitment/changeCandidateVacancyStatus?id=' . $this->form->historyId);
                }
            }
        }
    }

}

