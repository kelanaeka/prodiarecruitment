<?php

/**
 * OrangeHRM is a comprehensive Human Resource Management (HRM) System that captures
 * all the essential functionalities required for any enterprise.
 * Copyright (C) 2006 OrangeHRM Inc., http://www.orangehrm.com
 *
 * OrangeHRM is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * OrangeHRM is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA
 *
 */
class addCandidateAction extends baseAction {

    /**
     * @param sfForm $form
     * @return
     */
    public function setForm(sfForm $form) {
        if (is_null($this->form)) {
            $this->form = $form;
        }
    }

    public function getForm() {
        return $this->form;
    }

    /**
     *
     * @return <type>
     */
    public function getCandidateService() {
        if (is_null($this->candidateService)) {
            $this->candidateService = new CandidateService();
            $this->candidateService->setCandidateDao(new CandidateDao());
        }
        return $this->candidateService;
    }

    /**
     *
     * @return <type>
     */
    public function getVacancyService() {
        if (is_null($this->vacancyService)) {
            $this->vacancyService = new VacancyService();
            $this->vacancyService->setVacancyDao(new VacancyDao());
        }
        return $this->vacancyService;
    }

    public function sendNotification($emailAddr, $message, $subject){
        //$smtp2goUrl = "https://api.smtp2go.com/v3/email/send";
        //need to provide sender domain if using smtp2go
        $smtpBbUrl = "https://api.thebigbox.id/mail-sender/0.0.2/mails";

        /*$emailBodyArray = array(
            "api_key" => "api-CAF05D60704811EA8980F23C91C88F4E",
            "sender" => "kelanaeka73@nomornet.info",
            "to" => array($emailAddr),
            "subject" => $subject,
            "html_body" => "",
            "text_body" => $message
        );*/
        //bb body is form url encoded
        $emailBodyArray = array(
            "subject" => $subject,
            "message" => $message,
            "recipient" => $emailAddr
        );

        //$emailBodyJson = json_encode($emailBodyArray);
        $emailBodyUrlEncoded = http_build_query($emailBodyArray);

        //$curl = curl_init($smtp2goUrl);
        $curl = curl_init($smtpBbUrl);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        //curl_setopt($curl, CURLOPT_POSTFIELDS, $emailBodyJson);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $emailBodyUrlEncoded);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded',
            'accept: application/json',
            'x-api-key: Xy9q3r34G6ztzfxrUVG9ralextx1rkEQ'
        ));
        $result = curl_exec($curl);
        $errno = curl_errno($curl);
        if($errno){
            die("Connection Failure");
        }
        curl_close($curl);
    }

    /**
     *
     * @param <type> $request
     */
    public function execute($request) {
        /* For highlighting corresponding menu item */
        $request->setParameter('initialActionName', 'viewCandidates');

        $userRoleManager = $this->getContext()->getUserRoleManager();
        $requiredPermissions = array(
            BasicUserRoleManager::PERMISSION_TYPE_DATA_GROUP => array(
                'recruitment_candidates' => new ResourcePermission(true, false, false, false)
            )
        );
            
        $allowedVacancyList = $userRoleManager->getAccessibleEntityIds('Vacancy', 
                null, null, array(), array(), $requiredPermissions);
        
        $this->candidatePermissions = $this->getDataGroupPermissions('recruitment_candidates');

        $userObj = $this->getUser()->getAttribute('user');

        $allowedCandidateListToDelete = $userObj->getAllowedCandidateListToDelete();
        $this->candidateId = $request->getParameter('id');

        //check please GAK DAPAT ID

        $this->invalidFile = false;
        $reDirect = false;
        $this->edit = true;
        if ($this->candidateId > 0 && !(in_array($this->candidateId, $allowedCandidateListToDelete))) {
            $reDirect = true;
            $this->edit = false;
        }

        $param = array(
            'candidateId' => $this->candidateId,
            'allowedVacancyList' => $allowedVacancyList,
            'empNumber' => $userObj->getEmployeeNumber(),
            'isAdmin' => $userObj->isAdmin(),
            'candidatePermissions' => $this->candidatePermissions);

        $vacancyProperties = array('name', 'id', 'status');
        $this->jobVacancyList = $this->getVacancyService()->getVacancyPropertyList($vacancyProperties,$status = null);

        $this->candidateStatus = JobCandidate::ACTIVE;

        if ($this->candidateId > 0) {

            //check if this candidate applied to other vacancy and get shortlisted
            $this->shortlisted = false;
            $vacancyStatusList = $this->getCandidateService()->getCandidateVacancyStatusesForCandidateId($this->candidateId);
            $candidateVacancyList = $this->getCandidateService()->getCandidateById($this->candidateId)->getJobCandidateVacancy();
            $vacancy = $candidateVacancyList[0]->getJobVacancy();
            $this->vacancyStatusTxt = $candidateVacancyList[0]->getStatus();
            $candidateVacancyId = $candidateVacancyList[0]->getId();

            $isHiringManager = $this->getCandidateService()->isHiringManager($candidateVacancyId, $userObj->getEmployeeNumber());
            foreach($vacancyStatusList as $vs){
                if($vs['candidate_id'] != $this->candidateId && $vs['status'] != "APPLICATION INITIATED"){
                    $this->shortlisted = true;
                    break;
                }
            }

            $this->applicationId = $candidateVacancyId;
            $this->vacancyName = $vacancy->getVacancyName();

            $allowedCandidateList = $userObj->getAllowedCandidateList();
            if (!in_array($this->candidateId, $allowedCandidateList)) {
                $this->redirect('recruitment/viewCandidates');
            }
            $this->actionForm = new ViewCandidateActionForm(array(), $param, true);
            //this list contains all history id of selected candidate
            $allowedHistoryList = $userObj->getAllowedCandidateHistoryList($this->candidateId);

            $candidateHistory = $this->getCandidateService()->getCandidateHistoryForCandidateId($this->candidateId, $allowedHistoryList);
            $candidateHistoryService = new CandidateHistoryService();
            //send allowedHistoryList to form
            //need to add candidateId to chList
            $param["chList"] = $candidateHistoryService->getCandidateHistoryList($candidateHistory);
            if ($this->candidatePermissions->canRead()) {
                $this->_setListComponent($candidateHistoryService->getCandidateHistoryList($candidateHistory));
            }

            //enable the psikotest upload button
            //this to avoid error get on boolean, maybe need to separate checkstatus var checking and "shortlisted" checking
            $cdnsvc = $this->getCandidateService();
            $cdnvcn = $cdnsvc->getCandidateVacancyById($candidateVacancyId);
            $checkStatus = "";
            if(!empty($cdnvcn))
                $checkStatus = $cdnvcn->get('status');

            if($checkStatus && $checkStatus == "SHORTLISTED"){
                $this->candidateVacancyStatus = "SHORTLISTED";
                foreach ($candidateHistory as $historyObj) {
                    if($historyObj->getAction() == PluginWorkflowStateMachine::RECRUITMENT_APPLICATION_ACTION_SHORTLIST &&
                    $historyObj->getExtaction() == CandidateHistory::RECRUITMENT_CANDIDATE_EXT_ACTION_DETAILFORM){
                        $this->candidateVacancyStatus = "EXTACTION";
                    }

                }
            }

            $params = array();
            $this->parmetersForListCompoment = $params;

            $candidate = $this->getCandidateService()->getCandidateById($this->candidateId);
            $this->firstName = $candidate->getFirstName();
            $this->middleName = $candidate->getMiddleName();
            $this->lastName = $candidate->getLastName();
            $this->email = $candidate->getEmail();
            $this->contactNo = $candidate->getContactNumber();
            /*'resume' => new sfWidgetFormInputFileEditable(
                array('edit_mode' => false,
                    'with_delete' => false,
                    'file_src' => '')),*/
            $this->keyWords = $candidate->getKeywords();
            $this->comment = $candidate->getComment();
            $this->appliedDate = $candidate->getDateOfApplication();
            //'vacancy' => new sfWidgetFormSelect(array('choices' => $vacancyList)),
            //'resumeUpdate' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => $resumeUpdateChoices)),
            $this->gender = $candidate->getGender();
            $this->birthplace = $candidate->getBirthplace();
            $this->dob = $candidate->getDob();
            $this->currentaddr = $candidate->getCurrentaddr();
            $this->currentprovince = $candidate->getCurrentprovince();
            $this->currentcity = $candidate->getCurrentcity();
            $this->currentkecamatan = $candidate->getCurrentkecamatan();
            $this->currentkelurahan = $candidate->getCurrentkelurahan();
            $this->currentzip = $candidate->getCurrentzip();
            $this->permanentaddr = $candidate->getPermanentaddr();
            $this->permanentprovince = $candidate->getPermanentprovince();
            $this->permanentcity = $candidate->getPermanentcity();
            $this->permanentkecamatan = $candidate->getPermanentkecamatan();
            $this->permanentkelurahan = $candidate->getPermanentkelurahan();
            $this->permanentzip = $candidate->getPermanentzip();
            $this->religion = $candidate->getReligion();
            $this->marital = $candidate->getMarital();
            $this->phone = $candidate->getPhone();
            $this->mobile = $candidate->getMobile();
            $this->ektp = $candidate->getEktp();
            $this->wantsalary = $candidate->getWantsalary();
            $this->candidateStatus = $candidate->getStatus();
            $this->atlsexpire = $candidate->getAtlsexpire();
            $this->aclsexpire = $candidate->getAclsexpire();
            $this->ppgdexpire = $candidate->getPpgdexpire();
            $this->bloodtype = $candidate->getBloodtype();
            $this->heght = $candidate->getHeght();
            $this->weight = $candidate->getWeight();
            $this->npwp = $candidate->getNpwp();
            $this->bpjsnaker = $candidate->getBpjsnaker();
            $this->bpjskes = $candidate->getBpjskes();
            $this->faskes = $candidate->getFaskes();
            $this->kk = $candidate->getKk();
            $this->familyhead = $candidate->getFamilyhead();
            //if($this->getCandidateService()->getCandidateVacancyById($candidateVacancyId)->get('status') == "INTERVIEW SCHEDULED") {
            if($checkStatus == "INTERVIEW SCHEDULED") {
                $this->interviewUrl = "https://conf.umeetme.id/simpeginterview" . $candidateVacancyId;
            }

        } else {
            if (!($userObj->isAdmin() || $userObj->isHiringManager() || $this->candidatePermissions->canRead())) {
                $this->redirect('recruitment/viewCandidates');
            }
        }

        $this->setForm(new AddCandidateForm(array(), $param, true));
        $fileCount = count($this->getForm()->getResume());
        $attachmentName = array();
        if($fileCount > 0) {
            for($i = 0; $i < $fileCount; $i++){
                $attachmentName[$i] = $this->getForm()->getResume()[$i]->getFileName();
            }
            //echo $this->attachmentName;die;
            //getFileName();
            $this->attachmentName = $attachmentName;
        }

        if ($request->isMethod('post')) {
            if ($this->candidatePermissions->canCreate() || $this->candidatePermissions->canUpdate()) {

                $this->form->bind($request->getParameter($this->form->getName()), $request->getFiles($this->form->getName()));
                $file = $request->getFiles($this->form->getName());

                if (($_FILES['addCandidate']['size']['resume'] > 1024000) || ($_FILES['addCandidate']['error']['resume'] && $_FILES['addCandidate']['name']['resume'])) {
                    $title = ($this->candidateId > 0) ? __('Editing Candidate') : __('Adding Candidate');
                    $this->getUser()->setFlash('addcandidate.warning', __(TopLevelMessages::FILE_SIZE_SAVE_FAILURE));
                } elseif ($_FILES == null) {

                    //$title = ($this->candidateId > 0) ? __('Editing Candidate') : __('Adding Candidate');
                    //$this->getUser()->setFlash('addcandidate.warning', __(TopLevelMessages::FILE_SIZE_SAVE_FAILURE));
                    $this->redirect('recruitment/addCandidate?id=' . $this->candidateId);
                } else {

                    $this->form->bind($request->getParameter($this->form->getName()), $request->getFiles($this->form->getName()));
                    $file = $request->getFiles($this->form->getName());

                    if ($this->form->isValid()) {
                        $result = $this->form->save();

                        if (isset($result['messageType'])) {
                            $this->getUser()->setFlash('addcandidate.warning', $result['message']);
                            $this->invalidFile = true;
                        } else {
                            //remove psikotest email
                            $this->candidateId = $result['candidateId'];

                            $candidate = $this->getCandidateService()->getCandidateById($this->candidateId);
                            $candidateVacancyList = $candidate->getJobCandidateVacancy();
                            $candidateVacancyId = $candidateVacancyList[0]->getId();


                            $subject = "Aplikasi Lamaran Kerja ID 0" . $candidateVacancyId;
                            $message = "Terima kasih atas pengajuan aplikasi lamaran kerja anda. Kami akan segera menginformasikan status aplikasi anda. Gunakan ID Aplikasi berikut untuk mengetahui status aplikasi anda: 0" . $candidateVacancyId;

                            //$subject = "Hasil Psikotest Untuk Aplikasi 0" . $this->applicationId;
                            //$message = "Hasil Psikotest anda telah tersedia. Anda dapat mengunduh hasil psikotest melalui form Check Application Status.";
                            $this->sendNotification($candidate->getEmail(), $message, $subject);



                            $this->getUser()->setFlash('addcandidate.success', __(TopLevelMessages::SAVE_SUCCESS));
                            $this->redirect('recruitment/addCandidate?id=' . $this->candidateId);
                        }
                    } else {
                        $errors = array();
                        foreach ($this->form->getErrorSchema() as $key => $err){
                            if($key){
                                $errors[$key] = $err->getMessage();
                            }
                        }
                        print_r($errors);die;
                    }
                }
            }
        }
    }

    /**
     *
     * @param <type> $candidateHistory
     */
    private function _setListComponent($candidateHistory) {
        $configurationFactory = new CandidateHistoryHeaderFactory();
        ohrmListComponent::setConfigurationFactory($configurationFactory);
        ohrmListComponent::setListData($candidateHistory);
    }

}
