<?php
/**
 * Created by PhpStorm.
 * User: kelanaeka
 * Date: 4/4/2020
 * Time: 8:33 AM
 */
?>
<style type="text/css">
    #content {
        padding-top: 0;
    }
</style>

<div id="addCandidate" class="box">
    <div class="head"><h1 id="addCandidateHeading"><?php echo __("Apply for") . " " . $vacancyName; ?></h1></div>

    <?php include_component('core', 'ohrmPluginPannel', array('location' => 'add_layout_after_main_heading_1')) ?>

    <div class="inner">

        <?php include_partial('global/flash_messages', array('prefix' => 'applyVacancy')); ?>
        <ol>
            <li>
                <label><span class="keywrd" style="font-weight: bold">APPLICATION STATUS: SHORTLISTED</span></label>
            </li>
        </ol>

        <form name="frmUpdateApplicationData" id="frmUpdateApplicationData" method="post">
            <fieldset>
                <?php echo $form['_csrf_token']; ?>
                <?php echo $form['candidateId']->render(); ?>
                <?php echo $form['vacancyId']->render(); ?>
                <?php echo $form['appId']->render(); ?>
                <?php echo $form['vacancyName']->render(); ?>
                <?php echo $form['consent']->render(); ?>
                <hr style="border: 0px dashed #727272;">
                <label><span class="keywrd" style="font-weight: bold">IDENTITY</span></label>
                <ol>
                    <li>
                        <!--<p class="keywrd" style="font-weight: bold">IDENTITY</p>-->
                    </li>

                    <?php include_component('core', 'ohrmPluginPannel', array('location' => 'add_layout_after_main_heading_2')) ?>
                    <li>
                        <?php echo $form['bloodtype']->renderLabel(__('Blood Type'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['bloodtype']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['heght']->renderLabel(__('Height (cm)'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['heght']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['weight']->renderLabel(__('Weight (kg)'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['weight']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['npwp']->renderLabel(__('NPWP'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['npwp']->render(array("class" => "formInputText")); ?>
                        <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::NUMBER_ALT) . "</label>"; ?>
                    </li>
                    <li>
                        <?php echo $form['bpjsnaker']->renderLabel(__('BPJS Ketenagakerjaan Card Number'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['bpjsnaker']->render(array("class" => "formInputText")); ?>
                        <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::NUMBER_ALT) . "</label>"; ?>
                    </li>
                    <li>
                        <?php echo $form['bpjskes']->renderLabel(__('BPJS Kesehatan Card Number'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['bpjskes']->render(array("class" => "formInputText")); ?>
                        <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::NUMBER_ALT) . "</label>"; ?>
                    </li>
                    <li>
                        <?php echo $form['faskes']->renderLabel(__('Fasilitas Kesehatan'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['faskes']->render(array("cols" => 20, "rows" => 9)); ?>
                    </li>
                    <li>
                        <?php echo $form['kk']->renderLabel(__('Kartu Keluarga Number'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['kk']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['familyhead']->renderLabel(__('As Head of Family (based on Kartu Keluarga) '). ' <span class="required">*</span>'); ?>
                        <?php echo $form['familyhead']->render(array("class"=>"editable")); ?>
                    </li>
                </ol>
                <label><span class="keywrd" style="font-weight: bold">EMERGENCY</span></label>
                <div id="emergency">
                <ol>
                    <li></li>
                    <li>
                        <?php echo $form['emergencyname']->renderLabel(__('Nama'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['emergencyname']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['emergencyaddr']->renderLabel(__('Current Address'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['emergencyaddr']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['emergencyprovince']->renderLabel(__('Province'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['emergencyprovince']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['emergencycity']->renderLabel(__('City'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['emergencycity']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['emergencykecamatan']->renderLabel(__('Kecamatan'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['emergencykecamatan']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['emergencykelurahan']->renderLabel(__('Kelurahan'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['emergencykelurahan']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['emergencyzip']->renderLabel(__('Zip Code'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['emergencyzip']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['emergencyrelation']->renderLabel(__('Hubungan'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['emergencyrelation']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['emergencyphone']->renderLabel(__('Nomor Telpon'). ' <span class="required">*</span>'); ?>
                        <?php echo $form['emergencyphone']->render(array("class" => "formInputText")); ?>
                    </li>
                </ol>
                </div>
                <hr style="border: 0px dashed #727272;">
                <label><span class="keywrd" style="font-weight: bold">FAMILY</span><span id="familyExtend"> [+]</span></label>
                <div id="family">
                <ol>
                    <li></li>
                    <li>
                        <?php echo $form['spouse']->renderLabel(__('Spouse Name')); ?>
                        <?php echo $form['spouse']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['spouseage']->renderLabel(__('Spouse Age')); ?>
                        <?php echo $form['spouseage']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['spouseedu']->renderLabel(__('Spouse Education')); ?>
                        <?php echo $form['spouseedu']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['spouseoccupation']->renderLabel(__('Spouse Occupation')); ?>
                        <?php echo $form['spouseoccupation']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['firstchildname']->renderLabel(__('First Child Name')); ?>
                        <?php echo $form['firstchildname']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['firstchildage']->renderLabel(__('First Child Age')); ?>
                        <?php echo $form['firstchildage']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['firstchildedu']->renderLabel(__('First Child Education')); ?>
                        <?php echo $form['firstchildedu']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['firstchildoccupation']->renderLabel(__('First Child Occupation')); ?>
                        <?php echo $form['firstchildoccupation']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['secondchildname']->renderLabel(__('Second Child Name')); ?>
                        <?php echo $form['secondchildname']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['secondchildage']->renderLabel(__('Second Child Age')); ?>
                        <?php echo $form['secondchildage']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['secondchildedu']->renderLabel(__('Second Child Education')); ?>
                        <?php echo $form['secondchildedu']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['secondchildoccupation']->renderLabel(__('Second Child Occupation')); ?>
                        <?php echo $form['secondchildoccupation']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['thirdchildname']->renderLabel(__('Third Child Name')); ?>
                        <?php echo $form['thirdchildname']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['thirdchildage']->renderLabel(__('Third Child Age')); ?>
                        <?php echo $form['thirdchildage']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['thirdchildedu']->renderLabel(__('Third Child Education')); ?>
                        <?php echo $form['thirdchildedu']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['thirdchildoccupation']->renderLabel(__('Third Child Occupation')); ?>
                        <?php echo $form['thirdchildoccupation']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['fathername']->renderLabel(__('Father Name')); ?>
                        <?php echo $form['fathername']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['fatherage']->renderLabel(__('Father Age')); ?>
                        <?php echo $form['fatherage']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['fatheredu']->renderLabel(__('Father Education')); ?>
                        <?php echo $form['fatheredu']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['fatheroccupation']->renderLabel(__('Father Occupation')); ?>
                        <?php echo $form['fatheroccupation']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['mothername']->renderLabel(__('Mother Name')); ?>
                        <?php echo $form['mothername']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['motherage']->renderLabel(__('Mother Age')); ?>
                        <?php echo $form['motherage']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['motheredu']->renderLabel(__('Mother Education')); ?>
                        <?php echo $form['motheredu']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['motheroccupation']->renderLabel(__('Mother Occupation')); ?>
                        <?php echo $form['motheroccupation']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['siblingname1']->renderLabel(__('Brother/Sister Name (1)')); ?>
                        <?php echo $form['siblingname1']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['siblingage1']->renderLabel(__('Brother/Sister Age (1)')); ?>
                        <?php echo $form['siblingage1']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['siblingedu1']->renderLabel(__('Brother/Sister Education (1)')); ?>
                        <?php echo $form['siblingedu1']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['siblingoccupation1']->renderLabel(__('Brother/Sister Occupation (1)')); ?>
                        <?php echo $form['siblingoccupation1']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['siblingname2']->renderLabel(__('Brother/Sister Name (2)')); ?>
                        <?php echo $form['siblingname2']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['siblingage2']->renderLabel(__('Brother/Sister Age (2)')); ?>
                        <?php echo $form['siblingage2']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['siblingedu2']->renderLabel(__('Brother/Sister Education (2)')); ?>
                        <?php echo $form['siblingedu2']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['siblingoccupation2']->renderLabel(__('Brother/Sister Occupation (2)')); ?>
                        <?php echo $form['siblingoccupation2']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['siblingname3']->renderLabel(__('Brother/Sister Name (3)')); ?>
                        <?php echo $form['siblingname3']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['siblingage3']->renderLabel(__('Brother/Sister Age (3)')); ?>
                        <?php echo $form['siblingage3']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['siblingedu3']->renderLabel(__('Brother/Sister Education (3)')); ?>
                        <?php echo $form['siblingedu3']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['siblingoccupation3']->renderLabel(__('Brother/Sister Occupation (3)')); ?>
                        <?php echo $form['siblingoccupation3']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['siblingname4']->renderLabel(__('Brother/Sister Name (4)')); ?>
                        <?php echo $form['siblingname4']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['siblingage4']->renderLabel(__('Brother/Sister Age (4)')); ?>
                        <?php echo $form['siblingage4']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['siblingedu4']->renderLabel(__('Brother/Sister Education (4)')); ?>
                        <?php echo $form['siblingedu4']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['siblingoccupation4']->renderLabel(__('Brother/Sister Occupation (4)')); ?>
                        <?php echo $form['siblingoccupation4']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['siblingname5']->renderLabel(__('Brother/Sister Name (5)')); ?>
                        <?php echo $form['siblingname5']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['siblingage5']->renderLabel(__('Brother/Sister Age (5)')); ?>
                        <?php echo $form['siblingage5']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['siblingedu5']->renderLabel(__('Brother/Sister Education (5)')); ?>
                        <?php echo $form['siblingedu5']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['siblingoccupation5']->renderLabel(__('Brother/Sister Occupation (5)')); ?>
                        <?php echo $form['siblingoccupation5']->render(array("class" => "formInputText")); ?>
                    </li>
                </ol>
                </div>
                <hr style="border: 0px dashed #727272;">
                <label><span class="keywrd" style="font-weight: bold">ORGANIZATIONAL EXPERIENCE</span><span id="orgExtend"> [+]</span></label>
                <div id="org">
                <ol>
                    <li></li>
                    <li>
                        <?php echo $form['orgname1']->renderLabel(__('Organization Name (1)')); ?>
                        <?php echo $form['orgname1']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['orgyear1']->renderLabel(__('Year')); ?>
                        <?php echo $form['orgyear1']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['orgposition1']->renderLabel(__('Position')); ?>
                        <?php echo $form['orgposition1']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['orgname2']->renderLabel(__('Organization Name (2)')); ?>
                        <?php echo $form['orgname2']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['orgyear2']->renderLabel(__('Year')); ?>
                        <?php echo $form['orgyear2']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['orgposition2']->renderLabel(__('Position')); ?>
                        <?php echo $form['orgposition2']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['orgname3']->renderLabel(__('Organization Name (3)')); ?>
                        <?php echo $form['orgname3']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['orgyear3']->renderLabel(__('Year')); ?>
                        <?php echo $form['orgyear3']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['orgposition3']->renderLabel(__('Position')); ?>
                        <?php echo $form['orgposition3']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['orgname4']->renderLabel(__('Organization Name (4)')); ?>
                        <?php echo $form['orgname4']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['orgyear4']->renderLabel(__('Year')); ?>
                        <?php echo $form['orgyear4']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['orgposition4']->renderLabel(__('Position')); ?>
                        <?php echo $form['orgposition4']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['orgname5']->renderLabel(__('Organization Name (5)')); ?>
                        <?php echo $form['orgname5']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['orgyear5']->renderLabel(__('Year')); ?>
                        <?php echo $form['orgyear5']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['orgposition5']->renderLabel(__('Position')); ?>
                        <?php echo $form['orgposition5']->render(array("class" => "formInputText")); ?>
                    </li>
                </ol>
                </div>
                <hr style="border: 0px dashed #727272;">
                <label><span class="keywrd" style="font-weight: bold">MEDICAL HISTORY</span><span id="medExtend"> [+]</span></label>
                <div id="medhistory">
                <p>Choose according to the medical history that has been or is being suffered during the last 3 months.</p>
                <hr style="border: 0px dashed #727272;">
                <p style="font-weight: bold">Respiratory tract disease</p>
                <ol>
                    <li></li>
                    <li class="new radio">
                        <?php echo $form['diphteria']->renderLabel(__('Diphteria')); ?>
                        <?php echo $form['diphteria']->render(array("class"=>"editable")); ?>
                    </li class="new radio">
                    <li class="new radio">
                        <?php echo $form['sinusitis']->renderLabel(__('Sinusitis')); ?>
                        <?php echo $form['sinusitis']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['bronchitis']->renderLabel(__('Bronchitis')); ?>
                        <?php echo $form['bronchitis']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['hemoptoe']->renderLabel(__('Hemoptoe')); ?>
                        <?php echo $form['hemoptoe']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['tbc']->renderLabel(__('TBC')); ?>
                        <?php echo $form['tbc']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['lunginfection']->renderLabel(__('Lung Infection')); ?>
                        <?php echo $form['lunginfection']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['asthma']->renderLabel(__('Asthma')); ?>
                        <?php echo $form['asthma']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['dyspnoea']->renderLabel(__('Dyspnoea')); ?>
                        <?php echo $form['dyspnoea']->render(array("class"=>"editable")); ?>
                    </li>
                </ol>
                <p style="font-weight: bold">Kidney and urinary tract disease</p>
                <ol>
                    <li></li>
                    <li class="new radio">
                        <?php echo $form['urinateproblem']->renderLabel(__('Difficult to urinate')); ?>
                        <?php echo $form['urinateproblem']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['urinatedisorder']->renderLabel(__('Urinary tract disorder')); ?>
                        <?php echo $form['urinatedisorder']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['kidneydisease']->renderLabel(__('Kidney disease')); ?>
                        <?php echo $form['kidneydisease']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['kidneystone']->renderLabel(__('Kidney stone')); ?>
                        <?php echo $form['kidneystone']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['frequenturinate']->renderLabel(__('Frequent urination')); ?>
                        <?php echo $form['frequenturinate']->render(array("class"=>"editable")); ?>
                    </li>
                </ol>
                <p style="font-weight: bold">Nervous system disease</p>
                <ol>
                    <li></li>
                    <li class="new radio">
                        <?php echo $form['meningitis']->renderLabel(__('Meningitis')); ?>
                        <?php echo $form['meningitis']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['cerebralconcussion']->renderLabel(__('Cerebral concussion')); ?>
                        <?php echo $form['cerebralconcussion']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['poliomyelitis']->renderLabel(__('Poliomyelitis')); ?>
                        <?php echo $form['poliomyelitis']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['epilepsy']->renderLabel(__('Epilepsy')); ?>
                        <?php echo $form['epilepsy']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['stroke']->renderLabel(__('Stroke')); ?>
                        <?php echo $form['stroke']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['headache']->renderLabel(__('Headache')); ?>
                        <?php echo $form['headache']->render(array("class"=>"editable")); ?>
                    </li>
                </ol>
                <p style="font-weight: bold">Digestive tract disease</p>
                <ol>
                    <li></li>
                    <li class="new radio">
                        <?php echo $form['typhoid']->renderLabel(__('Typhoid')); ?>
                        <?php echo $form['typhoid']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['bloodvomit']->renderLabel(__('Blood vomiting')); ?>
                        <?php echo $form['bloodvomit']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['obstipation']->renderLabel(__('Obstipation')); ?>
                        <?php echo $form['obstipation']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['dyspepsia']->renderLabel(__('Dyspepsia')); ?>
                        <?php echo $form['dyspepsia']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['jaundice']->renderLabel(__('Jaundice')); ?>
                        <?php echo $form['jaundice']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['gallbladder']->renderLabel(__('Gall bladder disease')); ?>
                        <?php echo $form['gallbladder']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['swallowingdisorder']->renderLabel(__('Swallowing disorder')); ?>
                        <?php echo $form['swallowingdisorder']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['incontinentiaalvi']->renderLabel(__('Incontinentia alvi')); ?>
                        <?php echo $form['incontinentiaalvi']->render(array("class"=>"editable")); ?>
                    </li>
                </ol>
                <p style="font-weight: bold">Skin / sexual disesase</p>
                <ol>
                    <li></li>
                    <li class="new radio">
                        <?php echo $form['chickenpox']->renderLabel(__('Chicken pox')); ?>
                        <?php echo $form['chickenpox']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['fungalskin']->renderLabel(__('Fungal skin disease')); ?>
                        <?php echo $form['fungalskin']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['std']->renderLabel(__('Sexually transmitted disease')); ?>
                        <?php echo $form['std']->render(array("class"=>"editable")); ?>
                    </li>
                </ol>
                <p style="font-weight: bold">Heart disease</p>
                <ol>
                    <li></li>
                    <li class="new radio">
                        <?php echo $form['heartattack']->renderLabel(__('Heart attack')); ?>
                        <?php echo $form['heartattack']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['chestpains']->renderLabel(__('Chest pains')); ?>
                        <?php echo $form['chestpains']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['palpitation']->renderLabel(__('Palpitation')); ?>
                        <?php echo $form['palpitation']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['hypertension']->renderLabel(__('Hypertension')); ?>
                        <?php echo $form['hypertension']->render(array("class"=>"editable")); ?>
                    </li>
                </ol>
                <p style="font-weight: bold">Vascular system disease</p>
                <ol>
                    <li></li>
                    <li class="new radio">
                        <?php echo $form['hemorrhoid']->renderLabel(__('Hemorrhoid')); ?>
                        <?php echo $form['hemorrhoid']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['varicoseveins']->renderLabel(__('Varicose veins')); ?>
                        <?php echo $form['varicoseveins']->render(array("class"=>"editable")); ?>
                    </li>
                </ol>
                <p style="font-weight: bold">Glands disease</p>
                <ol>
                    <li></li>
                    <li class="new radio">
                        <?php echo $form['thyroiddisease']->renderLabel(__('Thyroid disease')); ?>
                        <?php echo $form['thyroiddisease']->render(array("class"=>"editable")); ?>
                    </li>
                </ol>
                <p style="font-weight: bold">Join and bones disease</p>
                <ol>
                    <li></li>
                    <li class="new radio">
                        <?php echo $form['rhematoidsarthritis']->renderLabel(__('Rhematoids arthritis')); ?>
                        <?php echo $form['rhematoidsarthritis']->render(array("class"=>"editable")); ?>
                    </li>
                </ol>
                <p style="font-weight: bold">Other disease</p>
                <ol>
                    <li></li>
                    <li class="new radio">
                        <?php echo $form['foodallergy']->renderLabel(__('Food allergy')); ?>
                        <?php echo $form['foodallergy']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['foodallergylist']->renderLabel(__('Mention it')); ?>
                        <?php echo $form['foodallergylist']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['drugallergy']->renderLabel(__('Drug allergy')); ?>
                        <?php echo $form['drugallergy']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['drugallergylist']->renderLabel(__('Mention it')); ?>
                        <?php echo $form['drugallergylist']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['tetanus']->renderLabel(__('Tetanus')); ?>
                        <?php echo $form['tetanus']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['fainting']->renderLabel(__('Fainting')); ?>
                        <?php echo $form['fainting']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['oblivion']->renderLabel(__('Oblivion')); ?>
                        <?php echo $form['oblivion']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['consentrationdif']->renderLabel(__('Dificulty to consentration')); ?>
                        <?php echo $form['consentrationdif']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['visiondisorder']->renderLabel(__('Vision disorder')); ?>
                        <?php echo $form['visiondisorder']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['hearingdisorder']->renderLabel(__('Hearing disorder')); ?>
                        <?php echo $form['hearingdisorder']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['lumbago']->renderLabel(__('Lumbago')); ?>
                        <?php echo $form['lumbago']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['neoplasm']->renderLabel(__('Neoplasm')); ?>
                        <?php echo $form['neoplasm']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['mentalillness']->renderLabel(__('Mental illness')); ?>
                        <?php echo $form['mentalillness']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['skintuberculose']->renderLabel(__('Skin tuberculose')); ?>
                        <?php echo $form['skintuberculose']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['bonetuberculose']->renderLabel(__('Bone/Other tuberculose')); ?>
                        <?php echo $form['bonetuberculose']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['measles']->renderLabel(__('Measles')); ?>
                        <?php echo $form['measles']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['malaria']->renderLabel(__('Malaria')); ?>
                        <?php echo $form['malaria']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['diabetes']->renderLabel(__('Diabetes')); ?>
                        <?php echo $form['diabetes']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['sleepdisorder']->renderLabel(__('Sleep disorder')); ?>
                        <?php echo $form['sleepdisorder']->render(array("class"=>"editable")); ?>
                    </li>
                </ol>
                <ol>
                    <li class="new radio">
                        <?php echo $form['hospitalized']->renderLabel(__('Have you ever been hospitalized?')); ?>
                        <?php echo $form['hospitalized']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['hospitalizeddate']->renderLabel(__('Mention when')); ?>
                        <?php echo $form['hospitalizeddate']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['hospitalizedlength']->renderLabel(__('How long')); ?>
                        <?php echo $form['hospitalizedlength']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['hospitalizedwhy']->renderLabel(__('Why')); ?>
                        <?php echo $form['hospitalizedwhy']->render(array("cols" => 20, "rows" => 9)); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['accident']->renderLabel(__('Have you ever had accident?')); ?>
                        <?php echo $form['accident']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['accidentdate']->renderLabel(__('Mention when')); ?>
                        <?php echo $form['accidentdate']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['accidenttxt']->renderLabel(__('What kind of accident')); ?>
                        <?php echo $form['accidenttxt']->render(array("cols" => 20, "rows" => 9)); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['surgery']->renderLabel(__('Have you ever been on surgical operation?')); ?>
                        <?php echo $form['surgery']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['surgerydate']->renderLabel(__('Mention when')); ?>
                        <?php echo $form['surgerydate']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['surgerytxt']->renderLabel(__('What kind of surgery')); ?>
                        <?php echo $form['surgerytxt']->render(array("cols" => 20, "rows" => 9)); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['smoker']->renderLabel(__('Are you smoker?')); ?>
                        <?php echo $form['smoker']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['smokernumber']->renderLabel(__('Cigarettes per day')); ?>
                        <?php echo $form['smokernumber']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['smokerage']->renderLabel(__('Begin to smoke at age')); ?>
                        <?php echo $form['smokerage']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['smokerstop']->renderLabel(__('Stop smoking in')); ?>
                        <?php echo $form['smokerstop']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['alcohol']->renderLabel(__('Do you drink alcohol?')); ?>
                        <?php echo $form['alcohol']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['alcoholperweek']->renderLabel(__('Drink per week')); ?>
                        <?php echo $form['alcoholperweek']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <?php echo $form['alcoholslokyperweek']->renderLabel(__('Sloky per week')); ?>
                        <?php echo $form['alcoholslokyperweek']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['exercise']->renderLabel(__('Do you like to exercise?')); ?>
                        <?php echo $form['exercise']->render(array("class"=>"editable")); ?>
                    </li>
                    <li>
                        <?php echo $form['exercisetxt']->renderLabel(__('What kind of exercise and exercise per week')); ?>
                        <?php echo $form['exercisetxt']->render(array("cols" => 20, "rows" => 9)); ?>
                    </li>
                    <li>
                        <?php echo $form['medicinelist']->renderLabel(__('What medicines you use frequently?')); ?>
                        <?php echo $form['medicinelist']->render(array("class" => "formInputText")); ?>
                    </li>
                </ol>
                </div>
                <hr style="border: 0px dashed #727272;">
                <label><span class="keywrd" style="font-weight: bold">VACCINATION HISTORY</span><span id="vacExtend"> [+]</span></label>
                <div id="vachistory">
                <ol>
                    <li></li>
                    <li class="new radio">
                        <?php echo $form['vacdiptheri']->renderLabel(__('Diptheri')); ?>
                        <?php echo $form['vacdiptheri']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['vactetanus']->renderLabel(__('Tetanus')); ?>
                        <?php echo $form['vactetanus']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['vacpolio']->renderLabel(__('Polio')); ?>
                        <?php echo $form['vacpolio']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['vacbcg']->renderLabel(__('BCG')); ?>
                        <?php echo $form['vacbcg']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['vacmmr']->renderLabel(__('MMR')); ?>
                        <?php echo $form['vacmmr']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['vacinfluenza']->renderLabel(__('Influenza')); ?>
                        <?php echo $form['vacinfluenza']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['vachepatitisa']->renderLabel(__('Hepatitis A')); ?>
                        <?php echo $form['vachepatitisa']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['vachepatitisb']->renderLabel(__('Hepatitis B')); ?>
                        <?php echo $form['vachepatitisb']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['vacvarisela']->renderLabel(__('Varisela')); ?>
                        <?php echo $form['vacvarisela']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['vactyphoid']->renderLabel(__('Typhoid')); ?>
                        <?php echo $form['vactyphoid']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['vachpv']->renderLabel(__('HPV')); ?>
                        <?php echo $form['vachpv']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['vacmeningokok']->renderLabel(__('Meningokok')); ?>
                        <?php echo $form['vacmeningokok']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['vacyellowfever']->renderLabel(__('Yellow fever')); ?>
                        <?php echo $form['vacyellowfever']->render(array("class"=>"editable")); ?>
                    </li>
                    <li class="new radio">
                        <?php echo $form['vacjapencephalitis']->renderLabel(__('Japanese encephalitis')); ?>
                        <?php echo $form['vacjapencephalitis']->render(array("class"=>"editable")); ?>
                    </li>
                </ol>
                </div>
                <hr style="border: 0px dashed #727272;">
                <ol>
                    <li>
                        <p style="width: 50%;font-weight: bold">Hereby declare that all data that I have entered is true. If proven otherwise in the future, I am willing to be sanctioned in accordance with the current PT. Prodia OHI International regulations.</p>
                    </li>
                </ol>
                <ol>
                    <li>
                        <input type="button" class="savebutton" name="btnSave" id="btnSave" value="<?php echo __("Submit"); ?>" />
                    </li>
                </ol>
            </fieldset>
        </form>
    </div>
</div>
<script type="text/javascript">
    var description	= '<?php $description; ?>';
    var vacancyId	= '<?php echo $vacancyId; ?>';
    var candidateId	= '<?php echo ($candidateId !="") ? $candidateId : 0;?>';
    var lang_bloodTypeRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_heightRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_weightRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_npwpRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_bpjsNakerRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_bpjsKesRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_faskesRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_kkRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_familyHeadRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_EmergencyRequired = "<?php echo(ValidationMessages::REQUIRED);?>";
    var linkForUpdateApplication = "<?php echo url_for('recruitmentApply/updateApplicationData'); ?>";

    $(document).ready(function() {
/*        var isEmergencyCollapse = false;
        $("#emergency").attr('disabled', 'disabled');
        $("#emergency").hide();

        $('#emergencyExtend').click(function(){
            if(!isEmergencyCollapse){
                $("#emergency").show();
                isEmergencyCollapse = true;
                $('#emergencyExtend').text(' [-]');
            } else {
                $("#emergency").hide();
                isEmergencyCollapse = false;
                $('#emergencyExtend').text(' [+]');
            }
        });*/

        var isFamilyCollapse = false;
        $("#family").attr('disabled', 'disabled');
        $("#family").hide();

        $('#familyExtend').click(function(){
            if(!isFamilyCollapse){
                $("#family").show();
                isFamilyCollapse = true;
                $('#familyExtend').text(' [-]');
            } else {
                $("#family").hide();
                isFamilyCollapse = false;
                $('#familyExtend').text(' [+]');
            }
        });

        var isOrgCollapse = false;
        $("#org").attr('disabled', 'disabled');
        $("#org").hide();

        $('#orgExtend').click(function(){
            if(!isOrgCollapse){
                $("#org").show();
                isOrgCollapse = true;
                $('#orgExtend').text(' [-]');
            } else {
                $("#org").hide();
                isOrgCollapse = false;
                $('#orgExtend').text(' [+]');
            }
        });

        var isMedCollapse = false;
        $("#medhistory").attr('disabled', 'disabled');
        $("#medhistory").hide();

        $('#medExtend').click(function(){
            if(!isMedCollapse){
                $("#medhistory").show();
                isMedCollapse = true;
                $('#medExtend').text(' [-]');
            } else {
                $("#medhistory").hide();
                isMedCollapse = false;
                $('#medExtend').text(' [+]');
            }
        });

        var isVacCollapse = false;
        $("#vachistory").attr('disabled', 'disabled');
        $("#vachistory").hide();

        $('#vacExtend').click(function(){
            if(!isVacCollapse){
                $("#vachistory").show();
                isVacCollapse = true;
                $('#vacExtend').text(' [-]');
            } else {
                $("#vachistory").hide();
                isVacCollapse = false;
                $('#vacExtend').text(' [+]');
            }
        });

        $('#btnSave').click(function() {
            if(isValidForm()){
                //$('#addCandidate_vacancyList').val(vacancyId);
                //$('#addCandidate_keyWords.inputFormatHint').val('');
                $('form#frmUpdateApplicationData').attr({
                    action:linkForUpdateApplication
                });
                $('form#frmUpdateApplicationData').submit();
            }
        });

        function isValidForm() {

            var validator = $("#frmUpdateApplicationData").validate({

                rules: {
                    'viewApplicationStatusData[bloodtype]': {
                        required: true
                    },
                    'viewApplicationStatusData[heght]': {
                        required: true,
                        maxlength:5
                    },
                    'viewApplicationStatusData[weight]': {
                        required: true,
                        maxlength:5
                    },
                    'viewApplicationStatusData[npwp]': {
                        required: true,
                        maxlength:45
                    },
                    'viewApplicationStatusData[bpjsnaker]': {
                        required: true,
                        maxlength:45
                    },
                    'viewApplicationStatusData[bpjskes]': {
                        required: true,
                        maxlength:45
                    },
                    'viewApplicationStatusData[faskes]': {
                        required: true,
                        maxlength:41000
                    },
                    'viewApplicationStatusData[kk]': {
                        required: true,
                        maxlength:45
                    },
                    'viewApplicationStatusData[familyhead]': {
                        required: true
                    },

                    'viewApplicationStatusData[emergencyname]': {
                        required: true
                    },

                    'viewApplicationStatusData[emergencyaddr]': {
                        required: true
                    },

                    'viewApplicationStatusData[emergencyprovince]': {
                        required: true
                    },

                    'viewApplicationStatusData[emergencycity]': {
                        required: true
                    },

                    'viewApplicationStatusData[emergencykecamatan]': {
                        required: true
                    },

                    'viewApplicationStatusData[emergencykelurahan]': {
                        required: true
                    },

                    'viewApplicationStatusData[emergencyzip]': {
                        required: true
                    },

                    'viewApplicationStatusData[emergencyrelation]': {
                        required: true
                    },

                    'viewApplicationStatusData[emergencyphone]': {
                        required: true
                    }

                },
                messages: {
                    'viewApplicationStatusData[bloodtype]': {
                        required: lang_bloodTypeRequired
                    },
                    'viewApplicationStatusData[heght]': {
                        required: lang_heightRequired
                    },
                    'viewApplicationStatusData[weight]': {
                        required: lang_weightRequired
                    },
                    'viewApplicationStatusData[npwp]': {
                        required: lang_npwpRequired
                    },
                    'viewApplicationStatusData[bpjsnaker]': {
                        required: lang_bpjsNakerRequired
                    },
                    'viewApplicationStatusData[bpjskes]': {
                        required: lang_bpjsKesRequired
                    },
                    'viewApplicationStatusData[faskes]': {
                        required: lang_faskesRequired
                    },
                    'viewApplicationStatusData[kk]': {
                        required: lang_kkRequired
                    },
                    'viewApplicationStatusData[familyhead]': {
                        required: lang_familyHeadRequired
                    },

                    'viewApplicationStatusData[emergencyname]': {
                        required: lang_EmergencyRequired
                    },

                    'viewApplicationStatusData[emergencyaddr]': {
                        required: lang_EmergencyRequired
                    },

                    'viewApplicationStatusData[emergencyprovince]': {
                        required: lang_EmergencyRequired
                    },

                    'viewApplicationStatusData[emergencycity]': {
                        required: lang_EmergencyRequired
                    },

                    'viewApplicationStatusData[emergencykecamatan]': {
                        required: lang_EmergencyRequired
                    },

                    'viewApplicationStatusData[emergencykelurahan]': {
                        required: lang_EmergencyRequired
                    },

                    'viewApplicationStatusData[emergencyzip]': {
                        required: lang_EmergencyRequired
                    },

                    'viewApplicationStatusData[emergencyrelation]': {
                        required: lang_EmergencyRequired
                    },

                    'viewApplicationStatusData[emergencyphone]': {
                        required: lang_EmergencyRequired
                    }
                }

            });
            return true;
        }
    });

    //Eka: add ajax for retrieving nomornet data
    var provinsi = "";
    var city = "";
    var kecamatan = "";
    var desaKodepos = {};

    //Eka: address domisili
    var domProvinsi = "";
    var domCity = "";
    var domKecamatan = "";
    var domDesaKodepos = {};

    //var nomornetHost = "http://localhost:8880";
    <?php
    //getting server's ip addr
        $host= gethostname();
        $ip_server = gethostbyname($host);
        //$ip_server = $_SERVER['SERVER_ADDR'];
        $nomornetHost = "http://" . $ip_server . "/nomornet_api";
    ?>
    var nomornetHost = "<?php echo $nomornetHost ?>";

    //var nomornetHost = "http://localhost/nomornet_api";
    //var nomornetHost = "http://118.97.180.122:8880";

    //emergency address
    $("#viewApplicationStatusData_emergencyprovince").change(function (){
        //alert($(this).val());
        provinsi = $(this).val();
        var $dropdown = $("#viewApplicationStatusData_emergencykecamatan");
        $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
        var $dropdown2 = $("#viewApplicationStatusData_emergencykelurahan");
        $dropdown2.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
        $("#viewApplicationStatusData_emergencyzip").val("");
        $.ajax({
            type:"GET",
            dataType: "json",
            url: nomornetHost + "/nomornet/kotakabupaten/" + provinsi,
            success: function(result){
                console.log(result);
                resultArray = result['records'];
                var $dropdown = $("#viewApplicationStatusData_emergencycity");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
                for(var i in resultArray){
                    console.log(resultArray[i].kotakabupaten);
                    $dropdown.append($("<option />").val(resultArray[i].kotakabupaten).text(resultArray[i].kotakabupaten));
                }
                //alert(result['records'][0].kotakabupaten);
            },
            error: function(jqxhrTxt, statusTxt, errTxt){
                console.log(statusTxt);
                var $dropdown = $("#viewApplicationStatusData_emergencycity");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
            }
        })
    });

    $("#viewApplicationStatusData_emergencycity").change(function (){
        //alert($(this).val());
        provinsi = $("#viewApplicationStatusData_emergencyprovince").val();
        city = $(this).val();
        var $dropdown = $("#viewApplicationStatusData_emergencykelurahan");
        $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
        $("#viewApplicationStatusData_emergencyzip").val("");
        $.ajax({
            type:"GET",
            dataType: "json",
            url: nomornetHost + "/nomornet/kecamatan/" + provinsi + "/" + city,
            success: function(result){
                console.log(result);
                resultArray = result['records'];
                var $dropdown = $("#viewApplicationStatusData_emergencykecamatan");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
                for(var i in resultArray){
                    console.log(resultArray[i].kecamatan);
                    $dropdown.append($("<option />").val(resultArray[i].kecamatan).text(resultArray[i].kecamatan));
                }
                //alert(result['records'][0].kotakabupaten);
            },
            error: function(jqxhrTxt, statusTxt, errTxt){
                console.log(statusTxt);
                var $dropdown = $("#viewApplicationStatusData_emergencykecamatan");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
            }
        })
    });

    $("#viewApplicationStatusData_emergencykecamatan").change(function (){
        //alert($(this).val());
        provinsi = $("#viewApplicationStatusData_emergencyprovince").val();
        city = $("#viewApplicationStatusData_emergencycity").val();
        kecamatan = $(this).val();
        $("#viewApplicationStatusData_emergencyzip").val("");
        $.ajax({
            type:"GET",
            dataType: "json",
            url: nomornetHost + "/nomornet/kodeposdesa/" + provinsi + "/" + city + "/" + kecamatan,
            success: function(result){
                console.log(result);
                resultArray = result['records'];
                var $dropdown = $("#viewApplicationStatusData_emergencykelurahan");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
                for(var i in resultArray){
                    console.log(resultArray[i].desakelurahan + " - " + resultArray[i].kodepos);
                    $dropdown.append($("<option />").val(resultArray[i].desakelurahan).text(resultArray[i].desakelurahan));
                    desaKodepos[resultArray[i].desakelurahan] = resultArray[i].kodepos;
                }
                //alert(result['records'][0].kotakabupaten);
            },
            error: function(jqxhrTxt, statusTxt, errTxt){
                console.log(statusTxt);
                var $dropdown = $("#viewApplicationStatusData_emergencykelurahan");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
            }
        })
    });

    $("#viewApplicationStatusData_emergencykelurahan").change(function (){
        //alert($(this).val());
        kelurahan = $(this).val();
        $("#viewApplicationStatusData_emergencyzip").val(desaKodepos[kelurahan]);
    });
</script>

