<?php use_javascript(plugin_web_path('orangehrmRecruitmentPlugin', 'js/viewJobsSuccess')); ?>

<style type="text/css">
    body
    {
        /* Font */
        /* Emoji fonts are added to visualise them nicely in Internet Explorer. */
        font-family: sans-serif, Arial, Verdana, "Trebuchet MS", "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
        font-size: 12px;

        /* Text color */
        color: #333;

        /* Remove the background color to make it transparent. */
        background-color: #fff;

    }

    blockquote
    {
        font-style: italic;
        font-family: Georgia, Times, "Times New Roman", serif;
        padding: 2px 0;
        border-style: solid;
        border-color: #ccc;
        border-width: 0;
    }

    a
    {
        color: #0782C1;
    }

    ul,dl
    {
        /* IE7: reset rtl list margin. (#7334) */
        *margin-right: 0px;
        /* Preserved spaces for list items with text direction different than the list. (#6249,#8049)*/
        padding: 0 40px;
        line-height: 0.5;
    }

    ol
    {
        /* IE7: reset rtl list margin. (#7334) */
        *margin-right: 0px;
        /* Preserved spaces for list items with text direction different than the list. (#6249,#8049)*/
        padding: 0 40px;
        list-style: decimal;
        line-height: 0.5;
    }

    ul li {
        list-style: disc outside none;
        margin: 0.5px 0;
    }

    ul li ul li{
        list-style: circle outside none;
        margin: 0.5px 0;
    }

    ul li ul li ul li{
        list-style: square outside none;
        margin: 0.5px 0;
    }

    h1,h2,h3,h4,h5,h6
    {
        font-weight: normal;
        line-height: 1.2;
    }

    hr
    {
        border: 0px;
        border-top: 1px solid #ccc;
    }

    img.right
    {
        border: 1px solid #ccc;
        float: right;
        margin-left: 15px;
        padding: 5px;
    }

    img.left
    {
        border: 1px solid #ccc;
        float: left;
        margin-right: 15px;
        padding: 5px;
    }

    pre
    {
        white-space: pre-wrap; /* CSS 2.1 */
        word-wrap: break-word; /* IE7 */
        -moz-tab-size: 4;
        tab-size: 4;
    }

    #content {
        padding-top: 0;
    }


    #toggleJobList {
        float: right;
        margin: -2px 10px 0px 0px;
        font-size: 12px;
    }

    #toggleJobList span {
        text-decoration: underline;
        cursor: pointer;
    }

    .vacancyDescription, .vacancyShortDescription {
        display: none;
        line-height: 0.5;
        margin-bottom: 10px;
    }

    /*eka modif*/
    .vacancyShortDescription {
        display: block;
    }

    .applyLink {
        display: none;
    }

    .vacancyTitle :hover {
        cursor: pointer;
    }

    .plusOrMinusmark {
        text-align: right;
        margin-top: 10px;
        padding-right: 3px;
        font-size: 12px;
        position: relative;
        top: 12px;
        right: 8px;
    }

    .plusMark, .minusMark {
        cursor: pointer;
    }

    .minusMark {
        display: none;
    }

    h3 {
        margin-bottom: 10px;
    }

</style>

<div id="jobPage">
    <div class="box">
        <div class="maincontent">
            <div class="head">
                <h1><?php echo __('Active Job Vacancies'); ?></h1>
            </div>

            <div class="inner">
                <?php if (count($publishedVacancies) != 0): ?>                    
                    <div id="toggleJobList">
                        <span id="expandJobList"><?php echo __('Expand all') ?></span> | <span id="collapsJobList"><?php echo __('Collapse all'); ?></span>
                    </div>

                    <?php foreach ($publishedVacancies as $vacancy): ?>

                        <div class="plusOrMinusmark">
                            <span class="plusMark">[+]</span><span class="minusMark">[-]</span>
                        </div>

                        <div class="jobItem">

                            <div class="vacancyTitle">
                                <h3><?php echo $vacancy->getName(); ?></h3>
                            </div>

                            <pre class="vacancyShortDescription"><?php echo html_entity_decode(getShortDescription($vacancy->getDescription(), 250, "...")); ?></pre>
                            <pre class="vacancyDescription"><?php echo html_entity_decode($vacancy->getDescription()); ?></pre>
                            
                            <p class="borderBottom">
                                <input type="button" class="apply" name="applyButton" value="<?php echo __("Apply"); ?>" onmouseout="moutButton(this);" onmouseover="moverButton(this);" />
                                <a href="<?php echo public_path('index.php/recruitmentApply/applyVacancy/id/' . $vacancy->getId(), true); ?>" class="applyLink"></a>
                                <input type="button" class="apply" name="applyButton" value="<?php echo __("Check Application Status"); ?>" onmouseout="moutButton(this);" onmouseover="moverButton(this);" />
                                <a href="<?php echo public_path('index.php/recruitmentApply/viewApplicationStatus/' , true); ?>" class="applyLink"></a>
                            </p>
                            
                        </div>
                    <?php endforeach; ?>
                <?php else: ?>
                    <span class="noVacanciesMessage"><?php echo __('No active job vacancies to display'); ?></span>
                <?php endif; ?>

            </div>

        </div>

    </div>

</div>
<div id="footer">
    <?php include_partial('global/copyright');?>
</div>
<?php
/*
 * Get short description to show in default view in view job list
 * @param string $description full description
 * @param int $limit Number of characters show in short description
 * @param string $endString String added to end of the short description
 * @return string $description short description 
 */

function getShortDescription($description, $limit, $endString) {

    if (strlen($description) > $limit) {
        $subString = substr($description, 0, $limit);
        $wordArray = explode(" ", $subString);
        $description = substr($subString, 0, -(strlen($wordArray[count($wordArray) - 1]) + 1)) . $endString;
    }
    return $description;
}