<?php
/**
 * Created by PhpStorm.
 * User: kelanaeka
 * Date: 4/10/2020
 * Time: 1:35 AM
 */
?>
<style type="text/css">
    #content {
        padding-top: 0;
    }
</style>
<div id="addCandidate" class="box">
    <div class="head"><h1 id="addCandidateHeading"><?php echo __("Apply for ") . $jobVacancyName; ?></h1></div>

    <?php include_component('core', 'ohrmPluginPannel', array('location' => 'add_layout_after_main_heading_1')) ?>

    <div class="inner">

        <?php include_partial('global/flash_messages', array('prefix' => 'applyVacancy')); ?>
        <!--<ol>

            <li>
                <label><span class="keywrd" style="font-weight: bold">DESCRIPTION</span> <span  id="extend">[+]</span></label>
                <div id="txtArea" style="width:100%;margin-left: 150px">
                    <pre class="vacancyDescription"><?php echo html_entity_decode($description); ?></pre>
                </div>
            </li>
        </ol>-->

        <form name="frmViewApplicationStatus" id="frmViewApplicationStatus" method="post">
            <fieldset>
                <hr style="border: 0px dashed #727272;">
                <label><span class="keywrd" style="font-weight: bold"><?php echo("Your Application ID: ") . $applicationId  ?></span></label>
                <ol>
                    <li>
                        <!--<p class="keywrd" style="font-weight: bold">IDENTITY</p>-->
                    </li>

                    <?php include_component('core', 'ohrmPluginPannel', array('location' => 'add_layout_after_main_heading_2')) ?>
                    <li>
                        <p style="width: 50%;line-height: 1.5"><?php echo htmlspecialchars_decode($closingStatement); ?></p>
                    </li>
                    <li>
                        <?php
                        $fileName = "";
                        $fileId = "";
                        //$kontrakFileName = "";
                        //$kontrakFileId = "";
                        //$gajiFileName = "";
                        //$gajiFileId = "";
                        if (count($attachments) > 0) {
                            //remove psikotest file
                            //$fileTitleArray = array(AddCandidateForm::PSIKOTEST_FILE, AddCandidateForm::CONTRACT_FILE, AddCandidateForm::SALARY_FILE);
                            //$fileTitleArray = array(AddCandidateForm::CONTRACT_FILE, AddCandidateForm::SALARY_FILE);
                            $fileTitleArray = array(AddCandidateForm::CONTRACT_FILE);
                            $linkHtml = "<table style='border-spacing: 50px'><tr>";
                            foreach($attachments as $attachment){
                                //$attachment = $form->attachment[$i];
                                /*if($attachment->getFiletitle() == AddCandidateForm::PSIKOTEST_FILE){
                                    $fileName = "Hasil Psikotest";
                                    $fileId = $attachment->getId();
                                }*/

                                if($attachment->getFiletitle() == AddCandidateForm::CONTRACT_FILE){
                                    $fileName = "Dokumen Kontrak";
                                    $fileId = $attachment->getId();
                                }

                                /*if($attachment->getFiletitle() == AddCandidateForm::SALARY_FILE){
                                    $fileName = "Rincian Gaji";
                                    $fileId = $attachment->getId();
                                }*/

                                if(in_array($attachment->getFiletitle(), $fileTitleArray)){
                                    $linkHtml .= "<td style='padding: 20px; text-align: center;'><div id=\"fileLink\"><a target=\"_blank\" class=\"fileLink\" href=\"";
                                    $linkHtml .= url_for('recruitmentApply/viewCandidateAttachment?attachId=' . $fileId);
                                    $linkHtml .= "\"><i class=\"material-icons\" style=\"font-size:48px;{$color}\">description</i><br>{$fileName}</a></div></td>";
                                }
                            }
                            $linkHtml .= "</tr></table>";

                            //$linkHtml .= "<td style='padding: 20px; text-align: center;'><div id=\"fileLink\"><a target=\"_blank\" class=\"fileLink\" href=\"";
                            //$linkHtml .= url_for('recruitmentApply/viewCandidateAttachment?attachId=' . $psiFileId);
                            //$linkHtml .= "\"><i class=\"material-icons\" style=\"font-size:48px;{$color}\">description</i><br>{$psiFileName}</a></div></td>";
                            //$linkHtml .= "</tr></table>";

                            //echo $form['resumeUpdate']->renderLabel(__('Attachment'));
                            echo $linkHtml;
                            /*echo "<li class=\"radio noLabel\" id=\"radio\">";
                            echo $form['resumeUpdate']->render(array("class" => "fileEditOptions"));
                            echo "</li>";
                            echo "<li id=\"fileUploadSection\" class=\"noLabel\">";
                            echo $form['resume']->renderLabel(' ');
                            echo $form['resume']->render(array("class " => "duplexBox"));
                            echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_DOC) . "</label>";
                            echo "</li>";*/
                        }
                        /*else {
                            echo $form['resume']->renderLabel(__('Attachment'), array("class " => "resume"));
                            //echo $form['resume']->render();
                            //echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_DOC) . "</label>";
                        }*/
                        ?>
                    </li>
                    <li>
                        <a id="backLink" href="<?php echo url_for('recruitmentApply/jobs') ?>"><?php echo __("Back to Job List"); ?></a>
                    </li>
                </ol>
            </fieldset>
        </form>
    </div>
</div>
<script type="text/javascript">
    var linkForViewJobs = "<?php echo url_for('recruitmentApply/viewJobs'); ?>";
    //var lang_back = "<?php echo __("Go to Job Page")?>";

    $(document).ready(function() {
        $('#backLink').click(function(){
            window.location.replace(linkForViewJobs);
        });

    });
</script>

