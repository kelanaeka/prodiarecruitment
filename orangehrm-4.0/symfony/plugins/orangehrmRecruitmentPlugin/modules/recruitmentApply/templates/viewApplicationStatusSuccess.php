<?php
/**
 * Created by PhpStorm.
 * User: kelanaeka
 * Date: 3/29/2020
 * Time: 10:59 AM
 */
?>
<style type="text/css">
    #content {
        padding-top: 0;
    }
</style>

<div id="addCandidate" class="box">
    <div class="head"><h1 id="addCandidateHeading"><?php echo __("Check Your Application Status"); ?></h1></div>

    <?php include_component('core', 'ohrmPluginPannel', array('location' => 'add_layout_after_main_heading_1')) ?>

    <div class="inner">

        <?php include_partial('global/flash_messages', array('prefix' => 'applyVacancy')); ?>
        <!--<ol>

            <li>
                <label><span class="keywrd" style="font-weight: bold">DESCRIPTION</span> <span  id="extend">[+]</span></label>
                <div id="txtArea" style="width:100%;margin-left: 150px">
                    <pre class="vacancyDescription"><?php echo html_entity_decode($description); ?></pre>
                </div>
            </li>
        </ol>-->

        <form name="frmViewApplicationStatus" id="frmViewApplicationStatus" method="post">
            <fieldset>
                <?php echo $form['_csrf_token']; ?>
                <hr style="border: 0px dashed #727272;">
                <label><span class="keywrd" style="font-weight: bold">Enter Your Application ID Below To Retrieve Application Status</span></label>
                <ol>
                    <li>
                        <!--<p class="keywrd" style="font-weight: bold">IDENTITY</p>-->
                    </li>

                    <?php include_component('core', 'ohrmPluginPannel', array('location' => 'add_layout_after_main_heading_2')) ?>
                    <li>
                        <?php echo $form['applicationId']->renderLabel(__('Application ID')); ?>
                        <?php echo $form['applicationId']->render(array("class" => "formInputText")); ?>
                    </li>
                    <li>
                        <input type="button" class="savebutton" name="btnSave" id="btnSave" value="<?php echo __("Submit"); ?>" /> <a id="backLink" href="<?php echo url_for('recruitmentApply/jobs') ?>"><?php echo __("Back to Job List"); ?></a>
                    </li>
                </ol>
            </fieldset>
        </form>
    </div>
</div>
<script type="text/javascript">
    var description	= '<?php $description; ?>';
    var vacancyId	= '<?php echo $vacancyId; ?>';
    var candidateId	= '<?php echo ($candidateId !="") ? $candidateId : 0;?>';
    var lang_firstNameRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_tooLargeInput = "<?php echo __(ValidationMessages::TEXT_LENGTH_EXCEEDS, array('%amount%' => 10)); ?>";
    var linkForViewApplication = "<?php echo url_for('recruitmentApply/viewApplicationStatus'); ?>";
    var linkForViewJobs = "<?php echo url_for('recruitmentApply/viewJobs'); ?>";
    var lang_back = "<?php echo __("Go to Job Page")?>";

    $(document).ready(function() {
        $('#backLink').click(function(){
            window.location.replace(linkForViewJobs);
        });

        $('#btnSave').click(function() {
            if(isValidForm()){
                //$('#addCandidate_vacancyList').val(vacancyId);
                //$('#addCandidate_keyWords.inputFormatHint').val('');
                $('form#frmViewApplicationStatus').attr({
                    action:linkForViewApplication
                });
                $('form#frmViewApplicationStatus').submit();
            }
        });

        function isValidForm() {

            var validator = $("#frmViewApplicationStatus").validate({

                rules: {
                    'viewApplicationStatusVerify[applicationId]': {
                        required: true,
                        maxlength: 10
                    }
                },
                messages: {
                    'viewApplicationStatusVerify[applicationId]': {
                        required: lang_firstNameRequired,
                        maxlength: lang_tooLargeInput
                    }
                }

            });
            return true;
        }
    });
</script>
