<?php
/**
 * OrangeHRM is a comprehensive Human Resource Management (HRM) System that captures
 * all the essential functionalities required for any enterprise.
 * Copyright (C) 2006 OrangeHRM Inc., http://www.orangehrm.com
 *
 * OrangeHRM is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * OrangeHRM is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA
 *
 */
?>

<?php use_javascript(plugin_web_path('orangehrmRecruitmentPlugin', 'js/applyVacancySuccess')); ?>

<style type="text/css">
    #content {
        padding-top: 0;
    }

    .inner #txtArea {

    blockquote
    {
        font-style: italic;
        font-family: Georgia, Times, "Times New Roman", serif;
        padding: 2px 0;
        border-style: solid;
        border-color: #ccc;
        border-width: 0;
    }

    a
    {
        color: #0782C1;
    }

    ul,dl
    {
        /* IE7: reset rtl list margin. (#7334) */
        *margin-right: 0px;
        /* Preserved spaces for list items with text direction different than the list. (#6249,#8049)*/
        padding: 0 40px;
        line-height: 0.5;
    }

    ol
    {
        /* IE7: reset rtl list margin. (#7334) */
        *margin-right: 0px;
        /* Preserved spaces for list items with text direction different than the list. (#6249,#8049)*/
        padding: 0 40px;
        list-style: decimal;
        line-height: 0.5;
    }

    ul li {
        list-style: disc outside none !important;
        margin: 0.5px 0;
    }

    ul li ul li{
        list-style: circle outside none !important;
        margin: 0.5px 0;
    }

    ul li ul li ul li{
        list-style: square outside none !important;
        margin: 0.5px 0;
    }

    h1,h2,h3,h4,h5,h6
    {
        font-weight: normal;
        line-height: 1.2;
    }

    hr
    {
        border: 0px;
        border-top: 1px solid #ccc;
    }

    img.right
    {
        border: 1px solid #ccc;
        float: right;
        margin-left: 15px;
        padding: 5px;
    }

    img.left
    {
        border: 1px solid #ccc;
        float: left;
        margin-right: 15px;
        padding: 5px;
    }

    pre
    {
        white-space: pre-wrap; /* CSS 2.1 */
        word-wrap: break-word; /* IE7 */
        -moz-tab-size: 4;
        tab-size: 4;
    }

    .vacancyDescription {
        display: none;
        line-height: 0.5;
        margin-bottom: 10px;
    }

    .applyLink {
        display: none;
    }

    .vacancyTitle :hover {
        cursor: pointer;
    }

    .plusOrMinusmark {
        text-align: right;
        margin-top: 10px;
        padding-right: 3px;
        font-size: 12px;
        position: relative;
        top: 12px;
        right: 8px;
    }

    .plusMark, .minusMark {
        cursor: pointer;
    }

    .minusMark {
        display: none;
    }

    h3 {
        margin-bottom: 10px;
    }

    }



</style>

<div id="addCandidate" class="box">

        <div class="head"><h1 id="addCandidateHeading"><?php echo __("Apply for") . " " . $name; ?></h1></div>
        
        <?php include_component('core', 'ohrmPluginPannel', array('location' => 'add_layout_after_main_heading_1')) ?>
        
        <div class="inner">
            
        <?php include_partial('global/flash_messages', array('prefix' => 'applyVacancy')); ?>
            <ol>

                <li>
                    <label><span class="keywrd" style="font-weight: bold">DESCRIPTION</span> <span  id="extend">[+]</span></label>
                    <div id="txtArea" style="width:100%;margin-left: 150px">
                        <pre class="vacancyDescription"><?php echo html_entity_decode($description); ?></pre>
                    </div>
                </li>
            </ol>

        <form name="frmAddCandidate" id="frmAddCandidate" method="post" enctype="multipart/form-data">

            <fieldset>
                
            <?php echo $form['_csrf_token']; ?>
            <?php echo $form["vacancyList"]->render(); ?>
            <hr style="border: 0px dashed #727272;">
            <label><span class="keywrd" style="font-weight: bold">IDENTITY</span></label>
            <ol>
                <li>
                    <!--<p class="keywrd" style="font-weight: bold">IDENTITY</p>-->
                </li>

                <li class="line nameContainer">

                    <label class="hasTopFieldHelp"><?php echo __('Full Name'); ?></label>
                    <ol class="fieldsInLine">
                        <li>
                            <div class="fieldDescription"> <?php echo __('Front Title'); ?></div>
                            <?php echo $form['firstName']->render(array("class" => "formInputText", "maxlength" => 35)); ?>
                        </li>
                        <li>
                            <div class="fieldDescription"><em>*</em> <?php echo __('Full Name'); ?></div>
                             <?php echo $form['middleName']->render(array("class" => "formInputText", "maxlength" => 255)); ?>
                        </li>
                        <li>
                            <div class="fieldDescription"><em>*</em> <?php echo __('End Title'); ?></div>
                            <?php echo $form['lastName']->render(array("class" => "formInputText", "maxlength" => 35)); ?>
                        </li>
                    </ol>                        

                </li>                
                
            <?php include_component('core', 'ohrmPluginPannel', array('location' => 'add_layout_after_main_heading_2')) ?>
            <li  class="new radio">
                <?php echo $form['gender']->renderLabel(__('Gender'). ' <span class="required">*</span>'); ?>
                <?php echo $form['gender']->render(array("class"=>"editable")); ?>
            </li>
            <li>
                <?php echo $form['birthplace']->renderLabel(__('Birth place'). ' <span class="required">*</span>'); ?>
                <?php echo $form['birthplace']->render(array("class" => "formInputText")); ?>
            </li>
            <li>
                <?php echo $form['dob']->renderLabel(__('Date of Birth'). ' <span class="required">*</span>'); ?>
                <?php echo $form['dob']->render(array("class"=>"editable")); ?>
            </li>
            <li>
                <?php echo $form['currentaddr']->renderLabel(__('Current Address'). ' <span class="required">*</span>'); ?>
                <?php echo $form['currentaddr']->render(array("class" => "formInputText")); ?>
            </li>
                <li>
                    <?php echo $form['currentprovince']->renderLabel(__('Current Province'). ' <span class="required">*</span>'); ?>
                    <?php echo $form['currentprovince']->render(array("class"=>"editable")); ?>
                </li>
            <li>
                <?php echo $form['currentcity']->renderLabel(__('Current City'). ' <span class="required">*</span>'); ?>
                <?php echo $form['currentcity']->render(array("class"=>"editable")); ?>
            </li>
                <li>
                    <?php echo $form['currentkecamatan']->renderLabel(__('Current Kecamatan'). ' <span class="required">*</span>'); ?>
                    <?php echo $form['currentkecamatan']->render(array("class"=>"editable")); ?>
                </li>
                <li>
                    <?php echo $form['currentkelurahan']->renderLabel(__('Current Kelurahan'). ' <span class="required">*</span>'); ?>
                    <?php echo $form['currentkelurahan']->render(array("class"=>"editable")); ?>
                </li>
            <li>
                <?php echo $form['currentzip']->renderLabel(__('Current Zip'). ' <span class="required">*</span>'); ?>
                <?php echo $form['currentzip']->render(array("class" => "formInputText")); ?>
            </li>
            <li>
                <?php echo $form['permanentaddr']->renderLabel(__('Permanent Addr'). ' <span class="required">*</span>'); ?>
                <?php echo $form['permanentaddr']->render(array("class" => "formInputText")); ?>
            </li>
                <li>
                    <?php echo $form['permanentprovince']->renderLabel(__('Permanent Province'). ' <span class="required">*</span>'); ?>
                    <?php echo $form['permanentprovince']->render(array("class"=>"editable")); ?>
                </li>
            <li>
                <?php echo $form['permanentcity']->renderLabel(__('Permanent City'). ' <span class="required">*</span>'); ?>
                <?php echo $form['permanentcity']->render(array("class"=>"editable")); ?>
            </li>
                <li>
                    <?php echo $form['permanentkecamatan']->renderLabel(__('Permanent Kecamatan'). ' <span class="required">*</span>'); ?>
                    <?php echo $form['permanentkecamatan']->render(array("class"=>"editable")); ?>
                </li>
                <li>
                    <?php echo $form['permanentkelurahan']->renderLabel(__('Permanent Kelurahan'). ' <span class="required">*</span>'); ?>
                    <?php echo $form['permanentkelurahan']->render(array("class"=>"editable")); ?>
                </li>
            <li>
                <?php echo $form['permanentzip']->renderLabel(__('Permanent Zip'). ' <span class="required">*</span>'); ?>
                <?php echo $form['permanentzip']->render(array("class" => "formInputText")); ?>
            </li>
            <li>
                <?php echo $form['religion']->renderLabel(__('Religion'). ' <span class="required">*</span>'); ?>
                <?php echo $form['religion']->render(array("class"=>"editable")); ?>
            </li>
            <li>
                <?php echo $form['marital']->renderLabel(__('Marital Status'). ' <span class="required">*</span>'); ?>
                <?php echo $form['marital']->render(array("class"=>"editable")); ?>
            </li>
            <li>
                <?php echo $form['phone']->renderLabel(__('Phone'). ' <span class="required">*</span>'); ?>
                <?php echo $form['phone']->render(array("class" => "formInputText")); ?>
            </li>
            <li>
                <?php echo $form['mobile']->renderLabel(__('Mobile Phone'). ' <span class="required">*</span>'); ?>
                <?php echo $form['mobile']->render(array("class" => "formInputText")); ?>
            </li>

            <li>
                <?php echo $form['email']->renderLabel(__('Email Address'). ' <span class="required">*</span>'); ?>
                <?php echo $form['email']->render(array("class" => "formInputText")); ?>
            </li>
            <li>
                <?php echo $form['ektp']->renderLabel(__('Identity Number (eKTP)'). ' <span class="required">*</span>'); ?>
                <?php echo $form['ektp']->render(array("class" => "formInputText")); ?>
            </li>

            <!--<li>
                <?//php echo $form['contactNo']->renderLabel(__('Contact No'), array("class " => "contactNoLable")); ?>
                <?//php echo $form['contactNo']->render(array("class" => "contactNo")); ?>
            </li>-->
            
        <!--</ol>
        <ol>-->
                <li class="fieldHelpContainer">
                    <?php if ($candidateId == "") : ?>

                        <?php echo $form['photo']->renderLabel(__('Upload Photo') . ' <span class="required">*</span>'); ?>
                        <?php echo $form['photo']->render(array("class " => "duplexBox")); ?>
                        <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_JPEG) . "</label>"; ?>

                    <?php else : ?>

                        <?php echo $form['photo']->renderLabel(__('Photo')); ?>
                        <?php echo "<span class=\"fileLink\">".__('Uploaded')."</span>"; ?>

                    <?php endif; ?>
                </li>

            <li class="fieldHelpContainer">
            <?php if ($candidateId == "") : ?>
            
                <?php echo $form['resume']->renderLabel(__('Upload eKTP') . ' <span class="required">*</span>'); ?>
                <?php echo $form['resume']->render(array("class " => "duplexBox")); ?>
                <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_JPEG) . "</label>"; ?>
            
            <?php else : ?>
                
                <?php echo $form['resume']->renderLabel(__('eKTP')); ?>
                <?php echo "<span class=\"fileLink\">".__('Uploaded')."</span>"; ?>
            
            <?php endif; ?>
            </li>
            </ol>
                <label><span class="keywrd" style="font-weight: bold">EDUCATION</span> <span  id="eduExtend">[+]</span></label>
                <!--<p style="font-weight: bold">EDUCATION</p>-->
                <div id="education">
                    <ol>
                        <li>
                        </li>
                        <li>
                            <?php echo $form['elementary']->renderLabel(__('Elementary School')); ?>
                            <?php echo $form['elementary']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['elementary_startyear']->renderLabel(__('Elementary School Start Year')); ?>
                            <?php echo $form['elementary_startyear']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['elementary_endyear']->renderLabel(__('Elementary School End Year')); ?>
                            <?php echo $form['elementary_endyear']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['juniorhigh']->renderLabel(__('Junior High School')); ?>
                            <?php echo $form['juniorhigh']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['juniorhigh_startyear']->renderLabel(__('Junior High School Start Year')); ?>
                            <?php echo $form['juniorhigh_startyear']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['juniorhigh_endyear']->renderLabel(__('Junior High School End Year')); ?>
                            <?php echo $form['juniorhigh_endyear']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['seniorhigh']->renderLabel(__('Senior High School')); ?>
                            <?php echo $form['seniorhigh']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['seniorhigh_startyear']->renderLabel(__('Senior High School Start Year')); ?>
                            <?php echo $form['seniorhigh_startyear']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['seniorhigh_endyear']->renderLabel(__('Senior High School End Year')); ?>
                            <?php echo $form['seniorhigh_endyear']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['associate']->renderLabel(__('Associate Degree (Diploma)')); ?>
                            <?php echo $form['associate']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['associate_startyear']->renderLabel(__('Associate Degree (Diploma) Start Year')); ?>
                            <?php echo $form['associate_startyear']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['associate_endyear']->renderLabel(__('Associate Degree (Diploma) End Year')); ?>
                            <?php echo $form['associate_endyear']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['associate_desc']->renderLabel(__('Associate Degree (Diploma) Required Information')); ?>
                            <?php echo $form['associate_desc']->render(array("cols" => 20, "rows" => 9)); ?>
                            <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::DEGREE_DESC) . "</label>"; ?>
                        </li>
                        <li>
                            <?php echo $form['bachelor']->renderLabel(__('Bachelor Degree (S1)')); ?>
                            <?php echo $form['bachelor']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['bachelor_startyear']->renderLabel(__('Bachelor Degree (S1) Start Year')); ?>
                            <?php echo $form['bachelor_startyear']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['bachelor_endyear']->renderLabel(__('Bachelor Degree (S1) End Year')); ?>
                            <?php echo $form['bachelor_endyear']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['bachelor_desc']->renderLabel(__('Bachelor Degree (S1) Required Information')); ?>
                            <?php echo $form['bachelor_desc']->render(array("cols" => 20, "rows" => 9)); ?>
                            <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::DEGREE_DESC) . "</label>"; ?>
                        </li>
                        <li>
                            <?php echo $form['master']->renderLabel(__('Master Degree (S2)')); ?>
                            <?php echo $form['master']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['master_startyear']->renderLabel(__('Master Degree (S2) Start Year')); ?>
                            <?php echo $form['master_startyear']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['master_endyear']->renderLabel(__('Master Degree (S2) End Year')); ?>
                            <?php echo $form['master_endyear']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['master_desc']->renderLabel(__('Master Degree (S2) Required Information')); ?>
                            <?php echo $form['master_desc']->render(array("cols" => 20, "rows" => 9)); ?>
                            <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::DEGREE_DESC) . "</label>"; ?>
                        </li>
                        <li>
                            <?php echo $form['doctoral']->renderLabel(__('Doctoral Degree (Ph.D)')); ?>
                            <?php echo $form['doctoral']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['doctoral_startyear']->renderLabel(__('Doctoral Degree (Ph.D) Start Year')); ?>
                            <?php echo $form['doctoral_startyear']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['doctoral_endyear']->renderLabel(__('Doctoral Degree (Ph.D) End Year')); ?>
                            <?php echo $form['doctoral_endyear']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['doctoral_desc']->renderLabel(__('Doctoral Degree (Ph.D) Required Information')); ?>
                            <?php echo $form['doctoral_desc']->render(array("cols" => 20, "rows" => 9)); ?>
                            <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::DEGREE_DESC) . "</label>"; ?>
                        </li>
                        <li class="fieldHelpContainer">
                            <?php if ($candidateId == "") : ?>

                                <?php echo $form['graduatecertificate']->renderLabel(__('Upload last graduate certificate')); ?>
                                <?php echo $form['graduatecertificate']->render(array("class " => "duplexBox")); ?>
                                <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_JPEG) . "</label>"; ?>

                            <?php else : ?>

                                <?php echo $form['graduatecertificate']->renderLabel(__('Last graduate certificate')); ?>
                                <?php echo "<span class=\"fileLink\">".__('Uploaded')."</span>"; ?>

                            <?php endif; ?>
                        </li>
                        <li class="fieldHelpContainer">
                            <?php if ($candidateId == "") : ?>

                                <?php echo $form['transcript']->renderLabel(__('Upload last graduate transcript')); ?>
                                <?php echo $form['transcript']->render(array("class " => "duplexBox")); ?>
                                <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_JPEG) . "</label>"; ?>

                            <?php else : ?>

                                <?php echo $form['transcript']->renderLabel(__('Last graduate transcript')); ?>
                                <?php echo "<span class=\"fileLink\">".__('Uploaded')."</span>"; ?>

                            <?php endif; ?>
                        </li>
                    </ol>
                </div>
                <hr style="border: 0px dashed #727272;">
                <label><span class="keywrd" style="font-weight: bold">WORKING EXPERIENCE</span><span id="weExtend"> [+]</span></label>
                <div id="we">
                    <ol>
                        <!--
                        <li>
                            <p class="keywrd" style="font-weight: bold">WORKING EXPERIENCE</p>
                        </li>-->
                        <li>
                        </li>
                        <li>
                            <?php echo $form['company1']->renderLabel(__('Company Name')); ?>
                            <?php echo $form['company1']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['position1']->renderLabel(__('Position')); ?>
                            <?php echo $form['position1']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['start1']->renderLabel(__('Start')); ?>
                            <?php echo $form['start1']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['end1']->renderLabel(__('End')); ?>
                            <?php echo $form['end1']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="largeTextBox">
                            <?php echo $form['jobdesc1']->renderLabel(__('Job Description')); ?><br><br>
                            <?php echo $form['jobdesc1']->render(array("cols" => 20, "rows" => 9)); ?>
                        </li>
                        <li>
                            <?php echo $form['refname1']->renderLabel(__('Reference Name')); ?>
                            <?php echo $form['refname1']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['reftelp1']->renderLabel(__('Reference Phone')); ?>
                            <?php echo $form['reftelp1']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li><hr style="border: 0.4px dashed #727272;"></li>
                        <li>
                            <?php echo $form['company2']->renderLabel(__('Company Name')); ?>
                            <?php echo $form['company2']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['position2']->renderLabel(__('Position')); ?>
                            <?php echo $form['position2']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['start2']->renderLabel(__('Start')); ?>
                            <?php echo $form['start2']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['end2']->renderLabel(__('End')); ?>
                            <?php echo $form['end2']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="largeTextBox">
                            <?php echo $form['jobdesc2']->renderLabel(__('Job Description')); ?><br><br>
                            <?php echo $form['jobdesc2']->render(array("cols" => 20, "rows" => 9)); ?>
                        </li>
                        <li>
                            <?php echo $form['refname2']->renderLabel(__('Reference Name')); ?>
                            <?php echo $form['refname2']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['reftelp2']->renderLabel(__('Reference Phone')); ?>
                            <?php echo $form['reftelp2']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li><hr style="border: 0.4px dashed #727272;"></li>
                        <li>
                            <?php echo $form['company3']->renderLabel(__('Company Name')); ?>
                            <?php echo $form['company3']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['position3']->renderLabel(__('Position')); ?>
                            <?php echo $form['position3']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['start3']->renderLabel(__('Start')); ?>
                            <?php echo $form['start3']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['end3']->renderLabel(__('End')); ?>
                            <?php echo $form['end3']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="largeTextBox">
                            <?php echo $form['jobdesc3']->renderLabel(__('Job Description')); ?><br><br>
                            <?php echo $form['jobdesc3']->render(array("cols" => 20, "rows" => 9)); ?>
                        </li>
                        <li>
                            <?php echo $form['refname3']->renderLabel(__('Reference Name')); ?>
                            <?php echo $form['refname3']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['reftelp3']->renderLabel(__('Reference Phone')); ?>
                            <?php echo $form['reftelp3']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li><hr style="border: 0.2px dashed #727272;"></li>
                        <li>
                            <?php echo $form['company4']->renderLabel(__('Company Name')); ?>
                            <?php echo $form['company4']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['position4']->renderLabel(__('Position')); ?>
                            <?php echo $form['position4']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['start4']->renderLabel(__('Start')); ?>
                            <?php echo $form['start4']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['end4']->renderLabel(__('End')); ?>
                            <?php echo $form['end4']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="largeTextBox">
                            <?php echo $form['jobdesc4']->renderLabel(__('Job Description')); ?><br><br>
                            <?php echo $form['jobdesc4']->render(array("cols" => 20, "rows" => 9)); ?>
                        </li>
                        <li>
                            <?php echo $form['refname4']->renderLabel(__('Reference Name')); ?>
                            <?php echo $form['refname4']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['reftelp4']->renderLabel(__('Reference Phone')); ?>
                            <?php echo $form['reftelp4']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li><hr style="border: 0.2px dashed #727272;"></li>
                        <li>
                            <?php echo $form['company5']->renderLabel(__('Company Name')); ?>
                            <?php echo $form['company5']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['position5']->renderLabel(__('Position')); ?>
                            <?php echo $form['position5']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['start5']->renderLabel(__('Start')); ?>
                            <?php echo $form['start5']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['end5']->renderLabel(__('End')); ?>
                            <?php echo $form['end5']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="largeTextBox">
                            <?php echo $form['jobdesc5']->renderLabel(__('Job Description')); ?><br><br>
                            <?php echo $form['jobdesc5']->render(array("cols" => 20, "rows" => 9)); ?>
                        </li>
                        <li>
                            <?php echo $form['refname5']->renderLabel(__('Reference Name')); ?>
                            <?php echo $form['refname5']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['reftelp5']->renderLabel(__('Reference Phone')); ?>
                            <?php echo $form['reftelp5']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li class="fieldHelpContainer">
                            <?php if ($candidateId == "") : ?>

                                <?php echo $form['refletter']->renderLabel(__('Upload references letter from last job')); ?>
                                <?php echo $form['refletter']->render(array("class " => "duplexBox")); ?>
                                <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_JPEG) . "</label>"; ?>

                            <?php else : ?>

                                <?php echo $form['refletter']->renderLabel(__('Reference letter')); ?>
                                <?php echo "<span class=\"fileLink\">".__('Uploaded')."</span>"; ?>

                            <?php endif; ?>
                        </li>
                    </ol>
                </div>
                <hr style="border: 0px dashed #727272;">
                <label><span class="keywrd" style="font-weight: bold">OTHERS</span></label>
                <!--<p class="keywrd" style="font-weight: bold">OTHERS</p>-->
            <ol>
                <li>

                </li>
                <li>
                    <?php echo $form['currentsalary']->renderLabel(__('Current Salary')); ?>
                    <?php echo $form['currentsalary']->render(array("class" => "formInputText")); ?>
                </li>
                <li>
                    <?php echo $form['wantsalary']->renderLabel(__('Expected Salary'). ' <span class="required">*</span>'); ?>
                    <?php echo $form['wantsalary']->render(array("class" => "formInputText")); ?>
                </li>
                <li>
                    <?php echo $form['prefworkplace']->renderLabel(__('Preference Work Place')); ?>
                    <?php echo $form['prefworkplace']->render(array("class" => "formInputText")); ?>
                </li>
            </ol>
                <hr style="border: 0px dashed #727272;">
                <label><span class="keywrd" style="font-weight: bold">LEGALITY (DOCTOR)</span><span id="docExtend"> [+]</span></label>
                <div id="doctor">
                    <ol>
                        <li>
                            <!--<p class="keywrd" style="font-weight: bold">LEGALITY (DOCTOR)</p>-->
                        </li>
                        <li><hr style="border: 0.4px dashed #727272;"></li>
                        <li>
                            <?php echo $form['doctor_str']->renderLabel(__('STR No.')); ?>
                            <?php echo $form['doctor_str']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['doctor_strissued']->renderLabel(__('STR Issue Date')); ?>
                            <?php echo $form['doctor_strissued']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['doctor_strexpire']->renderLabel(__('STR Expire Date')); ?>
                            <?php echo $form['doctor_strexpire']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="fieldHelpContainer">
                            <?php if ($candidateId == "") : ?>

                                <?php echo $form['dstrfile']->renderLabel(__('Upload STR')); ?>
                                <?php echo $form['dstrfile']->render(array("class " => "duplexBox")); ?>
                                <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_JPEG) . "</label>"; ?>

                            <?php else : ?>

                                <?php echo $form['dstrfile']->renderLabel(__('STR')); ?>
                                <?php echo "<span class=\"fileLink\">".__('Uploaded')."</span>"; ?>

                            <?php endif; ?>
                        </li>
                        <li><hr style="border: 0.4px dashed #727272;"></li>
                        <li>
                            <?php echo $form['doctor_idicard']->renderLabel(__('IDI Card No.')); ?>
                            <?php echo $form['doctor_idicard']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['doctor_idicardexpire']->renderLabel(__('IDI Card Expire Date')); ?>
                            <?php echo $form['doctor_idicardexpire']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="fieldHelpContainer">
                            <?php if ($candidateId == "") : ?>

                                <?php echo $form['idicardfile']->renderLabel(__('Upload IDI Card')); ?>
                                <?php echo $form['idicardfile']->render(array("class " => "duplexBox")); ?>
                                <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_JPEG) . "</label>"; ?>

                            <?php else : ?>

                                <?php echo $form['idicardfile']->renderLabel(__('IDI Card')); ?>
                                <?php echo "<span class=\"fileLink\">".__('Uploaded')."</span>"; ?>

                            <?php endif; ?>
                        </li>
                        <li><hr style="border: 0.4px dashed #727272;"></li>
                        <li>
                            <?php echo $form['doctor_hiperkes']->renderLabel(__('Hiperkes Certificate')); ?>
                            <?php echo $form['doctor_hiperkes']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li class="fieldHelpContainer">
                            <?php if ($candidateId == "") : ?>

                                <?php echo $form['dhiperkesfile']->renderLabel(__('Upload Hiperkes Certificate')); ?>
                                <?php echo $form['dhiperkesfile']->render(array("class " => "duplexBox")); ?>
                                <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_JPEG) . "</label>"; ?>

                            <?php else : ?>

                                <?php echo $form['dhiperkesfile']->renderLabel(__('Hiperkes Certificate')); ?>
                                <?php echo "<span class=\"fileLink\">".__('Uploaded')."</span>"; ?>

                            <?php endif; ?>
                        </li>
                        <li><hr style="border: 0.4px dashed #727272;"></li>
                        <li>
                            <?php echo $form['doctor_acls']->renderLabel(__('ACLS Certificate')); ?>
                            <?php echo $form['doctor_acls']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['doctor_aclsissued']->renderLabel(__('ACLS Certificate Issue Date')); ?>
                            <?php echo $form['doctor_aclsissued']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['doctor_aclsexpire']->renderLabel(__('ACLS Certificate Expire Date')); ?>
                            <?php echo $form['doctor_aclsexpire']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="fieldHelpContainer">
                            <?php if ($candidateId == "") : ?>

                                <?php echo $form['aclsfile']->renderLabel(__('Upload ACLS Certificate')); ?>
                                <?php echo $form['aclsfile']->render(array("class " => "duplexBox")); ?>
                                <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_JPEG) . "</label>"; ?>

                            <?php else : ?>

                                <?php echo $form['aclsfile']->renderLabel(__('ACLS Certificate')); ?>
                                <?php echo "<span class=\"fileLink\">".__('Uploaded')."</span>"; ?>

                            <?php endif; ?>
                        </li>
                        <li><hr style="border: 0.4px dashed #727272;"></li>
                        <li>
                            <?php echo $form['doctor_atls']->renderLabel(__('ATLS Certificate')); ?>
                            <?php echo $form['doctor_atls']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['doctor_atlsissued']->renderLabel(__('ATLS Certificate Issue Date')); ?>
                            <?php echo $form['doctor_atlsissued']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['doctor_atlsexpire']->renderLabel(__('ATLS Certificate Expire Date')); ?>
                            <?php echo $form['doctor_atlsexpire']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="fieldHelpContainer">
                            <?php if ($candidateId == "") : ?>

                                <?php echo $form['atlsfile']->renderLabel(__('Upload ATLS Certificate')); ?>
                                <?php echo $form['atlsfile']->render(array("class " => "duplexBox")); ?>
                                <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_JPEG) . "</label>"; ?>

                            <?php else : ?>

                                <?php echo $form['atlsfile']->renderLabel(__('ATLS Certificate')); ?>
                                <?php echo "<span class=\"fileLink\">".__('Uploaded')."</span>"; ?>

                            <?php endif; ?>
                        </li>

                    </ol>
                </div>
                <hr style="border: 0px dashed #727272;">
                <label><span class="keywrd" style="font-weight: bold">LEGALITY (NURSE)</span><span id="nurseExtend"> [+]</span></label>
                <div id="nurse">
                    <ol>
                        <li>
                            <!--<p class="keywrd" style="font-weight: bold">LEGALITY (NURSE)</p>-->
                        </li>
                        <li><hr style="border: 0.4px dashed #727272;"></li>
                        <li>
                            <?php echo $form['nurse_str']->renderLabel(__('STR No.')); ?>
                            <?php echo $form['nurse_str']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['nurse_strissued']->renderLabel(__('STR Issue Date')); ?>
                            <?php echo $form['nurse_strissued']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['nurse_strexpire']->renderLabel(__('STR Expire Date')); ?>
                            <?php echo $form['nurse_strexpire']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="fieldHelpContainer">
                            <?php if ($candidateId == "") : ?>

                                <?php echo $form['nstrfile']->renderLabel(__('Upload STR')); ?>
                                <?php echo $form['nstrfile']->render(array("class " => "duplexBox")); ?>
                                <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_JPEG) . "</label>"; ?>

                            <?php else : ?>

                                <?php echo $form['nstrfile']->renderLabel(__('STR')); ?>
                                <?php echo "<span class=\"fileLink\">".__('Uploaded')."</span>"; ?>

                            <?php endif; ?>
                        </li>
                        <li><hr style="border: 0.4px dashed #727272;"></li>
                        <li>
                            <?php echo $form['nurse_ppni']->renderLabel(__('PPNI Card No.')); ?>
                            <?php echo $form['nurse_ppni']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['nurse_ppniexpire']->renderLabel(__('PPNI Card Expire Date')); ?>
                            <?php echo $form['nurse_ppniexpire']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="fieldHelpContainer">
                            <?php if ($candidateId == "") : ?>

                                <?php echo $form['ppnifile']->renderLabel(__('Upload PPNI Card')); ?>
                                <?php echo $form['ppnifile']->render(array("class " => "duplexBox")); ?>
                                <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_JPEG) . "</label>"; ?>

                            <?php else : ?>

                                <?php echo $form['ppnifile']->renderLabel(__('PPNI Card')); ?>
                                <?php echo "<span class=\"fileLink\">".__('Uploaded')."</span>"; ?>

                            <?php endif; ?>
                        </li>
                        <li><hr style="border: 0.4px dashed #727272;"></li>
                        <li>
                            <?php echo $form['nurse_hiperkes']->renderLabel(__('Hiperkes Certificate')); ?>
                            <?php echo $form['nurse_hiperkes']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li class="fieldHelpContainer">
                            <?php if ($candidateId == "") : ?>

                                <?php echo $form['nhiperkesfile']->renderLabel(__('Upload Hiperkes Certificate')); ?>
                                <?php echo $form['nhiperkesfile']->render(array("class " => "duplexBox")); ?>
                                <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_JPEG) . "</label>"; ?>

                            <?php else : ?>

                                <?php echo $form['nhiperkesfile']->renderLabel(__('Hiperkes Certificate')); ?>
                                <?php echo "<span class=\"fileLink\">".__('Uploaded')."</span>"; ?>

                            <?php endif; ?>
                        </li>
                        <li><hr style="border: 0.4px dashed #727272;"></li>
                        <li>
                            <?php echo $form['nurse_ppgd']->renderLabel(__('PPGD Certificate')); ?>
                            <?php echo $form['nurse_ppgd']->render(array("class" => "formInputText")); ?>
                        </li>
                        <li>
                            <?php echo $form['nurse_ppgdissued']->renderLabel(__('PPGD Certificate Issue Date')); ?>
                            <?php echo $form['nurse_ppgdissued']->render(array("class"=>"editable")); ?>
                        </li>
                        <li>
                            <?php echo $form['nurse_ppgdexpire']->renderLabel(__('PPGD Certificate Expire Date')); ?>
                            <?php echo $form['nurse_ppgdexpire']->render(array("class"=>"editable")); ?>
                        </li>
                        <li class="fieldHelpContainer">
                            <?php if ($candidateId == "") : ?>

                                <?php echo $form['ppgdfile']->renderLabel(__('Upload PPGD Certificate')); ?>
                                <?php echo $form['ppgdfile']->render(array("class " => "duplexBox")); ?>
                                <?php echo "<label class=\"fieldHelpBottom\">" . __(CommonMessages::FILE_LABEL_JPEG) . "</label>"; ?>

                            <?php else : ?>

                                <?php echo $form['ppgdfile']->renderLabel(__('PPGD Certificate')); ?>
                                <?php echo "<span class=\"fileLink\">".__('Uploaded')."</span>"; ?>

                            <?php endif; ?>
                        </li>
                    </ol>
                </div>
                <hr style="border: 0px dashed #727272;">
            <ol>
                <li>

                </li>
            <li>
                <?php echo $form['keyWords']->renderLabel(__('Keywords'), array("class " => "keywrd")); ?>
                <?php echo $form['keyWords']->render(array("class" => "keyWords")); ?>
            </li>

            <li>
                <?php echo $form['comment']->renderLabel(__('Notes'), array("class " => "comment")); ?>
                <?php echo $form['comment']->render(array("class" => "formInputText","id" => "notes", "cols" => 43, "rows" => 4)); ?>
            </li>

            <?php include_component('core', 'ohrmPluginPannel', array('location' => 'add_layout_after_main_heading_3')) ?>

            <li class="required new">
                <em>*</em> <?php echo __(CommonMessages::REQUIRED_FIELD); ?>
            </li>

            </ol>
            
            <p>
                <input type="button" class="savebutton" name="btnSave" id="btnSave" value="<?php echo __("Submit"); ?>" /> <a id="backLink" href="<?php echo url_for('recruitmentApply/jobs') ?>"><?php echo __("Back to Job List"); ?></a>
            </p>
            
            </fieldset>

        </form>
        
        </div> <!-- inner -->
        
    </div>
    <div id="footer">
        <?php include_partial('global/copyright');?>
    </div>
<script type="text/javascript">
    //<![CDATA[
    var description	= '<?php $description; ?>';
    var vacancyId	= '<?php echo $vacancyId; ?>';
    var candidateId	= '<?php echo ($candidateId !="") ? $candidateId : 0;?>';
    var lang_firstNameRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_middleNameRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_lastNameRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_genderRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_birthPlaceRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_dobRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_currentAddrRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_currentCityRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_currentProvinceRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_currentZipRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_permanentAddrRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_permanentCityRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_permanentProvinceRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_permanentZipRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_religionRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_maritalRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_phoneRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_mobileRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_ektpRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_emailRequired = '<?php echo __(ValidationMessages::REQUIRED); ?>';
    var lang_validEmail = '<?php echo __(ValidationMessages::EMAIL_INVALID); ?>';
    var lang_validDate = '<?php echo __(ValidationMessages::DATE_FORMAT_INVALID); ?>';
    var lang_tooLargeInput = "<?php echo __(ValidationMessages::TEXT_LENGTH_EXCEEDS, array('%amount%' => 30)); ?>";
    var lang_commaSeparated = "<?php echo __("Enter comma separated words") . '...'; ?>";
    var lang_validPhoneNo = "<?php echo __(ValidationMessages::TP_NUMBER_INVALID); ?>";
    var lang_noMoreThan250 = "<?php echo __(ValidationMessages::TEXT_LENGTH_EXCEEDS, array('%amount%' => 250)); ?>";
    var lang_resumeRequired = "<?php echo __(ValidationMessages::REQUIRED); ?>";
    var linkForApplyVacancy = "<?php echo url_for('recruitmentApply/applyVacancy'); ?>";
    var linkForViewJobs = "<?php echo url_for('recruitmentApply/viewJobs'); ?>";
    var lang_back = "<?php echo __("Go to Job Page")?>";
    var lang_WantsalaryRequired = "<?php echo(ValidationMessages::REQUIRED);?>";
    var lang_WantsalaryNumber = "<?php echo(ValidationMessages::VALID_NUMBER);?>";
    var lang_Strexpire = "<?php echo(ValidationMessages::STR_EXPIRE);?>";
    var dateFormat = "<?php echo $dateFormat?>";

    //Eka: add ajax for retrieving nomornet data
    var provinsi = "";
    var city = "";
    var kecamatan = "";
    var desaKodepos = {};

    //Eka: address domisili
    var domProvinsi = "";
    var domCity = "";
    var domKecamatan = "";
    var domDesaKodepos = {};

    //var nomornetHost = "http://localhost:8880";
    <?php
        //getting server's ip addr
        $host= gethostname();
        $ip_server = gethostbyname($host);
        //$ip_server = $_SERVER['SERVER_ADDR'];
        $nomornetHost = "http://" . $ip_server . "/nomornet_api";
    ?>
    var nomornetHost = "<?php echo $nomornetHost ?>";
    //var nomornetHost = "http://localhost/nomornet_api";
    //var nomornetHost = "http://118.97.180.122:8880";

    //current address
    $("#addCandidate_currentprovince").change(function (){
        //alert($(this).val());
        provinsi = $(this).val();
        var $dropdown = $("#addCandidate_currentkecamatan");
        $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
        var $dropdown2 = $("#addCandidate_currentkelurahan");
        $dropdown2.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
        $("#addCandidate_currentzip").val("");
        $.ajax({
            type:"GET",
            dataType: "json",
            url: nomornetHost + "/nomornet/kotakabupaten/" + provinsi,
            success: function(result){
                console.log(result);
                resultArray = result['records'];
                var $dropdown = $("#addCandidate_currentcity");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
                for(var i in resultArray){
                    console.log(resultArray[i].kotakabupaten);
                    $dropdown.append($("<option />").val(resultArray[i].kotakabupaten).text(resultArray[i].kotakabupaten));
                }
                //alert(result['records'][0].kotakabupaten);
            },
            error: function(jqxhrTxt, statusTxt, errTxt){
                console.log(statusTxt);
                var $dropdown = $("#addCandidate_currentcity");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
            }
        })
    });

    $("#addCandidate_currentcity").change(function (){
        //alert($(this).val());
        provinsi = $("#addCandidate_currentprovince").val();
        city = $(this).val();
        var $dropdown = $("#addCandidate_currentkelurahan");
        $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
        $("#addCandidate_currentzip").val("");
        $.ajax({
            type:"GET",
            dataType: "json",
            url: nomornetHost + "/nomornet/kecamatan/" + provinsi + "/" + city,
            success: function(result){
                console.log(result);
                resultArray = result['records'];
                var $dropdown = $("#addCandidate_currentkecamatan");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
                for(var i in resultArray){
                    console.log(resultArray[i].kecamatan);
                    $dropdown.append($("<option />").val(resultArray[i].kecamatan).text(resultArray[i].kecamatan));
                }
                //alert(result['records'][0].kotakabupaten);
            },
            error: function(jqxhrTxt, statusTxt, errTxt){
                console.log(statusTxt);
                var $dropdown = $("#addCandidate_currentkecamatan");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
            }
        })
    });

    $("#addCandidate_currentkecamatan").change(function (){
        //alert($(this).val());
        provinsi = $("#addCandidate_currentprovince").val();
        city = $("#addCandidate_currentcity").val();
        kecamatan = $(this).val();
        $("#addCandidate_currentzip").val("");
        $.ajax({
            type:"GET",
            dataType: "json",
            url: nomornetHost + "/nomornet/kodeposdesa/" + provinsi + "/" + city + "/" + kecamatan,
            success: function(result){
                console.log(result);
                resultArray = result['records'];
                var $dropdown = $("#addCandidate_currentkelurahan");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
                for(var i in resultArray){
                    console.log(resultArray[i].desakelurahan + " - " + resultArray[i].kodepos);
                    $dropdown.append($("<option />").val(resultArray[i].desakelurahan).text(resultArray[i].desakelurahan));
                    desaKodepos[resultArray[i].desakelurahan] = resultArray[i].kodepos;
                }
                //alert(result['records'][0].kotakabupaten);
            },
            error: function(jqxhrTxt, statusTxt, errTxt){
                console.log(statusTxt);
                var $dropdown = $("#addCandidate_currentkelurahan");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
            }
        })
    });

    $("#addCandidate_currentkelurahan").change(function (){
        //alert($(this).val());
        kelurahan = $(this).val();
        $("#addCandidate_currentzip").val(desaKodepos[kelurahan]);
    });

    //permanent address
    $("#addCandidate_permanentprovince").change(function (){
        //alert($(this).val());
        provinsi = $(this).val();
        var $dropdown = $("#addCandidate_permanentkecamatan");
        $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
        var $dropdown2 = $("#addCandidate_permanentkelurahan");
        $dropdown2.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
        $("#addCandidate_permanentzip").val("");
        $.ajax({
            type:"GET",
            dataType: "json",
            url: nomornetHost + "/nomornet/kotakabupaten/" + provinsi,
            success: function(result){
                console.log(result);
                resultArray = result['records'];
                var $dropdown = $("#addCandidate_permanentcity");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
                for(var i in resultArray){
                    console.log(resultArray[i].kotakabupaten);
                    $dropdown.append($("<option />").val(resultArray[i].kotakabupaten).text(resultArray[i].kotakabupaten));
                }
                //alert(result['records'][0].kotakabupaten);
            },
            error: function(jqxhrTxt, statusTxt, errTxt){
                console.log(statusTxt);
                var $dropdown = $("#addCandidate_permanentcity");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
            }
        })
    });

    $("#addCandidate_permanentcity").change(function (){
        //alert($(this).val());
        provinsi = $("#addCandidate_permanentprovince").val();
        city = $(this).val();
        var $dropdown = $("#addCandidate_permanentkelurahan");
        $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
        $("#addCandidate_permanentzip").val("");
        $.ajax({
            type:"GET",
            dataType: "json",
            url: nomornetHost + "/nomornet/kecamatan/" + provinsi + "/" + city,
            success: function(result){
                console.log(result);
                resultArray = result['records'];
                var $dropdown = $("#addCandidate_permanentkecamatan");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
                for(var i in resultArray){
                    console.log(resultArray[i].kecamatan);
                    $dropdown.append($("<option />").val(resultArray[i].kecamatan).text(resultArray[i].kecamatan));
                }
                //alert(result['records'][0].kotakabupaten);
            },
            error: function(jqxhrTxt, statusTxt, errTxt){
                console.log(statusTxt);
                var $dropdown = $("#addCandidate_permanentkecamatan");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
            }
        })
    });

    $("#addCandidate_permanentkecamatan").change(function (){
        //alert($(this).val());
        provinsi = $("#addCandidate_permanentprovince").val();
        city = $("#addCandidate_permanentcity").val();
        kecamatan = $(this).val();
        $("#addCandidate_permanentzip").val("");
        $.ajax({
            type:"GET",
            dataType: "json",
            url: nomornetHost + "/nomornet/kodeposdesa/" + provinsi + "/" + city + "/" + kecamatan,
            success: function(result){
                console.log(result);
                resultArray = result['records'];
                var $dropdown = $("#addCandidate_permanentkelurahan");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
                for(var i in resultArray){
                    console.log(resultArray[i].desakelurahan + " - " + resultArray[i].kodepos);
                    $dropdown.append($("<option />").val(resultArray[i].desakelurahan).text(resultArray[i].desakelurahan));
                    desaKodepos[resultArray[i].desakelurahan] = resultArray[i].kodepos;
                }
                //alert(result['records'][0].kotakabupaten);
            },
            error: function(jqxhrTxt, statusTxt, errTxt){
                console.log(statusTxt);
                var $dropdown = $("#addCandidate_permanentkelurahan");
                $dropdown.find('option').remove().end().append('<option value="0">-- Select --</option>').val('0');
            }
        })
    });

    $("#addCandidate_permanentkelurahan").change(function (){
        //alert($(this).val());
        kelurahan = $(this).val();
        $("#addCandidate_permanentzip").val(desaKodepos[kelurahan]);
    });

</script>
