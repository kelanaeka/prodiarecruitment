<?php

/**
 * OrangeHRM is a comprehensive Human Resource Management (HRM) System that captures
 * all the essential functionalities required for any enterprise.
 * Copyright (C) 2006 OrangeHRM Inc., http://www.orangehrm.com
 *
 * OrangeHRM is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * OrangeHRM is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA
 *
 */
class applyVacancyAction extends sfAction {

    private $configService;

    /**
     * to get confuguration service
     * @return <type>
     */
    public function getConfigService() {
        if (is_null($this->configService)) {
            $this->configService = new ConfigService();
            $this->configService->setConfigDao(new ConfigDao());
        }
        return $this->configService;
    }
    /**
     * @param sfForm $form
     * @return
     */
    public function setForm(sfForm $form) {
        if (is_null($this->form)) {
            $this->form = $form;
        }
    }

    /**
     *
     * @return ApplyVacancyForm 
     */
    public function getForm() {
        return $this->form;
    }

    /**
     *
     * @return <type>
     */
    public function getVacancyService() {
        if (is_null($this->vacancyService)) {
            $this->vacancyService = new VacancyService();
            $this->vacancyService->setVacancyDao(new VacancyDao());
        }
        return $this->vacancyService;
    }

    public function sendNotification($emailAddr, $message, $subject){
        //$smtp2goUrl = "https://api.smtp2go.com/v3/email/send";
        $smtpBbUrl = "https://api.thebigbox.id/mail-sender/0.0.2/mails";
        /*$emailBodyArray = array(
            "api_key" => "api-CAF05D60704811EA8980F23C91C88F4E",
            "sender" => "kelanaeka73@nomornet.info",
            "to" => array($emailAddr),
            "subject" => $subject,
            "html_body" => "",
            "text_body" => $message
        );*/
        $emailBodyArray = array(
            "subject" => $subject,
            "message" => $message,
            "recipient" => $emailAddr
        );

        //$emailBodyJson = json_encode($emailBodyArray);
        $emailBodyUrlEncoded = http_build_query($emailBodyArray);
        //$curl = curl_init($smtp2goUrl);
        $curl = curl_init($smtpBbUrl);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        //curl_setopt($curl, CURLOPT_POSTFIELDS, $emailBodyJson);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $emailBodyUrlEncoded);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded',
            'accept: application/json',
            'x-api-key: Xy9q3r34G6ztzfxrUVG9ralextx1rkEQ'
        ));
        $result = curl_exec($curl);
        $errno = curl_errno($curl);
        if($errno){
            print_r($errno);
            die("Connection Failure");
        }
        curl_close($curl);
    }

    /**
     *
     * @param <type> $request
     */
    public function execute($request) {
        $param = null;
        $this->candidateId = null;

        $this->vacancyId = $request->getParameter('id');
        //$this->candidateId = $request->getParameter('candidateId');
        $this->getResponse()->setTitle(__("Vacancy Apply Form"));
        //$param = array('candidateId' => $this->candidateId);
        $this->setForm(new ApplyVacancyForm(array(), $param, true));

        if (!empty($this->vacancyId)) {
            $vacancy = $this->getVacancyService()->getVacancyById($this->vacancyId);
            if (empty($vacancy)) {
                $this->redirect('recruitmentApply/jobs.html');
            }
            $this->description = $vacancy->getDescription();
            $this->name = $vacancy->getName();
        } else {
            $this->redirect('recruitmentApply/jobs.html');
        }

        $this->dateFormat = $this->getConfigService()->getAdminLocalizationDefaultDateFormat();

        if ($request->isMethod('post')) {
            $this->form->bind($request->getParameter($this->form->getName()), $request->getFiles($this->form->getName()));

            if ($_FILES['addCandidate']['size']['resume'] > 1024000) {
                $this->getUser()->setFlash('applyVacancy.warning', __(TopLevelMessages::FILE_SIZE_SAVE_FAILURE));
            } else if ($_FILES == null) {
                $this->getUser()->setFlash('applyVacancy.warning', __(TopLevelMessages::FILE_SIZE_SAVE_FAILURE));
                $this->redirect('recruitmentApply/applyVacancy?id=' . $this->vacancyId);
            } else {

                if ($this->form->isValid()) {
                    $subject = "Aplikasi Lamaran Kerja ID ";
                    $message = "Terima kasih atas pengajuan aplikasi lamaran kerja anda. Kami akan segera menginformasikan status aplikasi anda. Gunakan ID Aplikasi berikut untuk mengetahui status aplikasi anda: ";
                    $emailAddr = $this->getForm()->getValue('email');

                    $result = $this->form->save();

                    if (isset($result['messageType'])) {
                        $this->getUser()->setFlash('applyVacancy.' . $result['messageType'], $result['message']);
                    } else {
                        $this->candidateId = $result['candidateId'];
                        if (!empty($this->candidateId)) {
                            $candidateVacancyId = $result['candidateVacancyId'];
                            $subject .= $candidateVacancyId;
                            $message .= $candidateVacancyId;
                            $this->sendNotification($emailAddr, $message, $subject);

                            $this->redirect("recruitmentApply/viewApplicationClosing?appid=" . $candidateVacancyId . "&stmttype=0");
                            $this->getUser()->setFlash('applyVacancy.success', __('Application Received'));
                            $this->getUser()->setFlash('applyVacancy.warning', null);
                        }
                    }
                } else {
                    $errors = array();
                    foreach ($this->form->getErrorSchema() as $key => $err){
                        if($key){
                            $errors[$key] = $err->getMessage();
                        }
                    }
                    print_r($errors);die;
                }
            }
        }
    }

}

