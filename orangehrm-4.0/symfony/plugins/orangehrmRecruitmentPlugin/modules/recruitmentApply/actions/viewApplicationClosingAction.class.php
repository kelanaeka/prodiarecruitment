<?php
/**
 * Created by PhpStorm.
 * User: kelanaeka
 * Date: 4/10/2020
 * Time: 1:29 AM
 */

class viewApplicationClosingAction extends sfAction
{
    public function getForm() {
        return $this->form;
    }

    /**
     *
     * @return <type>
     */
    public function getInterviewService() {
        if (is_null($this->interviewService)) {
            $this->interviewService = new JobInterviewService();
            $this->interviewService->setJobInterviewDao(new JobInterviewDao());
        }
        return $this->interviewService;
    }

    /**
     *
     * @return <type>
     */
    public function getVacancyService() {
        if (is_null($this->vacancyService)) {
            $this->vacancyService = new VacancyService();
            $this->vacancyService->setVacancyDao(new VacancyDao());
        }
        return $this->vacancyService;
    }

    /**
     *
     * @return <type>
     */
    public function getCandidateService() {
        if (is_null($this->candidateService)) {
            $this->candidateService = new CandidateService();
            $this->candidateService->setCandidateDao(new CandidateDao());
        }
        return $this->candidateService;
    }

    public function execute($request){
        $param = null;
        /*$check = $this->getCandidateService()->getCandidateById()
        $fileCount = count($);
        $attachmentName = array();
        if($fileCount > 0) {
            for($i = 0; $i < $fileCount; $i++){
                $attachmentName[$i] = $this->getForm()->getResume()[$i]->getFileName();
            }
            //echo $this->attachmentName;die;
            //getFileName();
            $this->attachmentName = $attachmentName;
        }*/

        $stmtType = $request->getParameter('stmttype');
        $this->applicationId = $request->getParameter('appid');
        $this->candidateId = $request->getParameter('cid');

        switch($stmtType){
            case "0":
                $this->closingStatement = "Thank You. We have received your application. Please check your e-mail regularly for further notification.";
                break;
            case "1":
                $this->closingStatement = "Thank You. Your Application Data Has Successfully Been Submitted.";
                break;
            case "2":
                $this->closingStatement = "Your Application Data Has Been Saved";
                break;
            case "3":
                $this->closingStatement = "Your Application has been shortlisted. We will notify you soon for interview.";
                //$this->attachments = $this->getCandidateService()->getCandidateById($this->candidateId)->getJobCandidateAttachment();
                break;
            case "4":
                $interview = $this->getInterviewService()->getInterviewsByCandidateVacancyId($this->applicationId);
                $interviewDateFormat = strtotime($interview->getData()[0]->getInterviewDate());
                $interviewDate = date('d-m-Y', $interviewDateFormat) . " pukul " . $interview->getData()[0]->getInterviewTime();
                $interviewNote = $interview->getData()[0]->getNote();
                $interviewUrl = "https://conf.umeetme.id/simpeginterview" . $this->applicationId;
                $this->closingStatement = 'Your application has been selected for interview. Check your e-mail for details.<br>';
                $this->closingStatement .= 'Interview Date: ' . $interviewDate . '.<br>';
                $this->closingStatement .= 'Interview Note: ' . $interviewNote . '.<br>';
                $this->closingStatement .= 'Online Interview Link: <a href="' . $interviewUrl . '">' . $interviewUrl . '</a>.<br><br>';
                //$this->closingStatement .= 'Your Psikotest Result is now available. Click on the file icon to download.';
                //$this->attachments = $this->getCandidateService()->getCandidateById($this->candidateId)->getJobCandidateAttachment();
                break;
            case "5":
                $this->closingStatement = "Congratulations! Based on the interview result, we have selected you for hire. We will reach you soon for the next process.";
                $this->attachments = $this->getCandidateService()->getCandidateById($this->candidateId)->getJobCandidateAttachment();
                break;
            case "6":
                $this->closingStatement = "We are sorry. Based on the interview result, we have not chosen you for current hire. Be optimistic though, we may have other opportunities for you in the future";
                $this->attachments = $this->getCandidateService()->getCandidateById($this->candidateId)->getJobCandidateAttachment();
                break;
        }


        $vacancyId = $this->getCandidateService()->getCandidateVacancyById($this->applicationId)->get('vacancyId');
        $this->jobVacancyName = $this->getVacancyService()->getVacancyById($vacancyId)->getName();
    }
}