<?php
/**
 * Created by PhpStorm.
 * User: kelanaeka
 * Date: 3/29/2020
 * Time: 7:44 PM
 */

class viewApplicationStatusAction extends sfAction
{

    public function setForm(sfForm $form) {
        if (is_null($this->form)) {
            $this->form = $form;
        }
    }

    /**
     *
     * @return <type>
     */
    public function getCandidateService() {
        if (is_null($this->candidateService)) {
            $this->candidateService = new CandidateService();
            $this->candidateService->setCandidateDao(new CandidateDao());
        }
        return $this->candidateService;
    }

    public function execute($request) {
        $param = null;

        //check candidate's current status

        $this->setForm(new ViewApplicationStatusVerifyForm(array(), $param, true));
        if ($request->isMethod('post')) {
            $this->form->bind($request->getParameter($this->form->getName()));
            if($this->form->isValid()){
                $candidateVacancy = $this->form->verify();
                if(!$candidateVacancy) {
                    //echo "candidate not found";
                    //die;
                    $this->getUser()->setFlash('applyVacancy.warning', 'Candidate Not Found...');
                } else {
                    $status = $candidateVacancy->getStatus();
                    $candidateId = $candidateVacancy->getCandidateId();
                    $vacancyId = $candidateVacancy->getVacancyId();
                    $appId = $candidateVacancy->getId();

                    if($status == "APPLICATION INITIATED"){
                        $this->getUser()->setFlash('applyVacancy.warning', 'Application Received...');
                    } else if($status == "REJECTED"){
                        $this->getUser()->setFlash('applyVacancy.warning', 'Application Rejected...');
                    } else {
                        //if($status == "SHORTLISTED" || $status == "INTERVIEW SCHEDULED"){
                        $this->redirect("recruitmentApply/updateApplicationData?appId=" . $appId . "&candidateId=" . $candidateId . "&vacancyId=" . $vacancyId);
                    }

                    //$candidate = $this->getCandidateService()->getCandidateById($candidateId);

                    //print_r($candidate);
                }
            } else {
                $errors = array();
                foreach ($this->form->getErrorSchema() as $key => $err){
                    if($key){
                        $errors[$key] = $err->getMessage();
                    }
                }
                //print_r($errors);die;
            }
        }
    }
}