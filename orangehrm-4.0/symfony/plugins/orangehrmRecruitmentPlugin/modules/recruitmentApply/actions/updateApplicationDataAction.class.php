<?php
/**
 * Created by PhpStorm.
 * User: kelanaeka
 * Date: 4/4/2020
 * Time: 3:47 AM
 */

class updateApplicationDataAction extends sfAction
{
    public $candidateId;
    /**
     * @param sfForm $form
     * @return
     */
    public function setForm(sfForm $form) {
        if (is_null($this->form)) {
            $this->form = $form;
        }
    }

    /**
     *
     * @return <type>
     */
    public function getInterviewService() {
        if (is_null($this->interviewService)) {
            $this->interviewService = new JobInterviewService();
            $this->interviewService->setJobInterviewDao(new JobInterviewDao());
        }
        return $this->interviewService;
    }

    /**
     *
     * @return <type>
     */
    public function getCandidateService() {
        if (is_null($this->candidateService)) {
            $this->candidateService = new CandidateService();
            $this->candidateService->setCandidateDao(new CandidateDao());
        }
        return $this->candidateService;
    }

    /**
     *
     * @return <type>
     */
    public function getVacancyService() {
        if (is_null($this->vacancyService)) {
            $this->vacancyService = new VacancyService();
            $this->vacancyService->setVacancyDao(new VacancyDao());
        }
        return $this->vacancyService;
    }


    public function execute($request) {
        $this->candidateId = $this->getRequestParameter('candidateId');
        $this->vacancyId = $this->getRequestParameter('vacancyId');
        $this->appId = $this->getRequestParameter('appId');
        //print_r($this->vacancyId);die;

        if(!empty($this->vacancyId)){
            $vacancy = $this->getVacancyService()->getVacancyById($this->vacancyId);
            //this will move vacancyName var to the template
            $this->vacancyName = $vacancy->getName();
        }

        if(!empty($this->candidateId)){
            $checkBloodType = $this->getCandidateService()->getCandidateById($this->candidateId)->getBloodtype();
            $attachments = $this->getCandidateService()->getCandidateById($this->candidateId)->getJobCandidateAttachment();
            $checkPsikotestFile = false;
            foreach($attachments as $attachment){
                $fileTitle = $attachment->getFiletitle();
                if($fileTitle == AddCandidateForm::PSIKOTEST_FILE){
                    $checkPsikotestFile = true;
                    break;
                }
            }

            $interview = $this->getInterviewService()->getInterviewsByCandidateVacancyId($this->appId);
            $history = $this->getCandidateService()->getCandidateLatestHistory($this->candidateId, $this->vacancyId);
            $action = $history[0]->getAction();
            if($interview->count()>0){
                if($action >= WorkflowStateMachine::RECRUITMENT_APPLICATION_ACTION_MARK_INTERVIEW_PASSED)
                    $this->redirect("recruitmentApply/viewApplicationClosing?cid=" . $this->candidateId . "&appid=" . $this->appId . "&stmttype=5");
                else if($action >= WorkflowStateMachine::RECRUITMENT_APPLICATION_ACTION_MARK_INTERVIEW_FAILED)
                    $this->redirect("recruitmentApply/viewApplicationClosing?cid=" . $this->candidateId . "&appid=" . $this->appId . "&stmttype=6");
                else
                    $this->redirect("recruitmentApply/viewApplicationClosing?cid=" . $this->candidateId . "&appid=" . $this->appId . "&stmttype=4");
            }
            else if($checkBloodType != null && $checkPsikotestFile)
                $this->redirect("recruitmentApply/viewApplicationClosing?cid=" . $this->candidateId . "&appid=" . $this->appId . "&stmttype=3");
            else if($checkBloodType != null)
                $this->redirect("recruitmentApply/viewApplicationClosing?cid=" . $this->candidateId . "&appid=" . $this->appId . "&stmttype=2");
        }

        $param = array('candidateId' => $this->candidateId, 'vacancyId' => $this->vacancyId, 'appId' => $this->appId, 'vacancyName' => $this->vacancyName);

        $this->setForm(new ViewApplicationStatusDataForm(array(), $param, true));
        if ($request->isMethod('post')) {
            $this->form->bind($request->getParameter($this->form->getName()));
            if($this->form->isValid()){
                $result = $this->form->save();
                if (isset($result['messageType'])) {
                    $this->getUser()->setFlash('applyVacancy.' . $result['messageType'], $result['message']);
                } else {
                    $this->candidateId = $result['candidateId'];
                    $appId = $result['appId'];
                    $this->redirect("recruitmentApply/viewApplicationClosing?appid=" . $appId . "&stmttype=1");
                    if (!empty($this->candidateId)) {
                        $this->getUser()->setFlash('applyVacancy.success', __('Application Received'));
                        $this->getUser()->setFlash('applyVacancy.warning', null);
                    }
                }

            } else {
                $errors = array();
                foreach ($this->form->getErrorSchema() as $key => $err){
                    if($key){
                        $errors[$key] = $err->getMessage();
                    }
                }
                print_r($errors);die;
            }
        }
    }
}