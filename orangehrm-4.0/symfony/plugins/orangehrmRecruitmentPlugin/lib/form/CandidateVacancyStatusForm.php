<?php

/**
 * OrangeHRM is a comprehensive Human Resource Management (HRM) System that captures
 * all the essential functionalities required for any enterprise.
 * Copyright (C) 2006 OrangeHRM Inc., http://www.orangehrm.com
 *
 * OrangeHRM is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * OrangeHRM is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA
 *
 */
class CandidateVacancyStatusForm extends BaseForm {

    private $candidateService;
    private $recruitmentAttachmentService;
    public $candidateVacancyId;
    public $selectedAction;
    public $actionName;
    public $candidateName;
    public $vacancyName;
    public $hiringManagerName;
    public $candidateId;
    public $id;
    public $performedActionName;
    public $currentStatus;
    public $performedDate;
    public $performedBy;
    public $vacancyId;
    private $selectedCandidateVacancy;
    private $interviewService;
    const CONTRACT_FILE = 15;
    //const SALARY_FILE = 16;

    private $allowedFileTypes = array(
        "docx" => "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "doc" => "application/msword",
        "doc" => "application/x-msword",
        "doc" => "application/vnd.ms-office",
        "odt" => "application/vnd.oasis.opendocument.text",
        "pdf" => "application/pdf",
        "pdf" => "application/x-pdf",
        "rtf" => "application/rtf",
        "rtf" => "text/rtf",
        "txt" => "text/plain"
    );


    /**
     *
     * @return <type>
     */
    public function getCandidateService() {
        if (is_null($this->candidateService)) {
            $this->candidateService = new CandidateService();
            $this->candidateService->setCandidateDao(new CandidateDao());
        }
        return $this->candidateService;
    }

    public function getInterviewService() {
        if (is_null($this->interviewService)) {
            $this->interviewService = new JobInterviewService();
            $this->interviewService->setJobInterviewDao(new JobInterviewDao());
        }
        return $this->interviewService;
    }

    /**
     *
     * @return <type>
     */
    public function getRecruitmentAttachmentService() {
        if (is_null($this->recruitmentAttachmentService)) {
            $this->recruitmentAttachmentService = new RecruitmentAttachmentService();
            $this->recruitmentAttachmentService->setRecruitmentAttachmentDao(new RecruitmentAttachmentDao());
        }
        return $this->recruitmentAttachmentService;
    }

    /**
     *
     */
    public function configure() {
        $this->candidateVacancyId = $this->getOption('candidateVacancyId');
        $this->selectedAction = $this->getOption('selectedAction');
        $this->id = $this->getOption('id');

        $candidatePermissions = $this->getOption('candidatePermissions');
        if ($this->candidateVacancyId > 0 && $this->selectedAction != "") {
            $stateMachine = new WorkflowStateMachine();
            $this->actionName = $stateMachine->getRecruitmentActionName($this->selectedAction);
            $this->selectedCandidateVacancy = $this->getCandidateService()->getCandidateVacancyById($this->candidateVacancyId);
        }
        if ($this->id > 0) {
            $candidateHistory = $this->getCandidateService()->getCandidateHistoryById($this->id);
            $this->selectedCandidateVacancy = $this->getCandidateService()->getCandidateVacancyByCandidateIdAndVacancyId($candidateHistory->getCandidateId(), $candidateHistory->getVacancyId());
            $this->performedActionName = $candidateHistory->getActionName();
            $date = explode(" ", $candidateHistory->getPerformedDate());
            $this->performedDate = set_datepicker_date_format($date[0]);
            $this->performedBy = $candidateHistory->getPerformerName();
            $this->vacancyId = $candidateHistory->getVacancyId();
            $this->selectedAction = $candidateHistory->getAction();
        }

//        if($this->selectedCandidateVacancy == null){
//
//        }

        $this->candidateId = $this->selectedCandidateVacancy->getCandidateId();
        $this->vacancyId = $this->selectedCandidateVacancy->getVacancyId();
        $this->candidateName = $this->selectedCandidateVacancy->getCandidateName();
        $this->vacancyName = $this->selectedCandidateVacancy->getVacancyName();
        $this->hiringManagerName = $this->selectedCandidateVacancy->getHiringManager();
        $this->currentStatus = ucwords(strtolower($this->selectedCandidateVacancy->getStatus()));

        $history = $this->getCandidateService()->getCandidateLatestHistory($this->candidateId, $this->vacancyId);
        $historyId = $history[0]->getId();

        if($this->selectedAction == WorkflowStateMachine::RECRUITMENT_APPLICATION_ACTION_HIRE){
            $widgets = array(
                //'candidateVacancyId' => new sfWidgetFormInputHidden(),
                //'selectedAction' => new sfWidgetFormInputHidden(),
                'historyId' => new sfWidgetFormInputHidden(),
                'contract' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
                //'salary' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
                'notes' => new sfWidgetFormTextArea(),
            );

            $validators = array(
                //'candidateVacancyId' => new sfValidatorString(array('required' => false)),
                //'selectedAction' => new sfValidatorString(array('required' => false)),
                'historyId' => new sfValidatorString(array('required' => false)),
                'contract' => new sfValidatorFile(array('required' => false, 'max_size' => 1024000, 'validated_file_class' => 'orangehrmValidatedFile')),
                //'salary' => new sfValidatorFile(array('required' => false, 'max_size' => 1024000, 'validated_file_class' => 'orangehrmValidatedFile')),
                'notes' => new sfValidatorString(array('required' => false, 'max_length' => 2147483647)),
            );

            $this->setWidgets($widgets);
            $this->setValidators($validators);

        } else {
            $this->setWidget('notes', new sfWidgetFormTextArea());
            $this->setValidator('notes', new sfValidatorString(array('required' => false, 'max_length' => 2147483647)));
        }

        if (!$candidatePermissions->canUpdate()) {
            $schema = $this->getWidgetSchema();
            $fields = $schema->getFields();

            foreach ($fields as $name => $widget) {
                $widget->setAttribute('disabled', 'disabled');
            }
        }

        $this->widgetSchema->setNameFormat('candidateVacancyStatus[%s]');

        //if($this->candidateVacancyId > 0){
        //    $this->setDefault('candidateVacancyId', $this->candidateVacancyId);
        //    $this->setDefault('selectedAction', $this->selectedAction);
        //}
        if($historyId > 0)
            $this->setDefault('historyId', $historyId);

        if ($this->id > 0) {
            //$candidate = $this->getCandidateService()->getCandidateById($this->candidateId);
            //$this->setDefault('contract', $candidate->getJobCandidateAttachment());
            //$this->setDefault('salary', $candidate->getJobCandidateAttachment());
            $this->setDefault('notes', $candidateHistory->getNote());
            $this->widgetSchema ['notes']->setAttribute('disabled', 'disable');
            $this->actionName = 'View Action History';
        }
    }

    /**
     *
     */
    public function performAction() {
        $note = $this->getValue('notes');
        $historyId = $this->getValue('historyId');

        if($this->selectedAction == WorkflowStateMachine::RECRUITMENT_APPLICATION_ACTION_HIRE){
            $contractFile = $this->getValue('contract');
            if(!empty($contractFile)){
                if (!($this->isValidResume($contractFile))) {
                    $this->historyId = $historyId;
                    $resultArray['messageType'] = 'warning';
                    $resultArray['message'] = __(TopLevelMessages::FILE_TYPE_SAVE_FAILURE);
                    return $resultArray;
                }
            }

            /*$salaryFile = $this->getValue('salary');
            if(!empty($salaryFile)){
                if (!($this->isValidResume($salaryFile))) {
                    $this->historyId = $historyId;
                    $resultArray['messageType'] = 'warning';
                    $resultArray['message'] = __(TopLevelMessages::FILE_TYPE_SAVE_FAILURE);
                    return $resultArray;
                }
            }*/
        }

        if ($this->id > 0) {

            $history = $this->getCandidateService()->getCandidateHistoryById($this->id);
            $history->setNote($note);
            $this->getCandidateService()->saveCandidateHistory($history);
            $this->historyId = $history->getId();
            $resultArray['messageType'] = 'success';
            $resultArray['message'] = __(TopLevelMessages::SAVE_SUCCESS);
            return $resultArray;
        }
        $result = $this->getCandidateService()->updateCandidateVacancy($this->selectedCandidateVacancy, $this->selectedAction, sfContext::getInstance()->getUser()->getAttribute('user'));
        $interviews = $this->getInterviewService()->getInterviewsByCandidateVacancyId($this->candidateVacancyId);
        $interview = $interviews[count($interviews) - 1];
        $candidateHistory = new CandidateHistory();
        $candidateHistory->setCandidateId($this->candidateId);
        $candidateHistory->setVacancyId($this->vacancyId);
        $candidateHistory->setAction($this->selectedAction);
        $candidateHistory->setCandidateVacancyName($this->selectedCandidateVacancy->getVacancyName());
        if (!empty($interview)) {
            if ($this->selectedAction == WorkflowStateMachine::RECRUITMENT_APPLICATION_ACTION_SHEDULE_INTERVIEW || $this->selectedAction == WorkflowStateMachine::RECRUITMENT_APPLICATION_ACTION_SHEDULE_2ND_INTERVIEW || $this->selectedAction == WorkflowStateMachine::RECRUITMENT_APPLICATION_ACTION_MARK_INTERVIEW_PASSED || $this->selectedAction == WorkflowStateMachine::RECRUITMENT_APPLICATION_ACTION_MARK_INTERVIEW_FAILED) {
                $candidateHistory->setInterviewId($interview->getId());
            }
        }
        $empNumber = sfContext::getInstance()->getUser()->getEmployeeNumber();
        if ($empNumber == 0) {
            $empNumber = null;
        }
        $candidateHistory->setPerformedBy($empNumber);
        //$date = date('Y-m-d');
        //$candidateHistory->setPerformedDate($date . " " . date('H:i:s'));
        $candidateHistory->setPerformedDate(date('Y-m-d H:i:sa'));
        $candidateHistory->setNote($note);

        $result = $this->getCandidateService()->saveCandidateHistory($candidateHistory);
        $this->historyId = $candidateHistory->getId();

        if ($this->selectedAction == WorkflowStateMachine::RECRUITMENT_APPLICATION_ACTION_HIRE) {
            if(!empty($contractFile)){
                $contractObj = new JobCandidateAttachment();
                $contractId = $this->_saveContract($contractFile, $contractObj, $this->candidateId);
            }

            /*if(!empty($salaryFile)){
                $salaryObj = new JobCandidateAttachment();
                $salaryId = $this->_saveSalary($salaryFile, $salaryObj, $this->candidateId);
            }*/

            $employee = new Employee();
            $employee->firstName = $this->selectedCandidateVacancy->getJobCandidate()->getFirstName();
            $employee->middleName = $this->selectedCandidateVacancy->getJobCandidate()->getMiddleName();
            $employee->lastName = $this->selectedCandidateVacancy->getJobCandidate()->getLastName();
            $employee->emp_oth_email = $this->selectedCandidateVacancy->getJobCandidate()->getEmail();
            $employee->job_title_code = $this->selectedCandidateVacancy->getJobVacancy()->getJobTitleCode();
            $employee->jobTitle = $this->selectedCandidateVacancy->getJobVacancy()->getJobTitle();

            $this->getCandidateService()->addEmployee($employee);
        }
    }

    /**
     *
     * @param sfValidatedFile $file
     * @return <type>
     */
    protected function isValidResume($file) {
        $validFile = false;

        $mimeTypes = array_values($this->allowedFileTypes);
        $originalName = $file->getOriginalName();

        if (($file instanceof orangehrmValidatedFile) && $originalName != "") {

            $fileType = $file->getType();

            if (!empty($fileType) && in_array($fileType, $mimeTypes)) {
                $validFile = true;
            } else {
                $fileType = $this->guessTypeFromFileExtension($originalName);

                if (!empty($fileType)) {
                    $file->setType($fileType);
                    $validFile = true;
                }
            }
        }

        return $validFile;
    }

    /**
     * Guess the file mime type from the file extension
     *
     * @param  string $file  The absolute path of a file
     *
     * @return string The mime type of the file (null if not guessable)
     */
    public function guessTypeFromFileExtension($file) {

        $mimeType = null;

        $extension = pathinfo($file, PATHINFO_EXTENSION);

        if (isset($this->allowedFileTypes[$extension])) {
            $mimeType = $this->allowedFileTypes[$extension];
        }

        return $mimeType;
    }

    private function _saveContract($file, $contract, $candidateId) {

        $tempName = $file->getTempName();
        $contract->fileContent = file_get_contents($tempName);
        $contract->fileName = $file->getOriginalName();
        $contract->fileType = $file->getType();
        $contract->fileSize = $file->getSize();
        $contract->fileSize = $file->getSize();
        $contract->candidateId = $candidateId;
        $contract->filetitle = CandidateVacancyStatusForm::CONTRACT_FILE;

        $recruitmentAttachmentService = $this->getRecruitmentAttachmentService();
        $recruitmentAttachmentService->saveCandidateAttachment($contract);

        $this->contract = $contract;
    }

    /*private function _saveSalary($file, $salary, $candidateId) {

        $tempName = $file->getTempName();
        $salary->fileContent = file_get_contents($tempName);
        $salary->fileName = $file->getOriginalName();
        $salary->fileType = $file->getType();
        $salary->fileSize = $file->getSize();
        $salary->fileSize = $file->getSize();
        $salary->candidateId = $candidateId;
        $salary->filetitle = CandidateVacancyStatusForm::SALARY_FILE;

        $recruitmentAttachmentService = $this->getRecruitmentAttachmentService();
        $recruitmentAttachmentService->saveCandidateAttachment($salary);

        $this->salary = $salary;
    }*/
}
