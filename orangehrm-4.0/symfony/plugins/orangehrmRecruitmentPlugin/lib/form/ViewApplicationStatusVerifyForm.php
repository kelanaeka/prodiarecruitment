<?php
/**
 * Created by PhpStorm.
 * User: kelanaeka
 * Date: 3/29/2020
 * Time: 8:23 PM
 */

class ViewApplicationStatusVerifyForm extends BaseForm
{
    private $candidateService;

    public function getCandidateService() {
        if (is_null($this->candidateService)) {
            $this->candidateService = new CandidateService();
            $this->candidateService->setCandidateDao(new CandidateDao());
        }
        return $this->candidateService;
    }



    public function configure(){
        $this->setWidgets(array(
            'applicationId' => new sfWidgetFormInputText(),
        ));

        $this->setValidators(array(
            'applicationId' => new sfValidatorString(array('required' => true, 'max_length' => 10, 'trim' => true)),
        ));

        $this->widgetSchema->setNameFormat('viewApplicationStatusVerify[%s]');
    }

    public function verify(){
        $applicationId = $this->getValue('applicationId');
        $candidateService = $this->getCandidateService();
        $candidateVacancy = $candidateService->getCandidateVacancyById($applicationId);
        //$candidateId = $candidateVacancy->getCandidateId();
        //$candidate = $candidateService->getCandidateById($candidateId);
        if($candidateVacancy == null)
            return false;
        return $candidateVacancy;
    }
}