<?php

/**
 * OrangeHRM is a comprehensive Human Resource Management (HRM) System that captures
 * all the essential functionalities required for any enterprise.
 * Copyright (C) 2006 OrangeHRM Inc., http://www.orangehrm.com
 *
 * OrangeHRM is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * OrangeHRM is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA
 *
 */
class AddCandidateForm extends BaseForm {
    //change this value to increase/reduce year span
    const yearSpan = 10;
    private $vacancyService;
    private $candidateService;
    public $attachment;
    public $candidateId;
    public $vacancySet;
    private $recruitmentAttachmentService;
    private $addedBy;
    private $addedHistory;
    private $removedHistory;
    public $allowedVacancyList;
    public $empNumber;
    private $isAdmin;
    private $candidatePermissions;
    private $chList;
    private $allowedFileTypes = array(
        "jpeg" => "image/jpeg",
        "jpg" => "image/jpeg"
    );
    private $allowedDocFileTypes = array(
        "docx" => "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "doc" => "application/msword",
        "doc" => "application/x-msword",
        "doc" => "application/vnd.ms-office",
        "odt" => "application/vnd.oasis.opendocument.text",
        "pdf" => "application/pdf",
        "pdf" => "application/x-pdf",
        "rtf" => "application/rtf",
        "rtf" => "text/rtf",
        "txt" => "text/plain"
    );


    private $nomornetHost;
    //= "http://localhost/nomornet_api";

    const CONTRACT_KEEP = 1;
    const CONTRACT_DELETE = 2;
    const CONTRACT_UPLOAD = 3;

    const KTP_FILE = 1;
    const PHOTO_FILE = 2;
    const EDU_FILE = 3;
    const JOBREF_FILE = 4;
    const DSTR_FILE = 5;
    const IDI_FILE = 6;
    const DHIPERKES_FILE = 7;
    const ACLS_FILE = 8;
    const ATLS_FILE = 9;
    const NSTR_FILE = 10;
    const PPNI_FILE = 11;
    const NHIPERKES_FILE = 12;
    const PPGD_FILE = 13;
    const PSIKOTEST_FILE = 14;
    const CONTRACT_FILE = 15;
    const TRANSCRIPT_FILE = 16;
    //const SALARY_FILE = 16;


    /**
     * Get VacancyService
     * @returns VacncyService
     */
    public function getVacancyService() {
        if (is_null($this->vacancyService)) {
            $this->vacancyService = new VacancyService();
            $this->vacancyService->setVacancyDao(new VacancyDao());
        }
        return $this->vacancyService;
    }

    /**
     *
     * @return <type>
     */
    public function getCandidateService() {
        if (is_null($this->candidateService)) {
            $this->candidateService = new CandidateService();
            $this->candidateService->setCandidateDao(new CandidateDao());
        }
        return $this->candidateService;
    }

    /**
     *
     * @return <type>
     */
    public function getRecruitmentAttachmentService() {
        if (is_null($this->recruitmentAttachmentService)) {
            $this->recruitmentAttachmentService = new RecruitmentAttachmentService();
            $this->recruitmentAttachmentService->setRecruitmentAttachmentDao(new RecruitmentAttachmentDao());
        }
        return $this->recruitmentAttachmentService;
    }

    public function getInterviewService() {
        if (is_null($this->interviewService)) {
            $this->interviewService = new JobInterviewService();
            $this->interviewService->setJobInterviewDao(new JobInterviewDao());
        }
        return $this->interviewService;
    }

    //Eka: list for Agama dropdown
    private function getAgamaList(){
        $list = array('' => "-- " . __('Select') . " --",
            __('Islam') => __('Islam'),
            __('Kristen') => __('Kristen'),
            __('Katolik') => __('Katolik'),
            __('Hindu') => __('Hindu'),
            __('Budha') => __('Budha'),
            __('KongHuCu') => __('KongHuCu'),
            __('Lainnya') => __('Lainnya')
        );

        return $list;
    }

    //Eka: list for GolDarah dropdown
    private function getGolDarahList(){
        $list = array('' => "-- " . __('Select') . " --",
            __('A') => __('A'),
            __('B') => __('B'),
            __('AB') => __('AB'),
            __('O') => __('O')
        );

        return $list;
    }

    //Eka: list for provinsi dropdown
    private function getProvinsiList(){
        $provinsiData = $this->getProvinsi();
        $list = array('' => "-- " . __('Select') . " --");
        if(sizeof($provinsiData->records) > 0){
            for($i = 0;$i < sizeof($provinsiData->records);$i++){
                $list[$provinsiData->records[$i]->provinsi] = $provinsiData->records[$i]->provinsi;
            }
        }

        return $list;
    }

    //Eka: list for city
    private function getCityList($provinsi){
        //print_r($provinsi);die;
        $kotaKabupatenData = $this->getKotakabupaten($provinsi);
        $list = array('' => "-- " . __('Select') . " --");
        if(sizeof($kotaKabupatenData->records) > 0){
            for($i = 0;$i < sizeof($kotaKabupatenData->records);$i++){
                $list[$kotaKabupatenData->records[$i]->kotakabupaten] = $kotaKabupatenData->records[$i]->kotakabupaten;
            }
        }
        return $list;
    }

    //Eka: list for kecamatan
    private function getKecamatanList($provinsi, $city){
        $kecamatanData = $this->getKecamatan($provinsi, $city);
        $list = array('' => "-- " . __('Select') . " --");
        if(sizeof($kecamatanData->records) > 0){
            for($i = 0;$i < sizeof($kecamatanData->records);$i++){
                $list[$kecamatanData->records[$i]->kecamatan] = $kecamatanData->records[$i]->kecamatan;
            }
        }
        return $list;
    }

    //Eka: list for kodeposdesa
    private function getKodeposDesaList($provinsi, $city, $kecamatan){
        $kelurahanData = $this->getKodeposDesa($provinsi, $city, $kecamatan);
        $list = array('' => "-- " . __('Select') . " --");
        if(sizeof($kelurahanData->records) > 0){
            for($i = 0;$i < sizeof($kelurahanData->records);$i++){
                $list[$kelurahanData->records[$i]->desakelurahan] = $kelurahanData->records[$i]->desakelurahan;
            }
        }
        return $list;
    }

    private function getYearRange($start, $end){
        $list = array("" => "-- " . __('Select') . " --");
        for($i = $start;$i <= $end; $i++){
            $list[$i] = $i;
        }

        return $list;
    }

    //Eka: list for relation dropdown
    private function getRelationList(){
        $list = array('' => "-- " . __('Select') . " --",
            __('Ayah') => __('Ayah'),
            __('Ibu') => __('Ibu'),
            __('Kakak') => __('Kakak'),
            __('Adik') => __('Adik'),
            __('Lainnya') => __('Lainnya')
        );

        return $list;
    }

    /**
     *
     */
    public function configure() {
        $host= gethostname();
        $ip_server = gethostbyname($host);
        $this->nomornetHost = "http://" . $ip_server . "/nomornet_api";

        $endYear = date("Y") + ApplyVacancyForm::yearSpan;

        $this->candidateId = $this->getOption('candidateId');
        $this->allowedVacancyList = $this->getOption('allowedVacancyList');
        $this->empNumber = $this->getOption('empNumber');
        $this->isAdmin = $this->getOption('isAdmin');
                
        $this->candidatePermissions = $this->getOption('candidatePermissions');

        $this->chList = $this->getOption('chList');

        $attachmentList = $this->attachment;
        if (count($attachmentList) > 0) {
            $this->attachment = $attachmentList[0];
        }
        $vacancyList = $this->getActiveVacancyList();

        $this->vacancySet = false;
        if ($this->candidateId != null) {
            $candidateVacancyList = $this->getCandidateService()->getCandidateById($this->candidateId)->getJobCandidateVacancy();

            if($candidateVacancyList->count() > 0)
                $this->vacancySet = true;

            $vacancy = $candidateVacancyList[0]->getJobVacancy();
            $this->vacancyStatus = $vacancy->getStatus();

            if ($vacancy->getStatus() == JobVacancy::CLOSED) {
                $vacancyList[$vacancy->getId()] = $vacancy->getVacancyName();
            } elseif ($vacancy->getStatus() == JobVacancy::ACTIVE) {
                $vacancyList[$vacancy->getId()] = $vacancy->getName();
            }
        }

        $resumeUpdateChoices = array(self::CONTRACT_KEEP => __('Keep Current'),
            //self::CONTRACT_DELETE => __('Delete Current'),
            self::CONTRACT_UPLOAD => __('Replace Current'));

        //get default values
        if($this->candidateId > 0){
            $candidate = $this->getCandidateService()->getCandidateById($this->candidateId);
            $this->currentprovince = $candidate->getCurrentprovince();
            $this->currentcity = $candidate->getCurrentcity();
            $this->currentkecamatan = $candidate->getCurrentkecamatan();
            $this->currentkelurahan = $candidate->getCurrentkelurahan();
            $this->permanentprovince = $candidate->getPermanentprovince();
            $this->permanentcity = $candidate->getPermanentcity();
            $this->permanentkecamatan = $candidate->getPermanentkecamatan();
            $this->permanentkelurahan = $candidate->getPermanentkelurahan();
            $this->emergencyprovince = $candidate->getEmergencyprovince();
            $this->emergencycity = $candidate->getEmergencycity();
            $this->emergencykecamatan = $candidate->getEmergencykecamatan();
            $this->emergencykelurahan = $candidate->getEmergencykelurahan();
        }
        // creating widgets
        $widgets = array(
            'firstName' => new sfWidgetFormInputText(),
            'middleName' => new sfWidgetFormInputText(),
            'lastName' => new sfWidgetFormInputText(),
            'email' => new sfWidgetFormInputText(),
            'contactNo' => new sfWidgetFormInputText(),
            'resume' => new sfWidgetFormInputFileEditable(
                    array('edit_mode' => false,
                        'with_delete' => false,
                        'file_src' => '')),
            'photo' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
            'keyWords' => new sfWidgetFormInputText(),
            'comment' => new sfWidgetFormTextArea(),
            'appliedDate' => new ohrmWidgetDatePicker(array(), array('id' => 'addCandidate_appliedDate')),
            'vacancy' => new sfWidgetFormSelect(array('choices' => $vacancyList)),
            'resumeUpdate' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => $resumeUpdateChoices)),
            'photoUpdate' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => $resumeUpdateChoices)),
            'gender' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array('Laki-laki' => 'Laki-laki', 'Perempuan' => 'Perempuan'))),
            'birthplace' => new sfWidgetFormInputText(),
            'dob' => new ohrmWidgetDatePicker(array(), array('id' => 'dob')),
            'currentaddr' => new sfWidgetFormInputText(),
            'currentprovince' => new sfWidgetFormSelect(array('choices' => $this->getProvinsiList())),
            'currentcity' => new sfWidgetFormSelect(array('choices' => $this->getCityList($this->currentprovince))),
            'currentkecamatan' => new sfWidgetFormSelect(array('choices' => $this->getKecamatanList($this->currentprovince,$this->currentcity))),
            'currentkelurahan' => new sfWidgetFormSelect(array('choices' => $this->getKodeposDesaList($this->currentprovince,$this->currentcity,$this->currentkecamatan))),
            'currentzip' => new sfWidgetFormInputText(),
            'permanentaddr' => new sfWidgetFormInputText(),
            'permanentprovince' => new sfWidgetFormSelect(array('choices' => $this->getProvinsiList())),
            'permanentcity' => new sfWidgetFormSelect(array('choices' => $this->getCityList($this->permanentprovince))),
            'permanentkecamatan' => new sfWidgetFormSelect(array('choices' => $this->getKecamatanList($this->permanentprovince,$this->permanentcity))),
            'permanentkelurahan' => new sfWidgetFormSelect(array('choices' => $this->getKodeposDesaList($this->permanentprovince,$this->permanentcity,$this->permanentkecamatan))),
            'permanentzip' => new sfWidgetFormInputText(),
            'religion' => new sfWidgetFormSelect(array('choices' => $this->getAgamaList())),
            'marital' => new sfWidgetFormSelect(array('choices' => array('' => "-- " . __('Select') . " --", 'Single' => __('Single'), 'Married' => __('Married'), 'Janda/Duda' => __('Janda/Duda')))),
            'phone' => new sfWidgetFormInputText(),
            'mobile' => new sfWidgetFormInputText(),
            'ektp' => new sfWidgetFormInputText(),
            //requested by user
            'elementary' => new sfWidgetFormInputText(),
            'elementary_startyear' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'elementary_endyear' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'juniorhigh' => new sfWidgetFormInputText(),
            'juniorhigh_startyear' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'juniorhigh_endyear' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'seniorhigh' => new sfWidgetFormInputText(),
            'seniorhigh_startyear' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'seniorhigh_endyear' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'associate' => new sfWidgetFormInputText(),
            'associate_startyear' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'associate_endyear' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'bachelor' => new sfWidgetFormInputText(),
            'bachelor_startyear' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'bachelor_endyear' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'master' => new sfWidgetFormInputText(),
            'master_startyear' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'master_endyear' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'doctoral' => new sfWidgetFormInputText(),
            'doctoral_startyear' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'doctoral_endyear' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'graduatecertificate' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
            'graduatecertificateUpdate' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => $resumeUpdateChoices)),
            'transcript' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
            'transcriptUpdate' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => $resumeUpdateChoices)),
            'company1' => new sfWidgetFormInputText(),
            'position1' => new sfWidgetFormInputText(),
            'start1' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'end1' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'jobdesc1' => new sfWidgetFormTextArea(),
            'refname1' => new sfWidgetFormInputText(),
            'reftelp1' => new sfWidgetFormInputText(),
            'company2' => new sfWidgetFormInputText(),
            'position2' => new sfWidgetFormInputText(),
            'start2' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'end2' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'jobdesc2' => new sfWidgetFormTextArea(),
            'refname2' => new sfWidgetFormInputText(),
            'reftelp2' => new sfWidgetFormInputText(),
            'company3' => new sfWidgetFormInputText(),
            'position3' => new sfWidgetFormInputText(),
            'start3' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'end3' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'jobdesc3' => new sfWidgetFormTextArea(),
            'refname3' => new sfWidgetFormInputText(),
            'reftelp3' => new sfWidgetFormInputText(),
            'company4' => new sfWidgetFormInputText(),
            'position4' => new sfWidgetFormInputText(),
            'start4' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'end4' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'jobdesc4' => new sfWidgetFormTextArea(),
            'refname4' => new sfWidgetFormInputText(),
            'reftelp4' => new sfWidgetFormInputText(),
            'company5' => new sfWidgetFormInputText(),
            'position5' => new sfWidgetFormInputText(),
            'start5' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'end5' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'jobdesc5' => new sfWidgetFormTextArea(),
            'refname5' => new sfWidgetFormInputText(),
            'reftelp5' => new sfWidgetFormInputText(),
            'refletter' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
            'refletterUpdate' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => $resumeUpdateChoices)),
            'currentsalary' => new sfWidgetFormInputText(),
            //
            'wantsalary' => new sfWidgetFormInputText(),
            //requested
            'prefworkplace' => new sfWidgetFormInputText(),
            'doctor_str' => new sfWidgetFormInputText(),
            'doctor_strissued' => new ohrmWidgetDatePicker(array(), array('id' => 'doctor_strissued')),
            'doctor_strexpire' => new ohrmWidgetDatePicker(array(), array('id' => 'doctor_strexpire')),
            'dstrfile' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
            'dstrfileUpdate' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => $resumeUpdateChoices)),
            'doctor_idicard' => new sfWidgetFormInputText(),
            'doctor_idicardexpire' => new ohrmWidgetDatePicker(array(), array('id' => 'doctor_idicardexpire')),
            'idicardfile' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
            'idicardfileUpdate' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => $resumeUpdateChoices)),
            'doctor_hiperkes' => new sfWidgetFormInputText(),
            'dhiperkesfile' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
            'dhiperkesfileUpdate' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => $resumeUpdateChoices)),
            'doctor_acls' => new sfWidgetFormInputText(),
            'doctor_aclsissued' => new ohrmWidgetDatePicker(array(), array('id' => 'doctor_aclsissued')),
            'doctor_aclsexpire' => new ohrmWidgetDatePicker(array(), array('id' => 'doctor_aclsexpire')),
            'aclsfile' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
            'aclsfileUpdate' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => $resumeUpdateChoices)),
            'doctor_atls' => new sfWidgetFormInputText(),
            'doctor_atlsissued' => new ohrmWidgetDatePicker(array(), array('id' => 'doctor_atlsissued')),
            'doctor_atlsexpire' => new ohrmWidgetDatePicker(array(), array('id' => 'doctor_atlsexpire')),
            'atlsfile' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
            'atlsfileUpdate' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => $resumeUpdateChoices)),
            'nurse_str' => new sfWidgetFormInputText(),
            'nurse_strissued' => new ohrmWidgetDatePicker(array(), array('id' => 'nurse_nurse_strissued')),
            'nurse_strexpire' => new ohrmWidgetDatePicker(array(), array('id' => 'nurse_nurse_strexpire')),
            'nstrfile' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
            'nstrfileUpdate' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => $resumeUpdateChoices)),
            'nurse_ppni' => new sfWidgetFormInputText(),
            'nurse_ppniexpire' => new ohrmWidgetDatePicker(array(), array('id' => 'nurse_ppniexpire')),
            'ppnifile' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
            'ppnifileUpdate' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => $resumeUpdateChoices)),
            'nurse_hiperkes' => new sfWidgetFormInputText(),
            'nhiperkesfile' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
            'nhiperkesfileUpdate' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => $resumeUpdateChoices)),
            'nurse_ppgd' => new sfWidgetFormInputText(),
            'nurse_ppgdissued' => new ohrmWidgetDatePicker(array(), array('id' => 'nurse_ppgdissued')),
            'nurse_ppgdexpire' => new ohrmWidgetDatePicker(array(), array('id' => 'nurse_ppgdexpire')),
            'ppgdfile' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
            'ppgdfileUpdate' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => $resumeUpdateChoices)),
            'atlsexpire' => new sfWidgetFormInputHidden(),
            'aclsexpire' => new sfWidgetFormInputHidden(),
            'ppgdexpire' => new sfWidgetFormInputHidden(),
            'associate_desc' => new sfWidgetFormTextarea(),
            'bachelor_desc' => new sfWidgetFormTextarea(),
            'master_desc' => new sfWidgetFormTextarea(),
            'doctoral_desc' => new sfWidgetFormTextarea(),
            //
            'psikotest' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
            'psikotestUpdate' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => $resumeUpdateChoices)),
            'bloodtype' => new sfWidgetFormSelect(array('choices' => $this->getGolDarahList())),
            'heght' => new sfWidgetFormInputText(),
            'weight' => new sfWidgetFormInputText(),
            'npwp' => new sfWidgetFormInputText(),
            'bpjsnaker' => new sfWidgetFormInputText(),
            'bpjskes' => new sfWidgetFormInputText(),
            'faskes' => new sfWidgetFormTextArea(),
            'kk' => new sfWidgetFormInputText(),
            'familyhead' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No'))),
            'emergencyname' => new sfWidgetFormInputText(),
            'emergencyaddr' => new sfWidgetFormInputText(),
            'emergencyprovince' => new sfWidgetFormSelect(array('choices' => $this->getProvinsiList())),
            'emergencycity' => new sfWidgetFormSelect(array('choices' => $this->getCityList($this->emergencyprovince))),
            'emergencykecamatan' => new sfWidgetFormSelect(array('choices' => $this->getKecamatanList($this->emergencyprovince,$this->emergencycity))),
            'emergencykelurahan' => new sfWidgetFormSelect(array('choices' => $this->getKodeposDesaList($this->emergencyprovince,$this->emergencycity,$this->emergencykecamatan))),
            'emergencyzip' => new sfWidgetFormInputText(),
            'emergencyrelation' => new sfWidgetFormSelect(array('choices' => $this->getRelationList())),
            'emergencyphone' => new sfWidgetFormInputText(),
            'spouse' => new sfWidgetFormInputText(),
            'spouseage' => new sfWidgetFormInputText(),
            'spouseedu' => new sfWidgetFormInputText(),
            'spouseoccupation' => new sfWidgetFormInputText(),
            'firstchildname' => new sfWidgetFormInputText(),
            'firstchildage' => new sfWidgetFormInputText(),
            'firstchildedu' => new sfWidgetFormInputText(),
            'firstchildoccupation' => new sfWidgetFormInputText(),
            'secondchildname' => new sfWidgetFormInputText(),
            'secondchildage' => new sfWidgetFormInputText(),
            'secondchildedu' => new sfWidgetFormInputText(),
            'secondchildoccupation' => new sfWidgetFormInputText(),
            'thirdchildname' => new sfWidgetFormInputText(),
            'thirdchildage' => new sfWidgetFormInputText(),
            'thirdchildedu' => new sfWidgetFormInputText(),
            'thirdchildoccupation' => new sfWidgetFormInputText(),
            'fathername' => new sfWidgetFormInputText(),
            'fatherage' => new sfWidgetFormInputText(),
            'fatheredu' => new sfWidgetFormInputText(),
            'fatheroccupation' => new sfWidgetFormInputText(),
            'mothername' => new sfWidgetFormInputText(),
            'motherage' => new sfWidgetFormInputText(),
            'motheredu' => new sfWidgetFormInputText(),
            'motheroccupation' => new sfWidgetFormInputText(),
            'siblingname1' => new sfWidgetFormInputText(),
            'siblingage1' => new sfWidgetFormInputText(),
            'siblingedu1' => new sfWidgetFormInputText(),
            'siblingoccupation1' => new sfWidgetFormInputText(),
            'siblingname2' => new sfWidgetFormInputText(),
            'siblingage2' => new sfWidgetFormInputText(),
            'siblingedu2' => new sfWidgetFormInputText(),
            'siblingoccupation2' => new sfWidgetFormInputText(),
            'siblingname3' => new sfWidgetFormInputText(),
            'siblingage3' => new sfWidgetFormInputText(),
            'siblingedu3' => new sfWidgetFormInputText(),
            'siblingoccupation3' => new sfWidgetFormInputText(),
            'siblingname4' => new sfWidgetFormInputText(),
            'siblingage4' => new sfWidgetFormInputText(),
            'siblingedu4' => new sfWidgetFormInputText(),
            'siblingoccupation4' => new sfWidgetFormInputText(),
            'siblingname5' => new sfWidgetFormInputText(),
            'siblingage5' => new sfWidgetFormInputText(),
            'siblingedu5' => new sfWidgetFormInputText(),
            'siblingoccupation5' => new sfWidgetFormInputText(),
            'orgname1' => new sfWidgetFormInputText(),
            'orgyear1' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'orgposition1' => new sfWidgetFormInputText(),
            'orgname2' => new sfWidgetFormInputText(),
            'orgyear2' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'orgposition2' => new sfWidgetFormInputText(),
            'orgname3' => new sfWidgetFormInputText(),
            'orgyear3' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'orgposition3' => new sfWidgetFormInputText(),
            'orgname4' => new sfWidgetFormInputText(),
            'orgyear4' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'orgposition4' => new sfWidgetFormInputText(),
            'orgname5' => new sfWidgetFormInputText(),
            'orgyear5' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'orgposition5' => new sfWidgetFormInputText(),
            'diphteria' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'sinusitis' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'bronchitis' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'hemoptoe' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'tbc' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'lunginfection' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'asthma' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'dyspnoea' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'urinateproblem' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'urinatedisorder' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'kidneydisease' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'kidneystone' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'frequenturinate' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'meningitis' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'cerebralconcussion' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'poliomyelitis' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'epilepsy' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'stroke' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'headache' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'typhoid' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'bloodvomit' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'obstipation' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'dyspepsia' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'jaundice' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'gallbladder' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'swallowingdisorder' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'incontinentiaalvi' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'chickenpox' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'fungalskin' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'std' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'heartattack' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'chestpains' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'palpitation' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'hypertension' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'hemorrhoid' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'varicoseveins' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'thyroiddisease' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'rhematoidsarthritis' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'foodallergy' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'foodallergylist' => new sfWidgetFormInputText(),
            'drugallergy' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'drugallergylist' => new sfWidgetFormInputText(),
            'tetanus' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'fainting' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'oblivion' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'consentrationdif' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'visiondisorder' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'hearingdisorder' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'lumbago' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'neoplasm' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'mentalillness' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'skintuberculose' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'bonetuberculose' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'measles' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'malaria' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'diabetes' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'sleepdisorder' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'hospitalized' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'hospitalizeddate' => new ohrmWidgetDatePicker(array(), array('id' => 'hospitalizeddate')),
            'hospitalizedlength' => new sfWidgetFormInputText(),
            'hospitalizedwhy' => new sfWidgetFormTextArea(),
            'accident' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'accidentdate' => new ohrmWidgetDatePicker(array(), array('id' => 'accidentdate')),
            'accidenttxt' => new sfWidgetFormTextArea(),
            'surgery' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'surgerydate' => new ohrmWidgetDatePicker(array(), array('id' => 'surgerydate')),
            'surgerytxt' => new sfWidgetFormTextArea(),
            'smoker' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'smokernumber' => new sfWidgetFormInputText(),
            'smokerage' => new sfWidgetFormInputText(),
            'smokerstop' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, date("Y")))),
            'alcohol' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'alcoholperweek' => new sfWidgetFormInputText(),
            'alcoholslokyperweek' => new sfWidgetFormInputText(),
            'exercise' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'exercisetxt' => new sfWidgetFormTextArea(),
            'medicinelist' => new sfWidgetFormInputText(),
            'vacdiptheri' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'vactetanus' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'vacpolio' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'vacbcg' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'vacmmr' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'vacinfluenza' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'vachepatitisa' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'vachepatitisb' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'vacvarisela' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'vactyphoid' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'vachpv' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'vacmeningokok' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'vacyellowfever' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'vacjapencephalitis' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            //
            'kontrak' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
            'kontrakUpdate' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => $resumeUpdateChoices)),
            //'gaji' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
        );

        $inputDatePattern = sfContext::getInstance()->getUser()->getDateFormat();

        $validators = array(
            'firstName' => new sfValidatorString(array('required' => false, 'max_length' => 35)),
            'middleName' => new sfValidatorString(array('required' => false, 'max_length' => 35)),
            'lastName' => new sfValidatorString(array('required' => false, 'max_length' => 35)),
            'email' => new sfValidatorEmail(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'contactNo' => new sfValidatorString(array('required' => false, 'max_length' => 35)),
            'resume' => new sfValidatorFile(array('required' => false, 'max_size' => 1024000,
                'validated_file_class' => 'orangehrmValidatedFile')),
            'photo' => new sfValidatorFile(array('required' => false, 'max_size' => 1024000,
                'validated_file_class' => 'orangehrmValidatedFile')),
            'keyWords' => new sfValidatorString(array('required' => false, 'max_length' => 255)),
            'comment' => new sfValidatorString(array('required' => false)),
            'appliedDate' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false),
                    array('invalid' => 'Date format should be ' . $inputDatePattern)),
            'vacancy' => new sfValidatorString(array('required' => false)),
            'resumeUpdate' => new sfValidatorString(array('required' => false)),
            'photoUpdate' => new sfValidatorString(array('required' => false)),
            'gender' => new sfValidatorChoice(array('required' => false,
                'choices' => array('Laki-laki', 'Perempuan'),
                'multiple' => false)),
            'birthplace' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'dob' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false), array('invalid' => "Date format should be" . $inputDatePattern)),
            'currentaddr' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'currentprovince' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getProvinsiList()))),
            'currentcity' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'currentkecamatan' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'currentkelurahan' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'currentzip' => new sfValidatorString(array('required' => false, 'max_length' => 10, 'trim' => true)),
            'permanentaddr' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'permanentprovince' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getProvinsiList()))),
            'permanentcity' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'permanentkecamatan' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'permanentkelurahan' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'permanentzip' => new sfValidatorString(array('required' => false, 'max_length' => 10, 'trim' => true)),
            'religion' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getAgamaList()))),
            'marital' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            //new sfValidatorChoice(array('required' => true, 'choices' => array('' => "-- " . __('Select') . " --", 'Single' => __('Single'), 'Married' => __('Married')))),
            'phone' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'mobile' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'ektp' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            //requested
            'elementary' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'elementary_startyear' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'elementary_endyear' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'juniorhigh' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'juniorhigh_startyear' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'juniorhigh_endyear' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'seniorhigh' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'seniorhigh_startyear' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'seniorhigh_endyear' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'associate' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'associate_startyear' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'associate_endyear' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'bachelor' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'bachelor_startyear' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'bachelor_endyear' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'master' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'master_startyear' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'master_endyear' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'doctoral' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'doctoral_startyear' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'doctoral_endyear' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'graduatecertificate' => new sfValidatorFile(array('required' => false, 'max_size' => 1024000, 'validated_file_class' => 'orangehrmValidatedFile')),
            'graduatecertificateUpdate' => new sfValidatorString(array('required' => false)),
            'transcript' => new sfValidatorFile(array('required' => false, 'max_size' => 1024000, 'validated_file_class' => 'orangehrmValidatedFile')),
            'transcriptUpdate' => new sfValidatorString(array('required' => false)),
            'company1' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'position1' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'start1' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'end1' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'jobdesc1' => new sfValidatorString(array('required' => false, 'max_length' => 41000)),
            'refname1' => new sfValidatorString(array('required' => false, 'max_length' => 200, 'trim' => true)),
            'reftelp1' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'company2' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'position2' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'start2' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'end2' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'jobdesc2' => new sfValidatorString(array('required' => false, 'max_length' => 41000)),
            'refname2' => new sfValidatorString(array('required' => false, 'max_length' => 200, 'trim' => true)),
            'reftelp2' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'company3' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'position3' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'start3' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'end3' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'jobdesc3' => new sfValidatorString(array('required' => false, 'max_length' => 41000)),
            'refname3' => new sfValidatorString(array('required' => false, 'max_length' => 200, 'trim' => true)),
            'reftelp3' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'company4' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'position4' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'start4' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'end4' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'jobdesc4' => new sfValidatorString(array('required' => false, 'max_length' => 41000)),
            'refname4' => new sfValidatorString(array('required' => false, 'max_length' => 200, 'trim' => true)),
            'reftelp4' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'company5' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'position5' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'start5' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'end5' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'jobdesc5' => new sfValidatorString(array('required' => false, 'max_length' => 41000)),
            'refname5' => new sfValidatorString(array('required' => false, 'max_length' => 200, 'trim' => true)),
            'reftelp5' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'refletter' => new sfValidatorFile(array('required' => false, 'max_size' => 1024000, 'validated_file_class' => 'orangehrmValidatedFile')),
            'refletterUpdate' => new sfValidatorString(array('required' => false)),
            'currentsalary' => new sfValidatorInteger(array('required' => false, 'min' => 0)),//new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            //
            'wantsalary' => new sfValidatorInteger(array('required' => false, 'min' => 0)),
            //requested
            'prefworkplace' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'doctor_str' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'doctor_strissued' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false), array('invalid' => 'Date format should be ' . $inputDatePattern)),
            'doctor_strexpire' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false), array('invalid' => 'Date format should be ' . $inputDatePattern)),
            'dstrfile' => new sfValidatorFile(array('required' => false, 'max_size' => 1024000, 'validated_file_class' => 'orangehrmValidatedFile')),
            'dstrfileUpdate' => new sfValidatorString(array('required' => false)),
            'doctor_idicard' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'doctor_idicardexpire' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false), array('invalid' => 'Date format should be ' . $inputDatePattern)),
            'idicardfile' => new sfValidatorFile(array('required' => false, 'max_size' => 1024000, 'validated_file_class' => 'orangehrmValidatedFile')),
            'idicardfileUpdate' => new sfValidatorString(array('required' => false)),
            'doctor_hiperkes' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'dhiperkesfile' => new sfValidatorFile(array('required' => false, 'max_size' => 1024000, 'validated_file_class' => 'orangehrmValidatedFile')),
            'dhiperkesfileUpdate' => new sfValidatorString(array('required' => false)),
            'doctor_acls' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'doctor_aclsissued' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false), array('invalid' => 'Date format should be ' . $inputDatePattern)),
            'doctor_aclsexpire' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false), array('invalid' => 'Date format should be ' . $inputDatePattern)),
            'aclsfile' => new sfValidatorFile(array('required' => false, 'max_size' => 1024000, 'validated_file_class' => 'orangehrmValidatedFile')),
            'aclsfileUpdate' => new sfValidatorString(array('required' => false)),
            'doctor_atls' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'doctor_atlsissued' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false), array('invalid' => 'Date format should be ' . $inputDatePattern)),
            'doctor_atlsexpire' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false), array('invalid' => 'Date format should be ' . $inputDatePattern)),
            'atlsfile' => new sfValidatorFile(array('required' => false, 'max_size' => 1024000, 'validated_file_class' => 'orangehrmValidatedFile')),
            'atlsfileUpdate' => new sfValidatorString(array('required' => false)),
            'nurse_str' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'nurse_strissued' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false), array('invalid' => 'Date format should be ' . $inputDatePattern)),
            'nurse_strexpire' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false), array('invalid' => 'Date format should be ' . $inputDatePattern)),
            'nstrfile' => new sfValidatorFile(array('required' => false, 'max_size' => 1024000, 'validated_file_class' => 'orangehrmValidatedFile')),
            'nstrfileUpdate' => new sfValidatorString(array('required' => false)),
            'nurse_ppni' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'nurse_ppniexpire' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false), array('invalid' => 'Date format should be ' . $inputDatePattern)),
            'ppnifile' => new sfValidatorFile(array('required' => false, 'max_size' => 1024000, 'validated_file_class' => 'orangehrmValidatedFile')),
            'ppnifileUpdate' => new sfValidatorString(array('required' => false)),
            'nurse_hiperkes' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'nhiperkesfile' => new sfValidatorFile(array('required' => false, 'max_size' => 1024000, 'validated_file_class' => 'orangehrmValidatedFile')),
            'nhiperkesfileUpdate' => new sfValidatorString(array('required' => false)),
            'nurse_ppgd' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'nurse_ppgdissued' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false), array('invalid' => 'Date format should be ' . $inputDatePattern)),
            'nurse_ppgdexpire' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false), array('invalid' => 'Date format should be ' . $inputDatePattern)),
            'ppgdfile' => new sfValidatorFile(array('required' => false, 'max_size' => 1024000, 'validated_file_class' => 'orangehrmValidatedFile')),
            'ppgdfileUpdate' => new sfValidatorString(array('required' => false)),
            'atlsexpire' => new sfValidatorString(array('required' => false)),
            'aclsexpire' => new sfValidatorString(array('required' => false)),
            'ppgdexpire' => new sfValidatorString(array('required' => false)),
            'associate_desc' => new sfValidatorString(array('required' => false, 'max_length' => 41000)),
            'bachelor_desc' => new sfValidatorString(array('required' => false, 'max_length' => 41000)),
            'master_desc' => new sfValidatorString(array('required' => false, 'max_length' => 41000)),
            'doctoral_desc' => new sfValidatorString(array('required' => false, 'max_length' => 41000)),
            //
            'psikotest' => new sfValidatorFile(array('required' => false, 'max_size' => 1024000, 'validated_file_class' => 'orangehrmValidatedFile')),
            'psikotestUpdate' => new sfValidatorString(array('required' => false)),
            'bloodtype' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getGolDarahList()))),
            'heght' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'weight' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'npwp' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'bpjsnaker' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'bpjskes' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'faskes' => new sfValidatorString(array('required' => false, 'max_length' => 41000)),
            'kk' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'familyhead' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'emergencyname' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'emergencyaddr' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'emergencyprovince' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getProvinsiList()))),
            'emergencycity' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'emergencykecamatan' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'emergencykelurahan' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'emergencyzip' => new sfValidatorString(array('required' => false, 'max_length' => 10, 'trim' => true)),
            'emergencyrelation' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getRelationList()))),
            'emergencyphone' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'spouse' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'spouseage' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'spouseedu' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'spouseoccupation' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'firstchildname' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'firstchildage' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'firstchildedu' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'firstchildoccupation' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'secondchildname' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'secondchildage' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'secondchildedu' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'secondchildoccupation' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'thirdchildname' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'thirdchildage' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'thirdchildedu' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'thirdchildoccupation' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'fathername' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'fatherage' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'fatheredu' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'fatheroccupation' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'mothername' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'motherage' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'motheredu' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'motheroccupation' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'siblingname1' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'siblingage1' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'siblingedu1' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'siblingoccupation1' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'siblingname2' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'siblingage2' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'siblingedu2' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'siblingoccupation2' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'siblingname3' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'siblingage3' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'siblingedu3' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'siblingoccupation3' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'siblingname4' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'siblingage4' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'siblingedu4' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'siblingoccupation4' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'siblingname5' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'siblingage5' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'siblingedu5' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'siblingoccupation5' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'orgname1' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'orgyear1' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'orgposition1' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'orgname2' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'orgyear2' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'orgposition2' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'orgname3' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'orgyear3' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'orgposition3' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'orgname4' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'orgyear4' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'orgposition4' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'orgname5' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'orgyear5' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'orgposition5' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'diphteria' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'sinusitis' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'bronchitis' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'hemoptoe' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'tbc' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'lunginfection' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'asthma' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'dyspnoea' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'urinateproblem' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'urinatedisorder' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'kidneydisease' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'kidneystone' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'frequenturinate' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'meningitis' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'cerebralconcussion' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'poliomyelitis' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'epilepsy' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'stroke' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'headache' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'typhoid' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'bloodvomit' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'obstipation' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'dyspepsia' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'jaundice' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'gallbladder' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'swallowingdisorder' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'incontinentiaalvi' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'chickenpox' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'fungalskin' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'std' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'heartattack' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'chestpains' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'palpitation' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'hypertension' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'hemorrhoid' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'varicoseveins' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'thyroiddisease' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'rhematoidsarthritis' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'foodallergy' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'foodallergylist' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'drugallergy' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'drugallergylist' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'tetanus' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'fainting' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'oblivion' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'consentrationdif' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'visiondisorder' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'hearingdisorder' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'lumbago' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'neoplasm' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'mentalillness' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'skintuberculose' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'bonetuberculose' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'measles' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'malaria' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'diabetes' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'sleepdisorder' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'hospitalized' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'hospitalizeddate' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false), array('invalid' => "Date format should be" . $inputDatePattern)),
            'hospitalizedlength' => new sfValidatorInteger(array('required' => false, 'min' => 0)),
            'hospitalizedwhy' => new sfValidatorString(array('required' => false, 'max_length' => 41000)),
            'accident' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'accidentdate' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false), array('invalid' => "Date format should be" . $inputDatePattern)),
            'accidenttxt' => new sfValidatorString(array('required' => false, 'max_length' => 41000)),
            'surgery' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'surgerydate' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false), array('invalid' => "Date format should be" . $inputDatePattern)),
            'surgerytxt' => new sfValidatorString(array('required' => false, 'max_length' => 41000)),
            'smoker' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'smokernumber' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'smokerage' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'smokerstop' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, date("Y"))))),
            'alcohol' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'alcoholperweek' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'alcoholslokyperweek' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'exercise' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'exercisetxt' => new sfValidatorString(array('required' => false, 'max_length' => 41000)),
            'medicinelist' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'vacdiptheri' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'vactetanus' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'vacpolio' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'vacbcg' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'vacmmr' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'vacinfluenza' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'vachepatitisa' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'vachepatitisb' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'vacvarisela' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'vactyphoid' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'vachpv' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'vacmeningokok' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'vacyellowfever' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'vacjapencephalitis' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            //
            'kontrak' => new sfValidatorFile(array('required' => false, 'max_size' => 1024000, 'validated_file_class' => 'orangehrmValidatedFile')),
            'kontrakUpdate' => new sfValidatorString(array('required' => false)),
            //'gaji' => new sfValidatorFile(array('required' => false, 'max_size' => 1024000, 'validated_file_class' => 'orangehrmValidatedFile')),
        );
        
            
        if(!($this->candidatePermissions->canCreate() && empty($this->candidateId)) || ($this->candidatePermissions->canUpdate() && $this->candidateId > 0)){
            foreach ($widgets as $widget){
                $widget->setAttribute('disabled', 'disabled');
            }
        }
        
        $this->setWidgets($widgets);
        $this->setValidators($validators);
        
        $this->widgetSchema->setNameFormat('addCandidate[%s]');
        $this->widgetSchema['appliedDate']->setAttribute($name =null,$value = null);
        $this->setDefault('appliedDate', set_datepicker_date_format(date('Y-m-d')));

        if ($this->candidateId != null) {
            $this->setDefaultValues($this->candidateId);
        }
    }

    private function setDefaultValues($candidateId) {

        $candidate = $this->getCandidateService()->getCandidateById($candidateId);
        $this->setDefault('firstName', $candidate->getFirstName());
        $this->setDefault('middleName', $candidate->getMiddleName());
        $this->setDefault('lastName', $candidate->getLastName());
        $this->setDefault('email', $candidate->getEmail());
        $this->setDefault('contactNo', $candidate->getContactNumber());
        $this->attachment = $candidate->getJobCandidateAttachment();
        $this->setDefault('keyWords', $candidate->getKeywords());
        $this->setDefault('comment', $candidate->getComment());
        $this->setDefault('appliedDate', set_datepicker_date_format($candidate->getDateOfApplication()));
        $candidateVacancyList = $candidate->getJobCandidateVacancy();
        $defaultVacancy = ($candidateVacancyList[0]->getVacancyId() == "") ? "" : $candidateVacancyList[0]->getVacancyId();
        $this->setDefault('vacancy', $defaultVacancy);
        $this->setDefault('gender', $candidate->getGender());
        $this->setDefault('birthplace', $candidate->getBirthplace());
        $this->setDefault('dob', $candidate->getDob());
        $this->setDefault('currentaddr', $candidate->getCurrentaddr());
        $this->setDefault('currentprovince', $candidate->getCurrentprovince());
        $this->setDefault('currentcity', $candidate->getCurrentcity());
        $this->setDefault('currentkecamatan', $candidate->getCurrentkecamatan());
        $this->setDefault('currentkelurahan', $candidate->getCurrentkelurahan());
        $this->setDefault('currentzip', $candidate->getCurrentzip());
        $this->setDefault('permanentaddr', $candidate->getPermanentaddr());
        $this->setDefault('permanentprovince', $candidate->getPermanentprovince());
        $this->setDefault('permanentcity', $candidate->getPermanentcity());
        $this->setDefault('permanentkecamatan', $candidate->getPermanentkecamatan());
        $this->setDefault('permanentkelurahan', $candidate->getPermanentkelurahan());
        $this->setDefault('permanentzip', $candidate->getPermanentzip());
        $this->setDefault('religion', $candidate->getReligion());
        $this->setDefault('marital', $candidate->getMarital());
        $this->setDefault('phone', $candidate->getPhone());
        $this->setDefault('mobile', $candidate->getMobile());
        $this->setDefault('ektp', $candidate->getEktp());
        //requested
        $this->setDefault('elementary', $candidate->getElementary());
        $this->setDefault('elementary_startyear', $candidate->getElementaryStartyear());
        $this->setDefault('elementary_endyear', $candidate->getElementaryEndyear());
        $this->setDefault('juniorhigh', $candidate->getJuniorhigh());
        $this->setDefault('juniorhigh_startyear', $candidate->getJuniorhighStartyear());
        $this->setDefault('juniorhigh_endyear', $candidate->getJuniorhighEndyear());
        $this->setDefault('seniorhigh', $candidate->getSeniorhigh());
        $this->setDefault('seniorhigh_startyear', $candidate->getSeniorhighStartyear());
        $this->setDefault('seniorhigh_endyear', $candidate->getSeniorhighEndyear());
        $this->setDefault('associate', $candidate->getAssociate());
        $this->setDefault('associate_startyear', $candidate->getAssociateStartyear());
        $this->setDefault('associate_endyear', $candidate->getAssociateEndyear());
        $this->setDefault('bachelor', $candidate->getBachelor());
        $this->setDefault('bachelor_startyear', $candidate->getBachelorStartyear());
        $this->setDefault('bachelor_endyear', $candidate->getBachelorEndyear());
        $this->setDefault('master', $candidate->getMaster());
        $this->setDefault('master_startyear', $candidate->getMasterStartyear());
        $this->setDefault('master_endyear', $candidate->getMasterEndyear());
        $this->setDefault('doctoral', $candidate->getDoctoral());
        $this->setDefault('doctoral_startyear', $candidate->getDoctoralStartyear());
        $this->setDefault('doctoral_endyear', $candidate->getDoctoralEndyear());
        $this->graduatecertificate = $candidate->getJobCandidateAttachment();
        $this->transcript = $candidate->getJobCandidateAttachment();
        $this->setDefault('company1', $candidate->getCompany1());
        $this->setDefault('position1', $candidate->getPosition1());
        $this->setDefault('start1', $candidate->getStart1());
        $this->setDefault('end1', $candidate->getEnd1());
        $this->setDefault('jobdesc1', $candidate->getJobdesc1());
        $this->setDefault('refname1', $candidate->getRefname1());
        $this->setDefault('reftelp1', $candidate->getReftelp1());
        $this->setDefault('company2', $candidate->getCompany2());
        $this->setDefault('position2', $candidate->getPosition2());
        $this->setDefault('start2', $candidate->getStart2());
        $this->setDefault('end2', $candidate->getEnd2());
        $this->setDefault('jobdesc2', $candidate->getJobdesc2());
        $this->setDefault('refname2', $candidate->getRefname2());
        $this->setDefault('reftelp2', $candidate->getReftelp2());
        $this->setDefault('company3', $candidate->getCompany3());
        $this->setDefault('position3', $candidate->getPosition3());
        $this->setDefault('start3', $candidate->getStart3());
        $this->setDefault('end3', $candidate->getEnd3());
        $this->setDefault('jobdesc3', $candidate->getJobdesc3());
        $this->setDefault('refname3', $candidate->getRefname3());
        $this->setDefault('reftelp3', $candidate->getReftelp3());
        $this->setDefault('company4', $candidate->getCompany4());
        $this->setDefault('position4', $candidate->getPosition4());
        $this->setDefault('start4', $candidate->getStart4());
        $this->setDefault('end4', $candidate->getEnd4());
        $this->setDefault('jobdesc4', $candidate->getJobdesc4());
        $this->setDefault('refname4', $candidate->getRefname4());
        $this->setDefault('reftelp4', $candidate->getReftelp4());
        $this->setDefault('company5', $candidate->getCompany5());
        $this->setDefault('position5', $candidate->getPosition5());
        $this->setDefault('start5', $candidate->getStart5());
        $this->setDefault('end5', $candidate->getEnd5());
        $this->setDefault('jobdesc5', $candidate->getJobdesc5());
        $this->setDefault('refname5', $candidate->getRefname5());
        $this->setDefault('reftelp5', $candidate->getReftelp5());
        $this->refletter = $candidate->getJobCandidateAttachment();
        $this->setDefault('currentsalary', $candidate->getCurrentsalary());
        //
        $this->setDefault('wantsalary', $candidate->getWantsalary());
        //requested
        $this->setDefault('prefworkplace', $candidate->getPrefworkplace());
        $this->setDefault('doctor_str', $candidate->getDoctorStr());
        $this->setDefault('doctor_strissued', $candidate->getDoctorStrissued());
        $this->setDefault('doctor_strexpire', $candidate->getDoctorStrexpire());
        $this->dstrfile = $candidate->getJobCandidateAttachment();
        $this->setDefault('doctor_idicard', $candidate->getDoctorIdicard());
        $this->setDefault('doctor_idicardexpire', $candidate->getDoctorIdicardexpire());
        $this->idicardfile = $candidate->getJobCandidateAttachment();
        $this->setDefault('doctor_hiperkes', $candidate->getDoctorHiperkes());
        $this->dhiperkesfile = $candidate->getJobCandidateAttachment();
        $this->setDefault('doctor_acls', $candidate->getDoctorAcls());
        $this->setDefault('doctor_aclsissued', $candidate->getDoctorAclsissued());
        $this->setDefault('doctor_aclsexpire', $candidate->getDoctorAclsexpire());
        $this->aclsfile = $candidate->getJobCandidateAttachment();
        $this->setDefault('doctor_atls', $candidate->getDoctorAtls());
        $this->setDefault('doctor_atlsissued', $candidate->getDoctorAtlsissued());
        $this->setDefault('doctor_atlsexpire', $candidate->getDoctorAtlsexpire());
        $this->atlsfile = $candidate->getJobCandidateAttachment();
        $this->setDefault('nurse_str', $candidate->getNurseStr());
        $this->setDefault('nurse_strissued', $candidate->getNurseStrissued());
        $this->setDefault('nurse_strexpire', $candidate->getNurseStrexpire());
        $this->nstrfile = $candidate->getJobCandidateAttachment();
        $this->setDefault('nurse_ppni', $candidate->getNursePpni());
        $this->setDefault('nurse_ppniexpire', $candidate->getNursePpniexpire());
        $this->ppnifile = $candidate->getJobCandidateAttachment();
        $this->setDefault('nurse_hiperkes', $candidate->getNurseHiperkes());
        $this->nhiperkesfile = $candidate->getJobCandidateAttachment();
        $this->setDefault('nurse_ppgd', $candidate->getNursePpgd());
        $this->setDefault('nurse_ppgdissued', $candidate->getNursePpgdissued());
        $this->setDefault('nurse_ppgdexpire', $candidate->getNursePpgdexpire());
        $this->ppgdfile = $candidate->getJobCandidateAttachment();
        $this->setDefault('atlsexpire', $candidate->getAtlsexpire());
        $this->setDefault('aclsexpire', $candidate->getAclsexpire());
        $this->setDefault('ppgdexpire', $candidate->getPpgdexpire());
        $this->setDefault('associate_desc', $candidate->getAssociateDesc());
        $this->setDefault('bachelor_desc', $candidate->getBachelorDesc());
        $this->setDefault('master_desc', $candidate->getMasterDesc());
        $this->setDefault('doctoral_desc', $candidate->getDoctoralDesc());
        //
        $this->psikotest = $candidate->getJobCandidateAttachment();
        $this->setDefault('bloodtype', $candidate->getBloodtype());
        $this->setDefault('heght', $candidate->getHeght());
        $this->setDefault('weight', $candidate->getWeight());
        $this->setDefault('npwp', $candidate->getNpwp());
        $this->setDefault('bpjsnaker', $candidate->getBpjsnaker());
        $this->setDefault('bpjskes', $candidate->getBpjskes());
        $this->setDefault('faskes', $candidate->getFaskes());
        $this->setDefault('kk', $candidate->getKk());
        $this->setDefault('familyhead', $candidate->getFamilyhead());
        //requested
        $this->setDefault('emergencyname', $candidate->getEmergencyname());
        $this->setDefault('emergencyaddr', $candidate->getEmergencyaddr());
        $this->setDefault('emergencyprovince', $candidate->getEmergencyprovince());
        $this->setDefault('emergencycity', $candidate->getEmergencycity());
        $this->setDefault('emergencykecamatan', $candidate->getEmergencykecamatan());
        $this->setDefault('emergencykelurahan', $candidate->getEmergencykelurahan());
        $this->setDefault('emergencyzip', $candidate->getEmergencyzip());
        $this->setDefault('emergencyrelation', $candidate->getEmergencyrelation());
        $this->setDefault('emergencyphone', $candidate->getEmergencyphone());
        $this->setDefault('spouse', $candidate->getSpouse());
        $this->setDefault('spouseage', $candidate->getSpouseage());
        $this->setDefault('spouseedu', $candidate->getSpouseedu());
        $this->setDefault('spouseoccupation', $candidate->getSpouseoccupation());
        $this->setDefault('firstchildname', $candidate->getFirstchildname());
        $this->setDefault('firstchildage', $candidate->getFirstchildage());
        $this->setDefault('firstchildedu', $candidate->getFirstchildedu());
        $this->setDefault('firstchildoccupation', $candidate->getFirstchildoccupation());
        $this->setDefault('secondchildname', $candidate->getSecondchildname());
        $this->setDefault('secondchildage', $candidate->getSecondchildage());
        $this->setDefault('secondchildedu', $candidate->getSecondchildedu());
        $this->setDefault('secondchildoccupation', $candidate->getSecondchildoccupation());
        $this->setDefault('thirdchildname', $candidate->getThirdchildname());
        $this->setDefault('thirdchildage', $candidate->getThirdchildage());
        $this->setDefault('thirdchildedu', $candidate->getThirdchildedu());
        $this->setDefault('thirdchildoccupation', $candidate->getThirdchildoccupation());
        $this->setDefault('fathername', $candidate->getFathername());
        $this->setDefault('fatherage', $candidate->getFatherage());
        $this->setDefault('fatheredu', $candidate->getFatheredu());
        $this->setDefault('fatheroccupation', $candidate->getFatheroccupation());
        $this->setDefault('mothername', $candidate->getMothername());
        $this->setDefault('motherage', $candidate->getMotherage());
        $this->setDefault('motheredu', $candidate->getMotheredu());
        $this->setDefault('motheroccupation', $candidate->getMotheroccupation());
        $this->setDefault('siblingname1', $candidate->getSiblingname1());
        $this->setDefault('siblingage1', $candidate->getSiblingage1());
        $this->setDefault('siblingedu1', $candidate->getSiblingedu1());
        $this->setDefault('siblingoccupation1', $candidate->getSiblingoccupation1());
        $this->setDefault('siblingname2', $candidate->getSiblingname2());
        $this->setDefault('siblingage2', $candidate->getSiblingage2());
        $this->setDefault('siblingedu2', $candidate->getSiblingedu2());
        $this->setDefault('siblingoccupation2', $candidate->getSiblingoccupation2());
        $this->setDefault('siblingname3', $candidate->getSiblingname3());
        $this->setDefault('siblingage3', $candidate->getSiblingage3());
        $this->setDefault('siblingedu3', $candidate->getSiblingedu3());
        $this->setDefault('siblingoccupation3', $candidate->getSiblingoccupation3());
        $this->setDefault('siblingname4', $candidate->getSiblingname4());
        $this->setDefault('siblingage4', $candidate->getSiblingage4());
        $this->setDefault('siblingedu4', $candidate->getSiblingedu4());
        $this->setDefault('siblingoccupation4', $candidate->getSiblingoccupation4());
        $this->setDefault('siblingname5', $candidate->getSiblingname5());
        $this->setDefault('siblingage5', $candidate->getSiblingage5());
        $this->setDefault('siblingedu5', $candidate->getSiblingedu5());
        $this->setDefault('siblingoccupation5', $candidate->getSiblingoccupation5());
        $this->setDefault('orgname1', $candidate->getOrgname1());
        $this->setDefault('orgyear1', $candidate->getOrgyear1());
        $this->setDefault('orgposition1', $candidate->getOrgposition1());
        $this->setDefault('orgname2', $candidate->getOrgname2());
        $this->setDefault('orgyear2', $candidate->getOrgyear2());
        $this->setDefault('orgposition2', $candidate->getOrgposition2());
        $this->setDefault('orgname3', $candidate->getOrgname3());
        $this->setDefault('orgyear3', $candidate->getOrgyear3());
        $this->setDefault('orgposition3', $candidate->getOrgposition3());
        $this->setDefault('orgname4', $candidate->getOrgname4());
        $this->setDefault('orgyear4', $candidate->getOrgyear4());
        $this->setDefault('orgposition4', $candidate->getOrgposition4());
        $this->setDefault('orgname5', $candidate->getOrgname5());
        $this->setDefault('orgyear5', $candidate->getOrgyear5());
        $this->setDefault('orgposition5', $candidate->getOrgposition5());
        $this->setDefault('diphteria', $candidate->getDiphteria());
        $this->setDefault('sinusitis', $candidate->getSinusitis());
        $this->setDefault('bronchitis', $candidate->getBronchitis());
        $this->setDefault('hemoptoe', $candidate->getHemoptoe());
        $this->setDefault('tbc', $candidate->getTbc());
        $this->setDefault('lunginfection', $candidate->getLunginfection());
        $this->setDefault('asthma', $candidate->getAsthma());
        $this->setDefault('dyspnoea', $candidate->getDyspnoea());
        $this->setDefault('urinateproblem', $candidate->getUrinateproblem());
        $this->setDefault('urinatedisorder', $candidate->getUrinatedisorder());
        $this->setDefault('kidneydisease', $candidate->getKidneydisease());
        $this->setDefault('kidneystone', $candidate->getKidneystone());
        $this->setDefault('frequenturinate', $candidate->getFrequenturinate());
        $this->setDefault('meningitis', $candidate->getMeningitis());
        $this->setDefault('cerebralconcussion', $candidate->getCerebralconcussion());
        $this->setDefault('poliomyelitis', $candidate->getPoliomyelitis());
        $this->setDefault('epilepsy', $candidate->getEpilepsy());
        $this->setDefault('stroke', $candidate->getStroke());
        $this->setDefault('headache', $candidate->getHeadache());
        $this->setDefault('typhoid', $candidate->getTyphoid());
        $this->setDefault('bloodvomit', $candidate->getBloodvomit());
        $this->setDefault('obstipation', $candidate->getObstipation());
        $this->setDefault('dyspepsia', $candidate->getDyspepsia());
        $this->setDefault('jaundice', $candidate->getJaundice());
        $this->setDefault('gallbladder', $candidate->getGallbladder());
        $this->setDefault('swallowingdisorder', $candidate->getSwallowingdisorder());
        $this->setDefault('incontinentiaalvi', $candidate->getIncontinentiaalvi());
        $this->setDefault('chickenpox', $candidate->getChickenpox());
        $this->setDefault('fungalskin', $candidate->getFungalskin());
        $this->setDefault('std', $candidate->getStd());
        $this->setDefault('heartattack', $candidate->getHeartattack());
        $this->setDefault('chestpains', $candidate->getChestpains());
        $this->setDefault('palpitation', $candidate->getPalpitation());
        $this->setDefault('hypertension', $candidate->getHypertension());
        $this->setDefault('hemorrhoid', $candidate->getHemorrhoid());
        $this->setDefault('varicoseveins', $candidate->getVaricoseveins());
        $this->setDefault('thyroiddisease', $candidate->getThyroiddisease());
        $this->setDefault('rhematoidsarthritis', $candidate->getRhematoidsarthritis());
        $this->setDefault('foodallergy', $candidate->getFoodallergy());
        $this->setDefault('foodallergylist', $candidate->getFoodallergylist());
        $this->setDefault('drugallergy', $candidate->getDrugallergy());
        $this->setDefault('drugallergylist', $candidate->getDrugallergylist());
        $this->setDefault('tetanus', $candidate->getTetanus());
        $this->setDefault('fainting', $candidate->getFainting());
        $this->setDefault('oblivion', $candidate->getOblivion());
        $this->setDefault('consentrationdif', $candidate->getConsentrationdif());
        $this->setDefault('visiondisorder', $candidate->getVisiondisorder());
        $this->setDefault('hearingdisorder', $candidate->getHearingdisorder());
        $this->setDefault('lumbago', $candidate->getLumbago());
        $this->setDefault('neoplasm', $candidate->getNeoplasm());
        $this->setDefault('mentalillness', $candidate->getMentalillness());
        $this->setDefault('skintuberculose', $candidate->getSkintuberculose());
        $this->setDefault('bonetuberculose', $candidate->getBonetuberculose());
        $this->setDefault('measles', $candidate->getMeasles());
        $this->setDefault('malaria', $candidate->getMalaria());
        $this->setDefault('diabetes', $candidate->getDiabetes());
        $this->setDefault('sleepdisorder', $candidate->getSleepdisorder());
        $this->setDefault('hospitalized', $candidate->getHospitalized());
        $this->setDefault('hospitalizeddate', $candidate->getHospitalizeddate());
        $this->setDefault('hospitalizedlength', $candidate->getHospitalizedlength());
        $this->setDefault('hospitalizedwhy', $candidate->getHospitalizedwhy());
        $this->setDefault('accident', $candidate->getAccident());
        $this->setDefault('accidentdate', $candidate->getAccidentdate());
        $this->setDefault('accidenttxt', $candidate->getAccidenttxt());
        $this->setDefault('surgery', $candidate->getSurgery());
        $this->setDefault('surgerydate', $candidate->getSurgerydate());
        $this->setDefault('surgerytxt', $candidate->getSurgerytxt());
        $this->setDefault('smoker', $candidate->getSmoker());
        $this->setDefault('smokernumber', $candidate->getSmokernumber());
        $this->setDefault('smokerage', $candidate->getSmokerage());
        $this->setDefault('smokerstop', $candidate->getSmokerstop());
        $this->setDefault('alcohol', $candidate->getAlcohol());
        $this->setDefault('alcoholperweek', $candidate->getAlcoholperweek());
        $this->setDefault('alcoholslokyperweek', $candidate->getAlcoholslokyperweek());
        $this->setDefault('exercise', $candidate->getExercise());
        $this->setDefault('exercisetxt', $candidate->getExercisetxt());
        $this->setDefault('medicinelist', $candidate->getMedicinelist());
        $this->setDefault('vacdiptheri', $candidate->getVacdiptheri());
        $this->setDefault('vactetanus', $candidate->getVactetanus());
        $this->setDefault('vacpolio', $candidate->getVacpolio());
        $this->setDefault('vacbcg', $candidate->getVacbcg());
        $this->setDefault('vacmmr', $candidate->getVacmmr());
        $this->setDefault('vacinfluenza', $candidate->getVacinfluenza());
        $this->setDefault('vachepatitisa', $candidate->getVachepatitisa());
        $this->setDefault('vachepatitisb', $candidate->getVachepatitisb());
        $this->setDefault('vacvarisela', $candidate->getVacvarisela());
        $this->setDefault('vactyphoid', $candidate->getVactyphoid());
        $this->setDefault('vachpv', $candidate->getVachpv());
        $this->setDefault('vacmeningokok', $candidate->getVacmeningokok());
        $this->setDefault('vacyellowfever', $candidate->getVacyellowfever());
        $this->setDefault('vacjapencephalitis', $candidate->getVacjapencephalitis());
        //
        $this->kontrak = $candidate->getJobCandidateAttachment();
        //$this->gaji = $candidate->getJobCandidateAttachment();

    }

    private function getActiveVacancyList() {
        $list = array("" => "-- " . __('Select') . " --");
        $vacancyProperties = array('name', 'id', 'hiringManagerId');
        $activeVacancyList = $this->getVacancyService()->getVacancyPropertyList($vacancyProperties, JobVacancy::ACTIVE);
        
        $predefined = sfContext::getInstance()->getUser()->getAttribute('auth.userRole.predefined');
        foreach ($activeVacancyList as $vacancy) {
            $vacancyId = $vacancy['id'];
            if (in_array($vacancyId, $this->allowedVacancyList) && ($vacancy['hiringManagerId'] == $this->empNumber || $this->isAdmin
                    || !$predefined)) {
                $list[$vacancyId] = $vacancy['name'];
             }
        }
        return $list;
    }

    /**
     *
     * @return string
     */
    public function save() {
        $file = $this->getValue('resume');
        $resumeUpdate = $this->getValue('resumeUpdate');
        $resume = new JobCandidateAttachment();
        $resumeId = "";

        $psikotestFile = $this->getValue('psikotest');
        $psikotestObj = new JobCandidateAttachment();
        if(!empty($psikotestFile)){
            if (!($this->isValidDocument($psikotestFile))) {
                $resultArray['messageType'] = 'warning';
                $resultArray['message'] = __(TopLevelMessages::FILE_TYPE_SAVE_FAILURE);
                return $resultArray;
            }
        }

        //requested
        //eka: create file array
        $fileArray = array();
        //Eka: get photo file
        $photoFile = $this->getValue('photo');

        //Eka: non-mandatory file. Put into array for checking
        $graduatecertificateFile = $this->getValue('graduatecertificate');
        $fileArray[0] = $graduatecertificateFile;
        $refletterFile = $this->getValue('refletter');
        $fileArray[1] = $refletterFile;
        $dstrFile = $this->getValue('dstrfile');
        $fileArray[2] = $dstrFile;
        $idicardFile = $this->getValue('idifile');
        $fileArray[3] = $idicardFile;
        $dhiperkesFile = $this->getValue('dhiperkesfile');
        $fileArray[4] = $dhiperkesFile;
        $aclsFile = $this->getValue('aclsfile');
        $fileArray[5] = $aclsFile;
        $atlsFile = $this->getValue('atlsfile');
        $fileArray[6] = $atlsFile;
        $nstrFile = $this->getValue('nstrfile');
        $fileArray[7] = $nstrFile;
        $ppniFile = $this->getValue('ppnifile');
        $fileArray[8] = $ppniFile;
        $nhiperkesFile = $this->getValue('nhiperkesfile');
        $fileArray[9] = $nhiperkesFile;
        $ppgdFile = $this->getValue('ppgdfile');
        $fileArray[10] = $ppgdFile;

        $transcriptFile = $this->getValue('transcript');
        $transcriptObj = new JobCandidateAttachment();

        //Eka: photo object
        $photoObj = new JobCandidateAttachment();
        //

        $candidate = new JobCandidate();
        $vacancy = $this->getValue('vacancy');
        $existingVacancyList = array();
        $empNumber = sfContext::getInstance()->getUser()->getEmployeeNumber();
        if ($empNumber == 0) {
            $empNumber = null;
        }
        $this->addedBy = $empNumber;

        if (!empty($file)) {

            if (!($this->isValidResume($file))) {

                $resultArray['messageType'] = 'warning';
                $resultArray['message'] = __(TopLevelMessages::FILE_TYPE_SAVE_FAILURE);
                return $resultArray;
            }
        }

        $vacancyStatus = '';
        if ($this->candidateId != null) {
            $candidate = $this->getCandidateService()->getCandidateById($this->candidateId);
            $storedResume = $candidate->getJobCandidateAttachment();
            if ($storedResume != "") {
                $resume = $storedResume;
            }
            $existingVacancyList = $candidate->getJobCandidateVacancy();
            $candidateVacancy = $existingVacancyList[0];
            $id = $candidateVacancy->getVacancyId();
            $vacancyStatus = $candidateVacancy->getStatus();

            //requested
            //no need
            if (!empty($id)) {
                if ($id != $vacancy) {
                    $interviews = $this->getInterviewService()->getInterviewsByCandidateVacancyId($candidateVacancy);
                    foreach ($interviews as $interview) {
                        $interviewers = $interview->getJobInterviewInterviewer();
                        foreach ($interviewers as $interviewer) {
                            $interviewer->delete();
                        }
                    }
                    $candidateVacancy->delete();
                    $vacancyName = $candidateVacancy->getVacancyName();
                    $this->removedHistory = new CandidateHistory();
                    $this->removedHistory->candidateId = $this->candidateId;
                    $this->removedHistory->action = CandidateHistory::RECRUITMENT_CANDIDATE_ACTION_REMOVE;
                    $this->removedHistory->performedBy = $this->addedBy;
                    $date = date('Y-m-d');
                    $this->removedHistory->performedDate = $date . " " . date('H:i:s');
                    $this->removedHistory->candidateVacancyName = $vacancyName;
                    $this->removedHistory->vacancyId = $id;
                    $this->_saveCandidateVacancies($vacancy, $this->candidateId);
                }
            } else {
                $this->_saveCandidateVacancies($vacancy, $this->candidateId);
            }

            if(!empty($psikotestFile)){
                $psikotestId = $this->_savePsikotest($psikotestFile, $psikotestObj, $this->candidateId);
            }
        }

        if ($resumeUpdate == self::CONTRACT_DELETE) {
            $resume->delete();
        }
        $candidateId = $this->_getNewlySavedCandidateId($candidate, $vacancyStatus);

        $resultArray = array();
        $resultArray['candidateId'] = $candidateId;
        if (!empty($file)) {
            //file checking already done in the beginning lines above
            $resumeId = $this->_saveResume($file, $resume, $candidateId);
        }

        //requested
        //Eka: checking and saving photo
        //using isValidResume for mandatory field
        if(!empty($photoFile)){
            if (!($this->isValidResume($photoFile))) {

                $resultArray['messageType'] = 'warning';
                $resultArray['message'] = __(TopLevelMessages::FILE_TYPE_SAVE_FAILURE);
                return $resultArray;
            } else {
                //$this->candidateId = $this->_getNewlySavedCandidateId($candidate);
                //$resultArray['candidateId'] = $this->candidateId;
                $photoId = $this->_savePhoto($photoFile, $photoObj, $candidateId);
            }
        }

        //special for transcript
        if($transcriptFile != null){
            if(!($this->isValidAttachment($transcriptFile))){
                $resultArray['messageType'] = 'warning';
                $resultArray['message'] = __(TopLevelMessages::FILE_TYPE_SAVE_FAILURE);
                return $resultArray;
            } else {
                $transcriptId = $this->_saveAttachment($transcriptFile, $transcriptObj, $candidateId, 11, $this::TRANSCRIPT_FILE);
            }
        }

        //eka: file checking and saving for non mandatory file fields
        for($i = 0;$i < sizeof($fileArray);$i++){
            if($fileArray[$i] != null){
                if (!($this->isValidAttachment($fileArray[$i]))) {

                    $resultArray['messageType'] = 'warning';
                    $resultArray['message'] = __(TopLevelMessages::FILE_TYPE_SAVE_FAILURE . ' at index: ' . $i);
                    return $resultArray;
                } else {
                    //$this->candidateId = $this->_getNewlySavedCandidateId($candidate);
                    //$resultArray['candidateId'] = $this->candidateId;
                    $fileObj = new JobCandidateAttachment();
                    $attachmentFileId = $this->_saveAttachment($fileArray[$i], $fileObj, $candidateId, $i, $i + 3);
                }
            }
        }
        //

        if ($this->candidateId == "") {
            $this->_saveCandidateVacancies($vacancy, $candidateId);
        }
        if (!empty($this->addedHistory)) {
            $this->getCandidateService()->saveCandidateHistory($this->addedHistory);
        }
        if (!empty($this->removedHistory)) {
            $this->getCandidateService()->saveCandidateHistory($this->removedHistory);
        }
        return $resultArray;
    }

    /**
     *
     * @param sfValidatedFile $file
     * @return <type>
     */
    protected function isValidResume($file) {
        $validFile = false;

        $mimeTypes = array_values($this->allowedFileTypes);
        $originalName = $file->getOriginalName();

        if (($file instanceof orangehrmValidatedFile) && $originalName != "") {

            $fileType = $file->getType();

            if (!empty($fileType) && in_array($fileType, $mimeTypes)) {
                $validFile = true;
            } else {
                $fileType = $this->guessTypeFromFileExtension($originalName);

                if (!empty($fileType)) {
                    $file->setType($fileType);
                    $validFile = true;
                }
            }
        }

        return $validFile;
    }

    /**
     *
     * @param sfValidatedFile $file
     * @return <type>
     */
    protected function isValidDocument($file) {
        $validFile = false;

        $mimeTypes = array_values($this->allowedDocFileTypes);
        $originalName = $file->getOriginalName();

        if (($file instanceof orangehrmValidatedFile) && $originalName != "") {

            $fileType = $file->getType();

            if (!empty($fileType) && in_array($fileType, $mimeTypes)) {
                $validFile = true;
            } else {
                $fileType = $this->guessDocTypeFromFileExtension($originalName);

                if (!empty($fileType)) {
                    $file->setType($fileType);
                    $validFile = true;
                }
            }
        }

        return $validFile;
    }

    /**
     *
     * @param <type> $file
     * @param <type> $resume
     * @param <type> $candidateId
     * @return <type> 
     */
    private function _saveResume($file, $resume, $candidateId) {

        $tempName = $file->getTempName();
        $resume->fileContent = file_get_contents($tempName);
        $resume->fileName = $file->getOriginalName();
        $resume->fileType = $file->getType();
        $resume->fileSize = $file->getSize();
        //$resume->fileSize = $file->getSize();
        $resume->candidateId = $candidateId;
        $resume->filetitle = ApplyVacancyForm::KTP_FILE;

        $recruitmentAttachmentService = $this->getRecruitmentAttachmentService();
        $recruitmentAttachmentService->saveCandidateAttachment($resume);
    }

    private function _savePsikotest($file, $psikotest, $candidateId) {

        $tempName = $file->getTempName();
        $psikotest->fileContent = file_get_contents($tempName);
        $psikotest->fileName = $file->getOriginalName();
        $psikotest->fileType = $file->getType();
        $psikotest->fileSize = $file->getSize();
        //$psikotest->fileSize = $file->getSize();
        $psikotest->candidateId = $candidateId;
        $psikotest->filetitle = AddCandidateForm::PSIKOTEST_FILE;

        $recruitmentAttachmentService = $this->getRecruitmentAttachmentService();
        $recruitmentAttachmentService->saveCandidateAttachment($psikotest);

        $this->psikotest = $psikotest;
    }

    /**
     *
     * @param <type> $candidate
     * @return <type>
     */
    private function _getNewlySavedCandidateId($candidate, $vs) {

        $candidate->firstName = trim($this->getValue('firstName'));
        $candidate->middleName = trim($this->getValue('middleName'));
        $candidate->lastName = trim($this->getValue('lastName'));
        $candidate->email = $this->getValue('email');
        $candidate->comment = $this->getValue('comment');
        $candidate->contactNumber = $this->getValue('contactNo');
        $candidate->keywords = $this->getValue('keyWords');
        $candidate->addedPerson = $this->addedBy;

        if ($this->getValue('appliedDate') == "") {
            $candidate->dateOfApplication = date('Y-m-d');
        } else {
            $candidate->dateOfApplication = $this->getValue('appliedDate');
        }
        $candidate->status = JobCandidate::ACTIVE;
        $candidate->modeOfApplication = JobCandidate::MODE_OF_APPLICATION_MANUAL;

        //requested
        //Eka: add set value from gender onward
        $candidate->gender = $this->getValue('gender');
        $candidate->birthplace = $this->getValue('birthplace');
        $candidate->dob = $this->getValue('dob');
        $candidate->currentaddr = $this->getValue('currentaddr');
        $candidate->currentprovince = $this->getValue('currentprovince');
        $candidate->currentcity = $this->getValue('currentcity');
        $candidate->currentkecamatan = $this->getValue('currentkecamatan');
        $candidate->currentkelurahan = $this->getValue('currentkelurahan');
        $candidate->currentzip = $this->getValue('currentzip');
        $candidate->permanentaddr = $this->getValue('permanentaddr');
        $candidate->permanentprovince = $this->getValue('permanentprovince');
        $candidate->permanentcity = $this->getValue('permanentcity');
        $candidate->permanentkecamatan = $this->getValue('permanentkecamatan');
        $candidate->permanentkelurahan = $this->getValue('permanentkelurahan');
        $candidate->permanentzip = $this->getValue('permanentzip');
        $candidate->religion = $this->getValue('religion');
        $candidate->marital = $this->getValue('marital');
        $candidate->phone = $this->getValue('phone');
        $candidate->mobile = $this->getValue('mobile');
        $candidate->ektp = $this->getValue('ektp');
        $candidate->elementary = $this->getValue('elementary');
        $candidate->elementary_startyear = $this->getValue('elementary_startyear');
        $candidate->elementary_endyear = $this->getValue('elementary_endyear');
        $candidate->juniorhigh = $this->getValue('juniorhigh');
        $candidate->juniorhigh_startyear = $this->getValue('juniorhigh_startyear');
        $candidate->juniorhigh_endyear = $this->getValue('juniorhigh_endyear');
        $candidate->seniorhigh = $this->getValue('seniorhigh');
        $candidate->seniorhigh_startyear = $this->getValue('seniorhigh_startyear');
        $candidate->seniorhigh_endyear = $this->getValue('seniorhigh_endyear');
        $candidate->associate = $this->getValue('associate');
        $candidate->associate_startyear = $this->getValue('associate_startyear');
        $candidate->associate_endyear = $this->getValue('associate_endyear');
        $candidate->bachelor = $this->getValue('bachelor');
        $candidate->bachelor_startyear = $this->getValue('bachelor_startyear');
        $candidate->bachelor_endyear = $this->getValue('bachelor_endyear');
        $candidate->master = $this->getValue('master');
        $candidate->master_startyear = $this->getValue('master_startyear');
        $candidate->master_endyear = $this->getValue('master_endyear');
        $candidate->doctoral = $this->getValue('doctoral');
        $candidate->doctoral_startyear = $this->getValue('doctoral_startyear');
        $candidate->doctoral_endyear = $this->getValue('doctoral_endyear');
        $candidate->company1 = $this->getValue('company1');
        $candidate->position1 = $this->getValue('position1');
        $candidate->start1 = $this->getValue('start1');
        $candidate->end1 = $this->getValue('end1');
        $candidate->jobdesc1 = $this->getValue('jobdesc1');
        $candidate->refname1 = $this->getValue('refname1');
        $candidate->reftelp1 = $this->getValue('reftelp1');
        $candidate->company2 = $this->getValue('company2');
        $candidate->position2 = $this->getValue('position2');
        $candidate->start2 = $this->getValue('start2');
        $candidate->end2 = $this->getValue('end2');
        $candidate->jobdesc2 = $this->getValue('jobdesc2');
        $candidate->refname2 = $this->getValue('refname2');
        $candidate->reftelp2 = $this->getValue('reftelp2');
        $candidate->company3 = $this->getValue('company3');
        $candidate->position3 = $this->getValue('position3');
        $candidate->start3 = $this->getValue('start3');
        $candidate->end3 = $this->getValue('end3');
        $candidate->jobdesc3 = $this->getValue('jobdesc3');
        $candidate->refname3 = $this->getValue('refname3');
        $candidate->reftelp3 = $this->getValue('reftelp3');
        $candidate->company4 = $this->getValue('company4');
        $candidate->position4 = $this->getValue('position4');
        $candidate->start4 = $this->getValue('start4');
        $candidate->end4 = $this->getValue('end4');
        $candidate->jobdesc4 = $this->getValue('jobdesc4');
        $candidate->refname4 = $this->getValue('refname4');
        $candidate->reftelp4 = $this->getValue('reftelp4');
        $candidate->company5 = $this->getValue('company5');
        $candidate->position5 = $this->getValue('position5');
        $candidate->start5 = $this->getValue('start5');
        $candidate->end5 = $this->getValue('end5');
        $candidate->jobdesc5 = $this->getValue('jobdesc5');
        $candidate->refname5 = $this->getValue('refname5');
        $candidate->reftelp5 = $this->getValue('reftelp5');
        $candidate->currentsalary = $this->getValue('currentsalary');
        $candidate->wantsalary = $this->getValue('wantsalary');
        $candidate->prefworkplace = $this->getValue('prefworkplace');
        $candidate->doctor_str = $this->getValue('doctor_str');
        $candidate->doctor_strissued = $this->getValue('doctor_strissued');
        $candidate->doctor_strexpire = $this->getValue('doctor_strexpire');
        $candidate->doctor_idicard = $this->getValue('doctor_idicard');
        $candidate->doctor_idicardexpire = $this->getValue('doctor_idicardexpire');
        $candidate->doctor_hiperkes = $this->getValue('doctor_hiperkes');
        $candidate->doctor_acls = $this->getValue('doctor_acls');
        $candidate->doctor_aclsissued = $this->getValue('doctor_aclsissued');
        $candidate->doctor_aclsexpire = $this->getValue('doctor_aclsexpire');
        $candidate->doctor_atls = $this->getValue('doctor_atls');
        $candidate->doctor_atlsissued = $this->getValue('doctor_atlsissued');
        $candidate->doctor_atlsexpire = $this->getValue('doctor_atlsexpire');
        $candidate->nurse_str = $this->getValue('nurse_str');
        $candidate->nurse_strissued = $this->getValue('nurse_strissued');
        $candidate->nurse_strexpire = $this->getValue('nurse_strexpire');
        $candidate->nurse_ppni = $this->getValue('nurse_ppni');
        $candidate->nurse_ppniexpire = $this->getValue('nurse_ppniexpire');
        $candidate->nurse_hiperkes = $this->getValue('nurse_hiperkes');
        $candidate->nurse_ppgd = $this->getValue('nurse_ppgd');
        $candidate->nurse_ppgdissued = $this->getValue('nurse_ppgdissued');
        $candidate->nurse_ppgdexpire = $this->getValue('nurse_ppgdexpire');
        $candidate->associate_desc = $this->getValue('associate_desc');
        $candidate->bachelor_desc = $this->getValue('bachelor_desc');
        $candidate->master_desc = $this->getValue('master_desc');
        $candidate->doctoral_desc = $this->getValue('doctoral_desc');
        $candidate->bloodtype = $this->getValue('bloodtype');
        $candidate->heght = $this->getValue('heght');
        $candidate->weight = $this->getValue('weight');
        $candidate->npwp = $this->getValue('npwp');
        $candidate->bpjsnaker = $this->getValue('bpjsnaker');
        $candidate->bpjskes = $this->getValue('bpjskes');
        $candidate->faskes = $this->getValue('faskes');
        $candidate->kk = $this->getValue('kk');
        $candidate->familyhead = $this->getValue('familyhead');
        $candidate->emergencyname = ($this->getValue('emergencyname')?:'');
        $candidate->emergencyaddr = ($this->getValue('emergencyaddr')?:'');
        $candidate->emergencyprovince = ($this->getValue('emergencyprovince')?:'');
        $candidate->emergencycity = ($this->getValue('emergencycity')?:'');
        $candidate->emergencykecamatan = ($this->getValue('emergencykecamatan')?:'');
        $candidate->emergencykelurahan = ($this->getValue('emergencykelurahan')?:'');
        $candidate->emergencyzip = ($this->getValue('emergencyzip')?:'');
        $candidate->emergencyrelation = ($this->getValue('emergencyrelation')?:'');
        $candidate->emergencyphone = ($this->getValue('emergencyphone')?:'');
        $candidate->spouse = ($this->getValue('spouse')?:'');
        $candidate->spouseage = ($this->getValue('spouseage')?:'');
        $candidate->spouseedu = ($this->getValue('spouseedu')?:'');
        $candidate->spouseoccupation = ($this->getValue('spouseoccupation')?:'');
        $candidate->firstchildname = ($this->getValue('firstchildname')?:'');
        $candidate->firstchildage = ($this->getValue('firstchildage')?:'');
        $candidate->firstchildedu = ($this->getValue('firstchildedu')?:'');
        $candidate->firstchildoccupation = ($this->getValue('firstchildoccupation')?:'');
        $candidate->secondchildname = ($this->getValue('secondchildname')?:'');
        $candidate->secondchildage = ($this->getValue('secondchildage')?:'');
        $candidate->secondchildedu = ($this->getValue('secondchildedu')?:'');
        $candidate->secondchildoccupation = ($this->getValue('secondchildoccupation')?:'');
        $candidate->thirdchildname = ($this->getValue('thirdchildname')?:'');
        $candidate->thirdchildage = ($this->getValue('thirdchildage')?:'');
        $candidate->thirdchildedu = ($this->getValue('thirdchildedu')?:'');
        $candidate->thirdchildoccupation = ($this->getValue('thirdchildoccupation')?:'');
        $candidate->fathername = ($this->getValue('fathername')?:'');
        $candidate->fatherage = ($this->getValue('fatherage')?:'');
        $candidate->fatheredu = ($this->getValue('fatheredu')?:'');
        $candidate->fatheroccupation = ($this->getValue('fatheroccupation')?:'');
        $candidate->mothername = ($this->getValue('mothername')?:'');
        $candidate->motherage = ($this->getValue('motherage')?:'');
        $candidate->motheredu = ($this->getValue('motheredu')?:'');
        $candidate->motheroccupation = ($this->getValue('motheroccupation')?:'');
        $candidate->siblingname1 = ($this->getValue('siblingname1')?:'');
        $candidate->siblingage1 = ($this->getValue('siblingage1')?:'');
        $candidate->siblingedu1 = ($this->getValue('siblingedu1')?:'');
        $candidate->siblingoccupation1 = ($this->getValue('siblingoccupation1')?:'');
        $candidate->siblingname2 = ($this->getValue('siblingname2')?:'');
        $candidate->siblingage2 = ($this->getValue('siblingage2')?:'');
        $candidate->siblingedu2 = ($this->getValue('siblingedu2')?:'');
        $candidate->siblingoccupation2 = ($this->getValue('siblingoccupation2')?:'');
        $candidate->siblingname3 = ($this->getValue('siblingname3')?:'');
        $candidate->siblingage3 = ($this->getValue('siblingage3')?:'');
        $candidate->siblingedu3 = ($this->getValue('siblingedu3')?:'');
        $candidate->siblingoccupation3 = ($this->getValue('siblingoccupation3')?:'');
        $candidate->siblingname4 = ($this->getValue('siblingname4')?:'');
        $candidate->siblingage4 = ($this->getValue('siblingage4')?:'');
        $candidate->siblingedu4 = ($this->getValue('siblingedu4')?:'');
        $candidate->siblingoccupation4 = ($this->getValue('siblingoccupation4')?:'');
        $candidate->siblingname5 = ($this->getValue('siblingname5')?:'');
        $candidate->siblingage5 = ($this->getValue('siblingage5')?:'');
        $candidate->siblingedu5 = ($this->getValue('siblingedu5')?:'');
        $candidate->siblingoccupation5 = ($this->getValue('siblingoccupation5')?:'');
        $candidate->orgname1 = ($this->getValue('orgname1')?:'');
        $candidate->orgyear1 = $this->getValue('orgyear1');
        $candidate->orgposition1 = ($this->getValue('orgposition1')?:'');
        $candidate->orgname2 = ($this->getValue('orgname2')?:'');
        $candidate->orgyear2 = $this->getValue('orgyear2');
        $candidate->orgposition2 = ($this->getValue('orgposition2')?:'');
        $candidate->orgname3 = ($this->getValue('orgname3')?:'');
        $candidate->orgyear3 = $this->getValue('orgyear3');
        $candidate->orgposition3 = ($this->getValue('orgposition3')?:'');
        $candidate->orgname4 = ($this->getValue('orgname4')?:'');
        $candidate->orgyear4 = $this->getValue('orgyear4');
        $candidate->orgposition4 = ($this->getValue('orgposition4')?:'');
        $candidate->orgname5 = ($this->getValue('orgname5')?:'');
        $candidate->orgyear5 = $this->getValue('orgyear5');
        $candidate->orgposition5 = ($this->getValue('orgposition5')?:'');
        $candidate->diphteria = $this->getValue('diphteria');
        $candidate->sinusitis = $this->getValue('sinusitis');
        $candidate->bronchitis = $this->getValue('bronchitis');
        $candidate->hemoptoe = $this->getValue('hemoptoe');
        $candidate->tbc = $this->getValue('tbc');
        $candidate->lunginfection = $this->getValue('lunginfection');
        $candidate->asthma = $this->getValue('asthma');
        $candidate->dyspnoea = $this->getValue('dyspnoea');
        $candidate->urinateproblem = $this->getValue('urinateproblem');
        $candidate->urinatedisorder = $this->getValue('urinatedisorder');
        $candidate->kidneydisease = $this->getValue('kidneydisease');
        $candidate->kidneystone = $this->getValue('kidneystone');
        $candidate->frequenturinate = $this->getValue('frequenturinate');
        $candidate->meningitis = $this->getValue('meningitis');
        $candidate->cerebralconcussion = $this->getValue('cerebralconcussion');
        $candidate->poliomyelitis = $this->getValue('poliomyelitis');
        $candidate->epilepsy = $this->getValue('epilepsy');
        $candidate->stroke = $this->getValue('stroke');
        $candidate->headache = $this->getValue('headache');
        $candidate->typhoid = $this->getValue('typhoid');
        $candidate->bloodvomit = $this->getValue('bloodvomit');
        $candidate->obstipation = $this->getValue('obstipation');
        $candidate->dyspepsia = $this->getValue('dyspepsia');
        $candidate->jaundice = $this->getValue('jaundice');
        $candidate->gallbladder = $this->getValue('gallbladder');
        $candidate->swallowingdisorder = $this->getValue('swallowingdisorder');
        $candidate->incontinentiaalvi = $this->getValue('incontinentiaalvi');
        $candidate->chickenpox = $this->getValue('chickenpox');
        $candidate->fungalskin = $this->getValue('fungalskin');
        $candidate->std = $this->getValue('std');
        $candidate->heartattack = $this->getValue('heartattack');
        $candidate->chestpains = $this->getValue('chestpains');
        $candidate->palpitation = $this->getValue('palpitation');
        $candidate->hypertension = $this->getValue('hypertension');
        $candidate->hemorrhoid = $this->getValue('hemorrhoid');
        $candidate->varicoseveins = $this->getValue('varicoseveins');
        $candidate->thyroiddisease = $this->getValue('thyroiddisease');
        $candidate->rhematoidsarthritis = $this->getValue('rhematoidsarthritis');
        $candidate->foodallergy = $this->getValue('foodallergy');
        $candidate->foodallergylist = ($this->getValue('foodallergylist')?:'');
        $candidate->drugallergy = $this->getValue('drugallergy');
        $candidate->drugallergylist = ($this->getValue('drugallergylist')?:'');
        $candidate->tetanus = $this->getValue('tetanus');
        $candidate->fainting = $this->getValue('fainting');
        $candidate->oblivion = $this->getValue('oblivion');
        $candidate->consentrationdif = $this->getValue('consentrationdif');
        $candidate->visiondisorder = $this->getValue('visiondisorder');
        $candidate->hearingdisorder = $this->getValue('hearingdisorder');
        $candidate->lumbago = $this->getValue('lumbago');
        $candidate->neoplasm = $this->getValue('neoplasm');
        $candidate->mentalillness = $this->getValue('mentalillness');
        $candidate->skintuberculose = $this->getValue('skintuberculose');
        $candidate->bonetuberculose = $this->getValue('bonetuberculose');
        $candidate->measles = $this->getValue('measles');
        $candidate->malaria = $this->getValue('malaria');
        $candidate->diabetes = $this->getValue('diabetes');
        $candidate->sleepdisorder = $this->getValue('sleepdisorder');
        $candidate->hospitalized = $this->getValue('hospitalized');
        $candidate->hospitalizeddate = $this->getValue('hospitalizeddate');
        $candidate->hospitalizedlength = $this->getValue('hospitalizedlength');
        $candidate->hospitalizedwhy = ($this->getValue('hospitalizedwhy')?:'');
        $candidate->accident = $this->getValue('accident');
        $candidate->accidentdate = $this->getValue('accidentdate');
        $candidate->accidenttxt = ($this->getValue('accidenttxt')?:'');
        $candidate->surgery = $this->getValue('surgery');
        $candidate->surgerydate = $this->getValue('surgerydate');
        $candidate->surgerytxt = ($this->getValue('surgerytxt')?:'');
        $candidate->smoker = $this->getValue('smoker');
        $candidate->smokernumber = ($this->getValue('smokernumber')?:'');
        $candidate->smokerage = ($this->getValue('smokerage')?:'');
        $candidate->smokerstop = $this->getValue('smokerstop');
        $candidate->alcohol = $this->getValue('alcohol');
        $candidate->alcoholperweek = ($this->getValue('alcoholperweek')?:'');
        $candidate->alcoholslokyperweek = ($this->getValue('alcoholslokyperweek')?:'');
        $candidate->exercise = $this->getValue('exercise');
        $candidate->exercisetxt = ($this->getValue('exercisetxt')?:'');
        $candidate->medicinelist = ($this->getValue('medicinelist')?:'');
        $candidate->vacdiptheri = $this->getValue('vacdiptheri');
        $candidate->vactetanus = $this->getValue('vactetanus');
        $candidate->vacpolio = $this->getValue('vacpolio');
        $candidate->vacbcg = $this->getValue('vacbcg');
        $candidate->vacmmr = $this->getValue('vacmmr');
        $candidate->vacinfluenza = $this->getValue('vacinfluenza');
        $candidate->vachepatitisa = $this->getValue('vachepatitisa');
        $candidate->vachepatitisb = $this->getValue('vachepatitisb');
        $candidate->vacvarisela = $this->getValue('vacvarisela');
        $candidate->vactyphoid = $this->getValue('vactyphoid');
        $candidate->vachpv = $this->getValue('vachpv');
        $candidate->vacmeningokok = $this->getValue('vacmeningokok');
        $candidate->vacyellowfever = $this->getValue('vacyellowfever');
        $candidate->vacjapencephalitis = $this->getValue('vacjapencephalitis');

        if(empty($this->getValue('nurse_ppgd'))){
            if(strtotime($this->getValue('doctor_atlsexpire')) > strtotime("today"))
                $candidate->atlsexpire = 1;
            else
                $candidate->atlsexpire = 0;

            if(strtotime($this->getValue('doctor_aclsexpire')) > strtotime("today"))
                $candidate->aclsexpire = 1;
            else
                $candidate->aclsexpire = 0;
        } else {
            if(strtotime($this->getValue('doctor_atlsexpire')) > strtotime("today"))
                $candidate->ppgdexpire = 1;
            else
                $candidate->ppgdexpire = 0;
        }

        $candidateService = $this->getCandidateService();
        if ($this->candidateId != null) {

            $candidateService->updateCandidate($candidate, $vs);
            if(!empty($vs) && $vs != "APPLICATION INITIATED"){
                $history = new CandidateHistory();
                $history->candidateId = $this->candidateId;
                $history->action = PluginWorkflowStateMachine::RECRUITMENT_APPLICATION_ACTION_SHORTLIST;
                $history->extaction = CandidateHistory::RECRUITMENT_CANDIDATE_EXT_ACTION_DETAILFORM;
                $history->performedDate = date('Y-m-d h:i:sa');
                $history->vacancyId = $this->getValue('vacancyId');
                $history->candidateVacancyName = $this->getValue('vacancyName');
                if(!$this->isExtaction())
                    $this->getCandidateService()->saveCandidateHistory($history);
            }
        } else {

            //MUST THINK ABOUT EXTENDED STATUS FOR ADDITIONAL DATA


            $candidateService->saveCandidate($candidate);
            $this->addedHistory = new CandidateHistory();
            $this->addedHistory->candidateId = $candidate->getId();
            $this->addedHistory->action = CandidateHistory::RECRUITMENT_CANDIDATE_ACTION_ADD;
            $this->addedHistory->performedBy = $this->addedBy;
            $date = date('Y-m-d');
            $this->addedHistory->performedDate = $date . " " . date('H:i:s');
        }

        $candidateId = $candidate->getId();
        return $candidateId;
    }

    /**
     *
     * @param <type> $vacnacyArray
     * @param <type> $candidateId
     */
    private function _saveCandidateVacancies($vacnacy, $candidateId) {

        if ($vacnacy != null) {
            $candidateVacancy = new JobCandidateVacancy();
            $candidateVacancy->candidateId = $candidateId;
            $candidateVacancy->vacancyId = $vacnacy;
            
            // Get correct status for candidate vacancy
            $userRoleManager = UserRoleManagerFactory::getUserRoleManager();
            $workflowItems = $userRoleManager->getAllowedActions(WorkflowStateMachine::FLOW_RECRUITMENT, 'INITIAL');
            
            if (isset($workflowItems[WorkflowStateMachine::RECRUITMENT_APPLICATION_ACTION_ATTACH_VACANCY])) {
                
                $workflowItem = $workflowItems[WorkflowStateMachine::RECRUITMENT_APPLICATION_ACTION_ATTACH_VACANCY];
                $candidateVacancy->status = $workflowItem->getResultingState();
                if ($this->getValue('appliedDate') == "") {
                    $candidateVacancy->appliedDate = date('Y-m-d');
                } else {
                    $candidateVacancy->appliedDate = $this->getValue('appliedDate');
                }
                $candidateService = $this->getCandidateService();
                $candidateService->saveCandidateVacancy($candidateVacancy);
                $history = new CandidateHistory();
                $history->candidateId = $candidateId;
                $history->action = WorkflowStateMachine::RECRUITMENT_APPLICATION_ACTION_ATTACH_VACANCY;
                $history->vacancyId = $candidateVacancy->getVacancyId();
                $history->performedBy = $this->addedBy;
                $date = date('Y-m-d');
                $history->performedDate = $date . " " . date('H:i:s');
                $history->candidateVacancyName = $candidateVacancy->getVacancyName();
                $this->getCandidateService()->saveCandidateHistory($history);
            } else {
                throw new RecruitmentExeption('No workflow items found for job vacancy INITIAL state');
            }
        }
    }

    /**
     *
     * @return JobCandidateAttachment
     */
    public function getResume() {
        return $this->attachment;
    }

    /**
     * Guess the file mime type from the file extension
     *
     * @param  string $file  The absolute path of a file
     *
     * @return string The mime type of the file (null if not guessable)
     */
    public function guessTypeFromFileExtension($file) {

        $mimeType = null;

        $extension = pathinfo($file, PATHINFO_EXTENSION);

        if (isset($this->allowedFileTypes[$extension])) {
            $mimeType = $this->allowedFileTypes[$extension];
        }

        return $mimeType;
    }

    /**
     * Guess the file mime type from the file extension
     *
     * @param  string $file  The absolute path of a file
     *
     * @return string The mime type of the file (null if not guessable)
     */
    public function guessDocTypeFromFileExtension($file) {

        $mimeType = null;

        $extension = pathinfo($file, PATHINFO_EXTENSION);

        if (isset($this->allowedDocFileTypes[$extension])) {
            $mimeType = $this->allowedDocFileTypes[$extension];
        }

        return $mimeType;
    }

    public function getProvinsi(){
        $url = $this->nomornetHost . "/nomornet/provinsi";
        $client = curl_init($url);
        curl_setopt($client,CURLOPT_RETURNTRANSFER,true);
        $response = curl_exec($client);
        $result = json_decode($response);
        return $result;
    }

    public function getKotakabupaten($provinsi){
        $url = $this->nomornetHost . "/nomornet/kotakabupaten/" . urlencode($provinsi);
        $client = curl_init($url);
        curl_setopt($client,CURLOPT_RETURNTRANSFER,true);
        $response = curl_exec($client);
        $result = json_decode($response);
        return $result;
//        return "";
    }

    public function getKecamatan($provinsi, $kotaKabupaten){
        $url = $this->nomornetHost . "/nomornet/kecamatan/" . urlencode($provinsi) . "/" . urlencode($kotaKabupaten);
        $client = curl_init($url);
        curl_setopt($client,CURLOPT_RETURNTRANSFER,true);
        $response = curl_exec($client);
        $result = json_decode($response);
        return $result;
//        return "";
    }

    public function getKodeposDesa($provinsi, $kotaKabupaten, $kecamatan){
        $url = $this->nomornetHost . "/nomornet/kodeposdesa/" . urlencode($provinsi) . "/" . urlencode($kotaKabupaten) . "/" . urlencode($kecamatan);
        $client = curl_init($url);
        curl_setopt($client,CURLOPT_RETURNTRANSFER,true);
        $response = curl_exec($client);
        $result = json_decode($response);
        return $result;
//        return "";
    }

    private function _savePhoto($file, $photo, $candidateId) {

        $tempName = $file->getTempName();
        $photo->fileContent = file_get_contents($tempName);
        $photo->fileName = $file->getOriginalName();
        $photo->fileType = $file->getType();
        $photo->fileSize = $file->getSize();
        //$photo->fileSize = $file->getSize();
        $photo->candidateId = $candidateId;
        $photo->filetitle = ApplyVacancyForm::PHOTO_FILE;

        $recruitmentAttachmentService = $this->getRecruitmentAttachmentService();
        $recruitmentAttachmentService->saveCandidateAttachment($photo);

        $this->photo = $photo;
    }

    //Eka: checking for valid file
    protected function isValidAttachment($file) {

        $validFile = false;

        $mimeTypes = array_values($this->allowedFileTypes);
        $originalName = $file->getOriginalName();

        if (($file instanceof orangehrmValidatedFile) && $originalName != "") {

            $fileType = $file->getType();

            if (!empty($fileType) && in_array($fileType, $mimeTypes)) {
                $validFile = true;
            } else {
                $fileType = $this->guessTypeFromFileExtension($originalName);

                if (!empty($fileType)) {
                    $file->setType($fileType);
                    $validFile = true;
                }
            }
        }

        return $validFile;
    }

    private function _saveAttachment($file, $attachmentObj, $candidateId, $type, $filetitle) {

        $tempName = $file->getTempName();
        $attachmentObj->fileContent = file_get_contents($tempName);
        $attachmentObj->fileName = $file->getOriginalName();
        $attachmentObj->fileType = $file->getType();
        $attachmentObj->fileSize = $file->getSize();
        //$attachmentObj->fileSize = $file->getSize();
        $attachmentObj->candidateId = $candidateId;
        $attachmentObj->filetitle = $filetitle;

        $recruitmentAttachmentService = $this->getRecruitmentAttachmentService();
        $recruitmentAttachmentService->saveCandidateAttachment($attachmentObj);
        //save attachment to local var based on type
        switch($type){
            /*case 'photo':
                $this->photo = $attachmentObj;
                break;*/
            //'graduatecertificate'
            case 0:
                $this->graduatecertificate = $attachmentObj;
                break;
            //'refletter'
            case 1:
                $this->refletter =  $attachmentObj;
                break;
            //'dstrfile'
            case 2:
                $this->dstrfile =  $attachmentObj;
                break;
            //'idicardfile'
            case 3:
                $this->idicardfile =  $attachmentObj;
                break;
            //'dhiperkesfile'
            case 4:
                $this->dhiperkesfile =  $attachmentObj;
                break;
            //'aclsfile'
            case 5:
                $this->aclsfile =  $attachmentObj;
                break;
            //'atlsfile'
            case 6:
                $this->atlsfile =  $attachmentObj;
                break;
            //'nstrfile'
            case 7:
                $this->nstrfile =  $attachmentObj;
                break;
            //'ppnifile'
            case 8:
                $this->ppnifile =  $attachmentObj;
                break;
            //'nhiperkesfile'
            case 9:
                $this->nhiperkesfile =  $attachmentObj;
                break;
            //'ppgdfile'
            case 10:
                $this->ppgdfile =  $attachmentObj;
                break;
            //'transcript'
            case 11:
                $this->transcript =  $attachmentObj;
                break;
            default:
        }
        //$this->attachment = $attachmentObj;
    }

    public function isExtaction(){
        if(!empty($this->chList)){
            foreach($this->chList as $chlist){
                if($chlist->getExtaction() == CandidateHistory::RECRUITMENT_CANDIDATE_EXT_ACTION_DETAILFORM)
                    return true;
            }
        }

        return false;
    }

}

