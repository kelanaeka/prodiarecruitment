<?php
/**
 * Created by PhpStorm.
 * User: kelanaeka
 * Date: 4/1/2020
 * Time: 4:56 AM
 */

class ViewApplicationStatusDataForm extends BaseForm
{
    //change this value to increase/reduce year span
    const yearSpan = 10;

    private $candidateService;
    private $candidate;
    private $candidateId;
    private $nomornetHost;
    //= "http://localhost/nomornet_api";

    /**
     *
     * @return <type>
     */
    public function getCandidateService() {
        if (is_null($this->candidateService)) {
            $this->candidateService = new CandidateService();
            $this->candidateService->setCandidateDao(new CandidateDao());
        }
        return $this->candidateService;
    }

    //Eka: list for GolDarah dropdown
    private function getGolDarahList(){
        $list = array(0 => "-- " . __('Select') . " --",
            __('A') => __('A'),
            __('B') => __('B'),
            __('AB') => __('AB'),
            __('O') => __('O')
        );

        return $list;
    }

    //Eka: list for relation dropdown
    private function getRelationList(){
        $list = array('' => "-- " . __('Select') . " --",
            __('Ayah') => __('Ayah'),
            __('Ibu') => __('Ibu'),
            __('Kakak') => __('Kakak'),
            __('Adik') => __('Adik'),
            __('Lainnya') => __('Lainnya')
        );

        return $list;
    }

    //Eka: list for provinsi dropdown
    private function getProvinsiList(){
        $provinsiData = $this->getProvinsi();
        $list = array('' => "-- " . __('Select') . " --");
        if(sizeof($provinsiData->records) > 0){
            for($i = 0;$i < sizeof($provinsiData->records);$i++){
                $list[$provinsiData->records[$i]->provinsi] = $provinsiData->records[$i]->provinsi;
            }
        }

        return $list;
    }

    //Eka: list for city
    private function getCityList($provinsi){
        //print_r($provinsi);die;
        $kotaKabupatenData = $this->getKotakabupaten($provinsi);
        $list = array('' => "-- " . __('Select') . " --");
        if(sizeof($kotaKabupatenData->records) > 0){
            for($i = 0;$i < sizeof($kotaKabupatenData->records);$i++){
                $list[$kotaKabupatenData->records[$i]->kotakabupaten] = $kotaKabupatenData->records[$i]->kotakabupaten;
            }
        }
        return $list;
    }

    //Eka: list for kecamatan
    private function getKecamatanList($provinsi, $city){
        $kecamatanData = $this->getKecamatan($provinsi, $city);
        $list = array('' => "-- " . __('Select') . " --");
        if(sizeof($kecamatanData->records) > 0){
            for($i = 0;$i < sizeof($kecamatanData->records);$i++){
                $list[$kecamatanData->records[$i]->kecamatan] = $kecamatanData->records[$i]->kecamatan;
            }
        }
        return $list;
    }

    //Eka: list for kodeposdesa
    private function getKodeposDesaList($provinsi, $city, $kecamatan){
        $kelurahanData = $this->getKodeposDesa($provinsi, $city, $kecamatan);
        $list = array('' => "-- " . __('Select') . " --");
        if(sizeof($kelurahanData->records) > 0){
            for($i = 0;$i < sizeof($kelurahanData->records);$i++){
                $list[$kelurahanData->records[$i]->desakelurahan] = $kelurahanData->records[$i]->desakelurahan;
            }
        }
        return $list;
    }

    private function getYearRange($start, $end){
        $list = array("" => "-- " . __('Select') . " --");
        for($i = $start;$i <= $end; $i++){
            $list[$i] = $i;
        }

        return $list;
    }

    public function configure(){
        $host= gethostname();
        $ip_server = gethostbyname($host);
        $this->nomornetHost = "http://" . $ip_server . "/nomornet_api";

        $endYear = date("Y") + ApplyVacancyForm::yearSpan;

        $inputDatePattern = sfContext::getInstance()->getUser()->getDateFormat();

        $this->candidateId = $this->getOption('candidateId');
        $this->vacancyId = $this->getOption('vacancyId');
        $this->appId = $this->getOption('appId');
        $this->vacancyName = $this->getOption('vacancyName');
        //get province: gak bisa
        /*if(!empty($this->candidateId)){
            $candidate = $this->getCandidateService()->getCandidateById($this->candidateId);
            $this->currentprovince = $candidate->getCurrentprovince();
        }*/

        $this->setWidgets(array(
            'candidateId' => new sfWidgetFormInputHidden(),
            'vacancyId' => new sfWidgetFormInputHidden(),
            'appId' => new sfWidgetFormInputHidden(),
            'vacancyName' => new sfWidgetFormInputHidden(),
            'bloodtype' => new sfWidgetFormSelect(array('choices' => $this->getGolDarahList())),
            'heght' => new sfWidgetFormInputText(),
            'weight' => new sfWidgetFormInputText(),
            'npwp' => new sfWidgetFormInputText(),
            'bpjsnaker' => new sfWidgetFormInputText(),
            'bpjskes' => new sfWidgetFormInputText(),
            'faskes' => new sfWidgetFormTextArea(),
            'kk' => new sfWidgetFormInputText(),
            'familyhead' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No'))),
            'emergencyname' => new sfWidgetFormInputText(),
            'emergencyaddr' => new sfWidgetFormInputText(),
            'emergencyprovince' => new sfWidgetFormSelect(array('choices' => $this->getProvinsiList())),
            'emergencycity' => new sfWidgetFormSelect(array('choices' => $this->getCityList($this->candidate->currentprovince))),
            'emergencykecamatan' => new sfWidgetFormSelect(array('choices' => $this->getKecamatanList($this->candidate->currentprovince,$this->candidate->currentcity))),
            'emergencykelurahan' => new sfWidgetFormSelect(array('choices' => $this->getKodeposDesaList($this->candidate->currentprovince,$this->candidate->currentcity,$this->candidate->currentkecamatan))),
            'emergencyzip' => new sfWidgetFormInputText(),
            'emergencyrelation' => new sfWidgetFormSelect(array('choices' => $this->getRelationList())),
            'emergencyphone' => new sfWidgetFormInputText(),
            'spouse' => new sfWidgetFormInputText(),
            'spouseage' => new sfWidgetFormInputText(),
            'spouseedu' => new sfWidgetFormInputText(),
            'spouseoccupation' => new sfWidgetFormInputText(),
            'firstchildname' => new sfWidgetFormInputText(),
            'firstchildage' => new sfWidgetFormInputText(),
            'firstchildedu' => new sfWidgetFormInputText(),
            'firstchildoccupation' => new sfWidgetFormInputText(),
            'secondchildname' => new sfWidgetFormInputText(),
            'secondchildage' => new sfWidgetFormInputText(),
            'secondchildedu' => new sfWidgetFormInputText(),
            'secondchildoccupation' => new sfWidgetFormInputText(),
            'thirdchildname' => new sfWidgetFormInputText(),
            'thirdchildage' => new sfWidgetFormInputText(),
            'thirdchildedu' => new sfWidgetFormInputText(),
            'thirdchildoccupation' => new sfWidgetFormInputText(),
            'fathername' => new sfWidgetFormInputText(),
            'fatherage' => new sfWidgetFormInputText(),
            'fatheredu' => new sfWidgetFormInputText(),
            'fatheroccupation' => new sfWidgetFormInputText(),
            'mothername' => new sfWidgetFormInputText(),
            'motherage' => new sfWidgetFormInputText(),
            'motheredu' => new sfWidgetFormInputText(),
            'motheroccupation' => new sfWidgetFormInputText(),
            'siblingname1' => new sfWidgetFormInputText(),
            'siblingage1' => new sfWidgetFormInputText(),
            'siblingedu1' => new sfWidgetFormInputText(),
            'siblingoccupation1' => new sfWidgetFormInputText(),
            'siblingname2' => new sfWidgetFormInputText(),
            'siblingage2' => new sfWidgetFormInputText(),
            'siblingedu2' => new sfWidgetFormInputText(),
            'siblingoccupation2' => new sfWidgetFormInputText(),
            'siblingname3' => new sfWidgetFormInputText(),
            'siblingage3' => new sfWidgetFormInputText(),
            'siblingedu3' => new sfWidgetFormInputText(),
            'siblingoccupation3' => new sfWidgetFormInputText(),
            'siblingname4' => new sfWidgetFormInputText(),
            'siblingage4' => new sfWidgetFormInputText(),
            'siblingedu4' => new sfWidgetFormInputText(),
            'siblingoccupation4' => new sfWidgetFormInputText(),
            'siblingname5' => new sfWidgetFormInputText(),
            'siblingage5' => new sfWidgetFormInputText(),
            'siblingedu5' => new sfWidgetFormInputText(),
            'siblingoccupation5' => new sfWidgetFormInputText(),
            'orgname1' => new sfWidgetFormInputText(),
            'orgyear1' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'orgposition1' => new sfWidgetFormInputText(),
            'orgname2' => new sfWidgetFormInputText(),
            'orgyear2' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'orgposition2' => new sfWidgetFormInputText(),
            'orgname3' => new sfWidgetFormInputText(),
            'orgyear3' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'orgposition3' => new sfWidgetFormInputText(),
            'orgname4' => new sfWidgetFormInputText(),
            'orgyear4' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'orgposition4' => new sfWidgetFormInputText(),
            'orgname5' => new sfWidgetFormInputText(),
            'orgyear5' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'orgposition5' => new sfWidgetFormInputText(),
            'diphteria' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'sinusitis' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'bronchitis' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'hemoptoe' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'tbc' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'lunginfection' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'asthma' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'dyspnoea' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'urinateproblem' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'urinatedisorder' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'kidneydisease' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'kidneystone' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'frequenturinate' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'meningitis' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'cerebralconcussion' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'poliomyelitis' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'epilepsy' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'stroke' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'headache' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'typhoid' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'bloodvomit' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'obstipation' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'dyspepsia' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'jaundice' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'gallbladder' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'swallowingdisorder' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'incontinentiaalvi' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'chickenpox' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'fungalskin' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'std' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'heartattack' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'chestpains' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'palpitation' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'hypertension' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'hemorrhoid' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'varicoseveins' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'thyroiddisease' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'rhematoidsarthritis' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'foodallergy' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'foodallergylist' => new sfWidgetFormInputText(),
            'drugallergy' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'drugallergylist' => new sfWidgetFormInputText(),
            'tetanus' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'fainting' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'oblivion' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'consentrationdif' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'visiondisorder' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'hearingdisorder' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'lumbago' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'neoplasm' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'mentalillness' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'skintuberculose' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'bonetuberculose' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'measles' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'malaria' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'diabetes' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'sleepdisorder' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'hospitalized' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'hospitalizeddate' => new ohrmWidgetDatePicker(array(), array('id' => 'hospitalizeddate')),
            'hospitalizedlength' => new sfWidgetFormInputText(),
            'hospitalizedwhy' => new sfWidgetFormTextArea(),
            'accident' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'accidentdate' => new ohrmWidgetDatePicker(array(), array('id' => 'accidentdate')),
            'accidenttxt' => new sfWidgetFormTextArea(),
            'surgery' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'surgerydate' => new ohrmWidgetDatePicker(array(), array('id' => 'surgerydate')),
            'surgerytxt' => new sfWidgetFormTextArea(),
            'smoker' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'smokernumber' => new sfWidgetFormInputText(),
            'smokerage' => new sfWidgetFormInputText(),
            'smokerstop' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, date("Y")))),
            'alcohol' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'alcoholperweek' => new sfWidgetFormInputText(),
            'alcoholslokyperweek' => new sfWidgetFormInputText(),
            'exercise' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'exercisetxt' => new sfWidgetFormTextArea(),
            'medicinelist' => new sfWidgetFormInputText(),
            'vacdiptheri' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'vactetanus' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'vacpolio' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'vacbcg' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'vacmmr' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'vacinfluenza' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'vachepatitisa' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'vachepatitisb' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'vacvarisela' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'vactyphoid' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'vachpv' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'vacmeningokok' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'vacyellowfever' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'vacjapencephalitis' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array(2 => 'Yes', 1 => 'No') )),
            'consent' => new sfWidgetFormInputHidden(),
        ));

        $this->setValidators(array(
            'candidateId' => new sfValidatorString(array('required' => false)),
            'vacancyId' => new sfValidatorString(array('required' => false)),
            'appId' => new sfValidatorString(array('required' => false)),
            'vacancyName' => new sfValidatorString(array('required' => false)),
            'bloodtype' => new sfValidatorChoice(array('required' => true, 'choices' => array_keys($this->getGolDarahList()))),
            'heght' => new sfValidatorString(array('required' => true, 'max_length' => 5, 'trim' => true)),
            'weight' => new sfValidatorString(array('required' => true, 'max_length' => 5, 'trim' => true)),
            'npwp' => new sfValidatorString(array('required' => true, 'max_length' => 45, 'trim' => true)),
            'bpjsnaker' => new sfValidatorString(array('required' => true, 'max_length' => 45, 'trim' => true)),
            'bpjskes' => new sfValidatorString(array('required' => true, 'max_length' => 45, 'trim' => true)),
            'faskes' => new sfValidatorString(array('required' => false, 'max_length' => 41000)),
            'kk' => new sfValidatorString(array('required' => true, 'max_length' => 45, 'trim' => true)),
            'familyhead' => new sfValidatorChoice(array('required' => true, 'choices' => array(2, 1), 'multiple' => false)),
            'emergencyname' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'emergencyaddr' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'emergencyprovince' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getProvinsiList()))),
            'emergencycity' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'emergencykecamatan' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'emergencykelurahan' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'emergencyzip' => new sfValidatorString(array('required' => false, 'max_length' => 10, 'trim' => true)),
            'emergencyrelation' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getRelationList()))),
            'emergencyphone' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'spouse' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'spouseage' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'spouseedu' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'spouseoccupation' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'firstchildname' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'firstchildage' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'firstchildedu' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'firstchildoccupation' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'secondchildname' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'secondchildage' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'secondchildedu' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'secondchildoccupation' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'thirdchildname' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'thirdchildage' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'thirdchildedu' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'thirdchildoccupation' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'fathername' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'fatherage' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'fatheredu' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'fatheroccupation' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'mothername' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'motherage' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'motheredu' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'motheroccupation' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'siblingname1' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'siblingage1' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'siblingedu1' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'siblingoccupation1' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'siblingname2' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'siblingage2' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'siblingedu2' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'siblingoccupation2' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'siblingname3' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'siblingage3' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'siblingedu3' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'siblingoccupation3' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'siblingname4' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'siblingage4' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'siblingedu4' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'siblingoccupation4' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'siblingname5' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'siblingage5' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'siblingedu5' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'siblingoccupation5' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'orgname1' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'orgyear1' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'orgposition1' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'orgname2' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'orgyear2' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'orgposition2' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'orgname3' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'orgyear3' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'orgposition3' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'orgname4' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'orgyear4' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'orgposition4' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'orgname5' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'orgyear5' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'orgposition5' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'diphteria' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'sinusitis' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'bronchitis' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'hemoptoe' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'tbc' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'lunginfection' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'asthma' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'dyspnoea' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'urinateproblem' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'urinatedisorder' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'kidneydisease' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'kidneystone' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'frequenturinate' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'meningitis' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'cerebralconcussion' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'poliomyelitis' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'epilepsy' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'stroke' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'headache' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'typhoid' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'bloodvomit' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'obstipation' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'dyspepsia' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'jaundice' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'gallbladder' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'swallowingdisorder' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'incontinentiaalvi' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'chickenpox' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'fungalskin' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'std' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'heartattack' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'chestpains' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'palpitation' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'hypertension' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'hemorrhoid' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'varicoseveins' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'thyroiddisease' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'rhematoidsarthritis' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'foodallergy' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'foodallergylist' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'drugallergy' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'drugallergylist' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'tetanus' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'fainting' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'oblivion' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'consentrationdif' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'visiondisorder' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'hearingdisorder' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'lumbago' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'neoplasm' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'mentalillness' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'skintuberculose' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'bonetuberculose' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'measles' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'malaria' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'diabetes' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'sleepdisorder' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'hospitalized' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'hospitalizeddate' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false), array('invalid' => "Date format should be" . $inputDatePattern)),
            'hospitalizedlength' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'hospitalizedwhy' => new sfValidatorString(array('required' => false, 'max_length' => 41000)),
            'accident' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'accidentdate' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false), array('invalid' => "Date format should be" . $inputDatePattern)),
            'accidenttxt' => new sfValidatorString(array('required' => false, 'max_length' => 41000)),
            'surgery' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'surgerydate' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false), array('invalid' => "Date format should be" . $inputDatePattern)),
            'surgerytxt' => new sfValidatorString(array('required' => false, 'max_length' => 41000)),
            'smoker' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'smokernumber' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'smokerage' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'smokerstop' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, date("Y"))))),
            'alcohol' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'alcoholperweek' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'alcoholslokyperweek' => new sfValidatorString(array('required' => false, 'max_length' => 5, 'trim' => true)),
            'exercise' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'exercisetxt' => new sfValidatorString(array('required' => false, 'max_length' => 41000)),
            'medicinelist' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'vacdiptheri' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'vactetanus' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'vacpolio' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'vacbcg' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'vacmmr' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'vacinfluenza' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'vachepatitisa' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'vachepatitisb' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'vacvarisela' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'vactyphoid' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'vachpv' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'vacmeningokok' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'vacyellowfever' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'vacjapencephalitis' => new sfValidatorChoice(array('required' => false, 'choices' => array(2, 1), 'multiple' => false)),
            'consent' => new sfValidatorString(array('required' => false)),
        ));

        $this->widgetSchema->setNameFormat('viewApplicationStatusData[%s]');

        if(!empty($this->candidateId)){
            $this->setDefault('candidateId', $this->candidateId);
            $this->setDefault('vacancyId', $this->vacancyId);
            $this->setDefault('appId', $this->appId);
            $this->setDefault('vacancyName', $this->vacancyName);

            $this->setDefault('consent', 2);
        }
    }

    public function save(){
        $resultArray = array();

        $candidateId = $this->getValue('candidateId');
        $candidate = $this->getCandidateService()->getCandidateById($candidateId);

        $appId = $this->getValue('appId');
        $candidate->bloodtype = $this->getValue('bloodtype');
        $candidate->heght = $this->getValue('heght');
        $candidate->weight = $this->getValue('weight');
        $candidate->npwp = $this->getValue('npwp');
        $candidate->bpjsnaker = $this->getValue('bpjsnaker');
        $candidate->bpjskes = $this->getValue('bpjskes');
        $candidate->faskes = $this->getValue('faskes');
        $candidate->kk = $this->getValue('kk');
        $candidate->familyhead = $this->getValue('familyhead');
        $candidate->emergencyname = $this->getValue('emergencyname');
        $candidate->emergencyaddr = $this->getValue('emergencyaddr');
        $candidate->emergencyprovince = $this->getValue('emergencyprovince');
        $candidate->emergencycity = $this->getValue('emergencycity');
        $candidate->emergencykecamatan = $this->getValue('emergencykecamatan');
        $candidate->emergencykelurahan = $this->getValue('emergencykelurahan');
        $candidate->emergencyzip = $this->getValue('emergencyzip');
        $candidate->emergencyrelation = $this->getValue('emergencyrelation');
        $candidate->emergencyphone = $this->getValue('emergencyphone');
        $candidate->spouse = $this->getValue('spouse');
        $candidate->spouseage = $this->getValue('spouseage');
        $candidate->spouseedu = $this->getValue('spouseedu');
        $candidate->spouseoccupation = $this->getValue('spouseoccupation');
        $candidate->firstchildname = $this->getValue('firstchildname');
        $candidate->firstchildage = $this->getValue('firstchildage');
        $candidate->firstchildedu = $this->getValue('firstchildedu');
        $candidate->firstchildoccupation = $this->getValue('firstchildoccupation');
        $candidate->secondchildname = $this->getValue('secondchildname');
        $candidate->secondchildage = $this->getValue('secondchildage');
        $candidate->secondchildedu = $this->getValue('secondchildedu');
        $candidate->secondchildoccupation = $this->getValue('secondchildoccupation');
        $candidate->thirdchildname = $this->getValue('thirdchildname');
        $candidate->thirdchildage = $this->getValue('thirdchildage');
        $candidate->thirdchildedu = $this->getValue('thirdchildedu');
        $candidate->thirdchildoccupation = $this->getValue('thirdchildoccupation');
        $candidate->fathername = $this->getValue('fathername');
        $candidate->fatherage = $this->getValue('fatherage');
        $candidate->fatheredu = $this->getValue('fatheredu');
        $candidate->fatheroccupation = $this->getValue('fatheroccupation');
        $candidate->mothername = $this->getValue('mothername');
        $candidate->motherage = $this->getValue('motherage');
        $candidate->motheredu = $this->getValue('motheredu');
        $candidate->motheroccupation = $this->getValue('motheroccupation');
        $candidate->siblingname1 = $this->getValue('siblingname1');
        $candidate->siblingage1 = $this->getValue('siblingage1');
        $candidate->siblingedu1 = $this->getValue('siblingedu1');
        $candidate->siblingoccupation1 = $this->getValue('siblingoccupation1');
        $candidate->siblingname2 = $this->getValue('siblingname2');
        $candidate->siblingage2 = $this->getValue('siblingage2');
        $candidate->siblingedu2 = $this->getValue('siblingedu2');
        $candidate->siblingoccupation2 = $this->getValue('siblingoccupation2');
        $candidate->siblingname3 = $this->getValue('siblingname3');
        $candidate->siblingage3 = $this->getValue('siblingage3');
        $candidate->siblingedu3 = $this->getValue('siblingedu3');
        $candidate->siblingoccupation3 = $this->getValue('siblingoccupation3');
        $candidate->siblingname4 = $this->getValue('siblingname4');
        $candidate->siblingage4 = $this->getValue('siblingage4');
        $candidate->siblingedu4 = $this->getValue('siblingedu4');
        $candidate->siblingoccupation4 = $this->getValue('siblingoccupation4');
        $candidate->siblingname5 = $this->getValue('siblingname5');
        $candidate->siblingage5 = $this->getValue('siblingage5');
        $candidate->siblingedu5 = $this->getValue('siblingedu5');
        $candidate->siblingoccupation5 = $this->getValue('siblingoccupation5');
        $candidate->orgname1 = $this->getValue('orgname1');
        $candidate->orgyear1 = $this->getValue('orgyear1');
        $candidate->orgposition1 = $this->getValue('orgposition1');
        $candidate->orgname2 = $this->getValue('orgname2');
        $candidate->orgyear2 = $this->getValue('orgyear2');
        $candidate->orgposition2 = $this->getValue('orgposition2');
        $candidate->orgname3 = $this->getValue('orgname3');
        $candidate->orgyear3 = $this->getValue('orgyear3');
        $candidate->orgposition3 = $this->getValue('orgposition3');
        $candidate->orgname4 = $this->getValue('orgname4');
        $candidate->orgyear4 = $this->getValue('orgyear4');
        $candidate->orgposition4 = $this->getValue('orgposition4');
        $candidate->orgname5 = $this->getValue('orgname5');
        $candidate->orgyear5 = $this->getValue('orgyear5');
        $candidate->orgposition5 = $this->getValue('orgposition5');
        $candidate->diphteria = $this->getValue('diphteria');
        $candidate->sinusitis = $this->getValue('sinusitis');
        $candidate->bronchitis = $this->getValue('bronchitis');
        $candidate->hemoptoe = $this->getValue('hemoptoe');
        $candidate->tbc = $this->getValue('tbc');
        $candidate->lunginfection = $this->getValue('lunginfection');
        $candidate->asthma = $this->getValue('asthma');
        $candidate->dyspnoea = $this->getValue('dyspnoea');
        $candidate->urinateproblem = $this->getValue('urinateproblem');
        $candidate->urinatedisorder = $this->getValue('urinatedisorder');
        $candidate->kidneydisease = $this->getValue('kidneydisease');
        $candidate->kidneystone = $this->getValue('kidneystone');
        $candidate->frequenturinate = $this->getValue('frequenturinate');
        $candidate->meningitis = $this->getValue('meningitis');
        $candidate->cerebralconcussion = $this->getValue('cerebralconcussion');
        $candidate->poliomyelitis = $this->getValue('poliomyelitis');
        $candidate->epilepsy = $this->getValue('epilepsy');
        $candidate->stroke = $this->getValue('stroke');
        $candidate->headache = $this->getValue('headache');
        $candidate->typhoid = $this->getValue('typhoid');
        $candidate->bloodvomit = $this->getValue('bloodvomit');
        $candidate->obstipation = $this->getValue('obstipation');
        $candidate->dyspepsia = $this->getValue('dyspepsia');
        $candidate->jaundice = $this->getValue('jaundice');
        $candidate->gallbladder = $this->getValue('gallbladder');
        $candidate->swallowingdisorder = $this->getValue('swallowingdisorder');
        $candidate->incontinentiaalvi = $this->getValue('incontinentiaalvi');
        $candidate->chickenpox = $this->getValue('chickenpox');
        $candidate->fungalskin = $this->getValue('fungalskin');
        $candidate->std = $this->getValue('std');
        $candidate->heartattack = $this->getValue('heartattack');
        $candidate->chestpains = $this->getValue('chestpains');
        $candidate->palpitation = $this->getValue('palpitation');
        $candidate->hypertension = $this->getValue('hypertension');
        $candidate->hemorrhoid = $this->getValue('hemorrhoid');
        $candidate->varicoseveins = $this->getValue('varicoseveins');
        $candidate->thyroiddisease = $this->getValue('thyroiddisease');
        $candidate->rhematoidsarthritis = $this->getValue('rhematoidsarthritis');
        $candidate->foodallergy = $this->getValue('foodallergy');
        $candidate->foodallergylist = $this->getValue('foodallergylist');
        $candidate->drugallergy = $this->getValue('drugallergy');
        $candidate->drugallergylist = $this->getValue('drugallergylist');
        $candidate->tetanus = $this->getValue('tetanus');
        $candidate->fainting = $this->getValue('fainting');
        $candidate->oblivion = $this->getValue('oblivion');
        $candidate->consentrationdif = $this->getValue('consentrationdif');
        $candidate->visiondisorder = $this->getValue('visiondisorder');
        $candidate->hearingdisorder = $this->getValue('hearingdisorder');
        $candidate->lumbago = $this->getValue('lumbago');
        $candidate->neoplasm = $this->getValue('neoplasm');
        $candidate->mentalillness = $this->getValue('mentalillness');
        $candidate->skintuberculose = $this->getValue('skintuberculose');
        $candidate->bonetuberculose = $this->getValue('bonetuberculose');
        $candidate->measles = $this->getValue('measles');
        $candidate->malaria = $this->getValue('malaria');
        $candidate->diabetes = $this->getValue('diabetes');
        $candidate->sleepdisorder = $this->getValue('sleepdisorder');
        $candidate->hospitalized = $this->getValue('hospitalized');
        $candidate->hospitalizeddate = $this->getValue('hospitalizeddate');
        $candidate->hospitalizedlength = $this->getValue('hospitalizedlength');
        $candidate->hospitalizedwhy = $this->getValue('hospitalizedwhy');
        $candidate->accident = $this->getValue('accident');
        $candidate->accidentdate = $this->getValue('accidentdate');
        $candidate->accidenttxt = $this->getValue('accidenttxt');
        $candidate->surgery = $this->getValue('surgery');
        $candidate->surgerydate = $this->getValue('surgerydate');
        $candidate->surgerytxt = $this->getValue('surgerytxt');
        $candidate->smoker = $this->getValue('smoker');
        $candidate->smokernumber = $this->getValue('smokernumber');
        $candidate->smokerage = $this->getValue('smokerage');
        $candidate->smokerstop = $this->getValue('smokerstop');
        $candidate->alcohol = $this->getValue('alcohol');
        $candidate->alcoholperweek = $this->getValue('alcoholperweek');
        $candidate->alcoholslokyperweek = $this->getValue('alcoholslokyperweek');
        $candidate->exercise = $this->getValue('exercise');
        $candidate->exercisetxt = $this->getValue('exercisetxt');
        $candidate->medicinelist = $this->getValue('medicinelist');
        $candidate->vacdiptheri = $this->getValue('vacdiptheri');
        $candidate->vactetanus = $this->getValue('vactetanus');
        $candidate->vacpolio = $this->getValue('vacpolio');
        $candidate->vacbcg = $this->getValue('vacbcg');
        $candidate->vacmmr = $this->getValue('vacmmr');
        $candidate->vacinfluenza = $this->getValue('vacinfluenza');
        $candidate->vachepatitisa = $this->getValue('vachepatitisa');
        $candidate->vachepatitisb = $this->getValue('vachepatitisb');
        $candidate->vacvarisela = $this->getValue('vacvarisela');
        $candidate->vactyphoid = $this->getValue('vactyphoid');
        $candidate->vachpv = $this->getValue('vachpv');
        $candidate->vacmeningokok = $this->getValue('vacmeningokok');
        $candidate->vacyellowfever = $this->getValue('vacyellowfever');
        $candidate->vacjapencephalitis = $this->getValue('vacjapencephalitis');
        $candidate->consent = $this->getValue('consent');

        //
        /*
        $candidate->emergencyname = ($this->getValue('emergencyname')?:'');
        $candidate->emergencyaddr = ($this->getValue('emergencyaddr')?:'');
        $candidate->emergencyprovince = ($this->getValue('emergencyprovince')?:'');
        $candidate->emergencycity = ($this->getValue('emergencycity')?:'');
        $candidate->emergencykecamatan = ($this->getValue('emergencykecamatan')?:'');
        $candidate->emergencykelurahan = ($this->getValue('emergencykelurahan')?:'');
        $candidate->emergencyzip = ($this->getValue('emergencyzip')?:'');
        $candidate->emergencyrelation = ($this->getValue('emergencyrelation')?:'');
        $candidate->emergencyphone = ($this->getValue('emergencyphone')?:'');
        $candidate->spouse = ($this->getValue('spouse')?:'');
        $candidate->spouseage = ($this->getValue('spouseage')?:'');
        $candidate->spouseedu = ($this->getValue('spouseedu')?:'');
        $candidate->spouseoccupation = ($this->getValue('spouseoccupation')?:'');
        $candidate->firstchildname = ($this->getValue('firstchildname')?:'');
        $candidate->firstchildage = ($this->getValue('firstchildage')?:'');
        $candidate->firstchildedu = ($this->getValue('firstchildedu')?:'');
        $candidate->firstchildoccupation = ($this->getValue('firstchildoccupation')?:'');
        $candidate->secondchildname = ($this->getValue('secondchildname')?:'');
        $candidate->secondchildage = ($this->getValue('secondchildage')?:'');
        $candidate->secondchildedu = ($this->getValue('secondchildedu')?:'');
        $candidate->secondchildoccupation = ($this->getValue('secondchildoccupation')?:'');
        $candidate->thirdchildname = ($this->getValue('thirdchildname')?:'');
        $candidate->thirdchildage = ($this->getValue('thirdchildage')?:'');
        $candidate->thirdchildedu = ($this->getValue('thirdchildedu')?:'');
        $candidate->thirdchildoccupation = ($this->getValue('thirdchildoccupation')?:'');
        $candidate->fathername = ($this->getValue('fathername')?:'');
        $candidate->fatherage = ($this->getValue('fatherage')?:'');
        $candidate->fatheredu = ($this->getValue('fatheredu')?:'');
        $candidate->fatheroccupation = ($this->getValue('fatheroccupation')?:'');
        $candidate->mothername = ($this->getValue('mothername')?:'');
        $candidate->motherage = ($this->getValue('motherage')?:'');
        $candidate->motheredu = ($this->getValue('motheredu')?:'');
        $candidate->motheroccupation = ($this->getValue('motheroccupation')?:'');
        $candidate->siblingname1 = ($this->getValue('siblingname1')?:'');
        $candidate->siblingage1 = ($this->getValue('siblingage1')?:'');
        $candidate->siblingedu1 = ($this->getValue('siblingedu1')?:'');
        $candidate->siblingoccupation1 = ($this->getValue('siblingoccupation1')?:'');
        $candidate->siblingname2 = ($this->getValue('siblingname2')?:'');
        $candidate->siblingage2 = ($this->getValue('siblingage2')?:'');
        $candidate->siblingedu2 = ($this->getValue('siblingedu2')?:'');
        $candidate->siblingoccupation2 = ($this->getValue('siblingoccupation2')?:'');
        $candidate->siblingname3 = ($this->getValue('siblingname3')?:'');
        $candidate->siblingage3 = ($this->getValue('siblingage3')?:'');
        $candidate->siblingedu3 = ($this->getValue('siblingedu3')?:'');
        $candidate->siblingoccupation3 = ($this->getValue('siblingoccupation3')?:'');
        $candidate->siblingname4 = ($this->getValue('siblingname4')?:'');
        $candidate->siblingage4 = ($this->getValue('siblingage4')?:'');
        $candidate->siblingedu4 = ($this->getValue('siblingedu4')?:'');
        $candidate->siblingoccupation4 = ($this->getValue('siblingoccupation4')?:'');
        $candidate->siblingname5 = ($this->getValue('siblingname5')?:'');
        $candidate->siblingage5 = ($this->getValue('siblingage5')?:'');
        $candidate->siblingedu5 = ($this->getValue('siblingedu5')?:'');
        $candidate->siblingoccupation5 = ($this->getValue('siblingoccupation5')?:'');
        $candidate->orgname1 = ($this->getValue('orgname1')?:'');
        $candidate->orgyear1 = ($this->getValue('orgyear1')?:'');
        $candidate->orgposition1 = ($this->getValue('orgposition1')?:'');
        $candidate->orgname2 = ($this->getValue('orgname2')?:'');
        $candidate->orgyear2 = ($this->getValue('orgyear2')?:'');
        $candidate->orgposition2 = ($this->getValue('orgposition2')?:'');
        $candidate->orgname3 = ($this->getValue('orgname3')?:'');
        $candidate->orgyear3 = ($this->getValue('orgyear3')?:'');
        $candidate->orgposition3 = ($this->getValue('orgposition3')?:'');
        $candidate->orgname4 = ($this->getValue('orgname4')?:'');
        $candidate->orgyear4 = ($this->getValue('orgyear4')?:'');
        $candidate->orgposition4 = ($this->getValue('orgposition4')?:'');
        $candidate->orgname5 = ($this->getValue('orgname5')?:'');
        $candidate->orgyear5 = ($this->getValue('orgyear5')?:'');
        $candidate->orgposition5 = ($this->getValue('orgposition5')?:'');
        $candidate->diphteria = ($this->getValue('diphteria')?:'');
        $candidate->sinusitis = ($this->getValue('sinusitis')?:'');
        $candidate->bronchitis = ($this->getValue('bronchitis')?:'');
        $candidate->hemoptoe = ($this->getValue('hemoptoe')?:'');
        $candidate->tbc = ($this->getValue('tbc')?:'');
        $candidate->lunginfection = ($this->getValue('lunginfection')?:'');
        $candidate->asthma = ($this->getValue('asthma')?:'');
        $candidate->dyspnoea = ($this->getValue('dyspnoea')?:'');
        $candidate->urinateproblem = ($this->getValue('urinateproblem')?:'');
        $candidate->urinatedisorder = ($this->getValue('urinatedisorder')?:'');
        $candidate->kidneydisease = ($this->getValue('kidneydisease')?:'');
        $candidate->kidneystone = ($this->getValue('kidneystone')?:'');
        $candidate->frequenturinate = ($this->getValue('frequenturinate')?:'');
        $candidate->meningitis = ($this->getValue('meningitis')?:'');
        $candidate->cerebralconcussion = ($this->getValue('cerebralconcussion')?:'');
        $candidate->poliomyelitis = ($this->getValue('poliomyelitis')?:'');
        $candidate->epilepsy = ($this->getValue('epilepsy')?:'');
        $candidate->stroke = ($this->getValue('stroke')?:'');
        $candidate->headache = ($this->getValue('headache')?:'');
        $candidate->typhoid = ($this->getValue('typhoid')?:'');
        $candidate->bloodvomit = ($this->getValue('bloodvomit')?:'');
        $candidate->obstipation = ($this->getValue('obstipation')?:'');
        $candidate->dyspepsia = ($this->getValue('dyspepsia')?:'');
        $candidate->jaundice = ($this->getValue('jaundice')?:'');
        $candidate->gallbladder = ($this->getValue('gallbladder')?:'');
        $candidate->swallowingdisorder = ($this->getValue('swallowingdisorder')?:'');
        $candidate->incontinentiaalvi = ($this->getValue('incontinentiaalvi')?:'');
        $candidate->chickenpox = ($this->getValue('chickenpox')?:'');
        $candidate->fungalskin = ($this->getValue('fungalskin')?:'');
        $candidate->std = ($this->getValue('std')?:'');
        $candidate->heartattack = ($this->getValue('heartattack')?:'');
        $candidate->chestpains = ($this->getValue('chestpains')?:'');
        $candidate->palpitation = ($this->getValue('palpitation')?:'');
        $candidate->hypertension = ($this->getValue('hypertension')?:'');
        $candidate->hemorrhoid = ($this->getValue('hemorrhoid')?:'');
        $candidate->varicoseveins = ($this->getValue('varicoseveins')?:'');
        $candidate->thyroiddisease = ($this->getValue('thyroiddisease')?:'');
        $candidate->rhematoidsarthritis = ($this->getValue('rhematoidsarthritis')?:'');
        $candidate->foodallergy = ($this->getValue('foodallergy')?:'');
        $candidate->foodallergylist = ($this->getValue('foodallergylist')?:'');
        $candidate->drugallergy = ($this->getValue('drugallergy')?:'');
        $candidate->drugallergylist = ($this->getValue('drugallergylist')?:'');
        $candidate->tetanus = ($this->getValue('tetanus')?:'');
        $candidate->fainting = ($this->getValue('fainting')?:'');
        $candidate->oblivion = ($this->getValue('oblivion')?:'');
        $candidate->consentrationdif = ($this->getValue('consentrationdif')?:'');
        $candidate->visiondisorder = ($this->getValue('visiondisorder')?:'');
        $candidate->hearingdisorder = ($this->getValue('hearingdisorder')?:'');
        $candidate->lumbago = ($this->getValue('lumbago')?:'');
        $candidate->neoplasm = ($this->getValue('neoplasm')?:'');
        $candidate->mentalillness = ($this->getValue('mentalillness')?:'');
        $candidate->skintuberculose = ($this->getValue('skintuberculose')?:'');
        $candidate->bonetuberculose = ($this->getValue('bonetuberculose')?:'');
        $candidate->measles = ($this->getValue('measles')?:'');
        $candidate->malaria = ($this->getValue('malaria')?:'');
        $candidate->diabetes = ($this->getValue('diabetes')?:'');
        $candidate->sleepdisorder = ($this->getValue('sleepdisorder')?:'');
        $candidate->hospitalized = ($this->getValue('hospitalized')?:'');
        $candidate->hospitalizeddate = ($this->getValue('hospitalizeddate')?:'');
        $candidate->hospitalizedlength = ($this->getValue('hospitalizedlength')?:'');
        $candidate->hospitalizedwhy = ($this->getValue('hospitalizedwhy')?:'');
        $candidate->accident = ($this->getValue('accident')?:'');
        $candidate->accidentdate = ($this->getValue('accidentdate')?:'');
        $candidate->accidenttxt = ($this->getValue('accidenttxt')?:'');
        $candidate->surgery = ($this->getValue('surgery')?:'');
        $candidate->surgerydate = ($this->getValue('surgerydate')?:'');
        $candidate->surgerytxt = ($this->getValue('surgerytxt')?:'');
        $candidate->smoker = ($this->getValue('smoker')?:'');
        $candidate->smokernumber = ($this->getValue('smokernumber')?:'');
        $candidate->smokerage = ($this->getValue('smokerage')?:'');
        $candidate->smokerstop = ($this->getValue('smokerstop')?:'');
        $candidate->alcohol = ($this->getValue('alcohol')?:'');
        $candidate->alcoholperweek = ($this->getValue('alcoholperweek')?:'');
        $candidate->alcoholslokyperweek = ($this->getValue('alcoholslokyperweek')?:'');
        $candidate->exercise = ($this->getValue('exercise')?:'');
        $candidate->exercisetxt = ($this->getValue('exercisetxt')?:'');
        $candidate->medicinelist = ($this->getValue('medicinelist')?:'');
        $candidate->vacdiptheri = ($this->getValue('vacdiptheri')?:'');
        $candidate->vactetanus = ($this->getValue('vactetanus')?:'');
        $candidate->vacpolio = ($this->getValue('vacpolio')?:'');
        $candidate->vacbcg = ($this->getValue('vacbcg')?:'');
        $candidate->vacmmr = ($this->getValue('vacmmr')?:'');
        $candidate->vacinfluenza = ($this->getValue('vacinfluenza')?:'');
        $candidate->vachepatitisa = ($this->getValue('vachepatitisa')?:'');
        $candidate->vachepatitisb = ($this->getValue('vachepatitisb')?:'');
        $candidate->vacvarisela = ($this->getValue('vacvarisela')?:'');
        $candidate->vactyphoid = ($this->getValue('vactyphoid')?:'');
        $candidate->vachpv = ($this->getValue('vachpv')?:'');
        $candidate->vacmeningokok = ($this->getValue('vacmeningokok')?:'');
        $candidate->vacyellowfever = ($this->getValue('vacyellowfever')?:'');
        $candidate->vacjapencephalitis = ($this->getValue('vacjapencephalitis')?:'');
        $candidate->consent = ($this->getValue('consent')?:'');
         */
        try{
            $this->getCandidateService()->updateCandidateDetail($candidate);
            $history = new CandidateHistory();
            $history->candidateId = $candidateId;
            $history->action = PluginWorkflowStateMachine::RECRUITMENT_APPLICATION_ACTION_SHORTLIST;
            $history->extaction = CandidateHistory::RECRUITMENT_CANDIDATE_EXT_ACTION_DETAILFORM;
            $history->performedDate = date('Y-m-d h:i:sa');
            $history->vacancyId = $this->getValue('vacancyId');
            $history->candidateVacancyName = $this->getValue('vacancyName');
            $this->getCandidateService()->saveCandidateHistory($history);
        } catch(Exception $e){
            print_r($e);die;
        }
        $resultArray['candidateId'] = $candidateId;
        $resultArray['appId'] = $appId;
        return $resultArray;
    }

    public function getProvinsi(){
        $url = $this->nomornetHost . "/nomornet/provinsi";
        $client = curl_init($url);
        curl_setopt($client,CURLOPT_RETURNTRANSFER,true);
        $response = curl_exec($client);
        $result = json_decode($response);
        return $result;
    }

    public function getKotakabupaten($provinsi){
        $url = $this->nomornetHost . "/nomornet/kotakabupaten/" . urlencode($provinsi);
        $client = curl_init($url);
        curl_setopt($client,CURLOPT_RETURNTRANSFER,true);
        $response = curl_exec($client);
        $result = json_decode($response);
        return $result;
//        return "";
    }

    public function getKecamatan($provinsi, $kotaKabupaten){
        $url = $this->nomornetHost . "/nomornet/kecamatan/" . urlencode($provinsi) . "/" . urlencode($kotaKabupaten);
        $client = curl_init($url);
        curl_setopt($client,CURLOPT_RETURNTRANSFER,true);
        $response = curl_exec($client);
        $result = json_decode($response);
        return $result;
//        return "";
    }

    public function getKodeposDesa($provinsi, $kotaKabupaten, $kecamatan){
        $url = $this->nomornetHost . "/nomornet/kodeposdesa/" . urlencode($provinsi) . "/" . urlencode($kotaKabupaten) . "/" . urlencode($kecamatan);
        $client = curl_init($url);
        curl_setopt($client,CURLOPT_RETURNTRANSFER,true);
        $response = curl_exec($client);
        $result = json_decode($response);
        return $result;
//        return "";
    }

}