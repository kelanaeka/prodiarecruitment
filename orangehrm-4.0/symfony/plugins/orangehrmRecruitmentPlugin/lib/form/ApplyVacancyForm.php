<?php

/**
 * OrangeHRM is a comprehensive Human Resource Management (HRM) System that captures
 * all the essential functionalities required for any enterprise.
 * Copyright (C) 2006 OrangeHRM Inc., http://www.orangehrm.com
 *
 * OrangeHRM is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * OrangeHRM is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA
 *
 */
class ApplyVacancyForm extends BaseForm {
    //change this value to increase/reduce year span
    const yearSpan = 10;

    const KTP_FILE = 1;
    const PHOTO_FILE = 2;
    const EDU_FILE = 3;
    const JOBREF_FILE = 4;
    const DSTR_FILE = 5;
    const IDI_FILE = 6;
    const DHIPERKES_FILE = 7;
    const ACLS_FILE = 8;
    const ATLS_FILE = 9;
    const NSTR_FILE = 10;
    const PPNI_FILE = 11;
    const NHIPERKES_FILE = 12;
    const PPGD_FILE = 13;
    const TRANSCRIPT_FILE = 16;

    private $candidateService;
    private $recruitmentAttachmentService;
    private $workflowService;
    //Eka: add candidate object for province, city, kecamatan, kelurahan, zip retrieval
    private $candidate;
    
    public $attachment;
    //Eka: photo handler
    public $photo;
    public $graduatecertificate;
    public $transcript;
    public $refletter;
    public $dstrfile;
    public $idicardfile;
    public $dhiperkesfile;
    public $aclsfile;
    public $atlsfile;
    public $nstrfile;
    public $ppnifile;
    public $nhiperkesfile;
    public $ppgdfile;

    private $currentprovince;
    private $nomornetHost;
    public $candidateId;


    //private $nomornetHost = "http://localhost/nomornet_api";


    /*
    private $allowedFileTypes = array(
        "docx" => "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
        "doc" => "application/msword",
        "doc" => "application/x-msword",
        "doc" => "application/vnd.ms-office",
        "odt" => "application/vnd.oasis.opendocument.text",
        "pdf" => "application/pdf",
        "pdf" => "application/x-pdf",
        "rtf" => "application/rtf",
        "rtf" => "text/rtf",
        "txt" => "text/plain"
    );*/

    public function setCurrentprovince($currentProvince){
        $this->currentprovince = $currentProvince;
    }

    //Eka: allow jpeg
    private $allowedFileTypes = array(
        "jpeg" => "image/jpeg",
        "jpg" => "image/jpeg"
    );

    //Eka: list for Agama dropdown
    private function getAgamaList(){
        $list = array('' => "-- " . __('Select') . " --",
            __('Islam') => __('Islam'),
            __('Kristen') => __('Kristen'),
            __('Katolik') => __('Katolik'),
            __('Hindu') => __('Hindu'),
            __('Budha') => __('Budha'),
            __('KongHuCu') => __('KongHuCu'),
            __('Lainnya') => __('Lainnya')
        );

        return $list;
    }

    //Eka: list for provinsi dropdown
    private function getProvinsiList(){
        $provinsiData = $this->getProvinsi();
        $list = array('' => "-- " . __('Select') . " --");
        if(sizeof($provinsiData->records) > 0){
            for($i = 0;$i < sizeof($provinsiData->records);$i++){
                $list[$provinsiData->records[$i]->provinsi] = $provinsiData->records[$i]->provinsi;
            }
        }

        return $list;
    }

    //Eka: list for city
    private function getCityList($provinsi){
        //print_r($provinsi);die;
        $kotaKabupatenData = $this->getKotakabupaten($provinsi);
        $list = array('' => "-- " . __('Select') . " --");
        if(sizeof($kotaKabupatenData->records) > 0){
            for($i = 0;$i < sizeof($kotaKabupatenData->records);$i++){
                $list[$kotaKabupatenData->records[$i]->kotakabupaten] = $kotaKabupatenData->records[$i]->kotakabupaten;
            }
        }
        return $list;
    }

    //Eka: list for kecamatan
    private function getKecamatanList($provinsi, $city){
        $kecamatanData = $this->getKecamatan($provinsi, $city);
        $list = array('' => "-- " . __('Select') . " --");
        if(sizeof($kecamatanData->records) > 0){
            for($i = 0;$i < sizeof($kecamatanData->records);$i++){
                $list[$kecamatanData->records[$i]->kecamatan] = $kecamatanData->records[$i]->kecamatan;
            }
        }
        return $list;
    }

    //Eka: list for kodeposdesa
    private function getKodeposDesaList($provinsi, $city, $kecamatan){
        $kelurahanData = $this->getKodeposDesa($provinsi, $city, $kecamatan);
        $list = array('' => "-- " . __('Select') . " --");
        if(sizeof($kelurahanData->records) > 0){
            for($i = 0;$i < sizeof($kelurahanData->records);$i++){
                $list[$kelurahanData->records[$i]->desakelurahan] = $kelurahanData->records[$i]->desakelurahan;
            }
        }
        return $list;
    }

    private function getYearRange($start, $end){
        $list = array("" => "-- " . __('Select') . " --");
        for($i = $start;$i <= $end; $i++){
            $list[$i] = $i;
        }

        return $list;
    }

    /**
     *
     * @return <type>
     */
    public function getCandidateService() {
        if (is_null($this->candidateService)) {
            $this->candidateService = new CandidateService();
            $this->candidateService->setCandidateDao(new CandidateDao());
        }
        return $this->candidateService;
    }

    /**
     *
     * @return <type>
     */
    public function getRecruitmentAttachmentService() {
        if (is_null($this->recruitmentAttachmentService)) {
            $this->recruitmentAttachmentService = new RecruitmentAttachmentService();
            $this->recruitmentAttachmentService->setRecruitmentAttachmentDao(new RecruitmentAttachmentDao());
        }
        return $this->recruitmentAttachmentService;
    }
    
    public function getWorkflowService() {
        if (is_null($this->workflowService)) {
            $this->workflowService = new AccessFlowStateMachineService();        
        }
        return $this->workflowService;
    }
    
    public function configure() {
        $host= gethostname();
        $ip_server = gethostbyname($host);
        $this->nomornetHost = "http://" . $ip_server . "/nomornet_api";

        $endYear = date("Y") + ApplyVacancyForm::yearSpan;

        $inputDatePattern = sfContext::getInstance()->getUser()->getDateFormat();

        $this->candidateId = $this->getOption('candidateId');
        //get province: gak bisa
        if(!empty($this->candidateId)){
            $candidate = $this->getCandidateService()->getCandidateById($this->candidateId);
            $this->currentprovince = $candidate->getCurrentprovince();
        }

        $attachmentList = $this->attachment;
        if (count($attachmentList) > 0) {
            $this->attachment = $attachmentList[0];
        }

        //Eka: set photo handler to the first item if returned as a list
        $photoList = $this->photo;
        if (count($photoList) > 0) {
            $this->photo = $photoList[0];
        }

        $graduatecertificateList = $this->graduatecertificate;
        if(count($graduatecertificateList) > 0){
            $this->graduatecertificate = $graduatecertificateList[0];
        }

        $refletterList = $this->refletter;
        if(count($refletterList) > 0){
            $this->refletter = $refletterList[0];
        }

        $dstrfileList = $this->dstrfile;
        if(count($dstrfileList) > 0){
            $this->dstrfile = $dstrfileList[0];
        }

        $idicardfileList = $this->idicardfile;
        if(count($idicardfileList) > 0){
            $this->idicardfile = $idicardfileList[0];
        }

        $dhiperkesfileList = $this->dhiperkesfile;
        if(count($dhiperkesfileList) > 0){
            $this->dhiperkesfile = $dhiperkesfileList[0];
        }

        $aclsfileList = $this->aclsfile;
        if(count($aclsfileList) > 0){
            $this->aclsfile = $aclsfileList[0];
        }

        $atlsfileList = $this->atlsfile;
        if(count($atlsfileList) > 0){
            $this->atlsfile = $atlsfileList[0];
        }

        $nstrfileList = $this->nstrfile;
        if(count($nstrfileList) > 0){
            $this->nstrfile = $nstrfileList[0];
        }

        $ppnifileList = $this->ppnifile;
        if(count($ppnifileList) > 0){
            $this->ppnifile = $ppnifileList[0];
        }

        $nhiperkesfileList = $this->nhiperkesfile;
        if(count($nhiperkesfileList) > 0){
            $this->nhiperkesfile = $nhiperkesfileList[0];
        }

        $ppgdfileList = $this->ppgdfile;
        if(count($ppgdfileList) > 0){
            $this->ppgdfile = $ppgdfileList[0];
        }

        //creating widgets
        //Eka: add fields start from gender
        $this->setWidgets(array(
            'firstName' => new sfWidgetFormInputText(),
            'middleName' => new sfWidgetFormInputText(),
            'lastName' => new sfWidgetFormInputText(),
            'email' => new sfWidgetFormInputText(),
            'contactNo' => new sfWidgetFormInputText(),
            'resume' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
            //Eka: photo widget
            'photo' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
            'keyWords' => new sfWidgetFormInputText(),
            'comment' => new sfWidgetFormTextArea(),
            'vacancyList' => new sfWidgetFormInputHidden(),
            'gender' => new sfWidgetFormChoice(array('expanded' => true, 'choices' => array('Laki-laki' => 'Laki-laki', 'Perempuan' => 'Perempuan'))),
            'birthplace' => new sfWidgetFormInputText(),
            'dob' => new ohrmWidgetDatePicker(array(), array('id' => 'dob')),
            'currentaddr' => new sfWidgetFormInputText(),
            'currentprovince' => new sfWidgetFormSelect(array('choices' => $this->getProvinsiList())),
            'currentcity' => new sfWidgetFormSelect(array('choices' => $this->getCityList($this->currentprovince))),
            'currentkecamatan' => new sfWidgetFormSelect(array('choices' => $this->getKecamatanList($this->candidate->currentprovince,$this->candidate->currentcity))),
            'currentkelurahan' => new sfWidgetFormSelect(array('choices' => $this->getKodeposDesaList($this->candidate->currentprovince,$this->candidate->currentcity,$this->candidate->currentkecamatan))),
            'currentzip' => new sfWidgetFormInputText(),
            'permanentaddr' => new sfWidgetFormInputText(),
            'permanentprovince' => new sfWidgetFormSelect(array('choices' => $this->getProvinsiList())),
            'permanentcity' => new sfWidgetFormSelect(array('choices' => $this->getCityList($this->candidate->permanentprovince))),
            'permanentkecamatan' => new sfWidgetFormSelect(array('choices' => $this->getKecamatanList($this->candidate->permanentprovince,$this->candidate->permanentcity))),
            'permanentkelurahan' => new sfWidgetFormSelect(array('choices' => $this->getKodeposDesaList($this->candidate->permanentprovince,$this->candidate->permanentcity,$this->candidate->permanentkecamatan))),
            'permanentzip' => new sfWidgetFormInputText(),
            'religion' => new sfWidgetFormSelect(array('choices' => $this->getAgamaList())),
            'marital' => new sfWidgetFormSelect(array('choices' => array('' => "-- " . __('Select') . " --", 'Single' => __('Single'), 'Married' => __('Married'), 'Janda/Duda' => __('Janda/Duda')))),
            'phone' => new sfWidgetFormInputText(),
            'mobile' => new sfWidgetFormInputText(),
            'ektp' => new sfWidgetFormInputText(),
            'elementary' => new sfWidgetFormInputText(),
            'elementary_startyear' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'elementary_endyear' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'juniorhigh' => new sfWidgetFormInputText(),
            'juniorhigh_startyear' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'juniorhigh_endyear' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'seniorhigh' => new sfWidgetFormInputText(),
            'seniorhigh_startyear' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'seniorhigh_endyear' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'associate' => new sfWidgetFormInputText(),
            'associate_startyear' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'associate_endyear' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'bachelor' => new sfWidgetFormInputText(),
            'bachelor_startyear' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'bachelor_endyear' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'master' => new sfWidgetFormInputText(),
            'master_startyear' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'master_endyear' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'doctoral' => new sfWidgetFormInputText(),
            'doctoral_startyear' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'doctoral_endyear' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'graduatecertificate' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
            'transcript' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
            'company1' => new sfWidgetFormInputText(),
            'position1' => new sfWidgetFormInputText(),
            'start1' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'end1' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'jobdesc1' => new sfWidgetFormTextArea(),
            'refname1' => new sfWidgetFormInputText(),
            'reftelp1' => new sfWidgetFormInputText(),
            'company2' => new sfWidgetFormInputText(),
            'position2' => new sfWidgetFormInputText(),
            'start2' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'end2' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'jobdesc2' => new sfWidgetFormTextArea(),
            'refname2' => new sfWidgetFormInputText(),
            'reftelp2' => new sfWidgetFormInputText(),
            'company3' => new sfWidgetFormInputText(),
            'position3' => new sfWidgetFormInputText(),
            'start3' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'end3' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'jobdesc3' => new sfWidgetFormTextArea(),
            'refname3' => new sfWidgetFormInputText(),
            'reftelp3' => new sfWidgetFormInputText(),
            'company4' => new sfWidgetFormInputText(),
            'position4' => new sfWidgetFormInputText(),
            'start4' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'end4' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'jobdesc4' => new sfWidgetFormTextArea(),
            'refname4' => new sfWidgetFormInputText(),
            'reftelp4' => new sfWidgetFormInputText(),
            'company5' => new sfWidgetFormInputText(),
            'position5' => new sfWidgetFormInputText(),
            'start5' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'end5' => new sfWidgetFormSelect(array('choices' => $this->getYearRange(1980, $endYear))),
            'jobdesc5' => new sfWidgetFormTextArea(),
            'refname5' => new sfWidgetFormInputText(),
            'reftelp5' => new sfWidgetFormInputText(),
            'refletter' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
            'currentsalary' => new sfWidgetFormInputText(),
            'wantsalary' => new sfWidgetFormInputText(),
            'prefworkplace' => new sfWidgetFormInputText(),
            'doctor_str' => new sfWidgetFormInputText(),
            'doctor_strissued' => new ohrmWidgetDatePicker(array(), array('id' => 'doctor_strissued')),
            'doctor_strexpire' => new ohrmWidgetDatePicker(array(), array('id' => 'doctor_strexpire')),
            'dstrfile' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
            'doctor_idicard' => new sfWidgetFormInputText(),
            'doctor_idicardexpire' => new ohrmWidgetDatePicker(array(), array('id' => 'doctor_idicardexpire')),
            'idicardfile' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
            'doctor_hiperkes' => new sfWidgetFormInputText(),
            'dhiperkesfile' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
            'doctor_acls' => new sfWidgetFormInputText(),
            'doctor_aclsissued' => new ohrmWidgetDatePicker(array(), array('id' => 'doctor_aclsissued')),
            'doctor_aclsexpire' => new ohrmWidgetDatePicker(array(), array('id' => 'doctor_aclsexpire')),
            'aclsfile' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
            'doctor_atls' => new sfWidgetFormInputText(),
            'doctor_atlsissued' => new ohrmWidgetDatePicker(array(), array('id' => 'doctor_atlsissued')),
            'doctor_atlsexpire' => new ohrmWidgetDatePicker(array(), array('id' => 'doctor_atlsexpire')),
            'atlsfile' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
            'nurse_str' => new sfWidgetFormInputText(),
            'nurse_strissued' => new ohrmWidgetDatePicker(array(), array('id' => 'nurse_nurse_strissued')),
            'nurse_strexpire' => new ohrmWidgetDatePicker(array(), array('id' => 'nurse_nurse_strexpire')),
            'nstrfile' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
            'nurse_ppni' => new sfWidgetFormInputText(),
            'nurse_ppniexpire' => new ohrmWidgetDatePicker(array(), array('id' => 'nurse_ppniexpire')),
            'ppnifile' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
            'nurse_hiperkes' => new sfWidgetFormInputText(),
            'nhiperkesfile' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
            'nurse_ppgd' => new sfWidgetFormInputText(),
            'nurse_ppgdissued' => new ohrmWidgetDatePicker(array(), array('id' => 'nurse_ppgdissued')),
            'nurse_ppgdexpire' => new ohrmWidgetDatePicker(array(), array('id' => 'nurse_ppgdexpire')),
            'ppgdfile' => new sfWidgetFormInputFileEditable(array('edit_mode' => false, 'with_delete' => false, 'file_src' => '')),
            'atlsexpire' => new sfWidgetFormInputHidden(),
            'aclsexpire' => new sfWidgetFormInputHidden(),
            'ppgdexpire' => new sfWidgetFormInputHidden(),
            'associate_desc' => new sfWidgetFormTextarea(),
            'bachelor_desc' => new sfWidgetFormTextarea(),
            'master_desc' => new sfWidgetFormTextarea(),
            'doctoral_desc' => new sfWidgetFormTextarea(),
        ));


        //Eka: add validators start from gender
        $this->setValidators(array(
            'firstName' => new sfValidatorString(array('required' => true, 'max_length' => 35, 'trim' => true)),
            'middleName' => new sfValidatorString(array('required' => true, 'max_length' => 255,'trim' => true)),
            'lastName' => new sfValidatorString(array('required' => true, 'max_length' => 35, 'trim' => true)),
            'email' => new sfValidatorEmail(array('required' => true, 'max_length' => 100, 'trim' => true)),
            'contactNo' => new sfValidatorString(array('required' => false, 'max_length' => 35, 'trim' => true)),
            'resume' => new sfValidatorFile(array('required' => true, 'max_size' => 1024000, 'validated_file_class' => 'orangehrmValidatedFile')),
            //Eka: photo validator
            'photo' => new sfValidatorFile(array('required' => true, 'max_size' => 1024000, 'validated_file_class' => 'orangehrmValidatedFile')),
            'keyWords' => new sfValidatorString(array('required' => false, 'max_length' => 255)),
            'comment' => new sfValidatorString(array('required' => false)),
            'vacancyList' => new sfValidatorString(array('required' => false)),
            'gender' => new sfValidatorChoice(array('required' => true,
                'choices' => array('Laki-laki', 'Perempuan'),
                'multiple' => false)),
            'birthplace' => new sfValidatorString(array('required' => true, 'max_length' => 255, 'trim' => true)),
            'dob' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => true), array('invalid' => "Date format should be" . $inputDatePattern)),
            'currentaddr' => new sfValidatorString(array('required' => true, 'max_length' => 255, 'trim' => true)),
            'currentprovince' => new sfValidatorChoice(array('required' => true, 'choices' => array_keys($this->getProvinsiList()))),
            'currentcity' => new sfValidatorString(array('required' => true, 'max_length' => 255, 'trim' => true)),
            'currentkecamatan' => new sfValidatorString(array('required' => true, 'max_length' => 100, 'trim' => true)),
            'currentkelurahan' => new sfValidatorString(array('required' => true, 'max_length' => 100, 'trim' => true)),
            'currentzip' => new sfValidatorString(array('required' => true, 'max_length' => 10, 'trim' => true)),
            'permanentaddr' => new sfValidatorString(array('required' => true, 'max_length' => 255, 'trim' => true)),
            'permanentprovince' => new sfValidatorChoice(array('required' => true, 'choices' => array_keys($this->getProvinsiList()))),
            'permanentcity' => new sfValidatorString(array('required' => true, 'max_length' => 255, 'trim' => true)),
            'permanentkecamatan' => new sfValidatorString(array('required' => true, 'max_length' => 100, 'trim' => true)),
            'permanentkelurahan' => new sfValidatorString(array('required' => true, 'max_length' => 100, 'trim' => true)),
            'permanentzip' => new sfValidatorString(array('required' => true, 'max_length' => 10, 'trim' => true)),
            'religion' => new sfValidatorChoice(array('required' => true, 'choices' => array_keys($this->getAgamaList()))),
            'marital' => new sfValidatorString(array('required' => true, 'max_length' => 45, 'trim' => true)),
                //new sfValidatorChoice(array('required' => true, 'choices' => array('' => "-- " . __('Select') . " --", 'Single' => __('Single'), 'Married' => __('Married')))),
            'phone' => new sfValidatorString(array('required' => true, 'max_length' => 45, 'trim' => true)),
            'mobile' => new sfValidatorString(array('required' => true, 'max_length' => 45, 'trim' => true)),
            'ektp' => new sfValidatorString(array('required' => true, 'max_length' => 45, 'trim' => true)),
            'elementary' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'elementary_startyear' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'elementary_endyear' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'juniorhigh' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'juniorhigh_startyear' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'juniorhigh_endyear' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'seniorhigh' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'seniorhigh_startyear' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'seniorhigh_endyear' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'associate' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'associate_startyear' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'associate_endyear' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'bachelor' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'bachelor_startyear' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'bachelor_endyear' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'master' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'master_startyear' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'master_endyear' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'doctoral' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'doctoral_startyear' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'doctoral_endyear' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'graduatecertificate' => new sfValidatorFile(array('required' => false, 'max_size' => 1024000, 'validated_file_class' => 'orangehrmValidatedFile')),
            'transcript' => new sfValidatorFile(array('required' => false, 'max_size' => 1024000, 'validated_file_class' => 'orangehrmValidatedFile')),
            'company1' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'position1' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'start1' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'end1' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'jobdesc1' => new sfValidatorString(array('required' => false, 'max_length' => 41000)),
            'refname1' => new sfValidatorString(array('required' => false, 'max_length' => 200, 'trim' => true)),
            'reftelp1' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'company2' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'position2' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'start2' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'end2' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'jobdesc2' => new sfValidatorString(array('required' => false, 'max_length' => 41000)),
            'refname2' => new sfValidatorString(array('required' => false, 'max_length' => 200, 'trim' => true)),
            'reftelp2' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'company3' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'position3' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'start3' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'end3' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'jobdesc3' => new sfValidatorString(array('required' => false, 'max_length' => 41000)),
            'refname3' => new sfValidatorString(array('required' => false, 'max_length' => 200, 'trim' => true)),
            'reftelp3' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'company4' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'position4' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'start4' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'end4' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'jobdesc4' => new sfValidatorString(array('required' => false, 'max_length' => 41000)),
            'refname4' => new sfValidatorString(array('required' => false, 'max_length' => 200, 'trim' => true)),
            'reftelp4' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'company5' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'position5' => new sfValidatorString(array('required' => false, 'max_length' => 255, 'trim' => true)),
            'start5' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'end5' => new sfValidatorChoice(array('required' => false, 'choices' => array_keys($this->getYearRange(1980, $endYear)))),
            'jobdesc5' => new sfValidatorString(array('required' => false, 'max_length' => 41000)),
            'refname5' => new sfValidatorString(array('required' => false, 'max_length' => 200, 'trim' => true)),
            'reftelp5' => new sfValidatorString(array('required' => false, 'max_length' => 45, 'trim' => true)),
            'refletter' => new sfValidatorFile(array('required' => false, 'max_size' => 1024000, 'validated_file_class' => 'orangehrmValidatedFile')),
            'currentsalary' => new sfValidatorInteger(array('required' => false, 'min' => 0)),//new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'wantsalary' => new sfValidatorInteger(array('required' => true, 'min' => 0)),//new sfValidatorString(array('required' => true, 'max_length' => 100, 'trim' => true)),
            'prefworkplace' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'doctor_str' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'doctor_strissued' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false), array('invalid' => 'Date format should be ' . $inputDatePattern)),
            'doctor_strexpire' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false), array('invalid' => 'Date format should be ' . $inputDatePattern)),
            'dstrfile' => new sfValidatorFile(array('required' => false, 'max_size' => 1024000, 'validated_file_class' => 'orangehrmValidatedFile')),
            'doctor_idicard' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'doctor_idicardexpire' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false), array('invalid' => 'Date format should be ' . $inputDatePattern)),
            'idicardfile' => new sfValidatorFile(array('required' => false, 'max_size' => 1024000, 'validated_file_class' => 'orangehrmValidatedFile')),
            'doctor_hiperkes' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'dhiperkesfile' => new sfValidatorFile(array('required' => false, 'max_size' => 1024000, 'validated_file_class' => 'orangehrmValidatedFile')),
            'doctor_acls' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'doctor_aclsissued' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false), array('invalid' => 'Date format should be ' . $inputDatePattern)),
            'doctor_aclsexpire' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false), array('invalid' => 'Date format should be ' . $inputDatePattern)),
            'aclsfile' => new sfValidatorFile(array('required' => false, 'max_size' => 1024000, 'validated_file_class' => 'orangehrmValidatedFile')),
            'doctor_atls' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'doctor_atlsissued' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false), array('invalid' => 'Date format should be ' . $inputDatePattern)),
            'doctor_atlsexpire' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false), array('invalid' => 'Date format should be ' . $inputDatePattern)),
            'atlsfile' => new sfValidatorFile(array('required' => false, 'max_size' => 1024000, 'validated_file_class' => 'orangehrmValidatedFile')),
            'nurse_str' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'nurse_strissued' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false), array('invalid' => 'Date format should be ' . $inputDatePattern)),
            'nurse_strexpire' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false), array('invalid' => 'Date format should be ' . $inputDatePattern)),
            'nstrfile' => new sfValidatorFile(array('required' => false, 'max_size' => 1024000, 'validated_file_class' => 'orangehrmValidatedFile')),
            'nurse_ppni' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'nurse_ppniexpire' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false), array('invalid' => 'Date format should be ' . $inputDatePattern)),
            'ppnifile' => new sfValidatorFile(array('required' => false, 'max_size' => 1024000, 'validated_file_class' => 'orangehrmValidatedFile')),
            'nurse_hiperkes' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'nhiperkesfile' => new sfValidatorFile(array('required' => false, 'max_size' => 1024000, 'validated_file_class' => 'orangehrmValidatedFile')),
            'nurse_ppgd' => new sfValidatorString(array('required' => false, 'max_length' => 100, 'trim' => true)),
            'nurse_ppgdissued' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false), array('invalid' => 'Date format should be ' . $inputDatePattern)),
            'nurse_ppgdexpire' => new ohrmDateValidator(array('date_format' => $inputDatePattern, 'required' => false), array('invalid' => 'Date format should be ' . $inputDatePattern)),
            'ppgdfile' => new sfValidatorFile(array('required' => false, 'max_size' => 1024000, 'validated_file_class' => 'orangehrmValidatedFile')),
            'atlsexpire' => new sfValidatorString(array('required' => false)),
            'aclsexpire' => new sfValidatorString(array('required' => false)),
            'ppgdexpire' => new sfValidatorString(array('required' => false)),
            'associate_desc' => new sfValidatorString(array('required' => false, 'max_length' => 41000)),
            'bachelor_desc' => new sfValidatorString(array('required' => false, 'max_length' => 41000)),
            'master_desc' => new sfValidatorString(array('required' => false, 'max_length' => 41000)),
            'doctoral_desc' => new sfValidatorString(array('required' => false, 'max_length' => 41000)),
        ));

        $this->widgetSchema->setNameFormat('addCandidate[%s]');

        //Eka: add set default start from gender
        if (!empty($this->candidateId)) {
            $candidate = $this->getCandidateService()->getCandidateById($this->candidateId);
            $this->setDefault('firstName', $candidate->getFirstName());
            $this->setDefault('middleName', $candidate->getMiddleName());
            $this->setDefault('lastName', $candidate->getLastName());
            $this->setDefault('email', $candidate->getEmail());
            $this->setDefault('contactNo', $candidate->getContactNumber());
            $this->attachment = $candidate->getJobCandidateAttachment();
            //Eka: set photo handler to JobCandidateAttachment object type
            $this->photo = $candidate->getJobCandidateAttachment();
            $this->setDefault('keyWords', $candidate->getKeywords());
            $this->setDefault('comment', $candidate->getComment());
            $candidateVacancyList = $candidate->getJobCandidateVacancy();
            $vacancyList = array();
            foreach ($candidateVacancyList as $candidateVacancy) {
                $vacancyList[] = $candidateVacancy->getVacancyId();
            }
            $this->setDefault('vacancyList', implode("_", $vacancyList));

            $this->setDefault('gender', $candidate->getGender());
            $this->setDefault('birthplace', $candidate->getBirthplace());
            $this->setDefault('dob', $candidate->getDob());
            $this->setDefault('currentaddr', $candidate->getCurrentaddr());
            $this->setDefault('currentprovince', $candidate->getCurrentprovince());
            $this->setDefault('currentcity', $candidate->getCurrentcity());
            $this->setDefault('currentkecamatan', $candidate->getCurrentkecamatan());
            $this->setDefault('currentkelurahan', $candidate->getCurrentkelurahan());
            $this->setDefault('currentzip', $candidate->getCurrentzip());
            $this->setDefault('permanentaddr', $candidate->getPermanentaddr());
            $this->setDefault('permanentprovince', $candidate->getPermanentprovince());
            $this->setDefault('permanentcity', $candidate->getPermanentcity());
            $this->setDefault('permanentkecamatan', $candidate->getPermanentkecamatan());
            $this->setDefault('permanentkelurahan', $candidate->getPermanentkelurahan());
            $this->setDefault('permanentzip', $candidate->getPermanentzip());
            $this->setDefault('religion', $candidate->getReligion());
            $this->setDefault('marital', $candidate->getMarital());
            $this->setDefault('phone', $candidate->getPhone());
            $this->setDefault('mobile', $candidate->getMobile());
            $this->setDefault('ektp', $candidate->getEktp());
            $this->setDefault('elementary', $candidate->getElementary());
            $this->setDefault('elementary_startyear', $candidate->getElementaryStartyear());
            $this->setDefault('elementary_endyear', $candidate->getElementaryEndyear());
            $this->setDefault('juniorhigh', $candidate->getJuniorhigh());
            $this->setDefault('juniorhigh_startyear', $candidate->getJuniorhighStartyear());
            $this->setDefault('juniorhigh_endyear', $candidate->getJuniorhighEndyear());
            $this->setDefault('seniorhigh', $candidate->getSeniorhigh());
            $this->setDefault('seniorhigh_startyear', $candidate->getSeniorhighStartyear());
            $this->setDefault('seniorhigh_endyear', $candidate->getSeniorhighEndyear());
            $this->setDefault('associate', $candidate->getAssociate());
            $this->setDefault('associate_startyear', $candidate->getAssociateStartyear());
            $this->setDefault('associate_endyear', $candidate->getAssociateEndyear());
            $this->setDefault('bachelor', $candidate->getBachelor());
            $this->setDefault('bachelor_startyear', $candidate->getBachelorStartyear());
            $this->setDefault('bachelor_endyear', $candidate->getBachelorEndyear());
            $this->setDefault('master', $candidate->getMaster());
            $this->setDefault('master_startyear', $candidate->getMasterStartyear());
            $this->setDefault('master_endyear', $candidate->getMasterEndyear());
            $this->setDefault('doctoral', $candidate->getDoctoral());
            $this->setDefault('doctoral_startyear', $candidate->getDoctoralStartyear());
            $this->setDefault('doctoral_endyear', $candidate->getDoctoralEndyear());
            $this->graduatecertificate = $candidate->getJobCandidateAttachment();
            $this->transcript = $candidate->getJobCandidateAttachment();
            $this->setDefault('company1', $candidate->getCompany1());
            $this->setDefault('position1', $candidate->getPosition1());
            $this->setDefault('start1', $candidate->getStart1());
            $this->setDefault('end1', $candidate->getEnd1());
            $this->setDefault('jobdesc1', $candidate->getJobdesc1());
            $this->setDefault('refname1', $candidate->getRefname1());
            $this->setDefault('reftelp1', $candidate->getReftelp1());
            $this->setDefault('company2', $candidate->getCompany2());
            $this->setDefault('position2', $candidate->getPosition2());
            $this->setDefault('start2', $candidate->getStart2());
            $this->setDefault('end2', $candidate->getEnd2());
            $this->setDefault('jobdesc2', $candidate->getJobdesc2());
            $this->setDefault('refname2', $candidate->getRefname2());
            $this->setDefault('reftelp2', $candidate->getReftelp2());
            $this->setDefault('company3', $candidate->getCompany3());
            $this->setDefault('position3', $candidate->getPosition3());
            $this->setDefault('start3', $candidate->getStart3());
            $this->setDefault('end3', $candidate->getEnd3());
            $this->setDefault('jobdesc3', $candidate->getJobdesc3());
            $this->setDefault('refname3', $candidate->getRefname3());
            $this->setDefault('reftelp3', $candidate->getReftelp3());
            $this->setDefault('company4', $candidate->getCompany4());
            $this->setDefault('position4', $candidate->getPosition4());
            $this->setDefault('start4', $candidate->getStart4());
            $this->setDefault('end4', $candidate->getEnd4());
            $this->setDefault('jobdesc4', $candidate->getJobdesc4());
            $this->setDefault('refname4', $candidate->getRefname4());
            $this->setDefault('reftelp4', $candidate->getReftelp4());
            $this->setDefault('company5', $candidate->getCompany5());
            $this->setDefault('position5', $candidate->getPosition5());
            $this->setDefault('start5', $candidate->getStart5());
            $this->setDefault('end5', $candidate->getEnd5());
            $this->setDefault('jobdesc5', $candidate->getJobdesc5());
            $this->setDefault('refname5', $candidate->getRefname5());
            $this->setDefault('reftelp5', $candidate->getReftelp5());
            $this->refletter = $candidate->getJobCandidateAttachment();
            $this->setDefault('currentsalary', $candidate->getCurrentsalary());
            $this->setDefault('wantsalary', $candidate->getWantsalary());
            $this->setDefault('prefworkplace', $candidate->getPrefworkplace());
            $this->setDefault('doctor_str', $candidate->getDoctorStr());
            $this->setDefault('doctor_strissued', $candidate->getDoctorStrissued());
            $this->setDefault('doctor_strexpire', $candidate->getDoctorStrexpire());
            $this->dstrfile = $candidate->getJobCandidateAttachment();
            $this->setDefault('doctor_idicard', $candidate->getDoctorIdicard());
            $this->setDefault('doctor_idicardexpire', $candidate->getDoctorIdicardexpire());
            $this->idicardfile = $candidate->getJobCandidateAttachment();
            $this->setDefault('doctor_hiperkes', $candidate->getDoctorHiperkes());
            $this->dhiperkesfile = $candidate->getJobCandidateAttachment();
            $this->setDefault('doctor_acls', $candidate->getDoctorAcls());
            $this->setDefault('doctor_aclsissued', $candidate->getDoctorAclsissued());
            $this->setDefault('doctor_aclsexpire', $candidate->getDoctorAclsexpire());
            $this->aclsfile = $candidate->getJobCandidateAttachment();
            $this->setDefault('doctor_atls', $candidate->getDoctorAtls());
            $this->setDefault('doctor_atlsissued', $candidate->getDoctorAtlsissued());
            $this->setDefault('doctor_atlsexpire', $candidate->getDoctorAtlsexpire());
            $this->atlsfile = $candidate->getJobCandidateAttachment();
            $this->setDefault('nurse_str', $candidate->getNurseStr());
            $this->setDefault('nurse_strissued', $candidate->getNurseStrissued());
            $this->setDefault('nurse_strexpire', $candidate->getNurseStrexpire());
            $this->nstrfile = $candidate->getJobCandidateAttachment();
            $this->setDefault('nurse_ppni', $candidate->getNursePpni());
            $this->setDefault('nurse_ppniexpire', $candidate->getNursePpniexpire());
            $this->ppnifile = $candidate->getJobCandidateAttachment();
            $this->setDefault('nurse_hiperkes', $candidate->getNurseHiperkes());
            $this->nhiperkesfile = $candidate->getJobCandidateAttachment();
            $this->setDefault('nurse_ppgd', $candidate->getNursePpgd());
            $this->setDefault('nurse_ppgdissued', $candidate->getNursePpgdissued());
            $this->setDefault('nurse_ppgdexpire', $candidate->getNursePpgdexpire());
            $this->ppgdfile = $candidate->getJobCandidateAttachment();
            $this->setDefault('atlsexpire', $candidate->getAtlsexpire());
            $this->setDefault('aclsexpire', $candidate->getAclsexpire());
            $this->setDefault('ppgdexpire', $candidate->getPpgdexpire());
            $this->setDefault('associate_desc', $candidate->getAssociateDesc());
            $this->setDefault('bachelor_desc', $candidate->getBachelorDesc());
            $this->setDefault('master_desc', $candidate->getMasterDesc());
            $this->setDefault('doctoral_desc', $candidate->getDoctoralDesc());
        }
    }

    public function save() {

        $file = $this->getValue('resume');
        //eka: create file array
        $fileArray = array();
        //Eka: get photo file
        $photoFile = $this->getValue('photo');
        //Eka: non-mandatory file. Put into array for checking
        $graduatecertificateFile = $this->getValue('graduatecertificate');
        $fileArray[0] = $graduatecertificateFile;
        $refletterFile = $this->getValue('refletter');
        $fileArray[1] = $refletterFile;
        $dstrFile = $this->getValue('dstrfile');
        $fileArray[2] = $dstrFile;
        $idicardFile = $this->getValue('idifile');
        $fileArray[3] = $idicardFile;
        $dhiperkesFile = $this->getValue('dhiperkesfile');
        $fileArray[4] = $dhiperkesFile;
        $aclsFile = $this->getValue('aclsfile');
        $fileArray[5] = $aclsFile;
        $atlsFile = $this->getValue('atlsfile');
        $fileArray[6] = $atlsFile;
        $nstrFile = $this->getValue('nstrfile');
        $fileArray[7] = $nstrFile;
        $ppniFile = $this->getValue('ppnifile');
        $fileArray[8] = $ppniFile;
        $nhiperkesFile = $this->getValue('nhiperkesfile');
        $fileArray[9] = $nhiperkesFile;
        $ppgdFile = $this->getValue('ppgdfile');
        $fileArray[10] = $ppgdFile;

        $transcriptFile = $this->getValue('transcript');
        $transcriptObj = new JobCandidateAttachment();

        $resume = new JobCandidateAttachment();

        //Eka: photo object
        $photoObj = new JobCandidateAttachment();
        /*
        $graduatecertificateObj = new JobCandidateAttachment();
        $refletterObj = new JobCandidateAttachment();
        $dstrObj = new JobCandidateAttachment();
        $idicardObj = new JobCandidateAttachment();
        $dhiperkesObj = new JobCandidateAttachment();
        $aclsObj = new JobCandidateAttachment();
        $atlsObj = new JobCandidateAttachment();
        $nstrObj = new JobCandidateAttachment();
        $ppniObj = new JobCandidateAttachment();
        $nhiperkesObj = new JobCandidateAttachment();
        $ppgdObj = new JobCandidateAttachment();
        */

        $candidate = new JobCandidate();
        //Eka: get candidateId is moved here as nothing to do with attachment file being valid
        $this->candidateId = $this->_getNewlySavedCandidateId($candidate);

        $vacnacyId = $this->getValue('vacancyList');
        $resultArray = array();

        if (!($this->isValidResume($file))) {

            $resultArray['messageType'] = 'warning';
            $resultArray['message'] = __(TopLevelMessages::FILE_TYPE_SAVE_FAILURE);
            return $resultArray;
        } else {
            $resumeId = $this->_saveResume($file, $resume, $this->candidateId);
        }

        //Eka: checking and saving photo
        //using isValidResume for mandatory field
        if (!($this->isValidResume($photoFile))) {

            $resultArray['messageType'] = 'warning';
            $resultArray['message'] = __(TopLevelMessages::FILE_TYPE_SAVE_FAILURE);
            return $resultArray;
        } else {
            //$this->candidateId = $this->_getNewlySavedCandidateId($candidate);
            //$resultArray['candidateId'] = $this->candidateId;
            $photoId = $this->_savePhoto($photoFile, $photoObj, $this->candidateId);
        }

        //special for transcript
        if($transcriptFile != null){
            if(!($this->isValidAttachment($transcriptFile))){
                $resultArray['messageType'] = 'warning';
                $resultArray['message'] = __(TopLevelMessages::FILE_TYPE_SAVE_FAILURE);
                return $resultArray;
            } else {
                $transcriptId = $this->_saveAttachment($transcriptFile, $transcriptObj, $this->candidateId, 11, $this::TRANSCRIPT_FILE);
            }
        }

        //eka: file checking and saving for non mandatory file fields
        for($i = 0;$i < sizeof($fileArray);$i++){
            if($fileArray[$i] != null){
                if (!($this->isValidAttachment($fileArray[$i]))) {

                    $resultArray['messageType'] = 'warning';
                    $resultArray['message'] = __(TopLevelMessages::FILE_TYPE_SAVE_FAILURE . ' at index: ' . $i);
                    return $resultArray;
                } else {
                    //$this->candidateId = $this->_getNewlySavedCandidateId($candidate);
                    //$resultArray['candidateId'] = $this->candidateId;
                    $fileObj = new JobCandidateAttachment();
                    $attachmentFileId = $this->_saveAttachment($fileArray[$i], $fileObj, $this->candidateId, $i, $i + 3);
                }
            }
        }

        /*
        if (!($this->isValidAttachment($graduatecertificateFile))) {

            $resultArray['messageType'] = 'warning';
            $resultArray['message'] = __(TopLevelMessages::FILE_TYPE_SAVE_FAILURE);
            return $resultArray;
        } else {
            $this->candidateId = $this->_getNewlySavedCandidateId($candidate);
            $resultArray['candidateId'] = $this->candidateId;
            $graduatecertificateId = $this->_saveAttachment($graduatecertificateFile, $graduatecertificateObj, $this->candidateId, 'graduatecertificate');
        }

        if (!($this->isValidAttachment($refletterFile))) {

            $resultArray['messageType'] = 'warning';
            $resultArray['message'] = __(TopLevelMessages::FILE_TYPE_SAVE_FAILURE);
            return $resultArray;
        } else {
            $this->candidateId = $this->_getNewlySavedCandidateId($candidate);
            $resultArray['candidateId'] = $this->candidateId;
            $refletterId = $this->_saveAttachment($refletterFile, $refletterObj, $this->candidateId, 'refletter');
        }

        if (!($this->isValidAttachment($dstrFile))) {

            $resultArray['messageType'] = 'warning';
            $resultArray['message'] = __(TopLevelMessages::FILE_TYPE_SAVE_FAILURE);
            return $resultArray;
        } else {
            $this->candidateId = $this->_getNewlySavedCandidateId($candidate);
            $resultArray['candidateId'] = $this->candidateId;
            $dstrfileId = $this->_saveAttachment($dstrFile, $dstrObj, $this->candidateId, 'dstrfile');
        }

        if (!($this->isValidAttachment($idicardFile))) {

            $resultArray['messageType'] = 'warning';
            $resultArray['message'] = __(TopLevelMessages::FILE_TYPE_SAVE_FAILURE);
            return $resultArray;
        } else {
            $this->candidateId = $this->_getNewlySavedCandidateId($candidate);
            $resultArray['candidateId'] = $this->candidateId;
            $idicardfileId = $this->_saveAttachment($idicardFile, $idicardObj, $this->candidateId, 'idicardfile');
        }

        if (!($this->isValidAttachment($dhiperkesFile))) {

            $resultArray['messageType'] = 'warning';
            $resultArray['message'] = __(TopLevelMessages::FILE_TYPE_SAVE_FAILURE);
            return $resultArray;
        } else {
            $this->candidateId = $this->_getNewlySavedCandidateId($candidate);
            $resultArray['candidateId'] = $this->candidateId;
            $dhiperkesfileId = $this->_saveAttachment($dhiperkesFile, $dhiperkesObj, $this->candidateId, 'dhiperkesfile');
        }

        if (!($this->isValidAttachment($aclsFile))) {

            $resultArray['messageType'] = 'warning';
            $resultArray['message'] = __(TopLevelMessages::FILE_TYPE_SAVE_FAILURE);
            return $resultArray;
        } else {
            $this->candidateId = $this->_getNewlySavedCandidateId($candidate);
            $resultArray['candidateId'] = $this->candidateId;
            $aclsfileId = $this->_saveAttachment($aclsFile, $aclsObj, $this->candidateId, 'aclsfile');
        }

        if (!($this->isValidAttachment($atlsFile))) {

            $resultArray['messageType'] = 'warning';
            $resultArray['message'] = __(TopLevelMessages::FILE_TYPE_SAVE_FAILURE);
            return $resultArray;
        } else {
            $this->candidateId = $this->_getNewlySavedCandidateId($candidate);
            $resultArray['candidateId'] = $this->candidateId;
            $atlsfileId = $this->_saveAttachment($atlsFile, $atlsObj, $this->candidateId, 'atlsfile');
        }

        if (!($this->isValidAttachment($nstrFile))) {

            $resultArray['messageType'] = 'warning';
            $resultArray['message'] = __(TopLevelMessages::FILE_TYPE_SAVE_FAILURE);
            return $resultArray;
        } else {
            $this->candidateId = $this->_getNewlySavedCandidateId($candidate);
            $resultArray['candidateId'] = $this->candidateId;
            $nstrfileId = $this->_saveAttachment($nstrFile, $nstrObj, $this->candidateId, 'nstrfile');
        }

        if (!($this->isValidAttachment($ppniFile))) {

            $resultArray['messageType'] = 'warning';
            $resultArray['message'] = __(TopLevelMessages::FILE_TYPE_SAVE_FAILURE);
            return $resultArray;
        } else {
            $this->candidateId = $this->_getNewlySavedCandidateId($candidate);
            $resultArray['candidateId'] = $this->candidateId;
            $ppnifileId = $this->_saveAttachment($ppniFile, $ppniObj, $this->candidateId, 'ppnifile');
        }

        if (!($this->isValidAttachment($nhiperkesFile))) {

            $resultArray['messageType'] = 'warning';
            $resultArray['message'] = __(TopLevelMessages::FILE_TYPE_SAVE_FAILURE);
            return $resultArray;
        } else {
            $this->candidateId = $this->_getNewlySavedCandidateId($candidate);
            $resultArray['candidateId'] = $this->candidateId;
            $nhiperkesfileId = $this->_saveAttachment($nhiperkesFile, $nhiperkesObj, $this->candidateId, 'nhiperkesfile');
        }

        if (!($this->isValidAttachment($ppgdFile))) {

            $resultArray['messageType'] = 'warning';
            $resultArray['message'] = __(TopLevelMessages::FILE_TYPE_SAVE_FAILURE);
            return $resultArray;
        } else {
            $this->candidateId = $this->_getNewlySavedCandidateId($candidate);
            $resultArray['candidateId'] = $this->candidateId;
            $ppgdfileId = $this->_saveAttachment($ppgdFile, $ppgdObj, $this->candidateId, 'ppgdfile');
        }
        */

        $candidateVacancyId = $this->_saveCandidateVacancies($vacnacyId, $this->candidateId);
        //Eka: moving resultArray assignment here
        $resultArray['candidateId'] = $this->candidateId;
        $resultArray['candidateVacancyId'] = $candidateVacancyId;
        return $resultArray;
    }

    /**
     *
     * @param <type> $candidate
     * @return <type>
     */
    private function _getNewlySavedCandidateId($candidate) {
        $candidate->firstName = $this->getValue('firstName');
        $candidate->middleName = $this->getValue('middleName');
        $candidate->lastName = $this->getValue('lastName');
        $candidate->email = $this->getValue('email');
        $candidate->comment = $this->getValue('comment');
        $candidate->contactNumber = $this->getValue('contactNo');
        $candidate->keywords = $this->getValue('keyWords');
        $date = date('Y-m-d');
        $candidate->dateOfApplication = $date . " " . date('H:i:s');
        $candidate->status = JobCandidate::ACTIVE;
        $candidate->modeOfApplication = JobCandidate::MODE_OF_APPLICATION_ONLINE;

        //Eka: add set value from gender onward
        $candidate->gender = $this->getValue('gender');
        $candidate->birthplace = $this->getValue('birthplace');
        $candidate->dob = $this->getValue('dob');
        $candidate->currentaddr = $this->getValue('currentaddr');
        $candidate->currentprovince = $this->getValue('currentprovince');
        $candidate->currentcity = $this->getValue('currentcity');
        $candidate->currentkecamatan = $this->getValue('currentkecamatan');
        $candidate->currentkelurahan = $this->getValue('currentkelurahan');
        $candidate->currentzip = $this->getValue('currentzip');
        $candidate->permanentaddr = $this->getValue('permanentaddr');
        $candidate->permanentprovince = $this->getValue('permanentprovince');
        $candidate->permanentcity = $this->getValue('permanentcity');
        $candidate->permanentkecamatan = $this->getValue('permanentkecamatan');
        $candidate->permanentkelurahan = $this->getValue('permanentkelurahan');
        $candidate->permanentzip = $this->getValue('permanentzip');
        $candidate->religion = $this->getValue('religion');
        $candidate->marital = $this->getValue('marital');
        $candidate->phone = $this->getValue('phone');
        $candidate->mobile = $this->getValue('mobile');
        $candidate->ektp = $this->getValue('ektp');
        $candidate->elementary = $this->getValue('elementary');
        $candidate->elementary_startyear = $this->getValue('elementary_startyear');
        $candidate->elementary_endyear = $this->getValue('elementary_endyear');
        $candidate->juniorhigh = $this->getValue('juniorhigh');
        $candidate->juniorhigh_startyear = $this->getValue('juniorhigh_startyear');
        $candidate->juniorhigh_endyear = $this->getValue('juniorhigh_endyear');
        $candidate->seniorhigh = $this->getValue('seniorhigh');
        $candidate->seniorhigh_startyear = $this->getValue('seniorhigh_startyear');
        $candidate->seniorhigh_endyear = $this->getValue('seniorhigh_endyear');
        $candidate->associate = $this->getValue('associate');
        $candidate->associate_startyear = $this->getValue('associate_startyear');
        $candidate->associate_endyear = $this->getValue('associate_endyear');
        $candidate->bachelor = $this->getValue('bachelor');
        $candidate->bachelor_startyear = $this->getValue('bachelor_startyear');
        $candidate->bachelor_endyear = $this->getValue('bachelor_endyear');
        $candidate->master = $this->getValue('master');
        $candidate->master_startyear = $this->getValue('master_startyear');
        $candidate->master_endyear = $this->getValue('master_endyear');
        $candidate->doctoral = $this->getValue('doctoral');
        $candidate->doctoral_startyear = $this->getValue('doctoral_startyear');
        $candidate->doctoral_endyear = $this->getValue('doctoral_endyear');
        $candidate->company1 = $this->getValue('company1');
        $candidate->position1 = $this->getValue('position1');
        $candidate->start1 = $this->getValue('start1');
        $candidate->end1 = $this->getValue('end1');
        $candidate->jobdesc1 = $this->getValue('jobdesc1');
        $candidate->refname1 = $this->getValue('refname1');
        $candidate->reftelp1 = $this->getValue('reftelp1');
        $candidate->company2 = $this->getValue('company2');
        $candidate->position2 = $this->getValue('position2');
        $candidate->start2 = $this->getValue('start2');
        $candidate->end2 = $this->getValue('end2');
        $candidate->jobdesc2 = $this->getValue('jobdesc2');
        $candidate->refname2 = $this->getValue('refname2');
        $candidate->reftelp2 = $this->getValue('reftelp2');
        $candidate->company3 = $this->getValue('company3');
        $candidate->position3 = $this->getValue('position3');
        $candidate->start3 = $this->getValue('start3');
        $candidate->end3 = $this->getValue('end3');
        $candidate->jobdesc3 = $this->getValue('jobdesc3');
        $candidate->refname3 = $this->getValue('refname3');
        $candidate->reftelp3 = $this->getValue('reftelp3');
        $candidate->company4 = $this->getValue('company4');
        $candidate->position4 = $this->getValue('position4');
        $candidate->start4 = $this->getValue('start4');
        $candidate->end4 = $this->getValue('end4');
        $candidate->jobdesc4 = $this->getValue('jobdesc4');
        $candidate->refname4 = $this->getValue('refname4');
        $candidate->reftelp4 = $this->getValue('reftelp4');
        $candidate->company5 = $this->getValue('company5');
        $candidate->position5 = $this->getValue('position5');
        $candidate->start5 = $this->getValue('start5');
        $candidate->end5 = $this->getValue('end5');
        $candidate->jobdesc5 = $this->getValue('jobdesc5');
        $candidate->refname5 = $this->getValue('refname5');
        $candidate->reftelp5 = $this->getValue('reftelp5');
        $candidate->currentsalary = $this->getValue('currentsalary');
        $candidate->wantsalary = $this->getValue('wantsalary');
        $candidate->prefworkplace = $this->getValue('prefworkplace');
        $candidate->doctor_str = $this->getValue('doctor_str');
        $candidate->doctor_strissued = $this->getValue('doctor_strissued');
        $candidate->doctor_strexpire = $this->getValue('doctor_strexpire');
        $candidate->doctor_idicard = $this->getValue('doctor_idicard');
        $candidate->doctor_idicardexpire = $this->getValue('doctor_idicardexpire');
        $candidate->doctor_hiperkes = $this->getValue('doctor_hiperkes');
        $candidate->doctor_acls = $this->getValue('doctor_acls');
        $candidate->doctor_aclsissued = $this->getValue('doctor_aclsissued');
        $candidate->doctor_aclsexpire = $this->getValue('doctor_aclsexpire');
        $candidate->doctor_atls = $this->getValue('doctor_atls');
        $candidate->doctor_atlsissued = $this->getValue('doctor_atlsissued');
        $candidate->doctor_atlsexpire = $this->getValue('doctor_atlsexpire');
        $candidate->nurse_str = $this->getValue('nurse_str');
        $candidate->nurse_strissued = $this->getValue('nurse_strissued');
        $candidate->nurse_strexpire = $this->getValue('nurse_strexpire');
        $candidate->nurse_ppni = $this->getValue('nurse_ppni');
        $candidate->nurse_ppniexpire = $this->getValue('nurse_ppniexpire');
        $candidate->nurse_hiperkes = $this->getValue('nurse_hiperkes');
        $candidate->nurse_ppgd = $this->getValue('nurse_ppgd');
        $candidate->nurse_ppgdissued = $this->getValue('nurse_ppgdissued');
        $candidate->nurse_ppgdexpire = $this->getValue('nurse_ppgdexpire');
        $candidate->associate_desc = $this->getValue('associate_desc');
        $candidate->bachelor_desc = $this->getValue('bachelor_desc');
        $candidate->master_desc = $this->getValue('master_desc');
        $candidate->doctoral_desc = $this->getValue('doctoral_desc');

        if(empty($this->getValue('nurse_ppgd'))){
            if(strtotime($this->getValue('doctor_atlsexpire')) > strtotime("today"))
                $candidate->atlsexpire = 1;
            else
                $candidate->atlsexpire = 0;

            if(strtotime($this->getValue('doctor_aclsexpire')) > strtotime("today"))
                $candidate->aclsexpire = 1;
            else
                $candidate->aclsexpire = 0;
        } else {
            if(strtotime($this->getValue('doctor_atlsexpire')) > strtotime("today"))
                $candidate->ppgdexpire = 1;
            else
                $candidate->ppgdexpire = 0;
        }

        $candidateService = $this->getCandidateService();
        $candidateService->saveCandidate($candidate);
        $candidateId = $candidate->getId();

        return $candidateId;
    }

    /**
     *
     * @param sfValidatedFile $file
     * @return <type> 
     */
    protected function isValidResume($file) {

        $validFile = false;

        $mimeTypes = array_values($this->allowedFileTypes);
        $originalName = $file->getOriginalName();

        if (($file instanceof orangehrmValidatedFile) && $originalName != "") {

            $fileType = $file->getType();

            if (!empty($fileType) && in_array($fileType, $mimeTypes)) {
                $validFile = true;
            } else {
                $fileType = $this->guessTypeFromFileExtension($originalName);

                if (!empty($fileType)) {
                    $file->setType($fileType);
                    $validFile = true;
                }
            }
        }

        return $validFile;
    }

    //Eka: checking for valid file
    protected function isValidAttachment($file) {

        $validFile = false;

        $mimeTypes = array_values($this->allowedFileTypes);
        $originalName = $file->getOriginalName();

        if (($file instanceof orangehrmValidatedFile) && $originalName != "") {

            $fileType = $file->getType();

            if (!empty($fileType) && in_array($fileType, $mimeTypes)) {
                $validFile = true;
            } else {
                $fileType = $this->guessTypeFromFileExtension($originalName);

                if (!empty($fileType)) {
                    $file->setType($fileType);
                    $validFile = true;
                }
            }
        }

        return $validFile;
    }

    /**
     *
     * @param <type> $vacnacyId
     * @param <type> $candidateId 
     */
    protected function _saveCandidateVacancies($vacnacyId, $candidateId) {
        $candidateVacancyId = "";
        if (!empty($vacnacyId)) {

            $candidateVacancy = new JobCandidateVacancy();
            $candidateVacancy->candidateId = $candidateId;
            $candidateVacancy->vacancyId = $vacnacyId;
            $candidateVacancy->status = $this->getResultingStateForJobApplication();
            $candidateVacancy->appliedDate = date('Y-m-d h:i:sa');
            $candidateService = $this->getCandidateService();
            $candidateService->saveCandidateVacancy($candidateVacancy);
            $candidateVacancyId = $candidateVacancy->id;
            $history = new CandidateHistory();
            $history->candidateId = $candidateId;
            $history->action = CandidateHistory::RECRUITMENT_CANDIDATE_ACTION_APPLY;
            $history->performedDate = $candidateVacancy->appliedDate;
            $history->vacancyId = $vacnacyId;
            $history->candidateVacancyName = $candidateVacancy->getVacancyName();
            $this->getCandidateService()->saveCandidateHistory($history);
        }

        return $candidateVacancyId;
    }

    /**
     * Get the resulting state of a job application
     */
    protected function getResultingStateForJobApplication() {
            
        $resultingState = NULL;
        
        // Since job application is done by a non-logged in user, current workflow does not support configuring
        // this via the workflow state machine.
        // Therefore, we set the state to ADMIN workflow item for the INITIAL state.
        $workFlowItems = $this->getWorkflowService()->getAllowedWorkflowItems(WorkflowStateMachine::FLOW_RECRUITMENT, 'INITIAL', 'ADMIN');     

        if (count($workFlowItems) > 0) {
            $item = $workFlowItems[0];
            $resultingState = $item->getResultingState();
        }        
        
        if (is_null($resultingState)) {
            throw new RecruitmentExeption('No workflow items found for job vacancy INITIAL state for ADMIN');
        }
        return $resultingState;
    }

    /**
     *
     * @param <type> $file
     * @param <type> $resume
     * @param <type> $candidateId
     * @return <type>
     */
    private function _saveResume($file, $resume, $candidateId) {

        $tempName = $file->getTempName();
        $resume->fileContent = file_get_contents($tempName);
        $resume->fileName = $file->getOriginalName();
        $resume->fileType = $file->getType();
        $resume->fileSize = $file->getSize();
        $resume->fileSize = $file->getSize();
        $resume->candidateId = $candidateId;
        $resume->filetitle = ApplyVacancyForm::KTP_FILE;

        $recruitmentAttachmentService = $this->getRecruitmentAttachmentService();
        $recruitmentAttachmentService->saveCandidateAttachment($resume);

        $this->attachment = $resume;
    }

    private function _savePhoto($file, $photo, $candidateId) {

        $tempName = $file->getTempName();
        $photo->fileContent = file_get_contents($tempName);
        $photo->fileName = $file->getOriginalName();
        $photo->fileType = $file->getType();
        $photo->fileSize = $file->getSize();
        $photo->fileSize = $file->getSize();
        $photo->candidateId = $candidateId;
        $photo->filetitle = ApplyVacancyForm::PHOTO_FILE;

        $recruitmentAttachmentService = $this->getRecruitmentAttachmentService();
        $recruitmentAttachmentService->saveCandidateAttachment($photo);

        $this->photo = $photo;
    }

    private function _saveAttachment($file, $attachmentObj, $candidateId, $type, $filetitle) {

        $tempName = $file->getTempName();
        $attachmentObj->fileContent = file_get_contents($tempName);
        $attachmentObj->fileName = $file->getOriginalName();
        $attachmentObj->fileType = $file->getType();
        $attachmentObj->fileSize = $file->getSize();
        $attachmentObj->fileSize = $file->getSize();
        $attachmentObj->candidateId = $candidateId;
        $attachmentObj->filetitle = $filetitle;

        $recruitmentAttachmentService = $this->getRecruitmentAttachmentService();
        $recruitmentAttachmentService->saveCandidateAttachment($attachmentObj);
        //save attachment to local var based on type
        switch($type){
            /*case 'photo':
                $this->photo = $attachmentObj;
                break;*/
            //'graduatecertificate'
            case 0:
                $this->graduatecertificate = $attachmentObj;
                break;
            //'refletter'
            case 1:
                $this->refletter =  $attachmentObj;
                break;
            //'dstrfile'
            case 2:
                $this->dstrfile =  $attachmentObj;
                break;
            //'idicardfile'
            case 3:
                $this->idicardfile =  $attachmentObj;
                break;
            //'dhiperkesfile'
            case 4:
                $this->dhiperkesfile =  $attachmentObj;
                break;
            //'aclsfile'
            case 5:
                $this->aclsfile =  $attachmentObj;
                break;
            //'atlsfile'
            case 6:
                $this->atlsfile =  $attachmentObj;
                break;
            //'nstrfile'
            case 7:
                $this->nstrfile =  $attachmentObj;
                break;
            //'ppnifile'
            case 8:
                $this->ppnifile =  $attachmentObj;
                break;
            //'nhiperkesfile'
            case 9:
                $this->nhiperkesfile =  $attachmentObj;
                break;
            //'ppgdfile'
            case 10:
                $this->ppgdfile =  $attachmentObj;
                break;
            //'transcript'
            case 11:
                $this->transcript =  $attachmentObj;
                break;
            default:
        }
        //$this->attachment = $attachmentObj;
    }

    /**
     * Guess the file mime type from the file extension
     *
     * @param  string $file  The absolute path of a file
     *
     * @return string The mime type of the file (null if not guessable)
     */
    public function guessTypeFromFileExtension($file) {

        $mimeType = null;

        $extension = pathinfo($file, PATHINFO_EXTENSION);

        if (isset($this->allowedFileTypes[$extension])) {
            $mimeType = $this->allowedFileTypes[$extension];
        }

        return $mimeType;
    }

    /**
     *
     * @return JobCandidateAttachment
     */
    public function getResume() {
        return $this->attachment;
    }

    //Eka: function to get photo
    public function getPhoto() {
        return $this->photo;
    }

    public function getProvinsi(){
        $url = $this->nomornetHost . "/nomornet/provinsi";
        $client = curl_init($url);
        curl_setopt($client,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($client, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($client);
        $result = json_decode($response);
        return $result;
    }

    public function getKotakabupaten($provinsi){
        $url = $this->nomornetHost . "/nomornet/kotakabupaten/" . urlencode($provinsi);
        $client = curl_init($url);
        curl_setopt($client,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($client, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($client);
        $result = json_decode($response);
        return $result;
//        return "";
    }

    public function getKecamatan($provinsi, $kotaKabupaten){
        $url = $this->nomornetHost . "/nomornet/kecamatan/" . urlencode($provinsi) . "/" . urlencode($kotaKabupaten);
        $client = curl_init($url);
        curl_setopt($client,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($client, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($client);
        $result = json_decode($response);
        return $result;
//        return "";
    }

    public function getKodeposDesa($provinsi, $kotaKabupaten, $kecamatan){
        $url = $this->nomornetHost . "/nomornet/kodeposdesa/" . urlencode($provinsi) . "/" . urlencode($kotaKabupaten) . "/" . urlencode($kecamatan);
        $client = curl_init($url);
        curl_setopt($client,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($client, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($client);
        $result = json_decode($response);
        return $result;
//        return "";
    }
}
?>
