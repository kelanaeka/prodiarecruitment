<?php

/**
 * OrangeHRM is a comprehensive Human Resource Management (HRM) System that captures
 * all the essential functionalities required for any enterprise.
 * Copyright (C) 2006 OrangeHRM Inc., http://www.orangehrm.com
 *
 * OrangeHRM is free software; you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * OrangeHRM is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program;
 * if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA
 */

/**
 * CandidateDao for CRUD operation
 *
 */
class CandidateDao extends BaseDao {

    /**
     * Retrieve candidate by candidateId
     * @returns jobCandidate doctrine collection
     * @throws DaoException
     */
    public function getCandidates() {
        try {
            $q = Doctrine_Query :: create()
                ->from('JobCandidate jc');
            $q->addWhere('jc.status = ?', JobCandidate::ACTIVE);
            return $q->execute();
        } catch (Exception $e) {
            throw new DaoException($e->getMessage());
        }
    }

    /**
     * Retrieve candidate by candidateId
     * @param int $candidateId
     * @returns jobCandidate doctrine object
     * @throws DaoException
     */
    public function getCandidateById($candidateId) {
        try {
            return Doctrine :: getTable('JobCandidate')->find($candidateId);
        } catch (Exception $e) {
            throw new DaoException($e->getMessage());
        }
    }

    /**
     * Retrieve all candidates
     * @returns JobCandidate doctrine collection
     * @throws DaoException
     */
    public function getCandidateList($allowedCandidateList, $status = JobCandidate::ACTIVE) {
        try {
            $q = Doctrine_Query :: create()
                    ->from('JobCandidate jc');
            if ($allowedCandidateList != null) {
                $q->whereIn('jc.id', $allowedCandidateList);
            }
            if (!empty($status)) {
                $q->addWhere('jc.status = ?', $status);
            }
            return $q->execute();
        } catch (Exception $e) {
            throw new DaoException($e->getMessage());
        }
    }
    
    /**
     * Return an array of candidate names
     * 
     * @version 2.7.1
     * @param Array $allowedCandidateIdList Allowed candidate Id List
     * @param Integer $status Cadidate Status
     * @returns Array Candidate Name List
     * @throws DaoException
     */
    public function getCandidateNameList($allowedCandidateIdList, $status = JobCandidate::ACTIVE) {
        try {
            
            if (!empty($allowedCandidateIdList)) {
                
                $escapeString = implode(',', array_fill(0, count($allowedCandidateIdList), '?'));
                $pdo = Doctrine_Manager::connection()->getDbh();
                $q = "SELECT jc.first_name AS firstName, jc.middle_name AS middleName, jc.last_name AS lastName, jc.id
                		FROM ohrm_job_candidate jc
                		WHERE jc.id IN ({$escapeString}) AND
                		jc.status = ?";
                
                $escapeValueArray = array_values($allowedCandidateIdList);
                $escapeValueArray[] = $status;
                
                $query = $pdo->prepare($q); 
                $query->execute($escapeValueArray);
                $results = $query->fetchAll(PDO::FETCH_ASSOC);
            }
            
            return $results;
        
        // @codeCoverageIgnoreStart
        } catch (Exception $e) {
            throw new DaoException($e->getMessage(), $e->getCode(), $e);
        }
        // @codeCoverageIgnoreEnd
    }

    public function getCandidateListForUserRole($role, $empNumber) {

        try {
            $q = Doctrine_Query :: create()
                    ->select('jc.id')
                    ->from('JobCandidate jc');
            if ($role == HiringManagerUserRoleDecorator::HIRING_MANAGER) {
                $q->leftJoin('jc.JobCandidateVacancy jcv')
                        ->leftJoin('jcv.JobVacancy jv')
                        ->where('jv.hiringManagerId = ?', $empNumber)
                        ->orWhere('jc.id NOT IN (SELECT ojcv.candidateId FROM JobCandidateVacancy ojcv) AND jc.addedPerson = ?', $empNumber);
            }
            if ($role == InterviewerUserRoleDecorator::INTERVIEWER) {
                $q->leftJoin('jc.JobCandidateVacancy jcv')
                        ->leftJoin('jcv.JobInterview ji')
                        ->leftJoin('ji.JobInterviewInterviewer jii')
                        ->where('jii.interviewerId = ?', $empNumber);
            }
            $result = $q->fetchArray();
            $idList = array();
            foreach ($result as $item) {
                $idList[] = $item['id'];
            }
            return $idList;
        } catch (Exception $e) {
            throw new DaoException($e->getMessage());
        }
    }

    /**
     * Retriving candidates based on the search criteria
     * @param CandidateSearchParameters $searchParam
     * @return CandidateSearchParameters
     */
    public function searchCandidates($searchCandidateQuery) {

        try {
            $pdo = Doctrine_Manager::connection()->getDbh();
            $res = $pdo->query($searchCandidateQuery);

            $candidateList = $res->fetchAll();

            $candidatesList = array();
            foreach ($candidateList as $candidate) {

                $param = new CandidateSearchParameters();
                $param->setVacancyName($candidate['name']);
                $param->setVacancyStatus($candidate['vacancyStatus']);
                $param->setCandidateId($candidate['id']);
                $param->setVacancyId($candidate['vacancyId']);
                $param->setCandidateName($candidate['first_name'] . " " . $candidate['middle_name'] . " " . $candidate['last_name'] . $this->_getCandidateNameSuffix($candidate['candidateStatus']));
                $employeeName = $candidate['emp_firstname'] . " " . $candidate['emp_middle_name'] . " " . $candidate['emp_lastname'];
                $hmName = (!empty($candidate['termination_id'])) ? $employeeName." (".__("Past Employee").")" : $employeeName;
                $param->setHiringManagerName($hmName);
                $param->setDateOfApplication($candidate['date_of_application']);
                $param->setAttachmentId($candidate['attachmentId']);
                $param->setStatusName(ucwords(strtolower($candidate['status'])));
                $candidatesList[] = $param;
            }
            return $candidatesList;
        } catch (Exception $e) {
            throw new DaoException($e->getMessage());
        }
    }

    /**
     *
     * @param CandidateSearchParameters $searchParam
     * @return <type>
     */
    public function getCandidateRecordsCount($countQuery) {

        try {
            $pdo = Doctrine_Manager::connection()->getDbh();
            $res = $pdo->query($countQuery);
            $count = $res->fetch();
            return $count[0];
        } catch (Exception $e) {
            throw new DaoException($e->getMessage());
        }
    }

    /**
     *
     * @param JobCandidate $candidate
     * @return <type>
     */
    public function saveCandidate(JobCandidate $candidate) {
        try {
            if ($candidate->getId() == "") {
                $idGenService = new IDGeneratorService();
                $idGenService->setEntity($candidate);
                $candidate->setId($idGenService->getNextID());
            }
            $candidate->save();
            return true;
        } catch (Exception $e) {
            throw new DaoException($e->getMessage());
        }
    }

    /**
     *
     * @param JobCandidateVacancy $candidateVacancy
     * @return <type>
     */
    public function saveCandidateVacancy(JobCandidateVacancy $candidateVacancy) {
        try {
            if ($candidateVacancy->getId() == '') {
                $idGenService = new IDGeneratorService();
                $idGenService->setEntity($candidateVacancy);
                $candidateVacancy->setId($idGenService->getNextID());
            }
            $candidateVacancy->save();
            return true;
        } catch (Exception $e) {
            throw new DaoException($e->getMessage());
        }
    }

    public function getCandidateVacancyStatusesForCandidateId($candidateId){
        try{
            $pdo = Doctrine_Manager::connection()->getDbh();
            $query = "select cv.candidate_id, cv.status from 
                        ohrm_job_candidate_vacancy cv join 
                        ohrm_job_candidate cc
                      on cv.candidate_id = cc.id
                      where cc.ektp = 
                      (select ektp from ohrm_job_candidate where id=" . $candidateId . ")";
            $q = $pdo->prepare($query);
            $q->execute();
            $results = $q->fetchAll(PDO::FETCH_ASSOC);
            return $results;
        } catch (Exception $e) {
            throw new DaoException($e->getMessage());
        }
    }

    /**
     *
     * @param JobCandidate $candidate
     * @return <type>
     */
    public function updateCandidate(JobCandidate $candidate, $vacancyStatus) {

        try {
            $pdo = Doctrine_Manager::connection()->getDbh();
            $query = "UPDATE ohrm_job_candidate SET ";
                $query .= 'first_name=\'' . $candidate->firstName . '\',';
                $query .= 'middle_name=\'' . $candidate->middleName . '\',';
                $query .= 'last_name=\'' . $candidate->lastName . '\',';
                $query .= 'contact_number=\'' . $candidate->contactNumber . '\',';
                $query .= 'email=\'' . $candidate->email . '\',';
                $query .= 'keywords=\'' . $candidate->keywords . '\',';
                $query .= 'comment=\'' . $candidate->comment . '\',';
                $query .= 'date_of_application=\'' . $candidate->dateOfApplication . '\',';
                $query .= 'gender=\'' . $candidate->gender . '\',';
                $query .= 'birthplace=\'' . $candidate->birthplace . '\',';
                $query .= 'dob=\'' . $candidate->dob . '\',';
                $query .= 'currentaddr=\'' . $candidate->currentaddr . '\',';
                $query .= 'currentprovince=\'' . $candidate->currentprovince . '\',';
                $query .= 'currentcity=\'' . $candidate->currentcity . '\',';
                $query .= 'currentkecamatan=\'' . $candidate->currentkecamatan . '\',';
                $query .= 'currentkelurahan=\'' . $candidate->currentkelurahan . '\',';
                $query .= 'currentzip=\'' . $candidate->currentzip . '\',';
                $query .= 'permanentaddr=\'' . $candidate->permanentaddr . '\',';
                $query .= 'permanentprovince=\'' . $candidate->permanentprovince . '\',';
                $query .= 'permanentcity=\'' . $candidate->permanentcity . '\',';
                $query .= 'permanentkecamatan=\'' . $candidate->permanentkecamatan . '\',';
                $query .= 'permanentkelurahan=\'' . $candidate->permanentkelurahan . '\',';
                $query .= 'permanentzip=\'' . $candidate->permanentzip . '\',';
                $query .= 'religion=\'' . $candidate->religion . '\',';
                $query .= 'marital=\'' . $candidate->marital . '\',';
                $query .= 'phone=\'' . $candidate->phone . '\',';
                $query .= 'mobile=\'' . $candidate->mobile . '\',';
                $query .= 'ektp=\'' . $candidate->ektp . '\',';

                if(!empty($vacancyStatus) && $vacancyStatus != "APPLICATION INITIATED"){
                    //add detail info update
                    $query .= 'bloodtype=\'' . $candidate->bloodtype . '\',';
                        $query .= 'heght=\'' . $candidate->heght . '\',';
                        $query .= 'weight=\'' . $candidate->weight . '\',';
                        $query .= 'npwp=\'' . $candidate->npwp . '\',';
                        $query .= 'bpjsnaker=\'' . $candidate->bpjsnaker . '\',';
                        $query .= 'bpjskes=\'' . $candidate->bpjskes . '\',';
                        $query .= 'faskes=\'' . $candidate->faskes . '\',';
                        $query .= 'kk=\'' . $candidate->kk . '\',';
                        $query .= 'familyhead=\'' . $candidate->familyhead . '\',';
                        $query .= 'emergencyname=\'' . $candidate->emergencyname . '\',';
                        $query .= 'emergencyaddr=\'' . $candidate->emergencyaddr . '\',';
                        $query .= 'emergencyprovince=\'' . $candidate->emergencyprovince . '\',';
                        $query .= 'emergencycity=\'' . $candidate->emergencycity . '\',';
                        $query .= 'emergencykecamatan=\'' . $candidate->emergencykecamatan . '\',';
                        $query .= 'emergencykelurahan=\'' . $candidate->emergencykelurahan . '\',';
                        $query .= 'emergencyzip=\'' . $candidate->emergencyzip . '\',';
                        $query .= 'emergencyrelation=\'' . $candidate->emergencyrelation . '\',';
                        $query .= 'emergencyphone=\'' . $candidate->emergencyphone . '\',';
                }

                if(!empty($candidate->elementary))
                $query .= 'elementary=\'' . $candidate->elementary . '\',';
                if(!empty($candidate->elementary_startyear))
                $query .= 'elementary_startyear=\'' . $candidate->elementary_startyear . '\',';
                if(!empty($candidate->elementary_endyear))
                $query .= 'elementary_endyear=\'' . $candidate->elementary_endyear . '\',';
                if(!empty($candidate->juniorhigh))
                $query .= 'juniorhigh=\'' . $candidate->juniorhigh . '\',';
                if(!empty($candidate->juniorhigh_startyear))
                $query .= 'juniorhigh_startyear=\'' . $candidate->juniorhigh_startyear . '\',';
                if(!empty($candidate->juniorhigh_endyear))
                $query .= 'juniorhigh_endyear=\'' . $candidate->juniorhigh_endyear . '\',';
                if(!empty($candidate->seniorhigh))
                $query .= 'seniorhigh=\'' . $candidate->seniorhigh . '\',';
                if(!empty($candidate->seniorhigh_startyear))
                $query .= 'seniorhigh_startyear=\'' . $candidate->seniorhigh_startyear . '\',';
                if(!empty($candidate->seniorhigh_endyear))
                $query .= 'seniorhigh_endyear=\'' . $candidate->seniorhigh_endyear . '\',';
                if(!empty($candidate->associate))
                $query .= 'associate=\'' . $candidate->associate . '\',';
                if(!empty($candidate->associate_startyear))
                $query .= 'associate_startyear=\'' . $candidate->associate_startyear . '\',';
                if(!empty($candidate->associate_endyear))
                $query .= 'associate_endyear=\'' . $candidate->associate_endyear . '\',';
                if(!empty($candidate->bachelor))
                $query .= 'bachelor=\'' . $candidate->bachelor . '\',';
                if(!empty($candidate->bachelor_startyear))
                $query .= 'bachelor_startyear=\'' . $candidate->bachelor_startyear . '\',';
                if(!empty($candidate->bachelor_endyear))
                $query .= 'bachelor_endyear=\'' . $candidate->bachelor_endyear . '\',';
                if(!empty($candidate->master))
                $query .= 'master=\'' . $candidate->master . '\',';
                if(!empty($candidate->master_startyear))
                $query .= 'master_startyear=\'' . $candidate->master_startyear . '\',';
                if(!empty($candidate->master_endyear))
                $query .= 'master_endyear=\'' . $candidate->master_endyear . '\',';
                if(!empty($candidate->doctoral))
                $query .= 'doctoral=\'' . $candidate->doctoral . '\',';
                if(!empty($candidate->doctoral_startyear))
                $query .= 'doctoral_startyear=\'' . $candidate->doctoral_startyear . '\',';
                if(!empty($candidate->doctoral_endyear))
                $query .= 'doctoral_endyear=\'' . $candidate->doctoral_endyear . '\',';
                if(!empty($candidate->company1))
                $query .= 'company1=\'' . $candidate->company1 . '\',';
                if(!empty($candidate->position1))
                $query .= 'position1=\'' . $candidate->position1 . '\',';
                if(!empty($candidate->start1))
                $query .= 'start1=\'' . $candidate->start1 . '\',';
                if(!empty($candidate->end1))
                $query .= 'end1=\'' . $candidate->end1 . '\',';
                if(!empty($candidate->jobdesc1))
                $query .= 'jobdesc1=\'' . $candidate->jobdesc1 . '\',';
                if(!empty($candidate->refname1))
                $query .= 'refname1=\'' . $candidate->refname1 . '\',';
                if(!empty($candidate->reftelp1))
                $query .= 'reftelp1=\'' . $candidate->reftelp1 . '\',';
                if(!empty($candidate->company2))
                $query .= 'company2=\'' . $candidate->company2 . '\',';
                if(!empty($candidate->position2))
                $query .= 'position2=\'' . $candidate->position2 . '\',';
                if(!empty($candidate->start2))
                $query .= 'start2=\'' . $candidate->start2 . '\',';
                if(!empty($candidate->end2))
                $query .= 'end2=\'' . $candidate->end2 . '\',';
                if(!empty($candidate->jobdesc2))
                $query .= 'jobdesc2=\'' . $candidate->jobdesc2 . '\',';
                if(!empty($candidate->refname2))
                $query .= 'refname2=\'' . $candidate->refname2 . '\',';
                if(!empty($candidate->reftelp2))
                $query .= 'reftelp2=\'' . $candidate->reftelp2 . '\',';
                if(!empty($candidate->company3))
                $query .= 'company3=\'' . $candidate->company3 . '\',';
                if(!empty($candidate->position3))
                $query .= 'position3=\'' . $candidate->position3 . '\',';
                if(!empty($candidate->start3))
                $query .= 'start3=\'' . $candidate->start3 . '\',';
                if(!empty($candidate->end3))
                $query .= 'end3=\'' . $candidate->end3 . '\',';
                if(!empty($candidate->jobdesc3))
                $query .= 'jobdesc3=\'' . $candidate->jobdesc3 . '\',';
                if(!empty($candidate->refname3))
                $query .= 'refname3=\'' . $candidate->refname3 . '\',';
                if(!empty($candidate->reftelp3))
                $query .= 'reftelp3=\'' . $candidate->reftelp3 . '\',';
                if(!empty($candidate->company4))
                $query .= 'company4=\'' . $candidate->company4 . '\',';
                if(!empty($candidate->position4))
                $query .= 'position4=\'' . $candidate->position4 . '\',';
                if(!empty($candidate->start4))
                $query .= 'start4=\'' . $candidate->start4 . '\',';
                if(!empty($candidate->end4))
                $query .= 'end4=\'' . $candidate->end4 . '\',';
                if(!empty($candidate->jobdesc4))
                $query .= 'jobdesc4=\'' . $candidate->jobdesc4 . '\',';
                if(!empty($candidate->refname4))
                $query .= 'refname4=\'' . $candidate->refname4 . '\',';
                if(!empty($candidate->reftelp4))
                $query .= 'reftelp4=\'' . $candidate->reftelp4 . '\',';
                if(!empty($candidate->company5))
                $query .= 'company5=\'' . $candidate->company5 . '\',';
                if(!empty($candidate->position5))
                $query .= 'position5=\'' . $candidate->position5 . '\',';
                if(!empty($candidate->start5))
                $query .= 'start5=\'' . $candidate->start5 . '\',';
                if(!empty($candidate->end5))
                $query .= 'end5=\'' . $candidate->end5 . '\',';
                if(!empty($candidate->jobdesc5))
                $query .= 'jobdesc5=\'' . $candidate->jobdesc5 . '\',';
                if(!empty($candidate->refname5))
                $query .= 'refname5=\'' . $candidate->refname5 . '\',';
                if(!empty($candidate->reftelp5))
                $query .= 'reftelp5=\'' . $candidate->reftelp5 . '\',';
                if(!empty($candidate->currentsalary))
                $query .= 'currentsalary=\'' . $candidate->currentsalary . '\',';
                if(!empty($candidate->wantsalary))
                $query .= 'wantsalary=\'' . $candidate->wantsalary . '\',';
                if(!empty($candidate->prefworkplace))
                $query .= 'prefworkplace=\'' . $candidate->prefworkplace . '\',';
                if(!empty($candidate->doctor_str))
                $query .= 'doctor_str=\'' . $candidate->doctor_str . '\',';
                if(!empty($candidate->doctor_strissued))
                $query .= 'doctor_strissued=\'' . $candidate->doctor_strissued . '\',';
                if(!empty($candidate->doctor_strexpire))
                $query .= 'doctor_strexpire=\'' . $candidate->doctor_strexpire . '\',';
                if(!empty($candidate->doctor_idicard))
                $query .= 'doctor_idicard=\'' . $candidate->doctor_idicard . '\',';
                if(!empty($candidate->doctor_idicardexpire))
                $query .= 'doctor_idicardexpire=\'' . $candidate->doctor_idicardexpire . '\',';
                if(!empty($candidate->doctor_hiperkes))
                $query .= 'doctor_hiperkes=\'' . $candidate->doctor_hiperkes . '\',';
                if(!empty($candidate->doctor_acls))
                $query .= 'doctor_acls=\'' . $candidate->doctor_acls . '\',';
                if(!empty($candidate->doctor_aclsissued))
                $query .= 'doctor_aclsissued=\'' . $candidate->doctor_aclsissued . '\',';
                if(!empty($candidate->doctor_aclsexpire))
                $query .= 'doctor_aclsexpire=\'' . $candidate->doctor_aclsexpire . '\',';
                if(!empty($candidate->doctor_atls))
                $query .= 'doctor_atls=\'' . $candidate->doctor_atls . '\',';
                if(!empty($candidate->doctor_atlsissued))
                $query .= 'doctor_atlsissued=\'' . $candidate->doctor_atlsissued . '\',';
                if(!empty($candidate->doctor_atlsexpire))
                $query .= 'doctor_atlsexpire=\'' . $candidate->doctor_atlsexpire . '\',';
                if(!empty($candidate->nurse_str))
                $query .= 'nurse_str=\'' . $candidate->nurse_str . '\',';
                if(!empty($candidate->nurse_strissued))
                $query .= 'nurse_strissued=\'' . $candidate->nurse_strissued . '\',';
                if(!empty($candidate->nurse_strexpire))
                $query .= 'nurse_strexpire=\'' . $candidate->nurse_strexpire . '\',';
                if(!empty($candidate->nurse_ppni))
                $query .= 'nurse_ppni=\'' . $candidate->nurse_ppni . '\',';
                if(!empty($candidate->nurse_ppniexpire))
                $query .= 'nurse_ppniexpire=\'' . $candidate->nurse_ppniexpire . '\',';
                if(!empty($candidate->nurse_hiperkes))
                $query .= 'nurse_hiperkes=\'' . $candidate->nurse_hiperkes . '\',';
                if(!empty($candidate->nurse_ppgd))
                $query .= 'nurse_ppgd=\'' . $candidate->nurse_ppgd . '\',';
                if(!empty($candidate->nurse_ppgdissued))
                $query .= 'nurse_ppgdissued=\'' . $candidate->nurse_ppgdissued . '\',';
                if(!empty($candidate->nurse_ppgdexpire))
                $query .= 'nurse_ppgdexpire=\'' . $candidate->nurse_ppgdexpire . '\',';
                if(!empty($candidate->atlsexpire))
                $query .= 'atlsexpire=\'' . $candidate->atlsexpire . '\',';
                if(!empty($candidate->aclsexpire))
                $query .= 'aclsexpire=\'' . $candidate->aclsexpire . '\',';
                if(!empty($candidate->ppgdexpire))
                $query .= 'ppgdexpire=\'' . $candidate->ppgdexpire . '\',';
                if(!empty($candidate->associate_desc))
                $query .= 'associate_desc=\'' . $candidate->associate_desc . '\',';
                if(!empty($candidate->bachelor_desc))
                $query .= 'bachelor_desc=\'' . $candidate->bachelor_desc . '\',';
                if(!empty($candidate->master_desc))
                $query .= 'master_desc=\'' . $candidate->master_desc . '\',';
                if(!empty($candidate->doctoral_desc))
                $query .= 'doctoral_desc=\'' . $candidate->doctoral_desc . '\',';

                if(!empty($candidate->spouse))
                $query .= 'spouse=\'' . $candidate->spouse . '\',';
                if(!empty($candidate->spouseage))
                $query .= 'spouseage=\'' . $candidate->spouseage . '\',';
                if(!empty($candidate->spouseedu))
                $query .= 'spouseedu=\'' . $candidate->spouseedu . '\',';
                if(!empty($candidate->spouseoccupation))
                $query .= 'spouseoccupation=\'' . $candidate->spouseoccupation . '\',';
                if(!empty($candidate->firstchildname))
                $query .= 'firstchildname=\'' . $candidate->firstchildname . '\',';
                if(!empty($candidate->firstchildage))
                $query .= 'firstchildage=\'' . $candidate->firstchildage . '\',';
                if(!empty($candidate->firstchildedu))
                $query .= 'firstchildedu=\'' . $candidate->firstchildedu . '\',';
                if(!empty($candidate->firstchildoccupation))
                $query .= 'firstchildoccupation=\'' . $candidate->firstchildoccupation . '\',';
                if(!empty($candidate->secondchildname))
                $query .= 'secondchildname=\'' . $candidate->secondchildname . '\',';
                if(!empty($candidate->secondchildage))
                $query .= 'secondchildage=\'' . $candidate->secondchildage . '\',';
                if(!empty($candidate->secondchildedu))
                $query .= 'secondchildedu=\'' . $candidate->secondchildedu . '\',';
                if(!empty($candidate->secondchildoccupation))
                $query .= 'secondchildoccupation=\'' . $candidate->secondchildoccupation . '\',';
                if(!empty($candidate->thirdchildname))
                $query .= 'thirdchildname=\'' . $candidate->thirdchildname . '\',';
                if(!empty($candidate->thirdchildage))
                $query .= 'thirdchildage=\'' . $candidate->thirdchildage . '\',';
                if(!empty($candidate->thirdchildedu))
                $query .= 'thirdchildedu=\'' . $candidate->thirdchildedu . '\',';
                if(!empty($candidate->thirdchildoccupation))
                $query .= 'thirdchildoccupation=\'' . $candidate->thirdchildoccupation . '\',';
                if(!empty($candidate->fathername))
                $query .= 'fathername=\'' . $candidate->fathername . '\',';
                if(!empty($candidate->fatherage))
                $query .= 'fatherage=\'' . $candidate->fatherage . '\',';
                if(!empty($candidate->fatheredu))
                $query .= 'fatheredu=\'' . $candidate->fatheredu . '\',';
                if(!empty($candidate->fatheroccupation))
                $query .= 'fatheroccupation=\'' . $candidate->fatheroccupation . '\',';
                if(!empty($candidate->mothername))
                $query .= 'mothername=\'' . $candidate->mothername . '\',';
                if(!empty($candidate->motherage))
                $query .= 'motherage=\'' . $candidate->motherage . '\',';
                if(!empty($candidate->motheredu))
                $query .= 'motheredu=\'' . $candidate->motheredu . '\',';
                if(!empty($candidate->motheroccupation))
                $query .= 'motheroccupation=\'' . $candidate->motheroccupation . '\',';
                if(!empty($candidate->siblingname1))
                $query .= 'siblingname1=\'' . $candidate->siblingname1 . '\',';
                if(!empty($candidate->siblingage1))
                $query .= 'siblingage1=\'' . $candidate->siblingage1 . '\',';
                if(!empty($candidate->siblingedu1))
                $query .= 'siblingedu1=\'' . $candidate->siblingedu1 . '\',';
                if(!empty($candidate->siblingoccupation1))
                $query .= 'siblingoccupation1=\'' . $candidate->siblingoccupation1 . '\',';
                if(!empty($candidate->siblingname2))
                $query .= 'siblingname2=\'' . $candidate->siblingname2 . '\',';
                if(!empty($candidate->siblingage2))
                $query .= 'siblingage2=\'' . $candidate->siblingage2 . '\',';
                if(!empty($candidate->siblingedu2))
                $query .= 'siblingedu2=\'' . $candidate->siblingedu2 . '\',';
                if(!empty($candidate->siblingoccupation2))
                $query .= 'siblingoccupation2=\'' . $candidate->siblingoccupation2 . '\',';
                if(!empty($candidate->siblingname3))
                $query .= 'siblingname3=\'' . $candidate->siblingname3 . '\',';
                if(!empty($candidate->siblingage3))
                $query .= 'siblingage3=\'' . $candidate->siblingage3 . '\',';
                if(!empty($candidate->siblingedu3))
                $query .= 'siblingedu3=\'' . $candidate->siblingedu3 . '\',';
                if(!empty($candidate->siblingoccupation3))
                $query .= 'siblingoccupation3=\'' . $candidate->siblingoccupation3 . '\',';
                if(!empty($candidate->siblingname4))
                $query .= 'siblingname4=\'' . $candidate->siblingname4 . '\',';
                if(!empty($candidate->siblingage4))
                $query .= 'siblingage4=\'' . $candidate->siblingage4 . '\',';
                if(!empty($candidate->siblingedu4))
                $query .= 'siblingedu4=\'' . $candidate->siblingedu4 . '\',';
                if(!empty($candidate->siblingoccupation4))
                $query .= 'siblingoccupation4=\'' . $candidate->siblingoccupation4 . '\',';
                if(!empty($candidate->siblingname5))
                $query .= 'siblingname5=\'' . $candidate->siblingname5 . '\',';
                if(!empty($candidate->siblingage5))
                $query .= 'siblingage5=\'' . $candidate->siblingage5 . '\',';
                if(!empty($candidate->siblingedu5))
                $query .= 'siblingedu5=\'' . $candidate->siblingedu5 . '\',';
                if(!empty($candidate->siblingoccupation5))
                $query .= 'siblingoccupation5=\'' . $candidate->siblingoccupation5 . '\',';
                if(!empty($candidate->orgname1))
                $query .= 'orgname1=\'' . $candidate->orgname1 . '\',';
                if(!empty($candidate->orgyear1))
                $query .= 'orgyear1=\'' . $candidate->orgyear1 . '\',';
                if(!empty($candidate->orgposition1))
                $query .= 'orgposition1=\'' . $candidate->orgposition1 . '\',';
                if(!empty($candidate->orgname2))
                $query .= 'orgname2=\'' . $candidate->orgname2 . '\',';
                if(!empty($candidate->orgyear2))
                $query .= 'orgyear2=\'' . $candidate->orgyear2 . '\',';
                if(!empty($candidate->orgposition2))
                $query .= 'orgposition2=\'' . $candidate->orgposition2 . '\',';
                if(!empty($candidate->orgname3))
                $query .= 'orgname3=\'' . $candidate->orgname3 . '\',';
                if(!empty($candidate->orgyear3))
                $query .= 'orgyear3=\'' . $candidate->orgyear3 . '\',';
                if(!empty($candidate->orgposition3))
                $query .= 'orgposition3=\'' . $candidate->orgposition3 . '\',';
                if(!empty($candidate->orgname4))
                $query .= 'orgname4=\'' . $candidate->orgname4 . '\',';
                if(!empty($candidate->orgyear4))
                $query .= 'orgyear4=\'' . $candidate->orgyear4 . '\',';
                if(!empty($candidate->orgposition4))
                $query .= 'orgposition4=\'' . $candidate->orgposition4 . '\',';
                if(!empty($candidate->orgname5))
                $query .= 'orgname5=\'' . $candidate->orgname5 . '\',';
                if(!empty($candidate->orgyear5))
                $query .= 'orgyear5=\'' . $candidate->orgyear5 . '\',';
                if(!empty($candidate->orgposition5))
                $query .= 'orgposition5=\'' . $candidate->orgposition5 . '\',';
                if(!empty($candidate->diphteria))
                $query .= 'diphteria=\'' . $candidate->diphteria . '\',';
                if(!empty($candidate->sinusitis))
                $query .= 'sinusitis=\'' . $candidate->sinusitis . '\',';
                if(!empty($candidate->bronchitis))
                $query .= 'bronchitis=\'' . $candidate->bronchitis . '\',';
                if(!empty($candidate->hemoptoe))
                $query .= 'hemoptoe=\'' . $candidate->hemoptoe . '\',';
                if(!empty($candidate->tbc))
                $query .= 'tbc=\'' . $candidate->tbc . '\',';
                if(!empty($candidate->lunginfection))
                $query .= 'lunginfection=\'' . $candidate->lunginfection . '\',';
                if(!empty($candidate->asthma))
                $query .= 'asthma=\'' . $candidate->asthma . '\',';
                if(!empty($candidate->dyspnoea))
                $query .= 'dyspnoea=\'' . $candidate->dyspnoea . '\',';
                if(!empty($candidate->urinateproblem))
                $query .= 'urinateproblem=\'' . $candidate->urinateproblem . '\',';
                if(!empty($candidate->urinatedisorder))
                $query .= 'urinatedisorder=\'' . $candidate->urinatedisorder . '\',';
                if(!empty($candidate->kidneydisease))
                $query .= 'kidneydisease=\'' . $candidate->kidneydisease . '\',';
                if(!empty($candidate->kidneystone))
                $query .= 'kidneystone=\'' . $candidate->kidneystone . '\',';
                if(!empty($candidate->frequenturinate))
                $query .= 'frequenturinate=\'' . $candidate->frequenturinate . '\',';
                if(!empty($candidate->meningitis))
                $query .= 'meningitis=\'' . $candidate->meningitis . '\',';
                if(!empty($candidate->cerebralconcussion))
                $query .= 'cerebralconcussion=\'' . $candidate->cerebralconcussion . '\',';
                if(!empty($candidate->poliomyelitis))
                $query .= 'poliomyelitis=\'' . $candidate->poliomyelitis . '\',';
                if(!empty($candidate->epilepsy))
                $query .= 'epilepsy=\'' . $candidate->epilepsy . '\',';
                if(!empty($candidate->stroke))
                $query .= 'stroke=\'' . $candidate->stroke . '\',';
                if(!empty($candidate->headache))
                $query .= 'headache=\'' . $candidate->headache . '\',';
                if(!empty($candidate->typhoid))
                $query .= 'typhoid=\'' . $candidate->typhoid . '\',';
                if(!empty($candidate->typhoid))
                $query .= 'bloodvomit=\'' . $candidate->typhoid . '\',';
                if(!empty($candidate->obstipation))
                $query .= 'obstipation=\'' . $candidate->obstipation . '\',';
                if(!empty($candidate->dyspepsia))
                $query .= 'dyspepsia=\'' . $candidate->dyspepsia . '\',';
                if(!empty($candidate->jaundice))
                $query .= 'jaundice=\'' . $candidate->jaundice . '\',';
                if(!empty($candidate->gallbladder))
                $query .= 'gallbladder=\'' . $candidate->gallbladder . '\',';
                if(!empty($candidate->swallowingdisorder))
                $query .= 'swallowingdisorder=\'' . $candidate->swallowingdisorder . '\',';
                if(!empty($candidate->incontinentiaalvi))
                $query .= 'incontinentiaalvi=\'' . $candidate->incontinentiaalvi . '\',';
                if(!empty($candidate->chickenpox))
                $query .= 'chickenpox=\'' . $candidate->chickenpox . '\',';
                if(!empty($candidate->fungalskin))
                $query .= 'fungalskin=\'' . $candidate->fungalskin . '\',';
                if(!empty($candidate->std))
                $query .= 'std=\'' . $candidate->std . '\',';
                if(!empty($candidate->heartattack))
                $query .= 'heartattack=\'' . $candidate->heartattack . '\',';
                if(!empty($candidate->chestpains))
                $query .= 'chestpains=\'' . $candidate->chestpains . '\',';
                if(!empty($candidate->palpitation))
                $query .= 'palpitation=\'' . $candidate->palpitation . '\',';
                if(!empty($candidate->hypertension))
                $query .= 'hypertension=\'' . $candidate->hypertension . '\',';
                if(!empty($candidate->hemorrhoid))
                $query .= 'hemorrhoid=\'' . $candidate->hemorrhoid . '\',';
                if(!empty($candidate->varicoseveins))
                $query .= 'varicoseveins=\'' . $candidate->varicoseveins . '\',';
                if(!empty($candidate->thyroiddisease))
                $query .= 'thyroiddisease=\'' . $candidate->thyroiddisease . '\',';
                if(!empty($candidate->rhematoidsarthritis))
                $query .= 'rhematoidsarthritis=\'' . $candidate->rhematoidsarthritis . '\',';
                if(!empty($candidate->foodallergy))
                $query .= 'foodallergy=\'' . $candidate->foodallergy . '\',';
                if(!empty($candidate->foodallergylist))
                $query .= 'foodallergylist=\'' . $candidate->foodallergylist . '\',';
                if(!empty($candidate->drugallergy))
                $query .= 'drugallergy=\'' . $candidate->drugallergy . '\',';
                if(!empty($candidate->drugallergylist))
                $query .= 'drugallergylist=\'' . $candidate->drugallergylist . '\',';
                if(!empty($candidate->tetanus))
                $query .= 'tetanus=\'' . $candidate->tetanus . '\',';
                if(!empty($candidate->fainting))
                $query .= 'fainting=\'' . $candidate->fainting . '\',';
                if(!empty($candidate->oblivion))
                $query .= 'oblivion=\'' . $candidate->oblivion . '\',';
                if(!empty($candidate->consentrationdif))
                $query .= 'consentrationdif=\'' . $candidate->consentrationdif . '\',';
                if(!empty($candidate->visiondisorder))
                $query .= 'visiondisorder=\'' . $candidate->visiondisorder . '\',';
                if(!empty($candidate->hearingdisorder))
                $query .= 'hearingdisorder=\'' . $candidate->hearingdisorder . '\',';
                if(!empty($candidate->lumbago))
                $query .= 'lumbago=\'' . $candidate->lumbago . '\',';
                if(!empty($candidate->neoplasm))
                $query .= 'neoplasm=\'' . $candidate->neoplasm . '\',';
                if(!empty($candidate->mentalillness))
                $query .= 'mentalillness=\'' . $candidate->mentalillness . '\',';
                if(!empty($candidate->skintuberculose))
                $query .= 'skintuberculose=\'' . $candidate->skintuberculose . '\',';
                if(!empty($candidate->bonetuberculose))
                $query .= 'bonetuberculose=\'' . $candidate->bonetuberculose . '\',';
                if(!empty($candidate->measles))
                $query .= 'measles=\'' . $candidate->measles . '\',';
                if(!empty($candidate->malaria))
                $query .= 'malaria=\'' . $candidate->malaria . '\',';
                if(!empty($candidate->diabetes))
                $query .= 'diabetes=\'' . $candidate->diabetes . '\',';
                if(!empty($candidate->sleepdisorder))
                $query .= 'sleepdisorder=\'' . $candidate->sleepdisorder . '\',';
                if(!empty($candidate->hospitalized))
                $query .= 'hospitalized=\'' . $candidate->hospitalized . '\',';
                if(!empty($candidate->hospitalizeddate))
                $query .= 'hospitalizeddate=\'' . $candidate->hospitalizeddate . '\',';
                if(!empty($candidate->hospitalizedlength))
                $query .= 'hospitalizedlength=\'' . $candidate->hospitalizedlength . '\',';
                if(!empty($candidate->hospitalizedwhy))
                $query .= 'hospitalizedwhy=\'' . $candidate->hospitalizedwhy . '\',';
                if(!empty($candidate->accident))
                $query .= 'accident=\'' . $candidate->accident . '\',';
                if(!empty($candidate->accidentdate))
                $query .= 'accidentdate=\'' . $candidate->accidentdate . '\',';
                if(!empty($candidate->accidenttxt))
                $query .= 'accidenttxt=\'' . $candidate->accidenttxt . '\',';
                if(!empty($candidate->surgery))
                $query .= 'surgery=\'' . $candidate->surgery . '\',';
                if(!empty($candidate->surgerydate))
                $query .= 'surgerydate=\'' . $candidate->surgerydate . '\',';
                if(!empty($candidate->surgerytxt))
                $query .= 'surgerytxt=\'' . $candidate->surgerytxt . '\',';
                if(!empty($candidate->smoker))
                $query .= 'smoker=\'' . $candidate->smoker . '\',';
                if(!empty($candidate->smokernumber))
                $query .= 'smokernumber=\'' . $candidate->smokernumber . '\',';
                if(!empty($candidate->smokerage))
                $query .= 'smokerage=\'' . $candidate->smokerage . '\',';
                if(!empty($candidate->smokerstop))
                $query .= 'smokerstop=\'' . $candidate->smokerstop . '\',';
                if(!empty($candidate->alcohol))
                $query .= 'alcohol=\'' . $candidate->alcohol . '\',';
                if(!empty($candidate->alcoholperweek))
                $query .= 'alcoholperweek=\'' . $candidate->alcoholperweek . '\',';
                if(!empty($candidate->alcoholslokyperweek))
                $query .= 'alcoholslokyperweek=\'' . $candidate->alcoholslokyperweek . '\',';
                if(!empty($candidate->exercise))
                $query .= 'exercise=\'' . $candidate->exercise . '\',';
                if(!empty($candidate->exercisetxt))
                $query .= 'exercisetxt=\'' . $candidate->exercisetxt . '\',';
                if(!empty($candidate->medicinelist))
                $query .= 'medicinelist=\'' . $candidate->medicinelist . '\',';
                if(!empty($candidate->vacdiptheri))
                $query .= 'vacdiptheri=\'' . $candidate->vacdiptheri . '\',';
                if(!empty($candidate->vactetanus))
                $query .= 'vactetanus=\'' . $candidate->vactetanus . '\',';
                if(!empty($candidate->vacpolio))
                $query .= 'vacpolio=\'' . $candidate->vacpolio . '\',';
                if(!empty($candidate->vacbcg))
                $query .= 'vacbcg=\'' . $candidate->vacbcg . '\',';
                if(!empty($candidate->vacmmr))
                $query .= 'vacmmr=\'' . $candidate->vacmmr . '\',';
                if(!empty($candidate->vacinfluenza))
                $query .= 'vacinfluenza=\'' . $candidate->vacinfluenza . '\',';
                if(!empty($candidate->vachepatitisa))
                $query .= 'vachepatitisa=\'' . $candidate->vachepatitisa . '\',';
                if(!empty($candidate->vachepatitisb))
                $query .= 'vachepatitisb=\'' . $candidate->vachepatitisb . '\',';
                if(!empty($candidate->vacvarisela))
                $query .= 'vacvarisela=\'' . $candidate->vacvarisela . '\',';
                if(!empty($candidate->vactyphoid))
                $query .= 'vactyphoid=\'' . $candidate->vactyphoid . '\',';
                if(!empty($candidate->vachpv))
                $query .= 'vachpv=\'' . $candidate->vachpv . '\',';
                if(!empty($candidate->vacmeningokok))
                $query .= 'vacmeningokok=\'' . $candidate->vacmeningokok . '\',';
                if(!empty($candidate->vacyellowfever))
                $query .= 'vacyellowfever=\'' . $candidate->vacyellowfever . '\',';
                if(!empty($candidate->vacjapencephalitis))
                $query .= 'vacjapencephalitis=\'' . $candidate->vacjapencephalitis . '\',';
                if(!empty($candidate->consent))
                $query .= 'consent=\'' . $candidate->consent . '\',';

                $query = substr($query, 0, strlen($query) - 1);

                $query .= " WHERE id=" . $candidate->id;
            $pdo->exec($query);
            /*$q = Doctrine_Query:: create()->update('JobCandidate')
                    ->set('firstName', '?', $candidate->firstName)
                    ->set('middleName', '?', $candidate->middleName)
                    ->set('lastName', '?', $candidate->lastName)
                    ->set('contactNumber', '?', $candidate->contactNumber)
                    ->set('email', '?', $candidate->email)
                    ->set('keywords', '?', $candidate->keywords)
                    ->set('comment', '?', $candidate->comment)
                    ->set('dateOfApplication', '?', $candidate->dateOfApplication)
                    ->set('gender', '?', $candidate->gender)
                    ->set('birthplace', '?', $candidate->birthplace)
                    ->set('dob', '?', $candidate->dob)
                    ->set('currentaddr', '?', $candidate->currentaddr)
                    ->set('currentprovince', '?', $candidate->currentprovince)
                    ->set('currentcity', '?', $candidate->currentcity)
                    ->set('currentkecamatan', '?', $candidate->currentkecamatan)
                    ->set('currentkelurahan', '?', $candidate->currentkelurahan)
                    ->set('currentzip', '?', $candidate->currentzip)
                    ->set('permanentaddr', '?', $candidate->permanentaddr)
                    ->set('permanentprovince', '?', $candidate->permanentprovince)
                    ->set('permanentcity', '?', $candidate->permanentcity)
                    ->set('permanentkecamatan', '?', $candidate->permanentkecamatan)
                    ->set('permanentkelurahan', '?', $candidate->permanentkelurahan)
                    ->set('permanentzip', '?', $candidate->permanentzip)
                    ->set('religion', '?', $candidate->religion)
                    ->set('marital', '?', $candidate->marital)
                    ->set('phone', '?', $candidate->phone)
                    ->set('mobile', '?', $candidate->mobile)
                    ->set('ektp', '?', $candidate->ektp)
                    ->set('elementary', '?', $candidate->elementary)
                    ->set('elementary_startyear', '?', $candidate->elementary_startyear)
                    ->set('elementary_endyear', '?', $candidate->elementary_endyear)
                    ->set('juniorhigh', '?', $candidate->juniorhigh)
                    ->set('juniorhigh_startyear', '?', $candidate->juniorhigh_startyear)
                    ->set('juniorhigh_endyear', '?', $candidate->juniorhigh_endyear)
                    ->set('seniorhigh', '?', $candidate->seniorhigh)
                    ->set('seniorhigh_startyear', '?', $candidate->seniorhigh_startyear)
                    ->set('seniorhigh_endyear', '?', $candidate->seniorhigh_endyear)
                    ->set('associate', '?', $candidate->associate)
                    ->set('associate_startyear', '?', $candidate->associate_startyear)
                    ->set('associate_endyear', '?', $candidate->associate_endyear)
                    ->set('bachelor', '?', $candidate->bachelor)
                    ->set('bachelor_startyear', '?', $candidate->bachelor_startyear)
                    ->set('bachelor_endyear', '?', $candidate->bachelor_endyear)
                    ->set('master', '?', $candidate->master)
                    ->set('master_startyear', '?', $candidate->master_startyear)
                    ->set('master_endyear', '?', $candidate->master_endyear)
                    ->set('doctoral', '?', $candidate->doctoral)
                    ->set('doctoral_startyear', '?', $candidate->doctoral_startyear)
                    ->set('doctoral_endyear', '?', $candidate->doctoral_endyear)
                    ->set('company1', '?', $candidate->company1)
                    ->set('position1', '?', $candidate->position1)
                    ->set('start1', '?', $candidate->start1)
                    ->set('end1', '?', $candidate->end1)
                    ->set('jobdesc1', '?', $candidate->jobdesc1)
                    ->set('refname1', '?', $candidate->refname1)
                    ->set('reftelp1', '?', $candidate->reftelp1)
                    ->set('company2', '?', $candidate->company2)
                    ->set('position2', '?', $candidate->position2)
                    ->set('start2', '?', $candidate->start2)
                    ->set('end2', '?', $candidate->end2)
                    ->set('jobdesc2', '?', $candidate->jobdesc2)
                    ->set('refname2', '?', $candidate->refname2)
                    ->set('reftelp2', '?', $candidate->reftelp2)
                    ->set('company3', '?', $candidate->company3)
                    ->set('position3', '?', $candidate->position3)
                    ->set('start3', '?', $candidate->start3)
                    ->set('end3', '?', $candidate->end3)
                    ->set('jobdesc3', '?', $candidate->jobdesc3)
                    ->set('refname3', '?', $candidate->refname3)
                    ->set('reftelp3', '?', $candidate->reftelp3)
                    ->set('company4', '?', $candidate->company4)
                    ->set('position4', '?', $candidate->position4)
                    ->set('start4', '?', $candidate->start4)
                    ->set('end4', '?', $candidate->end4)
                    ->set('jobdesc4', '?', $candidate->jobdesc4)
                    ->set('refname4', '?', $candidate->refname4)
                    ->set('reftelp4', '?', $candidate->reftelp4)
                    ->set('company5', '?', $candidate->company5)
                    ->set('position5', '?', $candidate->position5)
                    ->set('start5', '?', $candidate->start5)
                    ->set('end5', '?', $candidate->end5)
                    ->set('jobdesc5', '?', $candidate->jobdesc5)
                    ->set('refname5', '?', $candidate->refname5)
                    ->set('reftelp5', '?', $candidate->reftelp5)
                    ->set('currentsalary', '?', $candidate->currentsalary)
                    ->set('wantsalary', '?', $candidate->wantsalary)
                    ->set('prefworkplace', '?', $candidate->prefworkplace)
                    ->set('doctor_str', '?', $candidate->doctor_str)
                    ->set('doctor_strissued', '?', $candidate->doctor_strissued)
                    ->set('doctor_strexpire', '?', $candidate->doctor_strexpire)
                    ->set('doctor_idicard', '?', $candidate->doctor_idicard)
                    ->set('doctor_idicardexpire', '?', $candidate->doctor_idicardexpire)
                    ->set('doctor_hiperkes', '?', $candidate->doctor_hiperkes)
                    ->set('doctor_acls', '?', $candidate->doctor_acls)
                    ->set('doctor_aclsissued', '?', $candidate->doctor_aclsissued)
                    ->set('doctor_aclsexpire', '?', $candidate->doctor_aclsexpire)
                    ->set('doctor_atls', '?', $candidate->doctor_atls)
                    ->set('doctor_atlsissued', '?', $candidate->doctor_atlsissued)
                    ->set('doctor_atlsexpire', '?', $candidate->doctor_atlsexpire)
                    ->set('nurse_str', '?', $candidate->nurse_str)
                    ->set('nurse_strissued', '?', $candidate->nurse_strissued)
                    ->set('nurse_strexpire', '?', $candidate->nurse_strexpire)
                    ->set('nurse_ppni', '?', $candidate->nurse_ppni)
                    ->set('nurse_ppniexpire', '?', $candidate->nurse_ppniexpire)
                    ->set('nurse_hiperkes', '?', $candidate->nurse_hiperkes)
                    ->set('nurse_ppgd', '?', $candidate->nurse_ppgd)
                    ->set('nurse_ppgdissued', '?', $candidate->nurse_ppgdissued)
                    ->set('nurse_ppgdexpire', '?', $candidate->nurse_ppgdexpire)
                    ->set('atlsexpire', '?', $candidate->atlsexpire)
                    ->set('aclsexpire', '?', $candidate->aclsexpire)
                    ->set('ppgdexpire', '?', $candidate->ppgdexpire)
                    ->set('associate_desc', '?', $candidate->associate_desc)
                    ->set('bachelor_desc', '?', $candidate->bachelor_desc)
                    ->set('master_desc', '?', $candidate->master_desc)
                    ->set('doctoral_desc', '?', $candidate->doctoral_desc)
                    ->where('id = ?', $candidate->id);*/
            //echo $q->getSqlQuery();die;
            //return $q->execute();
        } catch (Exception $e) {
            throw new DaoException($e->getMessage());
        }
    }

    /**
     *
     * @param JobCandidate $candidate
     * @return <type>
     */
    public function updateCandidateDetail($candidate) {
        try {
            $pdo = Doctrine_Manager::connection()->getDbh();
            $query = "UPDATE ohrm_job_candidate SET ";
            $query .= 'bloodtype=\'' . $candidate->bloodtype . '\',';
            $query .= 'heght=\'' . $candidate->heght . '\',';
            $query .= 'weight=\'' . $candidate->weight . '\',';
            $query .= 'npwp=\'' . $candidate->npwp . '\',';
            $query .= 'bpjsnaker=\'' . $candidate->bpjsnaker . '\',';
            $query .= 'bpjskes=\'' . $candidate->bpjskes . '\',';
            $query .= 'faskes=\'' . $candidate->faskes . '\',';
            $query .= 'kk=\'' . $candidate->kk . '\',';
            $query .= 'familyhead=\'' . $candidate->familyhead . '\',';
            $query .= 'emergencyname=\'' . $candidate->emergencyname . '\',';
            $query .= 'emergencyaddr=\'' . $candidate->emergencyaddr . '\',';
            $query .= 'emergencyprovince=\'' . $candidate->emergencyprovince . '\',';
            $query .= 'emergencycity=\'' . $candidate->emergencycity . '\',';
            $query .= 'emergencykecamatan=\'' . $candidate->emergencykecamatan . '\',';
            $query .= 'emergencykelurahan=\'' . $candidate->emergencykelurahan . '\',';
            $query .= 'emergencyzip=\'' . $candidate->emergencyzip . '\',';
            $query .= 'emergencyrelation=\'' . $candidate->emergencyrelation . '\',';
            $query .= 'emergencyphone=\'' . $candidate->emergencyphone . '\',';
            if(!empty($candidate->spouse))
                $query .= 'spouse=\'' . $candidate->spouse . '\',';
            if(!empty($candidate->spouseage))
                $query .= 'spouseage=\'' . $candidate->spouseage . '\',';
            if(!empty($candidate->spouseedu))
                $query .= 'spouseedu=\'' . $candidate->spouseedu . '\',';
            if(!empty($candidate->spouseoccupation))
                $query .= 'spouseoccupation=\'' . $candidate->spouseoccupation . '\',';
            if(!empty($candidate->firstchildname))
                $query .= 'firstchildname=\'' . $candidate->firstchildname . '\',';
            if(!empty($candidate->firstchildage))
                $query .= 'firstchildage=\'' . $candidate->firstchildage . '\',';
            if(!empty($candidate->firstchildedu))
                $query .= 'firstchildedu=\'' . $candidate->firstchildedu . '\',';
            if(!empty($candidate->firstchildoccupation))
                $query .= 'firstchildoccupation=\'' . $candidate->firstchildoccupation . '\',';
            if(!empty($candidate->secondchildname))
                $query .= 'secondchildname=\'' . $candidate->secondchildname . '\',';
            if(!empty($candidate->secondchildage))
                $query .= 'secondchildage=\'' . $candidate->secondchildage . '\',';
            if(!empty($candidate->secondchildedu))
                $query .= 'secondchildedu=\'' . $candidate->secondchildedu . '\',';
            if(!empty($candidate->secondchildoccupation))
                $query .= 'secondchildoccupation=\'' . $candidate->secondchildoccupation . '\',';
            if(!empty($candidate->thirdchildname))
                $query .= 'thirdchildname=\'' . $candidate->thirdchildname . '\',';
            if(!empty($candidate->thirdchildage))
                $query .= 'thirdchildage=\'' . $candidate->thirdchildage . '\',';
            if(!empty($candidate->thirdchildedu))
                $query .= 'thirdchildedu=\'' . $candidate->thirdchildedu . '\',';
            if(!empty($candidate->thirdchildoccupation))
                $query .= 'thirdchildoccupation=\'' . $candidate->thirdchildoccupation . '\',';
            if(!empty($candidate->fathername))
                $query .= 'fathername=\'' . $candidate->fathername . '\',';
            if(!empty($candidate->fatherage))
                $query .= 'fatherage=\'' . $candidate->fatherage . '\',';
            if(!empty($candidate->fatheredu))
                $query .= 'fatheredu=\'' . $candidate->fatheredu . '\',';
            if(!empty($candidate->fatheroccupation))
                $query .= 'fatheroccupation=\'' . $candidate->fatheroccupation . '\',';
            if(!empty($candidate->mothername))
                $query .= 'mothername=\'' . $candidate->mothername . '\',';
            if(!empty($candidate->motherage))
                $query .= 'motherage=\'' . $candidate->motherage . '\',';
            if(!empty($candidate->motheredu))
                $query .= 'motheredu=\'' . $candidate->motheredu . '\',';
            if(!empty($candidate->motheroccupation))
                $query .= 'motheroccupation=\'' . $candidate->motheroccupation . '\',';
            if(!empty($candidate->siblingname1))
                $query .= 'siblingname1=\'' . $candidate->siblingname1 . '\',';
            if(!empty($candidate->siblingage1))
                $query .= 'siblingage1=\'' . $candidate->siblingage1 . '\',';
            if(!empty($candidate->siblingedu1))
                $query .= 'siblingedu1=\'' . $candidate->siblingedu1 . '\',';
            if(!empty($candidate->siblingoccupation1))
                $query .= 'siblingoccupation1=\'' . $candidate->siblingoccupation1 . '\',';
            if(!empty($candidate->siblingname2))
                $query .= 'siblingname2=\'' . $candidate->siblingname2 . '\',';
            if(!empty($candidate->siblingage2))
                $query .= 'siblingage2=\'' . $candidate->siblingage2 . '\',';
            if(!empty($candidate->siblingedu2))
                $query .= 'siblingedu2=\'' . $candidate->siblingedu2 . '\',';
            if(!empty($candidate->siblingoccupation2))
                $query .= 'siblingoccupation2=\'' . $candidate->siblingoccupation2 . '\',';
            if(!empty($candidate->siblingname3))
                $query .= 'siblingname3=\'' . $candidate->siblingname3 . '\',';
            if(!empty($candidate->siblingage3))
                $query .= 'siblingage3=\'' . $candidate->siblingage3 . '\',';
            if(!empty($candidate->siblingedu3))
                $query .= 'siblingedu3=\'' . $candidate->siblingedu3 . '\',';
            if(!empty($candidate->siblingoccupation3))
                $query .= 'siblingoccupation3=\'' . $candidate->siblingoccupation3 . '\',';
            if(!empty($candidate->siblingname4))
                $query .= 'siblingname4=\'' . $candidate->siblingname4 . '\',';
            if(!empty($candidate->siblingage4))
                $query .= 'siblingage4=\'' . $candidate->siblingage4 . '\',';
            if(!empty($candidate->siblingedu4))
                $query .= 'siblingedu4=\'' . $candidate->siblingedu4 . '\',';
            if(!empty($candidate->siblingoccupation4))
                $query .= 'siblingoccupation4=\'' . $candidate->siblingoccupation4 . '\',';
            if(!empty($candidate->siblingname5))
                $query .= 'siblingname5=\'' . $candidate->siblingname5 . '\',';
            if(!empty($candidate->siblingage5))
                $query .= 'siblingage5=\'' . $candidate->siblingage5 . '\',';
            if(!empty($candidate->siblingedu5))
                $query .= 'siblingedu5=\'' . $candidate->siblingedu5 . '\',';
            if(!empty($candidate->siblingoccupation5))
                $query .= 'siblingoccupation5=\'' . $candidate->siblingoccupation5 . '\',';
            if(!empty($candidate->orgname1))
                $query .= 'orgname1=\'' . $candidate->orgname1 . '\',';
            if(!empty($candidate->orgyear1))
                $query .= 'orgyear1=\'' . $candidate->orgyear1 . '\',';
            if(!empty($candidate->orgposition1))
                $query .= 'orgposition1=\'' . $candidate->orgposition1 . '\',';
            if(!empty($candidate->orgname2))
                $query .= 'orgname2=\'' . $candidate->orgname2 . '\',';
            if(!empty($candidate->orgyear2))
                $query .= 'orgyear2=\'' . $candidate->orgyear2 . '\',';
            if(!empty($candidate->orgposition2))
                $query .= 'orgposition2=\'' . $candidate->orgposition2 . '\',';
            if(!empty($candidate->orgname3))
                $query .= 'orgname3=\'' . $candidate->orgname3 . '\',';
            if(!empty($candidate->orgyear3))
                $query .= 'orgyear3=\'' . $candidate->orgyear3 . '\',';
            if(!empty($candidate->orgposition3))
                $query .= 'orgposition3=\'' . $candidate->orgposition3 . '\',';
            if(!empty($candidate->orgname4))
                $query .= 'orgname4=\'' . $candidate->orgname4 . '\',';
            if(!empty($candidate->orgyear4))
                $query .= 'orgyear4=\'' . $candidate->orgyear4 . '\',';
            if(!empty($candidate->orgposition4))
                $query .= 'orgposition4=\'' . $candidate->orgposition4 . '\',';
            if(!empty($candidate->orgname5))
                $query .= 'orgname5=\'' . $candidate->orgname5 . '\',';
            if(!empty($candidate->orgyear5))
                $query .= 'orgyear5=\'' . $candidate->orgyear5 . '\',';
            if(!empty($candidate->orgposition5))
                $query .= 'orgposition5=\'' . $candidate->orgposition5 . '\',';
            if(!empty($candidate->diphteria))
                $query .= 'diphteria=\'' . $candidate->diphteria . '\',';
            if(!empty($candidate->sinusitis))
                $query .= 'sinusitis=\'' . $candidate->sinusitis . '\',';
            if(!empty($candidate->bronchitis))
                $query .= 'bronchitis=\'' . $candidate->bronchitis . '\',';
            if(!empty($candidate->hemoptoe))
                $query .= 'hemoptoe=\'' . $candidate->hemoptoe . '\',';
            if(!empty($candidate->tbc))
                $query .= 'tbc=\'' . $candidate->tbc . '\',';
            if(!empty($candidate->lunginfection))
                $query .= 'lunginfection=\'' . $candidate->lunginfection . '\',';
            if(!empty($candidate->asthma))
                $query .= 'asthma=\'' . $candidate->asthma . '\',';
            if(!empty($candidate->dyspnoea))
                $query .= 'dyspnoea=\'' . $candidate->dyspnoea . '\',';
            if(!empty($candidate->urinateproblem))
                $query .= 'urinateproblem=\'' . $candidate->urinateproblem . '\',';
            if(!empty($candidate->urinatedisorder))
                $query .= 'urinatedisorder=\'' . $candidate->urinatedisorder . '\',';
            if(!empty($candidate->kidneydisease))
                $query .= 'kidneydisease=\'' . $candidate->kidneydisease . '\',';
            if(!empty($candidate->kidneystone))
                $query .= 'kidneystone=\'' . $candidate->kidneystone . '\',';
            if(!empty($candidate->frequenturinate))
                $query .= 'frequenturinate=\'' . $candidate->frequenturinate . '\',';
            if(!empty($candidate->meningitis))
                $query .= 'meningitis=\'' . $candidate->meningitis . '\',';
            if(!empty($candidate->cerebralconcussion))
                $query .= 'cerebralconcussion=\'' . $candidate->cerebralconcussion . '\',';
            if(!empty($candidate->poliomyelitis))
                $query .= 'poliomyelitis=\'' . $candidate->poliomyelitis . '\',';
            if(!empty($candidate->epilepsy))
                $query .= 'epilepsy=\'' . $candidate->epilepsy . '\',';
            if(!empty($candidate->stroke))
                $query .= 'stroke=\'' . $candidate->stroke . '\',';
            if(!empty($candidate->headache))
                $query .= 'headache=\'' . $candidate->headache . '\',';
            if(!empty($candidate->typhoid))
                $query .= 'typhoid=\'' . $candidate->typhoid . '\',';
            if(!empty($candidate->typhoid))
                $query .= 'bloodvomit=\'' . $candidate->typhoid . '\',';
            if(!empty($candidate->obstipation))
                $query .= 'obstipation=\'' . $candidate->obstipation . '\',';
            if(!empty($candidate->dyspepsia))
                $query .= 'dyspepsia=\'' . $candidate->dyspepsia . '\',';
            if(!empty($candidate->jaundice))
                $query .= 'jaundice=\'' . $candidate->jaundice . '\',';
            if(!empty($candidate->gallbladder))
                $query .= 'gallbladder=\'' . $candidate->gallbladder . '\',';
            if(!empty($candidate->swallowingdisorder))
                $query .= 'swallowingdisorder=\'' . $candidate->swallowingdisorder . '\',';
            if(!empty($candidate->incontinentiaalvi))
                $query .= 'incontinentiaalvi=\'' . $candidate->incontinentiaalvi . '\',';
            if(!empty($candidate->chickenpox))
                $query .= 'chickenpox=\'' . $candidate->chickenpox . '\',';
            if(!empty($candidate->fungalskin))
                $query .= 'fungalskin=\'' . $candidate->fungalskin . '\',';
            if(!empty($candidate->std))
                $query .= 'std=\'' . $candidate->std . '\',';
            if(!empty($candidate->heartattack))
                $query .= 'heartattack=\'' . $candidate->heartattack . '\',';
            if(!empty($candidate->chestpains))
                $query .= 'chestpains=\'' . $candidate->chestpains . '\',';
            if(!empty($candidate->palpitation))
                $query .= 'palpitation=\'' . $candidate->palpitation . '\',';
            if(!empty($candidate->hypertension))
                $query .= 'hypertension=\'' . $candidate->hypertension . '\',';
            if(!empty($candidate->hemorrhoid))
                $query .= 'hemorrhoid=\'' . $candidate->hemorrhoid . '\',';
            if(!empty($candidate->varicoseveins))
                $query .= 'varicoseveins=\'' . $candidate->varicoseveins . '\',';
            if(!empty($candidate->thyroiddisease))
                $query .= 'thyroiddisease=\'' . $candidate->thyroiddisease . '\',';
            if(!empty($candidate->rhematoidsarthritis))
                $query .= 'rhematoidsarthritis=\'' . $candidate->rhematoidsarthritis . '\',';
            if(!empty($candidate->foodallergy))
                $query .= 'foodallergy=\'' . $candidate->foodallergy . '\',';
            if(!empty($candidate->foodallergylist))
                $query .= 'foodallergylist=\'' . $candidate->foodallergylist . '\',';
            if(!empty($candidate->drugallergy))
                $query .= 'drugallergy=\'' . $candidate->drugallergy . '\',';
            if(!empty($candidate->drugallergylist))
                $query .= 'drugallergylist=\'' . $candidate->drugallergylist . '\',';
            if(!empty($candidate->tetanus))
                $query .= 'tetanus=\'' . $candidate->tetanus . '\',';
            if(!empty($candidate->fainting))
                $query .= 'fainting=\'' . $candidate->fainting . '\',';
            if(!empty($candidate->oblivion))
                $query .= 'oblivion=\'' . $candidate->oblivion . '\',';
            if(!empty($candidate->consentrationdif))
                $query .= 'consentrationdif=\'' . $candidate->consentrationdif . '\',';
            if(!empty($candidate->visiondisorder))
                $query .= 'visiondisorder=\'' . $candidate->visiondisorder . '\',';
            if(!empty($candidate->hearingdisorder))
                $query .= 'hearingdisorder=\'' . $candidate->hearingdisorder . '\',';
            if(!empty($candidate->lumbago))
                $query .= 'lumbago=\'' . $candidate->lumbago . '\',';
            if(!empty($candidate->neoplasm))
                $query .= 'neoplasm=\'' . $candidate->neoplasm . '\',';
            if(!empty($candidate->mentalillness))
                $query .= 'mentalillness=\'' . $candidate->mentalillness . '\',';
            if(!empty($candidate->skintuberculose))
                $query .= 'skintuberculose=\'' . $candidate->skintuberculose . '\',';
            if(!empty($candidate->bonetuberculose))
                $query .= 'bonetuberculose=\'' . $candidate->bonetuberculose . '\',';
            if(!empty($candidate->measles))
                $query .= 'measles=\'' . $candidate->measles . '\',';
            if(!empty($candidate->malaria))
                $query .= 'malaria=\'' . $candidate->malaria . '\',';
            if(!empty($candidate->diabetes))
                $query .= 'diabetes=\'' . $candidate->diabetes . '\',';
            if(!empty($candidate->sleepdisorder))
                $query .= 'sleepdisorder=\'' . $candidate->sleepdisorder . '\',';
            if(!empty($candidate->hospitalized))
                $query .= 'hospitalized=\'' . $candidate->hospitalized . '\',';
            if(!empty($candidate->hospitalizeddate))
                $query .= 'hospitalizeddate=\'' . $candidate->hospitalizeddate . '\',';
            if(!empty($candidate->hospitalizedlength))
                $query .= 'hospitalizedlength=\'' . $candidate->hospitalizedlength . '\',';
            if(!empty($candidate->hospitalizedwhy))
                $query .= 'hospitalizedwhy=\'' . $candidate->hospitalizedwhy . '\',';
            if(!empty($candidate->accident))
                $query .= 'accident=\'' . $candidate->accident . '\',';
            if(!empty($candidate->accidentdate))
                $query .= 'accidentdate=\'' . $candidate->accidentdate . '\',';
            if(!empty($candidate->accidenttxt))
                $query .= 'accidenttxt=\'' . $candidate->accidenttxt . '\',';
            if(!empty($candidate->surgery))
                $query .= 'surgery=\'' . $candidate->surgery . '\',';
            if(!empty($candidate->surgerydate))
                $query .= 'surgerydate=\'' . $candidate->surgerydate . '\',';
            if(!empty($candidate->surgerytxt))
                $query .= 'surgerytxt=\'' . $candidate->surgerytxt . '\',';
            if(!empty($candidate->smoker))
                $query .= 'smoker=\'' . $candidate->smoker . '\',';
            if(!empty($candidate->smokernumber))
                $query .= 'smokernumber=\'' . $candidate->smokernumber . '\',';
            if(!empty($candidate->smokerage))
                $query .= 'smokerage=\'' . $candidate->smokerage . '\',';
            if(!empty($candidate->smokerstop))
                $query .= 'smokerstop=\'' . $candidate->smokerstop . '\',';
            if(!empty($candidate->alcohol))
                $query .= 'alcohol=\'' . $candidate->alcohol . '\',';
            if(!empty($candidate->alcoholperweek))
                $query .= 'alcoholperweek=\'' . $candidate->alcoholperweek . '\',';
            if(!empty($candidate->alcoholslokyperweek))
                $query .= 'alcoholslokyperweek=\'' . $candidate->alcoholslokyperweek . '\',';
            if(!empty($candidate->exercise))
                $query .= 'exercise=\'' . $candidate->exercise . '\',';
            if(!empty($candidate->exercisetxt))
                $query .= 'exercisetxt=\'' . $candidate->exercisetxt . '\',';
            if(!empty($candidate->medicinelist))
                $query .= 'medicinelist=\'' . $candidate->medicinelist . '\',';
            if(!empty($candidate->vacdiptheri))
                $query .= 'vacdiptheri=\'' . $candidate->vacdiptheri . '\',';
            if(!empty($candidate->vactetanus))
                $query .= 'vactetanus=\'' . $candidate->vactetanus . '\',';
            if(!empty($candidate->vacpolio))
                $query .= 'vacpolio=\'' . $candidate->vacpolio . '\',';
            if(!empty($candidate->vacbcg))
                $query .= 'vacbcg=\'' . $candidate->vacbcg . '\',';
            if(!empty($candidate->vacmmr))
                $query .= 'vacmmr=\'' . $candidate->vacmmr . '\',';
            if(!empty($candidate->vacinfluenza))
                $query .= 'vacinfluenza=\'' . $candidate->vacinfluenza . '\',';
            if(!empty($candidate->vachepatitisa))
                $query .= 'vachepatitisa=\'' . $candidate->vachepatitisa . '\',';
            if(!empty($candidate->vachepatitisb))
                $query .= 'vachepatitisb=\'' . $candidate->vachepatitisb . '\',';
            if(!empty($candidate->vacvarisela))
                $query .= 'vacvarisela=\'' . $candidate->vacvarisela . '\',';
            if(!empty($candidate->vactyphoid))
                $query .= 'vactyphoid=\'' . $candidate->vactyphoid . '\',';
            if(!empty($candidate->vachpv))
                $query .= 'vachpv=\'' . $candidate->vachpv . '\',';
            if(!empty($candidate->vacmeningokok))
                $query .= 'vacmeningokok=\'' . $candidate->vacmeningokok . '\',';
            if(!empty($candidate->vacyellowfever))
                $query .= 'vacyellowfever=\'' . $candidate->vacyellowfever . '\',';
            if(!empty($candidate->vacjapencephalitis))
                $query .= 'vacjapencephalitis=\'' . $candidate->vacjapencephalitis . '\',';
            if(!empty($candidate->consent))
                $query .= 'consent=\'' . $candidate->consent . '\',';

            $query = substr($query, 0, strlen($query) - 1);

            $query .= " WHERE id=" . $candidate->id;
            $pdo->exec($query);

            /*$q = Doctrine_Query:: create()->update('JobCandidate')
                ->set('bloodtype', '?', $candidate->bloodtype)
                ->set('heght', '?', $candidate->heght)
                ->set('weight', '?', $candidate->weight)
                ->set('npwp', '?', $candidate->npwp)
                ->set('bpjsnaker', '?', $candidate->bpjsnaker)
                ->set('bpjskes', '?', $candidate->bpjskes)
                ->set('faskes', '?', $candidate->faskes)
                ->set('kk', '?', $candidate->kk)
                ->set('familyhead', '?', $candidate->familyhead)
                ->set('emergencyname', '?', $candidate->emergencyname)
                ->set('emergencyaddr', '?', $candidate->emergencyaddr)
                ->set('emergencyprovince', '?', $candidate->emergencyprovince)
                ->set('emergencycity', '?', $candidate->emergencycity)
                ->set('emergencykecamatan', '?', $candidate->emergencykecamatan)
                ->set('emergencykelurahan', '?', $candidate->emergencykelurahan)
                ->set('emergencyzip', '?', $candidate->emergencyzip)
                ->set('emergencyrelation', '?', $candidate->emergencyrelation)
                ->set('emergencyphone', '?', $candidate->emergencyphone)
                ->set('spouse', '?', $candidate->spouse)
                ->set('spouseage', '?', $candidate->spouseage)
                ->set('spouseedu', '?', $candidate->spouseedu)
                ->set('spouseoccupation', '?', $candidate->spouseoccupation)
                ->set('firstchildname', '?', $candidate->firstchildname)
                ->set('firstchildage', '?', $candidate->firstchildage)
                ->set('firstchildedu', '?', $candidate->firstchildedu)
                ->set('firstchildoccupation', '?', $candidate->firstchildoccupation)
                ->set('secondchildname', '?', $candidate->secondchildname)
                ->set('secondchildage', '?', $candidate->secondchildage)
                ->set('secondchildedu', '?', $candidate->secondchildedu)
                ->set('secondchildoccupation', '?', $candidate->secondchildoccupation)
                ->set('thirdchildname', '?', $candidate->thirdchildname)
                ->set('thirdchildage', '?', $candidate->thirdchildage)
                ->set('thirdchildedu', '?', $candidate->thirdchildedu)
                ->set('thirdchildoccupation', '?', $candidate->thirdchildoccupation)
                ->set('fathername', '?', $candidate->fathername)
                ->set('fatherage', '?', $candidate->fatherage)
                ->set('fatheredu', '?', $candidate->fatheredu)
                ->set('fatheroccupation', '?', $candidate->fatheroccupation)
                ->set('mothername', '?', $candidate->mothername)
                ->set('motherage', '?', $candidate->motherage)
                ->set('motheredu', '?', $candidate->motheredu)
                ->set('motheroccupation', '?', $candidate->motheroccupation)
                ->set('siblingname1', '?', $candidate->siblingname1)
                ->set('siblingage1', '?', $candidate->siblingage1)
                ->set('siblingedu1', '?', $candidate->siblingedu1)
                ->set('siblingoccupation1', '?', $candidate->siblingoccupation1)
                ->set('siblingname2', '?', $candidate->siblingname2)
                ->set('siblingage2', '?', $candidate->siblingage2)
                ->set('siblingedu2', '?', $candidate->siblingedu2)
                ->set('siblingoccupation2', '?', $candidate->siblingoccupation2)
                ->set('siblingname3', '?', $candidate->siblingname3)
                ->set('siblingage3', '?', $candidate->siblingage3)
                ->set('siblingedu3', '?', $candidate->siblingedu3)
                ->set('siblingoccupation3', '?', $candidate->siblingoccupation3)
                ->set('siblingname4', '?', $candidate->siblingname4)
                ->set('siblingage4', '?', $candidate->siblingage4)
                ->set('siblingedu4', '?', $candidate->siblingedu4)
                ->set('siblingoccupation4', '?', $candidate->siblingoccupation4)
                ->set('siblingname5', '?', $candidate->siblingname5)
                ->set('siblingage5', '?', $candidate->siblingage5)
                ->set('siblingedu5', '?', $candidate->siblingedu5)
                ->set('siblingoccupation5', '?', $candidate->siblingoccupation5)
                ->set('orgname1', '?', $candidate->orgname1)
                ->set('orgyear1', '?', $candidate->orgyear1)
                ->set('orgposition1', '?', $candidate->orgposition1)
                ->set('orgname2', '?', $candidate->orgname2)
                ->set('orgyear2', '?', $candidate->orgyear2)
                ->set('orgposition2', '?', $candidate->orgposition2)
                ->set('orgname3', '?', $candidate->orgname3)
                ->set('orgyear3', '?', $candidate->orgyear3)
                ->set('orgposition3', '?', $candidate->orgposition3)
                ->set('orgname4', '?', $candidate->orgname4)
                ->set('orgyear4', '?', $candidate->orgyear4)
                ->set('orgposition4', '?', $candidate->orgposition4)
                ->set('orgname5', '?', $candidate->orgname5)
                ->set('orgyear5', '?', $candidate->orgyear5)
                ->set('orgposition5', '?', $candidate->orgposition5)
                ->set('diphteria', '?', $candidate->diphteria)
                ->set('sinusitis', '?', $candidate->sinusitis)
                ->set('bronchitis', '?', $candidate->bronchitis)
                ->set('hemoptoe', '?', $candidate->hemoptoe)
                ->set('tbc', '?', $candidate->tbc)
                ->set('lunginfection', '?', $candidate->lunginfection)
                ->set('asthma', '?', $candidate->asthma)
                ->set('dyspnoea', '?', $candidate->dyspnoea)
                ->set('urinateproblem', '?', $candidate->urinateproblem)
                ->set('urinatedisorder', '?', $candidate->urinatedisorder)
                ->set('kidneydisease', '?', $candidate->kidneydisease)
                ->set('kidneystone', '?', $candidate->kidneystone)
                ->set('frequenturinate', '?', $candidate->frequenturinate)
                ->set('meningitis', '?', $candidate->meningitis)
                ->set('cerebralconcussion', '?', $candidate->cerebralconcussion)
                ->set('poliomyelitis', '?', $candidate->poliomyelitis)
                ->set('epilepsy', '?', $candidate->epilepsy)
                ->set('stroke', '?', $candidate->stroke)
                ->set('headache', '?', $candidate->headache)
                ->set('typhoid', '?', $candidate->typhoid)
                ->set('bloodvomit', '?', $candidate->bloodvomit)
                ->set('obstipation', '?', $candidate->obstipation)
                ->set('dyspepsia', '?', $candidate->dyspepsia)
                ->set('jaundice', '?', $candidate->jaundice)
                ->set('gallbladder', '?', $candidate->gallbladder)
                ->set('swallowingdisorder', '?', $candidate->swallowingdisorder)
                ->set('incontinentiaalvi', '?', $candidate->incontinentiaalvi)
                ->set('chickenpox', '?', $candidate->chickenpox)
                ->set('fungalskin', '?', $candidate->fungalskin)
                ->set('std', '?', $candidate->std)
                ->set('heartattack', '?', $candidate->heartattack)
                ->set('chestpains', '?', $candidate->chestpains)
                ->set('palpitation', '?', $candidate->palpitation)
                ->set('hypertension', '?', $candidate->hypertension)
                ->set('hemorrhoid', '?', $candidate->hemorrhoid)
                ->set('varicoseveins', '?', $candidate->varicoseveins)
                ->set('thyroiddisease', '?', $candidate->thyroiddisease)
                ->set('rhematoidsarthritis', '?', $candidate->rhematoidsarthritis)
                ->set('foodallergy', '?', $candidate->foodallergy)
                ->set('foodallergylist', '?', $candidate->foodallergylist)
                ->set('drugallergy', '?', $candidate->drugallergy)
                ->set('drugallergylist', '?', $candidate->drugallergylist)
                ->set('tetanus', '?', $candidate->tetanus)
                ->set('fainting', '?', $candidate->fainting)
                ->set('oblivion', '?', $candidate->oblivion)
                ->set('consentrationdif', '?', $candidate->consentrationdif)
                ->set('visiondisorder', '?', $candidate->visiondisorder)
                ->set('hearingdisorder', '?', $candidate->hearingdisorder)
                ->set('lumbago', '?', $candidate->lumbago)
                ->set('neoplasm', '?', $candidate->neoplasm)
                ->set('mentalillness', '?', $candidate->mentalillness)
                ->set('skintuberculose', '?', $candidate->skintuberculose)
                ->set('bonetuberculose', '?', $candidate->bonetuberculose)
                ->set('measles', '?', $candidate->measles)
                ->set('malaria', '?', $candidate->malaria)
                ->set('diabetes', '?', $candidate->diabetes)
                ->set('sleepdisorder', '?', $candidate->sleepdisorder)
                ->set('hospitalized', '?', $candidate->hospitalized)
                ->set('hospitalizeddate', '?', $candidate->hospitalizeddate)
                ->set('hospitalizedlength', '?', $candidate->hospitalizedlength)
                ->set('hospitalizedwhy', '?', $candidate->hospitalizedwhy)
                ->set('accident', '?', $candidate->accident)
                ->set('accidentdate', '?', $candidate->accidentdate)
                ->set('accidenttxt', '?', $candidate->accidenttxt)
                ->set('surgery', '?', $candidate->surgery)
                ->set('surgerydate', '?', $candidate->surgerydate)
                ->set('surgerytxt', '?', $candidate->surgerytxt)
                ->set('smoker', '?', $candidate->smoker)
                ->set('smokernumber', '?', $candidate->smokernumber)
                ->set('smokerage', '?', $candidate->smokerage)
                ->set('smokerstop', '?', $candidate->smokerstop)
                ->set('alcohol', '?', $candidate->alcohol)
                ->set('alcoholperweek', '?', $candidate->alcoholperweek)
                ->set('alcoholslokyperweek', '?', $candidate->alcoholslokyperweek)
                ->set('exercise', '?', $candidate->exercise)
                ->set('exercisetxt', '?', $candidate->exercisetxt)
                ->set('medicinelist', '?', $candidate->medicinelist)
                ->set('vacdiptheri', '?', $candidate->vacdiptheri)
                ->set('vactetanus', '?', $candidate->vactetanus)
                ->set('vacpolio', '?', $candidate->vacpolio)
                ->set('vacbcg', '?', $candidate->vacbcg)
                ->set('vacmmr', '?', $candidate->vacmmr)
                ->set('vacinfluenza', '?', $candidate->vacinfluenza)
                ->set('vachepatitisa', '?', $candidate->vachepatitisa)
                ->set('vachepatitisb', '?', $candidate->vachepatitisb)
                ->set('vacvarisela', '?', $candidate->vacvarisela)
                ->set('vactyphoid', '?', $candidate->vactyphoid)
                ->set('vachpv', '?', $candidate->vachpv)
                ->set('vacmeningokok', '?', $candidate->vacmeningokok)
                ->set('vacyellowfever', '?', $candidate->vacyellowfever)
                ->set('vacjapencephalitis', '?', $candidate->vacjapencephalitis)
                ->set('consent', '?', $candidate->consent)
                ->where('id = ?', $candidate->id);*/
            //print_r($q->getParams());
            //return $q->execute();
        } catch (Exception $e) {
            throw new DaoException($e->getMessage());
        }
    }

    /**
     *
     * @param JobCandidate $candidate
     * @return <type>
     */
    public function updateCandidateHistory(CandidateHistory $candidateHistory) {
        try {
            $q = Doctrine_Query:: create()->update('CandidateHistory')
                    ->set('interviewers', '?', $candidateHistory->interviewers)
                    ->where('id = ?', $candidateHistory->id);

            return $q->execute();
        } catch (Exception $e) {
            throw new DaoException($e->getMessage());
        }
    }

    /**
     *
     * @param <type> $candidateVacancyId
     * @return <type>
     */
    public function getCandidateVacancyById($candidateVacancyId) {
        try {
            $q = Doctrine_Query :: create()
                    ->from('JobCandidateVacancy jcv')
                    ->where('jcv.id = ?', $candidateVacancyId);
            return $q->fetchOne();
        } catch (Exception $e) {
            throw new DaoException($e->getMessage());
        }
    }

    /**
     *
     * @param JobCandidateVacancy $candidateVacancy
     * @return <type>
     */
    public function updateCandidateVacancy(JobCandidateVacancy $candidateVacancy) {
        try {
            $q = Doctrine_Query:: create()->update('JobCandidateVacancy')
                    ->set('status', '?', $candidateVacancy->status)
                    ->where('id = ?', $candidateVacancy->id);
            return $q->execute();
        } catch (Exception $e) {
            throw new DaoException($e->getMessage());
        }
    }

    /**
     *
     * @param CandidateHistory $candidateHistory
     * @return <type>
     */
    public function saveCandidateHistory(CandidateHistory $candidateHistory) {
        try {
            if ($candidateHistory->getId() == '') {
                $idGenService = new IDGeneratorService();
                $idGenService->setEntity($candidateHistory);
                $candidateHistory->setId($idGenService->getNextID());
            }
            $candidateHistory->save();
            return true;
        } catch (Exception $e) {
            throw new DaoException($e->getMessage());
        }
    }

    /**
     *
     * @param <type> $candidateId
     * @return <type>
     */
    public function getCandidateHistoryForCandidateId($candidateId, $allowedHistoryList) {
        try {
            $q = Doctrine_Query:: create()
                    ->from('CandidateHistory ch')
                    ->whereIn('ch.id', $allowedHistoryList)
                    ->andWhere('ch.candidateId = ?', $candidateId)
                    ->orderBy('ch.performedDate DESC');
            return $q->execute();
        } catch (Exception $e) {
            throw new DaoException($e->getMessage());
        }
    }

    /**
     *
     * @param <type> $candidateId
     * @param <type> $vacancyId
     * @return <type>
     */
    public function getCandidateLatestHistory($candidateId, $vacancyId) {
        try {
            $q = Doctrine_Query:: create()
                ->from('CandidateHistory')
                ->where('candidateId = ?', $candidateId)
                ->andWhere('vacancyId = ?', $vacancyId)
                ->orderBy('performedDate DESC')
                ->limit(1);
            return $q->execute();
        } catch (Exception $e) {
            throw new DaoException($e->getMessage());
        }
    }

    /**
     *
     * @param <type> $id
     * @return <type>
     */
    public function getCandidateHistoryById($id) {
        try {
            $q = Doctrine_Query:: create()
                    ->from('CandidateHistory')
                    ->where('id = ?', $id);
            return $q->fetchOne();
        } catch (Exception $e) {
            throw new DaoException($e->getMessage());
        }
    }

    /**
     * Return an array of Candidate History Ids based on user role
     * 
     * @version 2.7.1
     * @param String $role User Role
     * @param Integer $empNumber Employee Number
     * @param Integer $candidateId Candidate Id
     * @return Array of Candidate History Ids
     * @throws DaoException
     */
    public function getCanidateHistoryForUserRole($role, $empNumber, $candidateId) {
        try {
            $q = Doctrine_Query :: create()
                    ->select('ch.id')
                    ->from('CandidateHistory ch');
            if ($role == HiringManagerUserRoleDecorator::HIRING_MANAGER) {
                 $q->leftJoin('ch.JobVacancy jv')
                        ->leftJoin('ch.JobCandidate jc')
                        ->where('ch.candidateId = ?', $candidateId)
                        ->andWhere('jv.hiringManagerId = ? OR ( ch.action IN (?) OR (ch.candidateId NOT IN (SELECT ojcv.candidateId FROM JobCandidateVacancy ojcv) AND jc.addedPerson = ?) OR ch.performedBy = ? )', array($empNumber, CandidateHistory::RECRUITMENT_CANDIDATE_ACTION_ADD, $empNumber, $empNumber));
            }
            if ($role == InterviewerUserRoleDecorator::INTERVIEWER) {
                $q->leftJoin('ch.JobInterview ji ON ji.id = ch.interview_id')
                        ->leftJoin('ji.JobInterviewInterviewer jii')
                        ->where('ch.candidateId = ?', $candidateId)
                        ->andWhere('jii.interviewerId = ? OR (ch.performedBy = ? OR ch.action IN (?, ?, ?, ?, ?, ?))',  array($empNumber, $empNumber, CandidateHistory::RECRUITMENT_CANDIDATE_ACTION_ADD, WorkflowStateMachine::RECRUITMENT_APPLICATION_ACTION_ATTACH_VACANCY, WorkflowStateMachine::RECRUITMENT_APPLICATION_ACTION_SHORTLIST, WorkflowStateMachine::RECRUITMENT_APPLICATION_ACTION_SHEDULE_INTERVIEW, WorkflowStateMachine::RECRUITMENT_APPLICATION_ACTION_MARK_INTERVIEW_FAILED, WorkflowStateMachine::RECRUITMENT_APPLICATION_ACTION_MARK_INTERVIEW_PASSED));
            }
            if ($role == AdminUserRoleDecorator::ADMIN_USER) {
                $q->where('ch.candidateId = ?', $candidateId);
            }
            $result = $q->fetchArray();
            $idList = array();
            foreach ($result as $item) {
                $idList[] = $item['id'];
            }
            return $idList;

        // @codeCoverageIgnoreStart
        } catch (Exception $e) {
            throw new DaoException($e->getMessage(), $e->getCode(), $e);
        }
        // @codeCoverageIgnoreEnd
    }

    /**
     * Get all vacancy Ids for relevent candidate
     * @param int $candidateId
     * @return array $vacancies
     */
    public function getAllVacancyIdsForCandidate($candidateId) {

        try {

            $q = Doctrine_Query:: create()
                    ->from('JobCandidateVacancy v')
                    ->where('v.candidateId = ?', $candidateId);
            $vacancies = $q->execute();

            $vacancyIdsForCandidate = array();
            foreach ($vacancies as $value) {
                $vacancyIdsForCandidate[] = $value->getVacancyId();
            }
            return $vacancyIdsForCandidate;
        } catch (Exception $e) {
            throw new DaoException($e->getMessage());
        }
    }

    /**
     * Delete Candidate
     * @param array $toBeDeletedCandidateIds
     * @return boolean
     */
    public function deleteCandidates($toBeDeletedCandidateIds) {

        try {
            $q = Doctrine_Query:: create()
                    ->delete()
                    ->from('JobCandidate')
                    ->whereIn('id', $toBeDeletedCandidateIds);

            $result = $q->execute();
            return true;
        } catch (Exception $e) {
            throw new DaoException($e->getMessage());
        }
    }

    /**
     * Delete Candidate-Vacancy Relations
     * @param array $toBeDeletedRecords
     * @return boolean
     */
    public function deleteCandidateVacancies($toBeDeletedRecords) {

        try {
            $q = Doctrine_Query:: create()
                    ->delete()
                    ->from('JobCandidateVacancy cv')
                    ->where('candidateId = ? AND vacancyId = ?', $toBeDeletedRecords[0]);
            for ($i = 1; $i < count($toBeDeletedRecords); $i++) {
                $q->orWhere('candidateId = ? AND vacancyId = ?', $toBeDeletedRecords[$i]);
            }

            $deleted = $q->execute();
            if ($deleted > 0) {
                return true;
            }
            return false;
        } catch (Exception $e) {
            throw new DaoException($e->getMessage());
        }
    }

    public function buildSearchQuery(CandidateSearchParameters $paramObject, $countQuery = false) {

        try {
            $pdo = Doctrine_Manager::connection()->getDbh();
            //Eka: remove attachment join
            //ca.id as attachmentId,
            //
            $query = ($countQuery) ? "SELECT COUNT(*)" : "SELECT jc.id, jc.first_name, jc.middle_name, jc.last_name, jc.date_of_application, jcv.status, jv.name, e.emp_firstname, e.emp_middle_name, e.emp_lastname, e.termination_id, jv.status as vacancyStatus, jv.id as vacancyId, jc.status as candidateStatus";
            $query .= "  FROM ohrm_job_candidate jc";
            $query .= " LEFT JOIN ohrm_job_candidate_vacancy jcv ON jc.id = jcv.candidate_id";
            $query .= " LEFT JOIN ohrm_job_vacancy jv ON jcv.vacancy_id = jv.id";
            $query .= " LEFT JOIN hs_hr_employee e ON jv.hiring_manager_id = e.emp_number";
            //$query .= " LEFT JOIN ohrm_job_candidate_attachment ca ON jc.id = ca.candidate_id";
            $query .= ' WHERE jc.date_of_application BETWEEN ' .
                    $pdo->quote($paramObject->getFromDate(), PDO::PARAM_STR) . ' AND ' .
                    $pdo->quote($paramObject->getToDate(), PDO::PARAM_STR);

            $candidateStatuses = $paramObject->getCandidateStatus();
            if (!empty($candidateStatuses)) {
                $query .= " AND jc.status IN (";
                $comma = '';
                foreach ($candidateStatuses as $candidateStatus) {
                    $query .= $comma . $pdo->quote($candidateStatus, PDO::PARAM_INT);
                    $comma = ',';
                }
                $query .= ")";
            }

            $query .= $this->_buildKeywordsQueryClause($paramObject->getKeywords());
            $query .= $this->_buildAdditionalWhereClauses($paramObject);
            $query .= " ORDER BY " . $this->_buildSortQueryClause($paramObject->getSortField(), $paramObject->getSortOrder());
            if (!$countQuery) {
                $query .= " LIMIT " . $paramObject->getOffset() . ", " . $paramObject->getLimit();
            }
            return $query;
        } catch (Exception $e) {
            throw new DaoException($e->getMessage());
        }
    }

    /**
     *
     * @param array $keywords
     * @return string
     */
    private function _buildKeywordsQueryClause($keywords) {
        $keywordsQueryClause = '';
        if (!empty($keywords)) {
            $dbh = Doctrine_Manager::connection()->getDbh();
            $words = explode(',', $keywords);
            foreach ($words as $word) {
                $keywordsQueryClause .= ' AND jc.keywords LIKE ' . $dbh->quote('%' . trim($word) . '%');
            }
        }

        return $keywordsQueryClause;
    }

    /**
     *
     * @param string $sortField
     * @param string $sortOrder
     * @return string
     */
    private function _buildSortQueryClause($sortField, $sortOrder) {
        $sortQuery = '';
        $sortOrder = strcasecmp($sortOrder, 'DESC') === 0 ? 'DESC' : 'ASC';
        if ($sortField == 'jc.first_name') {
            $sortQuery = 'jc.last_name ' . $sortOrder . ', ' . 'jc.first_name ' . $sortOrder;
        } elseif ($sortField == 'e.emp_firstname') {
            $sortQuery = 'e.emp_lastname ' . $sortOrder . ', ' . 'e.emp_firstname ' . $sortOrder;
        } elseif ($sortField == 'jc.date_of_application') {
            $sortQuery = 'jc.date_of_application ' . $sortOrder . ', ' . 'jc.last_name ASC, jc.first_name ASC';
        } else if (in_array($sortField, array_column(CandidateHeaderFactory::getSortableFields(), 'sortField'))) {
            $sortQuery = $sortField . " " . $sortOrder;
        } else {
            // default sorting
            $sortQuery = "jc.date_of_application DESC";
        }

        return $sortQuery;
    }

    /**
     * @param CandidateSearchParameters $paramObject
     * @return string
     */
    private function _buildAdditionalWhereClauses(CandidateSearchParameters $paramObject) {

        $allowedCandidateList = $paramObject->getAllowedCandidateList();
        $jobTitleCode = $paramObject->getJobTitleCode();
        $jobVacancyId = $paramObject->getVacancyId();
        $hiringManagerId = $paramObject->getHiringManagerId();
        $status = $paramObject->getStatus();
        $allowedVacancyList = $paramObject->getAllowedVacancyList();
        $isAdmin = $paramObject->getIsAdmin();
        $empNumber = $paramObject->getEmpNumber();

        $whereClause = '';
        $whereFilters = array();
        if ($allowedVacancyList != null && !$isAdmin) {
            $this->_addAdditionalWhereClause($whereFilters, 'jv.id', '(' . implode(',', $allowedVacancyList) . ')', 'IN');
        }
        if ($allowedCandidateList != null && !$isAdmin) {
            $this->_addAdditionalWhereClause($whereFilters, 'jc.id', '(' . implode(',', $allowedCandidateList) . ')', 'IN');
        }
        if (!empty($jobTitleCode) || !empty($jobVacancyId) || !empty($hiringManagerId) || !empty($status)) {
            $this->_addAdditionalWhereClause($whereFilters, 'jv.status', $paramObject->getVacancyStatus());
        }


        $this->_addAdditionalWhereClause($whereFilters, 'jv.job_title_code', $paramObject->getJobTitleCode());
        $this->_addAdditionalWhereClause($whereFilters, 'jv.id', $paramObject->getVacancyId());
        $this->_addAdditionalWhereClause($whereFilters, 'jv.hiring_manager_id', $paramObject->getHiringManagerId());
        $this->_addAdditionalWhereClause($whereFilters, 'jcv.status', $paramObject->getStatus());

        $this->_addCandidateNameClause($whereFilters, $paramObject);

        $this->_addAdditionalWhereClause($whereFilters, 'jc.mode_of_application', $paramObject->getModeOfApplication());


        $whereClause .= ( count($whereFilters) > 0) ? (' AND ' . implode('AND ', $whereFilters)) : '';
        if ($empNumber != null) {
            $whereClause .= " OR jc.id NOT IN (SELECT ojcv.candidate_id FROM ohrm_job_candidate_vacancy ojcv) AND jc.added_person = " . $empNumber;
        }
        if(!empty($status)){
            $whereClause .=" AND NOT ISNULL(jcv.status)";
        }
        return $whereClause;
    }

    /**
     *
     * @param array_pointer $where
     * @param string $field
     * @param mixed $value
     * @param string $operator
     */
    private function _addAdditionalWhereClause(&$where, $field, $value, $operator = '=') {
        if (!empty($value)) {
            if ($operator === '=') {
                $dbh = Doctrine_Manager::connection()->getDbh();
                $value = $dbh->quote($value);
            }
            $where[] = "{$field}  {$operator} {$value}";
        }
    }

    /**
     * Add where clause to search by candidate name.
     * 
     * @param type $where Where Clause
     * @param type $paramObject Search Parameter object
     */
    private function _addCandidateNameClause(&$where, $paramObject) {

        // Search by Name
        $candidateName = $paramObject->getCandidateName();

        if (!empty($candidateName)) {

            // Trimming to avoid issues with names that have leading/trailing spaces (due to bug in older jobs.php)
            $candidateFullNameClause = "concat_ws(' ', trim(jc.first_name), " .
                    "IF(trim(jc.middle_name) <> '', trim(jc.middle_name), NULL), " .
                    "trim(jc.last_name))";

            // Replace multiple spaces in string with single space
            $candidateName = preg_replace('!\s+!', ' ', $candidateName);
            $candidateName = "%" . $candidateName . "%";
            $dbh = Doctrine_Manager::connection()->getDbh();
            $candidateName = $dbh->quote($candidateName);

            $this->_addAdditionalWhereClause($where, $candidateFullNameClause, $candidateName, 'LIKE');
        }
    }

    public function isHiringManager($candidateVacancyId, $empNumber) {
        try {
            $q = Doctrine_Query :: create()
                    ->select('COUNT(*)')
                    ->from('JobCandidateVacancy jcv')
                    ->leftJoin('jcv.JobVacancy jv')
                    ->where('jcv.id = ?', $candidateVacancyId)
                    ->andWhere('jv.hiringManagerId = ?', $empNumber);

            $count = $q->fetchOne(array(), Doctrine_Core::HYDRATE_SINGLE_SCALAR);
            return ($count > 0);
        } catch (Exception $e) {
            throw new DaoException($e->getMessage());
        }
    }

    public function isInterviewer($candidateVacancyId, $empNumber) {
        try {
            $q = Doctrine_Query :: create()
                    ->select('COUNT(*)')
                    ->from('JobInterviewInterviewer jii')
                    ->leftJoin('jii.JobInterview ji')
                    ->leftJoin('ji.JobCandidateVacancy jcv')
                    ->where('jcv.id = ?', $candidateVacancyId)
                    ->andWhere('jii.interviewerId = ?', $empNumber);

            $count = $q->fetchOne(array(), Doctrine_Core::HYDRATE_SINGLE_SCALAR);
            return ($count > 0);
        } catch (Exception $e) {
            throw new DaoException($e->getMessage());
        }
    }

    /**
     * Get candidate name suffix according to the candidate status
     * @param integer $statusCode
     * return string $suffix
     */
    private function _getCandidateNameSuffix($statusCode) {

        $suffix = "";

        if ($statusCode == JobCandidate::ARCHIVED) {
            $suffix = " (" . __('Archived') . ")";
        }

        return $suffix;
    }

    public function getCandidateVacancyByCandidateIdAndVacancyId($candidateId, $vacancyId) {
        try {
            $q = Doctrine_Query :: create()
                    ->from('JobCandidateVacancy jcv')
                    ->where('jcv.candidateId = ?', $candidateId)
                    ->andWhere('jcv.vacancyId = ?', $vacancyId);
            return $q->fetchOne();
        } catch (Exception $e) {
            throw new DaoException($e->getMessage());
        }
    }

    public function getCandidateVacDataQuery() {
        //SELECT jv.name lowongan, vc.status statuslamaran, vc.id idlamaran, cd.* FROM ohrm_mysql.ohrm_job_candidate cd join ohrm_job_candidate_vacancy vc on cd.id = candidate_id join ohrm_job_vacancy jv on vc.vacancy_id = jv.id;
        try {
            $pdo = Doctrine_Manager::connection()->getDbh();
            $query = "SELECT jv.name lowongan, vc.status statuslamaran, vc.id idlamaran, cd.*";
            $query .= " FROM ohrm_mysql.ohrm_job_candidate cd";
            $query .= " JOIN ohrm_job_candidate_vacancy vc on cd.id = candidate_id";
            $query .= " JOIN ohrm_job_vacancy jv on vc.vacancy_id = jv.id";

            $query .= " ORDER BY cd.date_of_application DESC";
            $result = $pdo->query($query);
            return $result->fetchAll();
        } catch (Exception $e) {
            throw new DaoException($e->getMessage());
        }
    }

}
